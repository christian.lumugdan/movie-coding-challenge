Before you start working on this project, please make sure you have read the following documentations.

- [iOS Baseplate](https://paper.dropbox.com/doc/iOS-Baseplate--AhQQCkgxMVgsgI2Xiou8qbOkAQ-pm2bogiDW8PwYhLUHtnKO)
- [Git Branching Standards and Conventions](https://paper.dropbox.com/doc/Git-Branching-Standards-and-Conventions--AhSkR4V6IXAPKOVk06SUKnf0AQ-mQQ2IcHe86inoM7cY9Omy)
- [iOS Coding Style and Standards Guidelines](https://paper.dropbox.com/doc/Rules-That-Complement-Our-Usage-of-R.Swift--Acyt8BzIxGsMQUe27Gd_6wpcAQ-6PkyyPTyqoiPp8Ma8axhq)
- [R.Swift Usage Rules](https://paper.dropbox.com/doc/Rules-That-Complements-Our-Usage-of-R.Swift--Acyt8BzIxGsMQUe27Gd_6wpcAQ-6PkyyPTyqoiPp8Ma8axhq)
- [Facebook Login](https://developers.facebook.com/docs/facebook-login/ios)
- [Apple Sign in](https://developer.apple.com/sign-in-with-apple/get-started/)

