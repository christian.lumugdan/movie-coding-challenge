import UIKit

struct APLHelpers {}

// MARK: - File Management

extension APLHelpers {
  static func documentsDirectoryURL() -> URL {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
  }

  static func sanitizedFileName(_ filename: String) -> String {
    return filename.components(separatedBy: .init(charactersIn: "/:?%*|\"<>")).joined()
  }
}

import MapKit

// MARK: - Maps

extension APLHelpers {
  static func visibleMapRectForCoordinates(_ coordinates: [CLLocationCoordinate2D]) -> MKMapRect {
    let points = coordinates.map { MKMapPoint($0) }
    let rects = points.map { MKMapRect(origin: $0, size: MKMapSize(width: 0, height: 0)) }
    return rects.reduce(MKMapRect.null, { (res, rect) -> MKMapRect in
      rect.union(res)
    })
  }
}
