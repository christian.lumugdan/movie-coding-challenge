//
//  InputSourceAlamofire.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireImage
import ImageSlideshow
import UIKit

@objcMembers
public class AlamofireSource: NSObject, InputSource {
  public var url: URL
  public var placeholder: UIImage?

  public init(url: URL, placeholder: UIImage? = nil) {
    self.url = url
    self.placeholder = placeholder
    super.init()
  }

  public init?(urlString: String, placeholder: UIImage? = nil) {
    if let validUrl = URL(string: urlString) {
      url = validUrl
      self.placeholder = placeholder
      super.init()
    } else {
      return nil
    }
  }

  public func load(to imageView: UIImageView, with callback: @escaping (UIImage?) -> Void) {
    imageView.setImageWithURL(url, placeholder: placeholder, onSuccess: callback) { [weak self] _ in
      guard let self = self else { return }
      callback(self.placeholder)
    }
  }

  public func cancelLoad(on imageView: UIImageView) {
    imageView.sd_cancelCurrentImageLoad()
  }
}
