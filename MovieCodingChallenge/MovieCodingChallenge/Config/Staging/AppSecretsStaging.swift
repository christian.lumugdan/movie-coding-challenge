//
//  AppSecretsStaging.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct AppSecretsStaging: AppSecretsProtocol {
  var googleClientID: String { "$(STAGING_GOOGLE_CLIENT_ID)" }
}
