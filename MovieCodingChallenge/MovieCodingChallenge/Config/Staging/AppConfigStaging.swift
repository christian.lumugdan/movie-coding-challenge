//
//  AppConfigStaging.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct AppConfigStaging: AppConfigProtocol {
  var baseUrl: String { "https://api.baseplate.appetiserdev.tech" }

  var secrets: AppSecretsProtocol { AppSecretsStaging() }
}
