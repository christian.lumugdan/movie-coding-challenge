//
//  AppConfig.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AppConfigProtocol {
  // MARK: Backend

  /// Backend server URL
  var baseUrl: String { get }

  /// API URL
  var apiUrl: String { get }
  var apiVersion: String { get }

  /// URL string for the privacy policy  page for the app
  var privacyPolicyUrl: String { get }
  /// URL string for the terms of service page for the app
  var termsOfServiceUrl: String { get }

  // MARK: Defaults

  /// Default pagination items per page
  var defaultPageSize: Int { get }
  /// Default compression quality for UIImage before sending to backend
  var defaultPhotoCompression: Float { get }
  /// Default debounce timer in seconds for resending of code to email and mobile number
  var defaultCodeResendTimerInSeconds: Float { get }

  // MARK: Password

  var minPasswordLength: Int { get }
  var maxPasswordLength: Int { get }

  // MARK: Verification

  /// If email verification is skippable
  var emailVerificationSkippable: Bool { get }

  // MARK: Others

  var secrets: AppSecretsProtocol { get }
}

// MARK: - Default values

extension AppConfigProtocol {
  var apiUrl: String { "\(baseUrl)/api" }
  var apiVersion: String { "v1" }
  var apiUrlWithVersion: String { "\(apiUrl)/\(apiVersion)" }

  var privacyPolicyUrl: String { "\(apiUrlWithVersion)/pages/privacy" }
  var termsOfServiceUrl: String { "\(apiUrlWithVersion)/pages/terms_of_service" }

  var defaultPageSize: Int { 10 }
  var defaultPhotoCompression: Float { 0.7 }
  var defaultCodeResendTimerInSeconds: Float { 60 }

  var minPasswordLength: Int { 8 }
  var maxPasswordLength: Int { 32 }

  var emailVerificationSkippable: Bool { true }
}

// MARK: - Concrete Type
   
struct AppConfig: AppConfigProtocol {
  var baseUrl: String { "https://api.baseplate.appetiserdev.tech" }

  var secrets: AppSecretsProtocol { AppSecrets() }
}
