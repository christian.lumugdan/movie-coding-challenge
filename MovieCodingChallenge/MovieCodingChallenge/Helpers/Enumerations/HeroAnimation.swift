//
//  HeroAnimation.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

enum HeroAnimation: String {
  case createPost
  case postAuthorView
  case postView
}

// MARK: - Helpers

extension HeroAnimation {
  var id: String { rawValue }

  func createID(withKey key: String) -> String {
    "\(key) \(rawValue)"
  }
}
