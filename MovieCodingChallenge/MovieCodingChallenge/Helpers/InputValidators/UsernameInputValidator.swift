//
//  UsernameInputValidator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct UsernameInputValidator: InputValidator {
  static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let username = inputs, !username.isEmpty else {
      return .failure(.requiredUsername)
    }

    if username.isAlphanumeric && !InputValidatorUtil.isValidEmail(username) {
      return .failure(.invalidEmail)
    }

    let validInputs = ValidInputs(username)

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

extension UsernameInputValidator {
  typealias Inputs = String?
  typealias ValidInputs = String

  enum ValidationError: LocalizedError {
    case requiredUsername
    case invalidEmail

    var errorDescription: String? {
      switch self {
      case .requiredUsername:
        return S.usernameErrorMissingValue()
      case .invalidEmail:
        return S.emailFieldErrorInvalidValue()
      }
    }
  }
}
