//
//  PasswordInputValidator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct PasswordInputValidator: InputValidator {
  static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let password = inputs, !password.isEmpty else {
      return .failure(.requiredPassword)
    }

    let validInputs = ValidInputs(password)

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

extension PasswordInputValidator {
  typealias Inputs = String?
  typealias ValidInputs = String

  enum ValidationError: LocalizedError {
    case requiredPassword

    var errorDescription: String? {
      switch self {
      case .requiredPassword:
        return S.passwordFieldErrorMissingValue()
      }
    }
  }
}
