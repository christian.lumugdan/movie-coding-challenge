//
//  PhoneNumberInputValidator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct PhoneNumberInputValidator: InputValidator {
  static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let phoneNumber = inputs, !phoneNumber.isEmpty else {
      return .failure(.requiredPhone)
    }

    let validInputs = ValidInputs(phoneNumber)

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

extension PhoneNumberInputValidator {
  typealias Inputs = String?

  typealias ValidInputs = String

  enum ValidationError: LocalizedError {
    case requiredPhone

    var errorDescription: String? {
      switch self {
      case .requiredPhone:
        return S.phoneFieldErrorMissingValue()
      }
    }
  }
}
