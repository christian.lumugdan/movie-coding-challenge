//
//  InputValidator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol InputValidator {
  associatedtype Inputs
  associatedtype ValidInputs
  associatedtype ValidationError: Error

  static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError>
}

// MARK: - Default

extension InputValidator {
  /// Check whether the provided string is a valid Email address or not.
  func isValidEmail(_ email: String) -> Bool {
    return InputValidatorUtil.isValidEmail(email)
  }

  func isValidFirstName(_ firstName: String) -> Bool {
    return InputValidatorUtil.isValidFirstName(firstName)
  }

  func isValidLastName(_ lastName: String) -> Bool {
    return InputValidatorUtil.isValidLastName(lastName)
  }

  func isValidPassword(
    _ password: String,
    minLength: Int,
    maxLength: Int
  ) -> Bool {
    return InputValidatorUtil.isValidPassword(
      password,
      minLength: minLength,
      maxLenth: maxLength
    )
  }
}
