//
//  ImagePickerPresenterProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

protocol ImagePickerPresenterProtocol {
  var onImagesPick: SingleResult<[UIImage]>? { get set }
  var onError: SingleResult<Error>? { get set }
  
  var anchorController: UIViewController? { get set }
  
  func presentPicker()
  
  func presentPicker(maxNumberOfItems: Int)
}
