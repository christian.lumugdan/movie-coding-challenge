//
//  ImagePickerPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import AVFoundation
import MobileCoreServices
import Photos
import UIKit

protocol ImagePickerPresenter where Self: UIViewController {
  var imagePicker: UIImagePickerController! { get set }
  var selectionTypes: [ImagePickerPresenterSelectionType] { get }
  var onImagePickerPresentError: SingleResult<ImagePickerPresenterPermissionError> { get }

  func presentImagePickerOptions()
  func presentPhotoLibrary()
  func presentVideoCaptureScreen()
  func presentPhotoCaptureScreen()
}

// MARK: - Default

extension ImagePickerPresenter {
  var selectionTypes: [ImagePickerPresenterSelectionType] { [.photo] }
  var onImagePickerPresentError: SingleResult<ImagePickerPresenterPermissionError> { handleImagePickerPresentError() }

  func presentImagePickerOptions() {
    guard isCameraSupported else {
      return presentPhotoLibrary()
    }

    let style: UIAlertController.Style = UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet
    let ac = UIAlertController(title: nil, message: nil, preferredStyle: style)

    ac.addAction(chooseFromPhotoLibraryAction)

    if shouldIncludePhotos {
      ac.addAction(takeAPhotoAction)
    }

    if shouldIncludeVideos {
      ac.addAction(takeAVideoAction)
    }

    ac.addAction(.cancelAction())

    present(ac, animated: true)
  }

  func presentPhotoLibrary() {
    if isPhotoLibraryNotAllowed {
      return onImagePickerPresentError(.photoLibraryPermissionRequired)
    }

    setupForPhotoLibrary()
    presentImagePicker()
  }

  func presentPhotoCaptureScreen() {
    guard isCameraSupported else {
      return debugLog("Warning: Device doesn't have camera.")
    }

    if isCameraNotAllowed {
      return onImagePickerPresentError(.cameraPermissionRequired)
    }

    setupForPhotoCapture()
    presentImagePicker()
  }

  func presentVideoCaptureScreen() {
    guard isCameraSupported else {
      return debugLog("Warning: Device doesn't have camera.")
    }

    if isCameraNotAllowed {
      return onImagePickerPresentError(.cameraPermissionRequired)
    }

    setupForVideoCapture()
    presentImagePicker()
  }
}

// MARK: - Utils

private extension ImagePickerPresenter {
  func setupForVideoCapture() {
    imagePicker.sourceType = .camera
    imagePicker.cameraDevice = .rear
    imagePicker.mediaTypes = [kUTTypeMovie as String]
    imagePicker.cameraCaptureMode = .video
    imagePicker.allowsEditing = true
    imagePicker.videoQuality = .typeHigh
  }

  func setupForPhotoCapture() {
    imagePicker.sourceType = .camera
    imagePicker.cameraDevice = .front
    imagePicker.allowsEditing = true
  }

  func setupForPhotoLibrary() {
    imagePicker.sourceType = .photoLibrary
    imagePicker.allowsEditing = true

    if shouldIncludeVideos {
      imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
      imagePicker.videoQuality = .typeMedium
    } else {
      imagePicker.mediaTypes = [kUTTypeImage as String]
    }
  }

  func presentImagePicker() {
    imagePicker.modalPresentationStyle = .overFullScreen

    present(
      imagePicker,
      animated: true,
      completion: handleImagePickerPresent()
    )
  }

  func presentPermissionAlert(for error: ImagePickerPresenterPermissionError) {
    let ac = UIAlertController(
      title: S.imagePickerPresenterAlertPermissionRequiredTitle(),
      message: error.localizedDescription,
      preferredStyle: .alert
    )

    let settings = UIAlertAction(
      title: S.settings(),
      style: .default,
      handler: handleSettingsTap()
    )

    ac.addAction(settings)
    ac.addAction(.cancelAction(with: S.later()))
    ac.preferredAction = settings

    present(ac, animated: true)
  }

  func navigateToAppSettings() {
    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString),
      UIApplication.shared.canOpenURL(settingsUrl) else { return }

    UIApplication.shared.open(settingsUrl)
  }
}

// MARK: - Handlers

private extension ImagePickerPresenter {
  func handlePhotoLibraryOptionTap() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.presentPhotoLibrary()
    }
  }

  func handlePhotoOptionTap() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.presentPhotoCaptureScreen()
    }
  }

  func handleVideoOptionTap() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.presentVideoCaptureScreen()
    }
  }

  func handleImagePickerPresent() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
    }
  }

  func handleImagePickerPresentError() -> SingleResult<ImagePickerPresenterPermissionError> {
    return { [weak self] permissionError in
      guard let self = self else { return }
      self.presentPermissionAlert(for: permissionError)
    }
  }

  func handleSettingsTap() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.navigateToAppSettings()
    }
  }
}

// MARK: - Private Getters

private extension ImagePickerPresenter {
  var shouldIncludePhotos: Bool { selectionTypes.contains(.photo) }

  var shouldIncludeVideos: Bool { selectionTypes.contains(.video) }

  var isCameraSupported: Bool { UIImagePickerController.isSourceTypeAvailable(.camera) }

  var isCameraNotAllowed: Bool { AVCaptureDevice.authorizationStatus(for: .video) == .denied }

  var isPhotoLibraryNotAllowed: Bool { PHPhotoLibrary.authorizationStatus() == .denied }

  var chooseFromPhotoLibraryAction: UIAlertAction {
    return UIAlertAction(
      title: S.imagePickerPresenterLabelsChooseFromLibrary(),
      style: .default,
      handler: handlePhotoLibraryOptionTap()
    )
  }

  var takeAPhotoAction: UIAlertAction {
    return UIAlertAction(
      title: S.imagePickerPresenterLabelsTakePhoto(),
      style: .default,
      handler: handlePhotoOptionTap()
    )
  }

  var takeAVideoAction: UIAlertAction {
    return UIAlertAction(
      title: S.imagePickerPresenterLabelsTakePhoto(),
      style: .default,
      handler: handlePhotoOptionTap()
    )
  }
}

// MARK: - ImagePickerPresenterSelectionType

enum ImagePickerPresenterSelectionType {
  case photo
  case video
}

// MARK: - ImagePickerPresenterError

enum ImagePickerPresenterPermissionError: LocalizedError {
  case cameraPermissionRequired
  case photoLibraryPermissionRequired

  var errorDescription: String? {
    switch self {
    case .cameraPermissionRequired:
      return S.imagePickerPresenterErrorCameraPermissionRequired()
    case .photoLibraryPermissionRequired:
      return S.imagePickerPresenterErrorPhotoLibraryPermissionRequired()
    }
  }
}
