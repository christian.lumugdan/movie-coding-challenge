//
//  InfoPresenterProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol InfoPresenterProtocol {
  func presentInfo(
    _ info: InfoProtocol,
    onComplete: @escaping VoidResult
  )

  func presentSuccessInfo(message: String)

  func presentSuccessInfo(
    message: String,
    onComplete: @escaping VoidResult
  )

  func presentErrorInfo(error: Error)

  func presentErrorMessage(message: String)

  func presentErrorInfo(
    error: Error,
    onComplete: @escaping VoidResult
  )
}
