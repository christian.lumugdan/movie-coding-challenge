//
//  InfoAction.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct InfoAction {
  let title: String
  let onSelect: VoidResult
}
