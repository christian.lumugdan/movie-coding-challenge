//
//  InfoAction+Snackbar.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import MaterialComponents.MaterialSnackbar

extension InfoAction {
  private static let dismiss: VoidResult = {
    MDCSnackbarManager.dismissAndCallCompletionBlocks(withCategory: nil)
  }

  static var snackbarDismiss: InfoAction { .init(title: S.hide(), onSelect: dismiss) }
}
