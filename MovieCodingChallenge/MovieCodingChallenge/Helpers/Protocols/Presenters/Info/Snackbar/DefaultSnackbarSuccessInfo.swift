//
//  DefaultSnackbarSuccessInfo.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

struct DefaultSnackbarSuccessInfo: InfoProtocol {
  let message: String
}

extension DefaultSnackbarSuccessInfo {
  var foregroundColor: UIColor { T.component.snackbar.success.foregroundColor }
  var backgroundColor: UIColor { T.component.snackbar.success.backgroundColor }
  var action: InfoAction? { .snackbarDismiss }
}
