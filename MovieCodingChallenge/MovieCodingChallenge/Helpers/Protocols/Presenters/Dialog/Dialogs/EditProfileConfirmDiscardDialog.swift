//
//  EditProfileConfirmDiscardDialog.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct EditProfileConfirmDiscardDialog: DialogProtocol {
  let onDiscard: VoidResult

  init(onDiscard: @escaping VoidResult) {
    self.onDiscard = onDiscard
  }
}

extension EditProfileConfirmDiscardDialog {
  var title: String? { S.editProfileConfirmDiscardTitle() }
  var message: String? { S.editProfileConfirmDiscardMessage() }
  
  var cancelOption: DialogOption? { nil }
  var positiveOption: DialogOption? {
    DialogOption(
      title: S.editProfileConfirmDiscardOptionCancel(),
      isPreferred: true
    )
  }
  var negativeOption: DialogOption? {
    DialogOption(
      title: S.editProfileConfirmDiscardOptionNegative(),
      onSelect: onDiscard
    )
  }
}
