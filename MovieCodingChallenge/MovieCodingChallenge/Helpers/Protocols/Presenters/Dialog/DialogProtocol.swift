//
//  DialogProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol DialogProtocol {
  var title: String? { get }
  var message: String? { get }
  var cancelOption: DialogOption? { get }
  var positiveOption: DialogOption? { get }
  var negativeOption: DialogOption? { get }
}
