//
//  DialogOption.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct DialogOption {
  let title: String
  var onSelect: VoidResult = DefaultClosure.voidResult()
  var isPreferred: Bool = false
}

// MARK: - Equatable

extension DialogOption: Equatable {
  static func == (lhs: DialogOption, rhs: DialogOption) -> Bool {
    lhs.title == rhs.title
  }
}
