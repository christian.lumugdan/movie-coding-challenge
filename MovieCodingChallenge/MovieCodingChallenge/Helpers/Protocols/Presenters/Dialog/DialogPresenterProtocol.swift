//
//  DialogPresenterProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol DialogPresenterProtocol {
  func presentDialog(
    _ dialog: DialogProtocol,
    from source: UIViewController
  )
}
