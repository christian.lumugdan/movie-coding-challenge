//
//  FacebookLoginPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import FBSDKLoginKit
import Foundation
import UIKit

protocol FacebookLoginPresenterProtocol {
  var loginManagerProvider: EmptyResult<LoginManager>! { get set }
  var controllerProvider: EmptyResult<UIViewController>? { get set }
  
  func presentFacebookLogin(
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult,
    onCancel: @escaping VoidResult
  )
}

class FacebookLoginPresenter: FacebookLoginPresenterProtocol {
  var loginManagerProvider: EmptyResult<LoginManager>! = { LoginManager() }
  var controllerProvider: EmptyResult<UIViewController>?
  
  private let readPermissions: [String] = ["public_profile", "email"]
}

extension FacebookLoginPresenter {
  func presentFacebookLogin(
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult,
    onCancel: @escaping VoidResult
  ) {
    let loginManager = loginManagerProvider()
    loginManager.logIn(
      permissions: readPermissions,
      from: controllerProvider?()
    ) { result, error in

      if let e = error {
        return onError(e)
      }

      guard let result = result else {
        preconditionFailure("result should be not nil at this point")
      }

      if result.isCancelled {
        return onCancel()
      }

      guard let token = result.token else {
        preconditionFailure("result.token should be not nil at this point")
      }

      onSuccess(token.tokenString)
    }
  }
}
