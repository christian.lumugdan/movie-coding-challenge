//
//  AppleSignInPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import AuthenticationServices
import Foundation
import UIKit

/// Apple Sign-In
///
/// Requirements:
/// - Enable Apple Sign-In capability on each project Target. Do the same in Dev Portal.
/// - For development, make sure to add a provisioning profile.
///

protocol AppleSignInPresenterProtocol: NSObjectProtocol {
  var onAuthorizationComplete: SingleResult<ASAuthorizationCredential>! { get set }

  var controllerProvider: EmptyResult<ASAuthorizationController>! { get set }
  var presentationAnchorProvider: EmptyResult<ASPresentationAnchor?>? { get set }

  var onSuccess: SingleResult<String>? { get set }
  var onError: ErrorResult? { get set }
  var onCancel: VoidResult? { get set }

  func presentAppleSignIn()
}

class AppleSignInPresenter: NSObject, AppleSignInPresenterProtocol {
  var onAuthorizationComplete: SingleResult<ASAuthorizationCredential>!

  var controllerProvider: EmptyResult<ASAuthorizationController>! = AppleSignInPresenter.defaultControllerProvider
  var presentationAnchorProvider: EmptyResult<ASPresentationAnchor?>?

  var onSuccess: SingleResult<String>?
  var onError: ErrorResult?
  var onCancel: VoidResult?

  override init() {
    super.init()

    onAuthorizationComplete = handleOnAuthorizationComplete()
  }
}

// MARK: - Methods

extension AppleSignInPresenter {
  func presentAppleSignIn() {
    let controller = controllerProvider()

    controller.presentationContextProvider = self
    controller.delegate = self

    controller.performRequests()
  }
}

// MARK: - Handlers

private extension AppleSignInPresenter {
  func handleOnAuthorizationComplete() -> SingleResult<ASAuthorizationCredential> {
    return { [weak self] credential in
      guard let self = self,
        let appleIdCredential = credential as? ASAuthorizationAppleIDCredential,
        let data = appleIdCredential.identityToken,
        let token = String(data: data, encoding: .utf8) else { return }

      self.onSuccess?(token)
    }
  }
}

// MARK: - Utils

private extension AppleSignInPresenter {
  static var defaultControllerProvider: EmptyResult<ASAuthorizationController> {
    return {
      let request = ASAuthorizationAppleIDProvider().createRequest()
      // request.state = UUID().uuidString
      request.requestedScopes = [.fullName, .email]

      return ASAuthorizationController(authorizationRequests: [request])
    }
  }
}

// MARK: ASAuthorizationControllerDelegate

extension AppleSignInPresenter: ASAuthorizationControllerDelegate {
  func authorizationController(
    controller: ASAuthorizationController,
    didCompleteWithAuthorization authorization: ASAuthorization
  ) {
    onAuthorizationComplete(authorization.credential)
  }

  func authorizationController(
    controller: ASAuthorizationController,
    didCompleteWithError error: Error
  ) {
    if let error = error as? ASAuthorizationError,
      error.code == .canceled || error.code == .unknown {
      onCancel?()
      return
    }

    onError?(error)
  }
}

// MARK: - ASAuthorizationControllerPresentationContextProviding

extension AppleSignInPresenter: ASAuthorizationControllerPresentationContextProviding {
  func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
    return presentationAnchorProvider?() ?? UIApplication.shared.multisceneKeyWindow!
  }
}
