//
//  GoogleSignInPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import GoogleSignIn
import UIKit

protocol GoogleSignInPresenterProtocol: NSObjectProtocol {
  var onSignInSuccess: SingleResult<GIDGoogleUser>! { get set }
  var onSignInError: ErrorResult! { get set }

  var controllerProvider: EmptyResult<UIViewController>? { get set }
  var sessionProvider: EmptyResult<GIDSignIn?>! { get set }

  var onSuccess: SingleResult<String>? { get set }
  var onError: SingleResult<GoogleSignInPresenterError>? { get set }
  var onCancel: VoidResult? { get set }

  func presentGoogleSignIn()
}

class GoogleSignInPresenter: NSObject,
  GoogleSignInPresenterProtocol, GIDSignInDelegate {
  var onSignInSuccess: SingleResult<GIDGoogleUser>!
  var onSignInError: ErrorResult!

  var controllerProvider: EmptyResult<UIViewController>?
  var sessionProvider: EmptyResult<GIDSignIn?>! = { GIDSignIn.sharedInstance() }

  var onSuccess: SingleResult<String>?
  var onError: SingleResult<GoogleSignInPresenterError>?
  var onCancel: VoidResult?

  override init() {
    super.init()

    onSignInSuccess = handleSignInSuccess()
    onSignInError = handleSignInError()
  }
}

extension GoogleSignInPresenter {
  func presentGoogleSignIn() {
    guard let session = sessionProvider() else {
      onError?(.noSession)
      return
    }

    session.delegate = self
    session.presentingViewController = controllerProvider?()

    if session.hasPreviousSignIn() {
      session.restorePreviousSignIn()
    } else {
      session.signIn()
    }
  }
}

extension GoogleSignInPresenter {
  func sign(
    _ signIn: GIDSignIn!,
    didSignInFor user: GIDGoogleUser!,
    withError error: Error!
  ) {
    if let e = error {
      return onSignInError(e)
    }

    onSignInSuccess(user)
  }

  func sign(
    _ signIn: GIDSignIn!,
    didDisconnectWith user: GIDGoogleUser!,
    withError error: Error!
  ) {
    // noop
  }
}

// MARK: - Utils

private extension GoogleSignInPresenter {
  func handleSignInSuccess() -> SingleResult<GIDGoogleUser> {
    return { [weak self] user in
      guard let s = self else { return }

      guard let token = user.authentication.idToken else {
        preconditionFailure("user.authentication.idToken should be not nil at this point")
      }

      s.onSuccess?(token)
    }
  }

  func handleSignInError() -> ErrorResult {
    return { [weak self] error in

      guard let self = self else { return }

      let nsError = error as NSError
      if nsError.code == GIDSignInErrorCode.canceled.rawValue {
        self.onCancel?()
      } else {
        self.onError?(.wrapped(error))
      }
    }
  }
}

// MARK: - GoogleSignInPresenterError

enum GoogleSignInPresenterError: LocalizedError {
  case noSession
  case wrapped(_ error: Error)

  var errorDescription: String? {
    switch self {
    case .noSession:
      return R.string.localizable.socialGoogleErrorNoSession()
    case let .wrapped(error):
      return error.localizedDescription
    }
  }
}

extension GoogleSignInPresenterError: Equatable {
  static func == (lhs: GoogleSignInPresenterError, rhs: GoogleSignInPresenterError) -> Bool {
    return lhs.localizedDescription == rhs.localizedDescription
  }
}
