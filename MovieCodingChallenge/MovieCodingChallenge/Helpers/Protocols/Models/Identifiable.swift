//
//  Identifiable.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol Identifiable {
  associatedtype ValueType: CustomStringConvertible

  var id: ValueType { get }
  var idString: String { get }
}

extension Identifiable {
  var idString: String { id.description }
}

extension Equatable where Self: Identifiable {
  static func == (lhs: Self, rhs: Self) -> Bool {
    return lhs.idString == rhs.idString
  }
}
