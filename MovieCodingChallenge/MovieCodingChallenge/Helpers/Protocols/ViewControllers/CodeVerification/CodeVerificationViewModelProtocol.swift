//
//  CodeVerificationViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol CodeVerificationViewModelProtocol {
  var titleText: String { get }
  var messageText: String { get }
  var subMessageText: String? { get }

  var shouldHideResendOption: Bool { get }
  var resendCountdownInSeconds: Float { get }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - Defaults

extension CodeVerificationViewModelProtocol {
  var shouldHideResendOption: Bool { false }

  var resendCountdownInSeconds: Float {
    App.shared.config.defaultCodeResendTimerInSeconds
  }
}
