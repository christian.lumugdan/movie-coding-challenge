//
//  CodeVerificationControllerProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import MaterialComponents.MaterialTextFields
import RxSwift
import SVProgressHUD

protocol CodeVerificationControllerProtocol where Self: ViewController {
  var codeVerificationVM: CodeVerificationViewModelProtocol { get }

  // Make sure that each field has its own designated Tag value (from 1 to N, left to right.)
  var textFields: [VerificationCodeField]! { get }
  var resendButtonView: DebouncedButtonView! { get }

  func setupFields()
  func setupFieldInputControllers()
  func setupResendButtonView()

  func bindFields()
  func bindResendButtonView()

  func handleCodeFillCompletion() -> SingleResult<String>
}

// MARK: - Setup

extension CodeVerificationControllerProtocol
  where Self: ViewController & VerificationCodeFieldDelegate & UITextFieldDelegate {
  func setupFields() {
    textFields.forEach(setup)
  }

  private func setup(_ field: VerificationCodeField) {
    field.keyboardType = .numberPad
    field.clearButtonMode = .never
    field.backgroundColor = .clear
    field.textAlignment = .center
    field.font = .boldSystemFont(ofSize: 24)
    field.textColor = R.color.form.textField.textColor()!
    field.leadingViewMode = .never

    field.delegate = self
    field.fieldDelegate = self
  }

  func setupResendButtonView() {
    resendButtonView.isHidden = codeVerificationVM.shouldHideResendOption
    resendButtonView.debounceInSeconds = codeVerificationVM.resendCountdownInSeconds
  }
}

// MARK: - Bind

extension CodeVerificationControllerProtocol where Self: ViewController {
  func bindFields() {
    let codeFills = textFields
      .map { $0.rx.text.share(replay: 1) }

    Observable
      .combineLatest(codeFills)
      .delay(.milliseconds(250), scheduler: MainScheduler.instance)
      .distinctUntilChanged()
      .filter { !$0.contains(nil) && !$0.contains("") }
      .map { $0.compactMap { $0 }.joined() }
      .subscribe(onNext: handleCodeFillCompletion())
      .disposed(by: rx.disposeBag)
  }

  func bindResendButtonView() {
    resendButtonView.button.rx.tap
      .asObservable()
      .bind(onNext: trigger(type(of: self).resendCodeButtonTapped))
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Actions

private extension CodeVerificationControllerProtocol {
  func resendCodeButtonTapped() {
    progressPresenter.presentIndefiniteProgress(from: self)
    codeVerificationVM.resendCode(
      onSuccess: handleResendCodeSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

extension CodeVerificationControllerProtocol {
  func handleVerificationError() -> ErrorResult {
    return { [weak self] error in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.infoPresenter.presentErrorInfo(error: error)
      self.clearFields(makeFirstFieldFirstResponder: true)
    }
  }

  func handleResendCodeSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.infoPresenter.presentSuccessInfo(
        message: S.accountVerifAlertsCodeSent()
      )
      self.clearFields(makeFirstFieldFirstResponder: true)
      self.resendButtonView.startTimer()
    }
  }
}

// MARK: - Utils

extension CodeVerificationControllerProtocol {
  func generateInputControllers(for fields: [VerificationCodeField]) -> [MDCTextInputControllerUnderline] {
    return fields.map { field in
      let controller = MDCTextInputControllerUnderline(textInput: field)
      controller.applyTheme(withScheme: MDCHelper.shared.theme.containerScheme)
      controller.isFloatingEnabled = false
      return controller
    }
  }

  func clearFields(makeFirstFieldFirstResponder: Bool = false) {
    textFields.forEach {
      $0.text = nil
      $0.sendActions(for: .valueChanged)
    }

    if makeFirstFieldFirstResponder {
      textFields.first?.becomeFirstResponder()
    }
  }

  func hasLeftAdjacentField(to codeField: VerificationCodeField) -> Bool {
    return codeField.tag - 1 >= 1
  }

  func shouldChangeCharacters(
    for textField: UITextField,
    in range: NSRange,
    replacementString string: String
  ) -> Bool {
    // Autofill suggestion
    if string.isEmpty {
      clearFields(makeFirstFieldFirstResponder: true)
      return true
    }

    let currentText = textField.text ?? ""
    let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
    let trimmedProspectiveText = prospectiveText.components(separatedBy: CharacterSet.decimalDigits.inverted)
      .joined()

    guard !trimmedProspectiveText.isEmpty else { return false }

    if trimmedProspectiveText.count <= 1 {
      if textField.tag + 1 <= textFields.count {
        // Update this field now then move cursor to the right adjacent field.
        textField.text = trimmedProspectiveText
        textField.superview?.viewWithTag(textField.tag + 1)?.becomeFirstResponder()
      } else {
        return true
      }
    } else if textField.tag + 1 <= textFields.count {
      // Replace code of current field.
      var str = trimmedProspectiveText
      var currentTag = textField.tag

      textField.text = String(str.prefix(upTo: String.Index(utf16Offset: 1, in: str)))

      repeat {
        // Move cursor to the next field.
        if let field = textField.superview?.viewWithTag(currentTag + 1) as? UITextField {
          field.becomeFirstResponder()

          // Remove the first digit
          str = String(str.suffix(from: String.Index(utf16Offset: 1, in: str)))
          if str.count >= 1 {
            field.text = String(str.prefix(upTo: String.Index(utf16Offset: 1, in: str)))
          }

          if currentTag + 1 == textFields.count {
            field.resignFirstResponder()
          }
        }
        currentTag += 1
      } while currentTag + 1 <= textFields.count && str.count > 0
    }

    return false
  }
}

// MARK: - VerificationCodeFieldDelegate Default

extension VerificationCodeFieldDelegate where Self: CodeVerificationControllerProtocol {
  func verificationCodeFieldDidDeleteBackward(_ codeField: VerificationCodeField, oldText: String?) {
    guard codeField.text.isNilOrEmpty && oldText.isNilOrEmpty else { return }

    guard hasLeftAdjacentField(to: codeField) else { return }

    guard let leftField = codeField.superview?.viewWithTag(codeField.tag - 1) as? UITextField else { return }

    // Tapping back button on the right field while it's empty should clear the left field's value.
    leftField.text = ""

    leftField.becomeFirstResponder()
  }
}
