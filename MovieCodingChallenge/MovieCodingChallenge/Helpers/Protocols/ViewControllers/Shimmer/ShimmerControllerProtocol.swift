//
//  ShimmerControllerProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import SkeletonView

protocol ShimmerControllerProtocol where Self: UIViewController {
  func show(in parent: UIViewController)
  func hide()
}

extension ShimmerControllerProtocol {
  func show(in vc: UIViewController) {
    addAsChild(to: vc)

    view.showAnimatedGradientSkeleton(
      animation: GradientDirection.topLeftBottomRight.slidingAnimation()
    )
  }

  func hide() {
    removeAsChild()
  }
}
