//
//  ShimmerPresenterProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol ShimmerPresenterProtocol where Self: UIViewController {
  var shimmerVC: ShimmerControllerProtocol! { get }

  func showShimmer()
  func hideShimmer()
}

extension ShimmerPresenterProtocol {
  func showShimmer() {
    shimmerVC?.show(in: self)
  }

  func hideShimmer() {
    shimmerVC?.hide()
  }
}
