//
//  SingleFormInputViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol SingleFormInputViewModelProtocol: AnyObject {
  func validate(_ input: String?) -> Result<String, Error>
}

// MARK: - Defaults

extension SingleFormInputViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return .success("")
  }
}
