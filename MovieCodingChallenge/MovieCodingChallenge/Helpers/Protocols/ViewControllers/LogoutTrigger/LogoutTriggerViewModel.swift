//
//  LogoutTriggerViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class LogoutTriggerViewModel: LogoutTriggerViewModelProtocol {
  
  private let service: LogoutServiceProtocol
  
  init(service: LogoutServiceProtocol = App.shared.logout) {
    self.service = service
  }
  
}

extension LogoutTriggerViewModel {

  func logout() {
    service.logout(
      shouldBroadcast: false,
      onSuccess: DefaultClosure.voidResult(),
      onError: DefaultClosure.singleResult()
    )
  }
  
}
