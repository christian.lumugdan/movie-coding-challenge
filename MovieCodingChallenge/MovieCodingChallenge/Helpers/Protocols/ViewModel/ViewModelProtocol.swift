//
//  ViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import NSObject_Rx

protocol ViewModelProtocol: TriggerableProtocol, HasDisposeBag {
  var onRefresh: VoidResult? { get set }
}
