//
//  FormProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol FormProtocol {
  associatedtype RawInput
  associatedtype ValidInput
  associatedtype ValidationHandlers

  static func validate(
    rawInput: RawInput,
    validationHandlers: ValidationHandlers
  ) -> ValidInput?
}
