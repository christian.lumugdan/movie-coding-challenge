//
//  NibLoadable.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

protocol NibLoadable where Self: UIView {
  var contentView: UIView! { get }
}

extension NibLoadable {
  func loadNib(named nibName: String? = nil) {
    let nib = nibName ?? String(describing: type(of: self))
    Bundle.main.loadNibNamed(nib, owner: self, options: nil)
    addSubview(contentView)
    contentView.autoPinEdgesToSuperviewEdges()
  }
}
