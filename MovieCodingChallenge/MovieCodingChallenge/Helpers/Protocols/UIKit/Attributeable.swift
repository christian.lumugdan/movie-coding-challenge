//
//  Attributeable.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

protocol Attributeable where Self: UIView {
  associatedtype AttributeType: ViewAttributeType
  func applyAttribute(_ attribute: AttributeType)
}

protocol ViewAttributeType {
  associatedtype ViewType: UIView
  func apply(to view: ViewType)
}
