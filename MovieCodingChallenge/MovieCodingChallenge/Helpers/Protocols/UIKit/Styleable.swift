//
//  Styleable.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

protocol Styleable where Self: UIView {
  associatedtype StyleType: ViewStyleType
  func applyStyle(_ style: StyleType)
}

protocol ViewStyleType {
  associatedtype ViewType: UIView
  func apply(to view: ViewType)
}
