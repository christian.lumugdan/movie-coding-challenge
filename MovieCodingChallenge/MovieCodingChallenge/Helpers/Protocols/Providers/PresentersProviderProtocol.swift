//
//  PresentersProviderProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

protocol PresentersProviderProtocol {
  var progressPresenter: ProgressPresenterProtocol { get }
  var dialogPresenter: DialogPresenterProtocol { get }
  var infoPresenter: InfoPresenterProtocol { get }
}
