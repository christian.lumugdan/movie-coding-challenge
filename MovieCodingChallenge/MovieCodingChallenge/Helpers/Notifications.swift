//
//  Notifications.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

// MARK: - Session

extension Notification.Name {
  private static var prefix: String { "com.MovieCodingChallenge.notification.name.session" }

  static let didLogin = Notification.Name(rawValue: "\(prefix).didLogin")
  static let didLogout = Notification.Name(rawValue: "\(prefix).didLogout")
  static let didSkipEmailVerification = Notification.Name(rawValue: "\(prefix).didSkipEmailVerification")
  static let didCompleteOTPOnboarding = Notification.Name(rawValue: "\(prefix).didCompleteOTPOnboarding")
  static let didVerifyAccount = Notification.Name(rawValue: "\(prefix).didVerifyAccount")
  static let didRefreshUser = Notification.Name(rawValue: "\(prefix).didRefreshUser")
  static let didCompleteOnboarding = Notification.Name(rawValue: "\(prefix).didCompleteOnboarding")
  static let didFinishShareFeed = Notification.Name(rawValue: "\(prefix).didFinishShareFeed")
}
