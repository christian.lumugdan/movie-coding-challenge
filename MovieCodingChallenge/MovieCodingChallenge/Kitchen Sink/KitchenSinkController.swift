//
//  KitchenSinkController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class KitchenSinkController: ViewController {}

// MARK: - ProgressHUD Actions

private extension KitchenSinkController {
  @IBAction
  func indefiniteProgressHUDButtonTapped(_ sender: Any) {
    progressPresenter.presentIndefiniteProgress(from: self)

    delay(5) { [weak self] in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
    }
  }

  @IBAction
  func definiteProgressHUDButtonTapped(_ sender: Any) {
    var progress: Float = 0
    Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { [weak self] timer in
      guard let self = self else { return }
      progress += 0.01
      self.progressPresenter.presentProgress(value: progress, from: self)

      if progress >= 1 {
        self.progressPresenter.dismiss()
        timer.invalidate()
      }
    }
  }
}

// MARK: - Snackbar Actions

private extension KitchenSinkController {
  @IBAction
  func showSuccessSnackbarButtonTapped(_ sender: Any) {
    infoPresenter.presentSuccessInfo(message: "This is a success message")
  }

  @IBAction
  func showErrorSnackbarButtonTapped(_ sender: Any) {
    infoPresenter.presentErrorMessage(message: "This is an error message")
  }
}
