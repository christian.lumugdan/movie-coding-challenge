//
//  KitchenSinkPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol KitchenSinkPresenter where Self: ViewController {}

extension KitchenSinkPresenter {
  func setupKitchensinkTrigger(
    forView view: UIView,
    action: Selector
  ) {
    #if DEBUG
      let longPressRecognizer = UILongPressGestureRecognizer(
        target: self,
        action: action
      )
      view.addGestureRecognizer(longPressRecognizer)
    #endif
  }

  func presentKitchenSinkController() {
    let vc = R.storyboard.kitchenSink.instantiateInitialViewController()!
    vc.modalPresentationStyle = .fullScreen
    present(vc, animated: true)
  }
}
