//
//  AppDelegate+UserNotifications.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

// MARK: - User Notifications

extension AppDelegate {
  /// Receives the device token needed to deliver remote notifications. Device tokens can change,
  /// so your app needs to re-register every time it is launched and pass the received token back
  /// to your server. Device tokens always change when the user restores backup data to a new
  /// device or computer or re-installs the operating system.
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
    let token = tokenParts.joined()
    debugLog("Device Token: \(token)")

    App.shared.pushNotif.registerDevice()
  }

  func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    // you should process the error object appropriately and disable any features related to
    // remote notifications. Because notifications are not going to be arriving anyway, it is
    // usually better to degrade gracefully and avoid any unnecessary work needed to process
    // or display those notifications.
    debugLog("Failed to register: \(error)")

    App.shared.errorHandling.processError(error)
  }

  func application(
    _ application: UIApplication,
    didReceiveRemoteNotification userInfo: [AnyHashable: Any],
    fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
  ) {
    debugLog(String(describing: userInfo))

    App.shared.deepLink.handlePushNotification(with: userInfo)
  }
}

