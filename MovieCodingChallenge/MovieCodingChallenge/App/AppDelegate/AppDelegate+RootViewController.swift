//
//  AppDelegate+RootViewController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import NSObject_Rx
import RxSwift
import UIKit

/// Extension for RootViewController Management
///
/// Notes:
/// - This fits perfectly with the Coordinator Pattern as the AppCoordinator type.
///
extension AppDelegate {
  func setupSessionServiceNotificationObservers() {
    let nc = NotificationCenter.default

    Observable
      .merge(
        nc.rx.notification(.didLogin, object: App.shared.session),
        nc.rx.notification(.didLogout, object: App.shared.session),
        nc.rx.notification(.didVerifyAccount, object: App.shared.session),
        nc.rx.notification(.didSkipEmailVerification, object: App.shared.session),
        nc.rx.notification(.didCompleteOTPOnboarding, object: App.shared.session)
      )
      .subscribe(onNext: { [unowned self] _ in
        self.updateRootViewController()
      })
      .disposed(by: rx.disposeBag)

    nc.rx.notification(.didCompleteOnboarding, object: App.shared.session)
      .subscribe(onNext: { [unowned self] _ in
        self.switchToDashboard()        
      })
      .disposed(by: rx.disposeBag)
  }

  func updateRootViewController(isFromAppLaunch: Bool = false) {
    guard App.shared.session.isActive,
      let user = App.shared.session.user else {
      return switchToFrontPage(isFromAppLaunch: isFromAppLaunch)
    }

    if user.hasCompletedOnboarding {
      return switchToDashboard()
    }
    
    let needToPresentVerifyEmail =
      user.hasJustSignedUp &&
      (user.primaryUsername == .email) &&
      !App.shared.session.hasSkippedEmailVerification
    
    let needToPresentOTPAddEmail =
      user.hasJustSignedUp &&
      (user.primaryUsername == .phone) &&
      !App.shared.session.hasCompletedOTPAddEmail
    
    if needToPresentVerifyEmail {
      presentEmailVerificationPage()
    } else if needToPresentOTPAddEmail {
      presentOTPAddEmailPage()
    } else if !user.hasName {
      presentOnboardingNamePage()
    } else if !user.hasAvatar {
      presentOnboardingPicturePage()
    } else {
      switchToDashboard()
    }
  }

  func switchToDashboard() {
    window?.rootViewController = R.storyboard.dashboard.instantiateInitialViewController()
  }

  func switchToFrontPage(isFromAppLaunch: Bool) {
    guard isFromAppLaunch else {
      return switchToAuth()
    }

    let rvc = R.storyboard.appIntro.appIntroController()!
    rvc.viewModel = AppIntroViewModel()
    rvc.viewModel?.onContinue = handleAppIntroContinue()
    rvc.viewModel?.onSkip = handleAppIntroSkip()

    let nc = NavigationController(rootViewController: rvc)
    window?.setRootViewControllerAnimated(nc)
  }

  func switchToAuth(shouldPushToExistingNav: Bool = false) {
    let authVC = R.storyboard.auth.landingController()!
    authVC.viewModel = LandingViewModel()

    if shouldPushToExistingNav,
      let navCtrl = window?.rootViewController as? UINavigationController {
      return navCtrl.setViewControllers([authVC], animated: true)
    }

    let nc = NavigationController(rootViewController: authVC)
    window?.setRootViewControllerAnimated(nc)
  }

  func handleAppIntroContinue() -> VoidResult {
    return { [unowned self] in
      self.switchToAuth(shouldPushToExistingNav: true)
    }
  }
  
  func handleAppIntroSkip() -> VoidResult {
    return { [unowned self] in
      self.switchToAuth(shouldPushToExistingNav: true)
    }
  }
}

// MARK: - Verification Code Flow

private extension AppDelegate {
  func presentEmailVerificationPage() {
    presentVerificationCodePage(type: .email)
  }

  func presentPhoneVerificationPage() {
    presentVerificationCodePage(type: .phone)
  }

  func presentVerificationCodePage(type: VerificationType) {
    guard App.shared.session.isActive else {
      preconditionFailure("SessionService.user should not be nil at this point")
    }

    let controller = R.storyboard.authSignup.accountVerificationController()!

    var viewModel: AccountVerificationViewModelProtocol
    switch type {
    case .email:
      viewModel = EmailVerificationViewModel(shouldLogOutOnBackTap: true)
    case .phone:
      viewModel = PhoneVerificationViewModel(shouldLogOutOnBackTap: true)
    }

    controller.viewModel = viewModel

    if window?.rootViewController is UINavigationController {
      // Standard flow
      presentVerificationControllerFromRegistrationFlow(controller)
    } else {
      // Resume verification after re-launch.
      presentAuthControllerFromRelaunch(controller)
    }
  }

  func presentVerificationControllerFromRegistrationFlow(_ controller: AccountVerificationController) {
    guard let nav = self.window?.rootViewController as? UINavigationController else {
      preconditionFailure("Expecting window.rootViewController to be UINavigationController")
    }

    // Pop out RegistrationForm from stack before adding AccountVerification screen.
    if nav.topViewController is CreatePasswordController {
      nav.popViewController(animated: false)
    }

    nav.pushViewController(controller, animated: true)
  }
}

// MARK: - Profile Setup Flow

private extension AppDelegate {
  func presentOTPAddEmailPage() {
    let vc = R.storyboard.otpAuth.otpAddEmailController()!
    vc.viewModel = OTPAddEmailViewModel()
    presentOnboardingController(vc)
  }
  
  func presentOnboardingNamePage() {
    let vc = R.storyboard.authSignup.onboardingNameController()!
    vc.viewModel = OnboardingNameViewModel()
    vc.logoutTriggerVM = LogoutTriggerViewModel()
    vc.inputCache = OnboardingInputCache()
    presentOnboardingController(vc)
  }

  func presentOnboardingPicturePage() {
    let vc = R.storyboard.authSignup.onboardingPictureController()!
    vc.viewModel = OnboardingPictureViewModel()
    vc.inputCache = OnboardingInputCache()
    vc.imagePickerPresenter = vc
    vc.shouldHideBackButton = true
    presentOnboardingController(vc)
  }

  func presentOnboardingController(_ controller: UIViewController) {
    if let nav = window?.rootViewController as? UINavigationController {
      nav.setViewControllers([controller], animated: true)
    } else {
      let nc = NavigationController(rootViewController: controller)
      window?.rootViewController = nc
    }
  }
}

// MARK: - Utils

private extension AppDelegate {
  func presentAuthControllerFromRelaunch(_ controller: UIViewController) {
    guard App.shared.session.user != nil else {
      preconditionFailure("SessionService.user should not be nil at this point")
    }

//    var accountChecker: UIViewController?

//    if let email = user.email {
//      let emailChecker = R.storyboard.auth.emailCheckerController()!
//      emailChecker.viewModel = EmailCheckerViewModel(initialEmail: email)
//      accountChecker = emailChecker
//    } else if user.phoneNumber != nil {
//      let phoneChecker = R.storyboard.auth.phoneCheckerController()!
//      phoneChecker.viewModel = PhoneCheckerViewModel()
//      accountChecker = phoneChecker
//    } else {
//      preconditionFailure("user.email or user.phoneNumber should not be nil at this point")
//    }

    // This path re-construction would allow the user to tap Back from
    // AccountVerification screen unto EmailChecker form.
    //
    
    let landingVC = R.storyboard.auth.landingController()!
    landingVC.viewModel = LandingViewModel()
    
    let path = [
      landingVC,
//      accountChecker!,
      controller
    ]

    let nav = NavigationController(rootViewController: controller)
    nav.setViewControllers(path, animated: false)

    window?.setRootViewControllerAnimated(nav)
  }
}
