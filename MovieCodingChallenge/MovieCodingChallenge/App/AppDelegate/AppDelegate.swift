//
//  AppDelegate.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import AlamofireNetworkActivityIndicator
import AlamofireNetworkActivityLogger
import DropDown
import FBSDKCoreKit
import Firebase
import GoogleSignIn
import IQKeyboardManagerSwift
import UIKit

class AppDelegate: UIResponder, UIApplicationDelegate {
  // swiftlint:disable:next force_cast
  static var shared: AppDelegate { UIApplication.shared.delegate as! AppDelegate }
  
  var window: UIWindow?

  override init() {}

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
  ) -> Bool {
    if !UIApplication.isRunningTests {
      FirebaseApp.configure()
    }

    App.shared.bootstrap(with: application, launchOptions: launchOptions)

    DropDown.startListeningToKeyboard()
    
    #if DEBUG
      NetworkActivityLogger.shared.level = .debug
      NetworkActivityLogger.shared.startLogging()
    #endif

    IQKeyboardManager.shared.enable = true
    IQKeyboardManager.shared.disabledDistanceHandlingClasses = [
      CreatePasswordController.self
    ]
    IQKeyboardManager.shared.disabledToolbarClasses = [
      OnboardingNameController.self
    ]

    NetworkActivityIndicatorManager.shared.isEnabled = true


    // Facebook Application Delegate
    //
    // Requires the ff info.plist keys:
    // - FacebookAppID
    // - FacebookDisplayName
    // - LSApplicationQueriesSchemes
    //
    // Check facebook documentation for updated keys:
    // - https://developers.facebook.com/docs/facebook-login/ios
    ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

    window = UIWindow(frame: UIScreen.main.bounds)
    window?.backgroundColor = T.component.window.backgroundColor
    window?.tintColor = T.component.window.tintColor
    updateRootViewController(isFromAppLaunch: true)
    loadAppearancePreferences()
    window?.makeKeyAndVisible()

    setupSessionServiceNotificationObservers()
    
    return true
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    if App.shared.session.isActive {
      App.shared.deepLink.executeDeepLink()

      App.shared.profile.refreshProfile(
        onSuccess: DefaultClosure.voidResult(),
        onError: DefaultClosure.singleResult()
      )
    }

    application.applicationIconBadgeNumber = 0
  }

  func applicationDidEnterBackground(_ application: UIApplication) {}

  func applicationWillResignActive(_ application: UIApplication) {}

  func applicationWillEnterForeground(_ application: UIApplication) {}

  func applicationWillTerminate(_ application: UIApplication) {}
}

// MARK: - Inter-App Communication

extension AppDelegate {
  func application(
    _ app: UIApplication,
    open url: URL,
    options: [UIApplication.OpenURLOptionsKey: Any] = [:]
  ) -> Bool {
    // Handle Facebook URL Scheme
    if ApplicationDelegate.shared.application(app, open: url, options: options) {
      return true
    }

    // Handle Google URL Scheme
    if GIDSignIn.sharedInstance().handle(url) {
      return true
    }

    return false
  }
}
