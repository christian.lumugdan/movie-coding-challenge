//
//  App.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import Firebase
import GooglePlaces
import GoogleSignIn
import UIKit
import Valet

/// This is our main application object. This holds instances of all the services available
/// in the app like the APIClient, SessionService, etc.
///
/// IMPORTANT:
/// - Defer creation of service instance up to the point where it's first needed.
///
class App {
  enum Environment: String {
    case staging
    case production
  }

  static let shared = App()

  static var environment: Environment {
    // These values are set in their corresponding Targets.
    // See: Target > Build Settings > Other Swift Flags.
    #if STAGING
      return .staging
    #else
      return .production
    #endif
  }

  static let valet = Valet.valet(
    with: Identifier(nonEmpty: App.bundleIdentifier!)!,
    accessibility: .whenUnlocked
  )

  private(set) var config: AppConfigProtocol!

  private(set) var api: APIClient!

  // MARK: PushNotifications

  private(set) var pushNotif: PushNotificationService!

  // MARK: Shared

  private(set) var session: SessionService!
  private(set) var deepLink: DeepLinkServiceProtocol!
  private(set) var errorHandling: ErrorHandlingServiceProtocol!

  // MARK: Auth

  private(set) var usernameChecker: UsernameCheckerServiceProtocol!
  private(set) var forgotPassword: ForgotPasswordServiceProtocol!
  private(set) var register: RegisterServiceProtocol!
  private(set) var login: LoginServiceProtocol!
  private(set) var socialAuth: SocialAuthServiceProtocol!
  private(set) var accountVerification: AccountVerificationServiceProtocol!
  private(set) var otp: OTPServiceProtocol!
  private(set) var profile: ProfileServiceProtocol!
  private(set) var credentials: CredentialsServiceProtocol!
  private(set) var accountDeletion: AccountDeletionServiceProtocol!
  private(set) var logout: LogoutServiceProtocol!

  // MARK: Initialization

  private init() {
    debugLog("env: \(App.environment.rawValue)")
  }

  func bootstrap(
    with application: UIApplication,
    launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) {
    config = (App.environment == .production) ? AppConfig() : AppConfigStaging()
    api = APIClient(
      baseURL: URL(string: config.apiUrl)!,
      version: "v1"
    )

    session = SessionService(api: api)
    deepLink = DeepLinkService(
      launchOptions: launchOptions
    )
    errorHandling = ErrorHandlingService()

    usernameChecker = UsernameCheckerService(api: api)
    forgotPassword = ForgotPasswordService(api: api)
    register = RegisterService(api: api)
    login = LoginService(api: api)
    socialAuth = SocialAuthService(api: api)
    accountVerification = AccountVerificationService(api: api)
    otp = OTPService(api: api)
    profile = ProfileService(
      onboardingAPI: api,
      profileAPI: api
    )
    credentials = CredentialsService(
      authChangeAPI: api,
      authVerificationAPI: api
    )
    accountDeletion = AccountDeletionService(api: api)
    logout = LogoutService(api: api)

    pushNotif = PushNotificationService(
      api: api,
      session: session,
      deepLink: deepLink
    )
    
    setup()
  }
}

// MARK: - Setup

extension App {
  func setup() {
    GIDSignIn.sharedInstance().clientID = config.secrets.googleClientID

    session.onResumeSessionError = errorHandling.processError
    errorHandling.onUnauthorizedError = session.handleUnauthorizedError()

    register.onAuth = session.handleAuthResult()
    login.onAuth = session.handleAuthResult()
    socialAuth.onAuth = session.handleAuthResult()

    accountVerification.onUserResult = session.handleVerifyAccountResult()

    profile.onUserResult = session.handleUserResult()
    profile.onAvatarResult = session.handleAvatarResult()

    credentials.onUserResult = session.handleUserResult()

    accountDeletion.onDeAuth = session.handleDeAuth()
    accountDeletion.onError = session.handleDeAuthError()

    logout.onDeAuth = session.handleDeAuth()
    logout.onError = session.handleDeAuthError()
  }
}

// MARK: - App Info

extension App {
  static var bundleIdentifier: String? {
    return Bundle.main.bundleIdentifier
  }

  /// A dictionary, constructed from the bundle’s Info.plist file.
  static var info: [String: Any] {
    return Bundle.main.infoDictionary ?? [:]
  }

  static var displayName: String {
    return (info["CFBundleDisplayName"] as? String) ?? "MovieCodingChallenge"
  }

  /// Alias for `CFBundleShortVersionString`.
  static var releaseVersion: String {
    return (info["CFBundleShortVersionString"] as? String) ?? "1.0"
  }

  /// Alias for `CFBundleVersion`.
  static var buildNumber: String {
    return (info["CFBundleVersion"] as? String) ?? "1"
  }

  static var icon: UIImage {
    guard
      let icons = info["CFBundleIcons"] as? [String: Any],
      let primaryIcon = icons["CFBundlePrimaryIcon"] as? [String: Any],
      let iconFiles = primaryIcon["CFBundleIconFiles"] as? [String],
      let lastIcon = iconFiles.last,
      let appIcon = UIImage(named: lastIcon)
    else {
      preconditionFailure("app icon should not be nil")
    }

    return appIcon
  }
}

/// Use this for all App-level errors.
// TODO: Add conformance to CustomNSError.
enum AppError: Error {
  case unauthorized(_ reason: String)
  case unknown
}

extension AppError: LocalizedError {
  var errorDescription: String? {
    switch self {
    case .unauthorized:
      return S.errorDevAuthorization()
    default:
      return S.errorDevSomethingWrong()
    }
  }

  var failureReason: String? {
    switch self {
    case let .unauthorized(reason):
      return reason
    default:
      return S.errorDevUnknown()
    }
  }
}
