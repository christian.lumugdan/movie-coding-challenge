//
//  Progress+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

extension Progress {
  /// Value should be from 0 to 1.
  convenience init(value: Float) {
    let adjustedExpectedProgress = min(value, 1)
    self.init(totalUnitCount: 100)
    self.completedUnitCount = Int64(Float(100 * adjustedExpectedProgress))
  }
}
