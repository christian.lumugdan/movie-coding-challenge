//
//  Result+Validate.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension Result {
  var isSuccess: Bool {
    (try? get()) != nil
  }

  var genericResult: Result<Success, Error> {
    switch self {
    case let .success(validInput):
      return .success(validInput)
    case let .failure(error):
      return .failure(error)
    }
  }
}
