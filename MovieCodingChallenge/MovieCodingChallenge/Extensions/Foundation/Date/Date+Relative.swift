//
//  Date+Relative.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import SwiftDate

private typealias Gradation = RelativeFormatter.Gradation
private typealias Rule = Gradation.Rule
private typealias Unit = RelativeFormatter.Unit

extension Date {
  func toPreciseRelative() -> String {
    toRelative(style: .precise)
  }
}

// MARK: - Custom RelativeFormatter.Style

extension RelativeFormatter.Style {
  static var precise: RelativeFormatter.Style {
    return .init(
      flavours: [.longConvenient],
      gradation: .precise
    )
  }
}

// MARK: - Custom RelativeFormatter.Gradation

extension RelativeFormatter.Gradation {
  static var precise: RelativeFormatter.Gradation {
    Gradation([
      Rule(.now, threshold: .value(0)),
      Rule(.second, threshold: .value(0.5)),
      Rule(.minute, threshold: .value(59.5)),
      Rule(.hour, threshold: .value(59.5 * 60.0)),
      Rule(.day, threshold: .value(23.5 * 60 * 60)),
      Rule(.week, threshold: .value(6.5 * Unit.day.factor)),
      Rule(.month, threshold: .value(3.5 * 7 * Unit.day.factor)),
      Rule(.year, threshold: .value(11.5 * Unit.month.factor))
    ])
  }
}
