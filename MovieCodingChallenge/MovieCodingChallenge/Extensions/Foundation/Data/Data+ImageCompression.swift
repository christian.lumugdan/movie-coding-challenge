//
//  Data+ImageCompression.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

extension Data {
  func compressedImageData(quality: CGFloat) -> Data? {
    guard let image = UIImage(data: self) else {
      return nil
    }
    return image.jpegData(compressionQuality: quality)
  }

  func compressedImageData(quality: Float) -> Data? {
    compressedImageData(quality: CGFloat(quality))
  }
}

extension Array where Element == Data {
  func mapCompressedImageData(quality: CGFloat) -> [Data?] {
    map { $0.compressedImageData(quality: quality) }
  }

  func mapCompressedImageData(quality: Float) -> [Data?] {
    mapCompressedImageData(quality: CGFloat(quality))
  }
}
