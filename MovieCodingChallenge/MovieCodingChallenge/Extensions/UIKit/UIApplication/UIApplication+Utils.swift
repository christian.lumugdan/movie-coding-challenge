//
//  UIApplication+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

extension UIApplication {
  var firstKeyWindow: UIWindow? { windows.first { $0.isKeyWindow } }
  
  static var bundleIdentifier: String? {
    return Bundle.main.bundleIdentifier
  }

  static var isRunningTests: Bool {
    return ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil
  }
}
