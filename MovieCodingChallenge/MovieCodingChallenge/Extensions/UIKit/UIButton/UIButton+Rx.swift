//
//  UIButton+Rx.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import RxCocoa
import RxSwift

extension Reactive where Base: UIButton {
  /// Reactive wrapper for `sendActions(for:)`
  func sendActions<T>(for controlEvents: UIControl.Event) -> Binder<T> {
    return Binder(base) { button, _ in
      button.sendActions(for: controlEvents)
    }
  }
}
