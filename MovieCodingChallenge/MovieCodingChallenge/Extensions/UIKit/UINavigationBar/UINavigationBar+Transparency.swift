//
//  UINavigationBar+Transparency.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
  func setTransparent() {
    setBackgroundImage(UIImage(), for: .default)
    shadowImage = UIImage()
    barTintColor = .clear
    isTranslucent = true
  }
  
  func setNonTransparent() {
    setBackgroundImage(nil, for: .default)
    shadowImage = nil
    barTintColor = .white
    isTranslucent = true
  }
}
