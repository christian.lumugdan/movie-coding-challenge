//
//  UILabel+Rx.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import RxCocoa
import RxSwift

extension UILabel {
  func bindHiddenIfEmptyOrNilText() {
    rx.observe(String.self, "text")
      .map { $0.isNilOrEmpty }
      .bind(to: rx.isHidden)
      .disposed(by: rx.disposeBag)
  }
}
