//
//  WKWebView+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit
import WebKit

extension WKWebView {
  func load(_ urlString: String?) {
    guard let urlStr = urlString, let url = URL(string: urlStr) else { return }
    let request = URLRequest(url: url)
    load(request)
  }
}
