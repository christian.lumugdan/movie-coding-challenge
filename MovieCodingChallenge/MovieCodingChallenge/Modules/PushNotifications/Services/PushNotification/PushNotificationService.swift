//
//  PushNotificationService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Firebase
import FirebaseMessaging
import RxSwift

class PushNotificationService: NSObject, PushNotificationServiceProtocol {
  private let api: DevicesAPI
  private let session: SessionServiceProtocol
  private let deepLink: DeepLinkServiceProtocol

  init(
    api: DevicesAPI,
    session: SessionServiceProtocol,
    deepLink: DeepLinkServiceProtocol,
    messaging: Messaging = .messaging()
  ) {
    self.api = api
    self.session = session
    self.deepLink = deepLink

    super.init()
    messaging.delegate = self

    setup()
    bind()
  }
}

// MARK: - Register

extension PushNotificationService {
  func registerDevice() {
    registerDevice(
      onSuccess: DefaultClosure.voidResult(),
      onError: DefaultClosure.singleResult()
    )
  }
  
  private func registerDevice(
    onSuccess: @escaping VoidResult = DefaultClosure.voidResult(),
    onError: @escaping ErrorResult = DefaultClosure.singleResult()
  ) {
    retrieveDeviceToken(
      onSuccess: handleRetrieveTokenSuccess(
        onSuccess: onSuccess,
        onError: onError
      ),
      onError: onError
    )
  }

  private func registerDevice(
    token: String,
    onSuccess: @escaping VoidResult = DefaultClosure.voidResult(),
    onError: @escaping ErrorResult = DefaultClosure.singleResult()
  ) {
    guard canRegisterDevice else { return }
    
    api.postDevices(
      token: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Utils

private extension PushNotificationService {
  func setup() {
    requestPermissions()
  }

  func bind() {
    let nc = NotificationCenter.default
    let didLogin = nc.rx.notification(.didLogin, object: session)
    let didRefreshUser = nc.rx.notification(.didRefreshUser, object: session)

    Observable
      .merge(
        didLogin,
        didRefreshUser
      )
      .subscribe(onNext: handleSessionStart())
      .disposed(by: rx.disposeBag)
  }

  func requestPermissions() {
    guard canRegisterDevice else { return }

    notifCenter.requestAuthorization(
      options: options,
      completionHandler: handlePermissionResult()
    )
  }

  func registerIfAuthorized() {
    notifCenter.getNotificationSettings { settings in
      debugLog("Notification settings: \(settings)")
      guard settings.authorizationStatus == .authorized else { return }
      DispatchQueue.main.async {
        UIApplication.shared.registerForRemoteNotifications()
      }
    }
  }

  func retrieveDeviceToken(
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    InstanceID.instanceID().instanceID { result, error in
      if let e = error {
        return onError(e)
      }

      guard let token = result?.token else {
        return debugLog("No FCM token found")
      }

      debugLog("New FCM token: \(token)")
      onSuccess(token)
    }
  }
}

// MARK: - Handlers

private extension PushNotificationService {
  func handleSessionStart() -> SingleResult<Notification> {
    return { [unowned self] _ in
      self.requestPermissions()
    }
  }

  func handlePermissionResult() -> DoubleResult<Bool, Error?> {
    return { [unowned self] granted, _ in
      debugLog("Permission granted: \(granted)")
      guard granted else { return }
      self.registerIfAuthorized()
    }
  }

  func handleRetrieveTokenSuccess(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> SingleResult<String> {
    return { [unowned self] token in
      self.registerDevice(
        token: token,
        onSuccess: onSuccess,
        onError: onError
      )
    }
  }
}

// MARK: - Getters

private extension PushNotificationService {
  var options: UNAuthorizationOptions { .init(arrayLiteral: [.alert, .badge, .sound]) }
  var notifCenter: UNUserNotificationCenter { .current() }
}

// MARK: - MessagingDelegate

extension PushNotificationService: MessagingDelegate {
  func messaging(
    _ messaging: Messaging,
    didReceiveRegistrationToken fcmToken: String
  ) {
    debugLog("FCM token: \(fcmToken)")
    guard canRegisterDevice else { return }
    registerDevice(token: fcmToken)
  }

  func messaging(
    _ messaging: Messaging,
    didReceive remoteMessage: MessagingRemoteMessage
  ) {
    debugLog(String(describing: remoteMessage.appData))
  }
}

// MARK: - UNUserNotificationCenterDelegate

extension PushNotificationService: UNUserNotificationCenterDelegate {
  // This method will be called when app received push notifications in foreground
  func userNotificationCenter(
    _ center: UNUserNotificationCenter,
    willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
  ) {
    var options: UNNotificationPresentationOptions = []

    let userInfo = notification.request.content.userInfo
    if deepLink.shouldPresentPushNotificationInForeground(with: userInfo) {
      options = [.alert, .badge, .sound]
    }

    completionHandler(options)
  }

  func userNotificationCenter(
    _ center: UNUserNotificationCenter,
    didReceive response: UNNotificationResponse,
    withCompletionHandler completionHandler: @escaping () -> Void
  ) {
    guard response.actionIdentifier == UNNotificationDefaultActionIdentifier else { return }
    let userInfo = response.notification.request.content.userInfo
    deepLink.handlePushNotification(with: userInfo)
    deepLink.executeDeepLink()
  }
}

// MARK: - Private Getters

private extension PushNotificationService {
  var canRegisterDevice: Bool { session.isActive }
}
