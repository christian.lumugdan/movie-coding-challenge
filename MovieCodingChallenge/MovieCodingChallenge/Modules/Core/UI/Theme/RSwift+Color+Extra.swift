//
//  RSwift+Color+Extra.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

// swiftlint:disable type_name

import UIKit

extension R.color {
  enum form {}
}

// MARK: - Form

extension R.color.form {
  enum textField {
    static func placeholderTextColor() -> UIColor? { R.color.greysInactive() }
    static func textColor() -> UIColor? { R.color.textOnLightRegular() }
  }
}
