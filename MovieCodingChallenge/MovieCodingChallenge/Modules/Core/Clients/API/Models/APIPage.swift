//
//  APIPage.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct APIPage: Equatable {
  let index: Int
  let size: Int
}

// MARK: - Static

extension APIPage {
  static var `default`: APIPage { .init(index: 1, size: App.shared.config.defaultPageSize) }
}
