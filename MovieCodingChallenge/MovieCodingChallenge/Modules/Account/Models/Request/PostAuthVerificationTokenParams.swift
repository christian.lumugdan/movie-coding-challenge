//
//  PostAuthVerificationTokenParams.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

struct PostAuthVerificationTokenParams: APIModel, Codable {
  // Either only. Not both
  var password: String?
  var otp: String?
}
