//
//  AccountController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class AccountController: StaticTableViewController {
  var viewModel: AccountViewModelProtocol!

  @IBOutlet private(set) var profileImageView: ImageView!
  @IBOutlet private(set) var fullnameLabel: UILabel!
  @IBOutlet private(set) var usernameLabel: UILabel!
}  

// MARK: - Lifecycle

extension AccountController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bind()
  }
}

// MARK: - Setup

private extension AccountController {
  func setup() {
    setupTable()
    refresh()
  }

  func setupTable() {
    tableView.tableFooterView = UIView()
  }
}

// MARK: - Bind

private extension AccountController {
  func bind() {
    bindPostNotification(
      notificationName: .didRefreshUser,
      onPost: handleUserRefresh()
    )
  }
}

// MARK: - Helpers

private extension AccountController {
  func refresh() {
    profileImageView.setImageWithURL(viewModel.profileImageURL)
    fullnameLabel.text = viewModel.fullNameText
    usernameLabel.text = viewModel.usernameText
  }
}

// MARK: - Handlers

private extension AccountController {
  func handleUserRefresh() -> SingleResult<Notification> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.refresh()
    }
  }
}

// MARK: - UITableViewDelegate

extension AccountController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)

    guard let row = Row(rawValue: indexPath.row) else { return }
    navigateToPage(at: row)
  }
}

// MARK: - Routing

private extension AccountController {
  func navigateToPage(at row: Row) {
    switch row {
    case .editProfile:
      navigateToEditProfilePage()
    case .about:
      navigateToAboutPage()
    case .accountSettings:
      if viewModel.isEmailPrimary {
        navigateToEmailPrimaryAccountSettingsPage()
      } else {
        navigateToPhonePrimaryAccountSettingsPage()
      }
    }
  }

  func navigateToEditProfilePage() {
    let vc = R.storyboard.editProfile.editProfileController()!
    vc.viewModel = EditProfileViewModel()
    navigationController?.pushViewController(vc, animated: true)
  }

  func navigateToAboutPage() {
    let vc = R.storyboard.about.aboutController()!
    navigationController?.pushViewController(vc, animated: true)
  }

  func navigateToEmailPrimaryAccountSettingsPage() {
    let vc = R.storyboard.accountSettings.emailPrimaryAccountSettingsController()!
    vc.viewModel = EmailPrimaryAccountSettingsViewModel()
    navigationController?.pushViewController(vc, animated: true)
  }

  func navigateToPhonePrimaryAccountSettingsPage() {
    let vc = R.storyboard.accountSettings.phonePrimaryAccountSettingsController()!
    vc.viewModel = PhonePrimaryAccountSettingsViewModel()
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - Types

private enum Row: Int {
  case editProfile = 0
  case about
  case accountSettings
}
