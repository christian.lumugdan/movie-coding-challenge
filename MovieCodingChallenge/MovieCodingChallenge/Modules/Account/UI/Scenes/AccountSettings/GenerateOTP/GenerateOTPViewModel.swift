//
//  GenerateOTPViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class GenerateOTPViewModel: GenerateOTPViewModelProtocol {
  private let session: SessionServiceProtocol
  private let service: OTPServiceProtocol

  init(
    session: SessionServiceProtocol = App.shared.session,
    service: OTPServiceProtocol = App.shared.otp
  ) {
    self.session = session
    self.service = service
  }
}

extension GenerateOTPViewModel {
  func generateOTP(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    guard let phoneNumber = session.user?.phoneNumber else {
      preconditionFailure("session.user.phoneNumber should not be nil at this point")
    }

    service.generateOTP(
      for: phoneNumber,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
