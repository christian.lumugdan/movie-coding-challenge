//
//  GenerateOTPViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol GenerateOTPViewModelProtocol {
  func generateOTP(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
