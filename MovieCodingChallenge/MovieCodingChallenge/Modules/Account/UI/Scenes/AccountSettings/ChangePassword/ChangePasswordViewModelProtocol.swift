//
//  ChangePasswordViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ChangePasswordViewModelProtocol: SingleFormInputViewModelProtocol {
  var onComplete: VoidResult? { get set }
  
  var titleText: String { get }
  var messageText: String { get }

  func changePassword(
    with newPassword: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension ChangePasswordViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return NewPasswordInputValidator.validate(input).genericResult
  }
}
