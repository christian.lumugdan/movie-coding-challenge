//
//  ChangePhoneViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ChangePhoneViewModelProtocol: SingleFormInputViewModelProtocol {
  var onComplete: SingleResult<String>? { get set }

  func changePhone(
    phoneNumber: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension ChangePhoneViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return PhoneNumberInputValidator.validate(input).genericResult
  }
}
