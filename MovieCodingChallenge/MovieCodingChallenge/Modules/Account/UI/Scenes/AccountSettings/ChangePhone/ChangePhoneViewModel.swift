//
//  ChangePhoneViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ChangePhoneViewModel: ChangePhoneViewModelProtocol {
  var onComplete: SingleResult<String>?

  private let token: String
  private let service: CredentialsServiceProtocol

  init(
    token: String,
    service: CredentialsServiceProtocol = App.shared.credentials
  ) {
    self.token = token
    self.service = service
  }
}

// MARK: - Methods

extension ChangePhoneViewModel {
  func changePhone(
    phoneNumber: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(phoneNumber)
    switch result {
    case let .success(phoneNumber):
      service.requestChangePhoneNumber(
        with: phoneNumber,
        token: token,
        onSuccess: handleSuccess(
          phoneNumber: phoneNumber,
          thenExecute: onSuccess
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Util Handlers

private extension ChangePhoneViewModel {
  func handleSuccess(
    phoneNumber: String,
    thenExecute completionHandler: @escaping VoidResult
  ) -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.onComplete?(phoneNumber)
      completionHandler()
    }
  }
}
