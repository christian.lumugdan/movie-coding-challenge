//
//  VerifyChangeCredentialViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol VerifyChangeCredentialViewModelProtocol: CodeVerificationViewModelProtocol {
  var onComplete: VoidResult? { get set }

  func verify(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
