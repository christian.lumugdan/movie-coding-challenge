//
//  VerifyChangePhoneViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class VerifyChangePhoneViewModel: VerifyChangeCredentialViewModelProtocol {
  var onComplete: VoidResult?

  private let phoneNumber: String
  private let verificationToken: String
  private let service: CredentialsServiceProtocol

  init(
    phoneNumber: String,
    verificationToken: String,
    service: CredentialsServiceProtocol = App.shared.credentials
  ) {
    self.phoneNumber = phoneNumber
    self.verificationToken = verificationToken
    self.service = service
  }
}

// MARK: - Methods

extension VerifyChangePhoneViewModel {
  func verify(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.verifyChangePhoneNumber(
      with: token,
      verificationToken: verificationToken,
      onSuccess: handleSuccess(
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.requestChangePhoneNumber(
      with: phoneNumber,
      token: verificationToken,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Util Handlers

private extension VerifyChangePhoneViewModel {
  func handleSuccess(
    thenExecute completionHandler: @escaping VoidResult
  ) -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.onComplete?()
      completionHandler()
    }
  }
}

// MARK: - Getters

extension VerifyChangePhoneViewModel {
  var titleText: String { S.verifyCredentialChangePhoneTitle() }
  var messageText: String { S.verifyCredentialChangePhoneMessage() }
  var subMessageText: String? { phoneNumber.formattedPhoneNumber }
}
