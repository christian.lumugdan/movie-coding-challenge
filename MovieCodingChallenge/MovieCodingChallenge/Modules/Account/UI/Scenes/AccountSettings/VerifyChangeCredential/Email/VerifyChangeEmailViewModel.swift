//
//  VerifyChangeCredentialViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class VerifyChangeEmailViewModel: VerifyChangeCredentialViewModelProtocol {
  var onComplete: VoidResult?

  private let email: String
  private let verificationToken: String
  private let service: CredentialsServiceProtocol

  init(
    email: String,
    verificationToken: String,
    service: CredentialsServiceProtocol = App.shared.credentials
  ) {
    self.email = email
    self.verificationToken = verificationToken
    self.service = service
  }
}

// MARK: - Methods

extension VerifyChangeEmailViewModel {
  func verify(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.verifyChangeEmail(
      with: token,
      verificationToken: verificationToken,
      onSuccess: handleSuccess(
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.requestChangeEmail(
      with: email,
      token: verificationToken,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Util Handlers

private extension VerifyChangeEmailViewModel {
  func handleSuccess(
    thenExecute completionHandler: @escaping VoidResult
  ) -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.onComplete?()
      completionHandler()
    }
  }
}

// MARK: - Getters

extension VerifyChangeEmailViewModel {
  var titleText: String { S.verifyCredentialChangeEmailTitle() }
  var messageText: String { S.verifyCredentialChangeEmailMessage() }
  var subMessageText: String? { email }
}
