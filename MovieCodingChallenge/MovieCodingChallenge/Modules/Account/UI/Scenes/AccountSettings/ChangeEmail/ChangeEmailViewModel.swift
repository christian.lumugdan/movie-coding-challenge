//
//  ChangeEmailViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ChangeEmailViewModel: ChangeEmailViewModelProtocol {
  var onComplete: SingleResult<String>?
  
  private let token: String
  private let service: CredentialsServiceProtocol

  init(
    token: String,
    service: CredentialsServiceProtocol = App.shared.credentials
  ) {
    self.token = token
    self.service = service
  }
}

// MARK: - Methods

extension ChangeEmailViewModel {
  func changeEmail(
    with newEmail: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(newEmail)
    switch result {
    case let .success(newEmail):
      service.requestChangeEmail(
        with: newEmail,
        token: token,
        onSuccess: handleSuccess(
          with: newEmail,
          thenExecute: onSuccess
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Util Handlers

private extension ChangeEmailViewModel {
  func handleSuccess(
    with newEmail: String,
    thenExecute completionHandler: @escaping VoidResult
  ) -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.onComplete?(newEmail)
      completionHandler()
    }
  }
}
