//
//  ChangeEmailViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ChangeEmailViewModelProtocol: SingleFormInputViewModelProtocol {
  var onComplete: SingleResult<String>? { get set }
  
  func changeEmail(
    with newEmail: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension ChangeEmailViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return EmailAddressInputValidator.validate(input).genericResult
  }
}
