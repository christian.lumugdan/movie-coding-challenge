//
//  ChangeEmailController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class ChangeEmailController: ScrollViewController {
  var viewModel: ChangeEmailViewModelProtocol!

  @IBOutlet private(set) var field: MDCTextField!
  @IBOutlet private(set) var continueButton: MDCButton!

  private(set) var fieldInputController: MDCTextInputControllerBase!

  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension ChangeEmailController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    progressPresenter.dismiss()

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true

    view.endEditing(true)
  }
}

// MARK: - Setup

private extension ChangeEmailController {
  func setup() {
    setupEmailField()
    continueButton.applyStyle(.primary)
  }
  
  func setupEmailField() {
    field.applyAttribute(.email)
    field.placeholder = S.emailFieldPlaceholder()
    field.returnKeyType = .continue
    field.enablesReturnKeyAutomatically = true
    
    fieldInputController = MDCHelper.inputController(for: field)
    
    field.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension ChangeEmailController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

private extension ChangeEmailController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.changeEmail(
      with: field.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - SingleFormInputControllerProtocol

extension ChangeEmailController: SingleFormInputControllerProtocol {
  var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }
}

// MARK: - UsernameCheckerControllerProtocol

//extension ChangeEmailController: UsernameCheckerControllerProtocol {}
