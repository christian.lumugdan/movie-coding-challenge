//
//  VerifyOTPViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class VerifyOTPViewModel: VerifyOTPViewModelProtocol {
  var onComplete: DoubleResult<String, String>?

  private let session: SessionServiceProtocol
  private let credentialsService: CredentialsServiceProtocol
  private let otpService: OTPServiceProtocol

  init(
    session: SessionServiceProtocol = App.shared.session,
    credentialsService: CredentialsServiceProtocol = App.shared.credentials,
    otpService: OTPServiceProtocol = App.shared.otp
  ) {
    self.session = session
    self.credentialsService = credentialsService
    self.otpService = otpService
  }
}

// MARK: - Methods

extension VerifyOTPViewModel {
  func verifyOTP(
    _ otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    credentialsService.requestVerificationTokenWithOTP(
      otp,
      onSuccess: handleSuccess(
        with: otp,
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    guard let phoneNumber = session.user?.phoneNumber else {
      preconditionFailure("session.user.phoneNumber should not be nil at this point")
    }

    otpService.generateOTP(
      for: phoneNumber,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Util Handlers

private extension VerifyOTPViewModel {
  func handleSuccess(
    with otp: String,
    thenExecute completionHandler: @escaping VoidResult
  ) -> SingleResult<String> {
    return { [weak self] token in
      guard let self = self else { return }
      self.onComplete?(token, otp)
      completionHandler()
    }
  }
}

// MARK: - Getters

extension VerifyOTPViewModel {
  var titleText: String { S.verifyOtpTitle() }
  var messageText: String { S.verifyOtpMessage() }
  var subMessageText: String? { session.user?.phoneNumber?.formattedPhoneNumber }
}
