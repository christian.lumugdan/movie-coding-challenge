//
//  VerifyOTPViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol VerifyOTPViewModelProtocol: CodeVerificationViewModelProtocol {
  var onComplete: DoubleResult<String, String>? { get set }
  
  func verifyOTP(
    _ otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
