//
//  PhonePrimaryAccountSettingsViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhonePrimaryAccountSettingsViewModel: PhonePrimaryAccountSettingsViewModelProtocol {
  private let sessionService: SessionServiceProtocol
  private let logoutService: LogoutServiceProtocol

  init(
    sessionService: SessionServiceProtocol = App.shared.session,
    logoutService: LogoutServiceProtocol = App.shared.logout
  ) {
    self.sessionService = sessionService
    self.logoutService = logoutService
  }
}

// MARK: - Getters

extension PhonePrimaryAccountSettingsViewModel {
  var emailAddress: String { sessionService.user?.email ?? "" }
  var mobileNumber: String { sessionService.user?.phoneNumber?.formattedPhoneNumber ?? "" }
}

// MARK: - API

extension PhonePrimaryAccountSettingsViewModel {
  func logout(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    logoutService.logout(
      shouldBroadcast: true,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
