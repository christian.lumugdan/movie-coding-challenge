//
//  PhonePrimaryAccountSettingsController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import SVProgressHUD

class PhonePrimaryAccountSettingsController: StaticTableViewController {
  var viewModel: PhonePrimaryAccountSettingsViewModelProtocol!

  @IBOutlet private(set) var emailAddressLabel: UILabel!
  @IBOutlet private(set) var mobileNumberLabel: UILabel!

  private var changePhoneFlowCoordinator: PhonePrimaryChangePhoneFlowCoordinator!
  private var changeEmailFlowCoordinator: PhonePrimaryChangeEmailFlowCoordinator!
  private var deleteAccountFlowCoordinator: PhonePrimaryDeleteAccountFlowCoordinator!
}

// MARK: - Lifecycle

extension PhonePrimaryAccountSettingsController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bind()
  }
}

// MARK: - Setup

private extension PhonePrimaryAccountSettingsController {
  func setup() {
    setupTable()
    setupFlowCoordinators()
    refresh()
  }

  func setupTable() {
    tableView.tableFooterView = UIView()
  }

  func setupFlowCoordinators() {
    guard let nc = navigationController else {
      preconditionFailure("Expecting non-nil navigationController got nil")
    }
    changePhoneFlowCoordinator = PhonePrimaryChangePhoneFlowCoordinator(navigationController: nc)
    changeEmailFlowCoordinator = PhonePrimaryChangeEmailFlowCoordinator(navigationController: nc)
    deleteAccountFlowCoordinator = PhonePrimaryDeleteAccountFlowCoordinator(navigationController: nc)
  }
}

// MARK: - Bind

private extension PhonePrimaryAccountSettingsController {
  func bind() {
    bindPostNotification(
      notificationName: .didRefreshUser,
      onPost: handleUserRefresh()
    )
  }
}

// MARK: - Actions

private extension PhonePrimaryAccountSettingsController {
  func deleteCellTapped() {
    showAlertDialog(
      title: S.dialogsDeleteAccountTitle(),
      positiveButtonText: S.dialogsDeleteAccountButtonsPositive(),
      positiveHandler: handleDeleteAccountConfirmed(),
      negativeButtonText: S.dialogsDeleteAccountButtonsNegative()
    )
  }

  func logoutCellTapped() {
    showAlertDialog(
      title: S.dialogsLogoutTitle(),
      positiveButtonText: S.dialogsLogoutButtonsPositive(),
      positiveHandler: handleLogoutConfirmed(),
      negativeButtonText: S.dialogsLogoutButtonsNegative()
    )
  }
}

// MARK: - Helpers

extension PhonePrimaryAccountSettingsController {
  private func refresh() {
    emailAddressLabel.text = viewModel.emailAddress
    mobileNumberLabel.text = viewModel.mobileNumber
  }

  private func showAlertDialog(
    title: String,
    positiveButtonText: String,
    positiveHandler: SingleResult<UIAlertAction>?,
    negativeButtonText: String
  ) {
    let dialog = UIAlertController(
      title: title,
      message: nil,
      preferredStyle: .alert
    )

    dialog.addAction(UIAlertAction(
      title: negativeButtonText,
      style: .cancel,
      handler: nil
    ))

    dialog.addAction(UIAlertAction(
      title: positiveButtonText,
      style: .destructive,
      handler: positiveHandler
    ))

    present(dialog, animated: true)
  }
}

// MARK: - Event Handlers

private extension PhonePrimaryAccountSettingsController {
  // MARK: Logout

  func handleLogoutConfirmed() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let self = self else { return }

      self.progressPresenter.presentIndefiniteProgress(from: self)
      self.viewModel.logout(
        onSuccess: self.handleSuccess(),
        onError: self.handleError()
      )
    }
  }

  // MARK: Delete Account

  func handleDeleteAccountConfirmed() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.deleteAccountFlowCoordinator.start()
    }
  }
  
  func handleUserRefresh() -> SingleResult<Notification> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.refresh()
    }
  }
}

// MARK: - UITableViewDelegate

extension PhonePrimaryAccountSettingsController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)

    guard let row = Row(rawValue: indexPath.row) else { return }
    navigateToPage(at: row)
  }
}

// MARK: - Routing

private extension PhonePrimaryAccountSettingsController {
  func navigateToPage(at row: Row) {
    switch row {
    case .mobileNumber:
      changePhoneFlowCoordinator.start()
    case .emailAddress:
      changeEmailFlowCoordinator.start()
    case .deleteAccount:
      deleteCellTapped()
    case .logout:
      logoutCellTapped()
    }
  }
}

// MARK: - Types

private enum Row: Int {
  case mobileNumber = 0
  case emailAddress
  case deleteAccount
  case logout
}
