//
//  PhonePrimaryAccountSettingsViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PhonePrimaryAccountSettingsViewModelProtocol {
  var emailAddress: String { get }
  var mobileNumber: String { get }

  func logout(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
