//
//  PhonePrimaryChangeEmailFlowCoordinator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class PhonePrimaryChangeEmailFlowCoordinator {
  lazy var viewModel: PhonePrimaryChangeEmailFlowViewModelProtocol! = { PhonePrimaryChangeEmailFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: StaticTableViewController

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last as? StaticTableViewController else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}
// MARK: - Methods

extension PhonePrimaryChangeEmailFlowCoordinator {
  func start() {
    generateOTP()
  }
}

// MARK: - GenerateOTP

private extension PhonePrimaryChangeEmailFlowCoordinator {
  func generateOTP() {
    baseController.progressPresenter.presentIndefiniteProgress(
      from: baseController
    )
    viewModel.generateOTPVM.generateOTP(
      onSuccess: handleGenerateOTPSuccess(),
      onError: baseController.handleError()
    )
  }
  
  func handleGenerateOTPSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.baseController.progressPresenter.dismiss()
      self.navigateToVerifyOTPPage()
    }
  }
}
// MARK: - VerifyOTP

private extension PhonePrimaryChangeEmailFlowCoordinator {
  func navigateToVerifyOTPPage() {
    var vm = viewModel.verifyOTPVM
    vm.onComplete = handleVerifyOTP()

    let vc = R.storyboard.accountSettings.verifyOTPController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyOTP() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let self = self else { return }
      self.viewModel.save(verificationToken: token)
      self.navigateToChangeEmailPage()
    }
  }
}

// MARK: - ChangeEmail

private extension PhonePrimaryChangeEmailFlowCoordinator {
  func navigateToChangeEmailPage() {
    let vm = viewModel.changeEmailVM
    vm.onComplete = handleChangeEmail()

    let vc = R.storyboard.accountSettings.changeEmailController()!
    vc.viewModel = vm

    var updatedVCs = navigationController.viewControllers
    updatedVCs.removeLast()
    updatedVCs.append(vc)
    navigationController.setViewControllers(updatedVCs, animated: true)
  }

  func handleChangeEmail() -> SingleResult<String> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.presentSuccessMessage()
      self.dismiss()
    }
  }
}

// MARK: - Dismiss

private extension PhonePrimaryChangeEmailFlowCoordinator {
  func presentSuccessMessage() {
    baseController.infoPresenter.presentSuccessInfo(
      message: S.verifyCredentialChangeEmailSuccessMessage()
    )
  }

  func dismiss() {
    navigationController.popToViewController(
      baseController,
      animated: true
    )
  }
}
