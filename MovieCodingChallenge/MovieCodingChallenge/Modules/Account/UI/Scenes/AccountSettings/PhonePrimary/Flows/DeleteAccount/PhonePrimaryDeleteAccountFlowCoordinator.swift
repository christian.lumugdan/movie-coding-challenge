//
//  PhonePrimaryDeleteAccountFlowCoordinator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class PhonePrimaryDeleteAccountFlowCoordinator {
  lazy var viewModel: PhonePrimaryDeleteAccountFlowViewModelProtocol! = { PhonePrimaryDeleteAccountFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: StaticTableViewController

  private var verificationToken: String!

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last as? StaticTableViewController else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension PhonePrimaryDeleteAccountFlowCoordinator {
  func start() {
    generateOTP()
  }
}

// MARK: - GenerateOTP

private extension PhonePrimaryDeleteAccountFlowCoordinator {
  func generateOTP() {
    baseController.progressPresenter.presentIndefiniteProgress(
      from: baseController
    )
    viewModel.generateOTPVM.generateOTP(
      onSuccess: handleGenerateOTPSuccess(),
      onError: baseController.handleError()
    )
  }

  func handleGenerateOTPSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.baseController.progressPresenter.dismiss()
      self.navigateToVerifyOTPPage()
    }
  }
}

// MARK: - VerifyOTP

private extension PhonePrimaryDeleteAccountFlowCoordinator {
  func navigateToVerifyOTPPage() {
    var vm = viewModel.verifyOTPVM
    vm.onComplete = handleVerifyOTP()

    let vc = R.storyboard.accountSettings.verifyOTPController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyOTP() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let self = self else { return }
      self.baseController.progressPresenter.presentIndefiniteProgress(
        from: self.baseController
      )
      self.viewModel.deleteAccount(
        with: token,
        onSuccess: self.baseController.handleSuccess(),
        onError: self.baseController.handleError()
      )
    }
  }
}
