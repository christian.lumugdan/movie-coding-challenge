//
//  PhonePrimaryDeleteAccountFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhonePrimaryDeleteAccountFlowViewModel: PhonePrimaryDeleteAccountFlowViewModelProtocol {
  private let service: AccountDeletionServiceProtocol

  init(service: AccountDeletionServiceProtocol = App.shared.accountDeletion) {
    self.service = service
  }
}

// MARK: - Methods

extension PhonePrimaryDeleteAccountFlowViewModel {
  func deleteAccount(
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.deleteAccount(
      token: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Getters

extension PhonePrimaryDeleteAccountFlowViewModel {
  var generateOTPVM: GenerateOTPViewModelProtocol {
    GenerateOTPViewModel()
  }

  var verifyOTPVM: VerifyOTPViewModelProtocol {
    VerifyOTPViewModel()
  }
}
