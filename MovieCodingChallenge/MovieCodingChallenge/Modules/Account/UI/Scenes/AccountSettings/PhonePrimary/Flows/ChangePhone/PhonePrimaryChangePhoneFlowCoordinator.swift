//
//  PhonePrimaryChangePhoneFlowCoordinator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class PhonePrimaryChangePhoneFlowCoordinator {
  lazy var viewModel: PhonePrimaryChangePhoneFlowViewModelProtocol! = { PhonePrimaryChangePhoneFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: StaticTableViewController

  private var verificationToken: String!

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last as? StaticTableViewController else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension PhonePrimaryChangePhoneFlowCoordinator {
  func start() {
    generateOTP()
  }
}

// MARK: - GenerateOTP

private extension PhonePrimaryChangePhoneFlowCoordinator {
  func generateOTP() {
    baseController.progressPresenter.presentIndefiniteProgress(
      from: baseController
    )
    viewModel.generateOTPVM.generateOTP(
      onSuccess: handleGenerateOTPSuccess(),
      onError: baseController.handleError()
    )
  }

  func handleGenerateOTPSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.baseController.progressPresenter.dismiss()
      self.navigateToVerifyOTPPage()
    }
  }
}

// MARK: - VerifyOTP

private extension PhonePrimaryChangePhoneFlowCoordinator {
  func navigateToVerifyOTPPage() {
    var vm = viewModel.verifyOTPVM
    vm.onComplete = handleVerifyOTP()

    let vc = R.storyboard.accountSettings.verifyOTPController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyOTP() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let self = self else { return }
      self.viewModel.save(verificationToken: token)
      self.navigateToChangePhonePage()
    }
  }
}

// MARK: - ChangePhone

private extension PhonePrimaryChangePhoneFlowCoordinator {
  func navigateToChangePhonePage() {
    let vm = viewModel.changePhoneVM
    vm.onComplete = handleChangePhone()

    let vc = R.storyboard.accountSettings.changePhoneController()!
    vc.viewModel = vm
    
    var updatedVCs = navigationController.viewControllers
    updatedVCs.removeLast()
    updatedVCs.append(vc)
    navigationController.setViewControllers(updatedVCs, animated: true)
  }

  func handleChangePhone() -> SingleResult<String> {
    return { [weak self] phoneNumber in
      guard let self = self else { return }
      self.viewModel.save(phoneNumber: phoneNumber)
      self.navigateToVerifyChangePhonePage()
    }
  }
}

// MARK: - VerifyChangePhone

private extension PhonePrimaryChangePhoneFlowCoordinator {
  func navigateToVerifyChangePhonePage() {
    var vm = viewModel.verifyChangeCredentialVM
    vm.onComplete = handleVerifyChangePhone()

    let vc = R.storyboard.accountSettings.verifyChangeCredentialController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyChangePhone() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.presentSuccessMessage()
      self.dismiss()
    }
  }
}

// MARK: - Dismiss

private extension PhonePrimaryChangePhoneFlowCoordinator {
  func presentSuccessMessage() {
    baseController.infoPresenter.presentSuccessInfo(
      message: S.verifyCredentialChangePhoneSuccessMessage()
    )
  }

  func dismiss() {
    navigationController.popToViewController(
      baseController,
      animated: true
    )
  }
}
