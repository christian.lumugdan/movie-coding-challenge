//
//  PhonePrimaryChangeEmailFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhonePrimaryChangeEmailFlowViewModel: PhonePrimaryChangeEmailFlowViewModelProtocol {
  private(set) var verificationToken: String!
  private(set) var email: String!
}

// MARK: - Methods

extension PhonePrimaryChangeEmailFlowViewModel {
  func save(verificationToken: String) {
    self.verificationToken = verificationToken
  }

  func save(email: String) {
    self.email = email
  }
}

// MARK: - Getters

extension PhonePrimaryChangeEmailFlowViewModel {
  var generateOTPVM: GenerateOTPViewModelProtocol {
    GenerateOTPViewModel()
  }
  
  var verifyOTPVM: VerifyOTPViewModelProtocol {
    VerifyOTPViewModel()
  }

  var changeEmailVM: ChangeEmailViewModelProtocol {
    ChangeEmailViewModel(
      token: verificationToken
    )
  }

  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol {
    VerifyChangeEmailViewModel(
      email: email,
      verificationToken: verificationToken
    )
  }
}
