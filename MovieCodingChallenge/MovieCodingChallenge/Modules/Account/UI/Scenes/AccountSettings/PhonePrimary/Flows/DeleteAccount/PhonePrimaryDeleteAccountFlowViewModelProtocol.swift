//
//  PhonePrimaryDeleteAccountFlowViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PhonePrimaryDeleteAccountFlowViewModelProtocol {
  var generateOTPVM: GenerateOTPViewModelProtocol { get }
  var verifyOTPVM: VerifyOTPViewModelProtocol { get }
  
  func deleteAccount(
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
