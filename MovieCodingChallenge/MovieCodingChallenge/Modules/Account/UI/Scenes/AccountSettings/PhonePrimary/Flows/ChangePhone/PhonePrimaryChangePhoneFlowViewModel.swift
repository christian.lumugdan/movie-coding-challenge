//
//  PhonePrimaryChangePhoneFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhonePrimaryChangePhoneFlowViewModel: PhonePrimaryChangePhoneFlowViewModelProtocol {
  private(set) var verificationToken: String!
  private(set) var phoneNumber: String!
}

// MARK: - Methods

extension PhonePrimaryChangePhoneFlowViewModel {
  func save(verificationToken: String) {
    self.verificationToken = verificationToken
  }

  func save(phoneNumber: String) {
    self.phoneNumber = phoneNumber
  }
}

// MARK: - Getters

extension PhonePrimaryChangePhoneFlowViewModel {
  var generateOTPVM: GenerateOTPViewModelProtocol {
    GenerateOTPViewModel()
  }

  var verifyOTPVM: VerifyOTPViewModelProtocol {
    VerifyOTPViewModel()
  }

  var changePhoneVM: ChangePhoneViewModelProtocol {
    ChangePhoneViewModel(
      token: verificationToken
    )
  }

  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol {
    VerifyChangePhoneViewModel(
      phoneNumber: phoneNumber,
      verificationToken: verificationToken
    )
  }
}
