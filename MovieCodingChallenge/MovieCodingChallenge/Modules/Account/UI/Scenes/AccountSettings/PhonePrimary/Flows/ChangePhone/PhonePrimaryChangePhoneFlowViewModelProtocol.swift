//
//  PhonePrimaryChangePhoneFlowViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PhonePrimaryChangePhoneFlowViewModelProtocol {
  var generateOTPVM: GenerateOTPViewModelProtocol { get }
  var verifyOTPVM: VerifyOTPViewModelProtocol { get }
  var changePhoneVM: ChangePhoneViewModelProtocol { get }
  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol { get }

  func save(verificationToken: String)
  func save(phoneNumber: String)
}
