//
//  VerifyPasswordController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD
import UIKit

class VerifyPasswordController: ScrollViewController {
  var viewModel: VerifyPasswordViewModelProtocol!
  var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }

  @IBOutlet private(set) var passwordField: APPasswordField!
  @IBOutlet private(set) var continueButton: MDCButton!

  @IBOutlet private(set) var passwordResetButton: UIButton!
  @IBOutlet private(set) var messageLabel: UILabel!

  private(set) var fieldInputController: MDCTextInputControllerBase!
  
  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - View LifeCycle

extension VerifyPasswordController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)
    navigationController?.navigationBar.setTransparent()

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    if viewWillAppearCallCount == 1 {
      passwordField.becomeFirstResponder()
    }
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension VerifyPasswordController {
  func setup() {
    messageLabel.text = viewModel.messageText
    setupPasswordField()

    continueButton.applyStyle(.primary)
  }

  func setupPasswordField() {
    passwordField.returnKeyType = .continue

    fieldInputController = MDCHelper.inputController(for: passwordField)
    
    field.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension VerifyPasswordController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

private extension VerifyPasswordController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.verifyPassword(
      passwordField.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

extension VerifyPasswordController {
  func handleSuccess() -> SingleResult<String> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.resetForm()
    }
  }
}

// MARK: - SingleFormInputControllerProtocol

extension VerifyPasswordController: SingleFormInputControllerProtocol {
  var field: MDCTextField! { passwordField }
}
