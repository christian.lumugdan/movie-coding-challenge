//
//  VerifyPasswordViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol VerifyPasswordViewModelProtocol: SingleFormInputViewModelProtocol {
  var onComplete: DoubleResult<String, String>? { get set }
  var messageText: String { get }
  
  func verifyPassword(
    _ password: String?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension VerifyPasswordViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return PasswordInputValidator.validate(input).genericResult
  }
}
