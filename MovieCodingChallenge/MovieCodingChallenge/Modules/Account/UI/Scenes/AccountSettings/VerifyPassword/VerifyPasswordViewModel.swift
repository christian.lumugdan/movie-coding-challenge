//
//  VerifyPasswordViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class VerifyPasswordViewModel: VerifyPasswordViewModelProtocol {
  var onComplete: DoubleResult<String, String>?

  let messageText: String

  private let service: CredentialsServiceProtocol

  init(
    messageText: String,
    service: CredentialsServiceProtocol = App.shared.credentials
  ) {
    self.messageText = messageText
    self.service = service
  }
}

// MARK: - Methods

extension VerifyPasswordViewModel {
  func verifyPassword(
    _ password: String?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    let result = validate(password)
    switch result {
    case let .success(password):
      service.requestVerificationTokenWithPassword(
        password,
        onSuccess: handleSuccess(
          with: password,
          thenExecute: onSuccess
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Util Handlers

private extension VerifyPasswordViewModel {
  func handleSuccess(
    with password: String,
    thenExecute completionHandler: @escaping SingleResult<String>
  ) -> SingleResult<String> {
    return { [weak self] token in
      guard let self = self else { return }
      self.onComplete?(token, password)
      completionHandler(token)
    }
  }
}
