//
//  EmailPrimaryAccountSettingsController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import SVProgressHUD

class EmailPrimaryAccountSettingsController: StaticTableViewController {
  var viewModel: EmailPrimaryAccountSettingsViewModelProtocol!

  @IBOutlet private(set) var emailAddressLabel: UILabel!
  @IBOutlet private(set) var mobileNumberLabel: UILabel!

  private var changeEmailFlowCoordinator: EmailPrimaryChangeEmailFlowCoordinator!
  private var changePhoneFlowCoordinator: EmailPrimaryChangePhoneFlowCoordinator!
  private var changePasswordFlowCoordinator: EmailPrimaryChangePasswordFlowCoordinator!
  private var deleteAccountFlowCoordinator: EmailPrimaryDeleteAccountFlowCoordinator!
}

// MARK: - Lifecycle

extension EmailPrimaryAccountSettingsController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bind()
  }
}

// MARK: - Setup

private extension EmailPrimaryAccountSettingsController {
  func setup() {
    setupTable()
    setupFlowCoordinators()
    refresh()
  }

  func setupTable() {
    tableView.tableFooterView = UIView()
  }

  func setupFlowCoordinators() {
    guard let nc = navigationController else {
      preconditionFailure("Expecting non-nil navigationController got nil")
    }
    changeEmailFlowCoordinator = EmailPrimaryChangeEmailFlowCoordinator(navigationController: nc)
    changePhoneFlowCoordinator = EmailPrimaryChangePhoneFlowCoordinator(navigationController: nc)
    changePasswordFlowCoordinator = EmailPrimaryChangePasswordFlowCoordinator(navigationController: nc)
    deleteAccountFlowCoordinator = EmailPrimaryDeleteAccountFlowCoordinator(navigationController: nc)
  }
}

// MARK: - Bind

private extension EmailPrimaryAccountSettingsController {
  func bind() {
    bindPostNotification(
      notificationName: .didRefreshUser,
      onPost: handleUserRefresh()
    )
  }
}

// MARK: - Actions

private extension EmailPrimaryAccountSettingsController {
  func deleteCellTapped() {
    showAlertDialog(
      title: S.dialogsDeleteAccountTitle(),
      positiveButtonText: S.dialogsDeleteAccountButtonsPositive(),
      positiveHandler: handleDeleteAccountConfirmed(),
      negativeButtonText: S.dialogsDeleteAccountButtonsNegative()
    )
  }

  func logoutCellTapped() {
    showAlertDialog(
      title: S.dialogsLogoutTitle(),
      positiveButtonText: S.dialogsLogoutButtonsPositive(),
      positiveHandler: handleLogoutConfirmed(),
      negativeButtonText: S.dialogsLogoutButtonsNegative()
    )
  }
}

// MARK: - Helpers

extension EmailPrimaryAccountSettingsController {
  private func refresh() {
    emailAddressLabel.text = viewModel.emailAddress
    mobileNumberLabel.text = viewModel.mobileNumber
  }

  private func showAlertDialog(
    title: String,
    positiveButtonText: String,
    positiveHandler: SingleResult<UIAlertAction>?,
    negativeButtonText: String
  ) {
    let dialog = UIAlertController(
      title: title,
      message: nil,
      preferredStyle: .alert
    )

    dialog.addAction(UIAlertAction(
      title: negativeButtonText,
      style: .cancel,
      handler: nil
    ))

    dialog.addAction(UIAlertAction(
      title: positiveButtonText,
      style: .destructive,
      handler: positiveHandler
    ))

    present(dialog, animated: true)
  }
}

// MARK: - Event Handlers

private extension EmailPrimaryAccountSettingsController {
  // MARK: Logout

  func handleLogoutConfirmed() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let self = self else { return }

      self.progressPresenter.presentIndefiniteProgress(from: self)
      self.viewModel.logout(
        onSuccess: self.handleSuccess(),
        onError: self.handleError()
      )
    }
  }

  // MARK: Delete Account

  func handleDeleteAccountConfirmed() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.deleteAccountFlowCoordinator.start()
    }
  }
  
  func handleUserRefresh() -> SingleResult<Notification> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.refresh()
    }
  }
}

// MARK: - UITableViewDelegate

extension EmailPrimaryAccountSettingsController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)

    guard let row = Row(rawValue: indexPath.row) else { return }
    navigateToPage(at: row)
  }
}

// MARK: - Routing

private extension EmailPrimaryAccountSettingsController {
  func navigateToPage(at row: Row) {
    switch row {
    case .emailAddress:
      changeEmailFlowCoordinator.start()
    case .mobileNumber:
      changePhoneFlowCoordinator.start()
    case .password:
      changePasswordFlowCoordinator.start()
    case .deleteAccount:
      deleteCellTapped()
    case .logout:
      logoutCellTapped()
    }
  }
}

// MARK: - Types

private enum Row: Int {
  case emailAddress = 0
  case mobileNumber
  case password
  case deleteAccount
  case logout
}
