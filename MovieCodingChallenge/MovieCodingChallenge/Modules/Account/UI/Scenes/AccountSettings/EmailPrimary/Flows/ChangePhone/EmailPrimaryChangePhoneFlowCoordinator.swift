//
//  EmailPrimaryChangePhoneFlowCoordinator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class EmailPrimaryChangePhoneFlowCoordinator {
  lazy var viewModel: EmailPrimaryChangePhoneFlowViewModelProtocol! = { EmailPrimaryChangePhoneFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: UIViewController

  private var verificationToken: String!

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension EmailPrimaryChangePhoneFlowCoordinator {
  func start() {
    navigateToVerifyPasswordPage()
  }
}

// MARK: - VerifyPassword

private extension EmailPrimaryChangePhoneFlowCoordinator {
  func navigateToVerifyPasswordPage() {
    let vm = viewModel.verifyPasswordVM
    vm.onComplete = handleVerifyPassword()

    let vc = R.storyboard.accountSettings.verifyPasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyPassword() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let self = self else { return }
      self.viewModel.save(verificationToken: token)
      self.navigateToChangePhonePage()
    }
  }
}

// MARK: - ChangePhone

private extension EmailPrimaryChangePhoneFlowCoordinator {
  func navigateToChangePhonePage() {
    let vm = viewModel.changePhoneVM
    vm.onComplete = handleChangePhone()

    let vc = R.storyboard.accountSettings.changePhoneController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleChangePhone() -> SingleResult<String> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.presentSuccessMessage()
      self.dismiss()
    }
  }
}

// MARK: - Routing

private extension EmailPrimaryChangePhoneFlowCoordinator {
  func presentSuccessMessage() {
    guard let vc = baseController as? PresentersProviderProtocol else { return }
    vc.infoPresenter.presentSuccessInfo(message: S.verifyCredentialChangePhoneSuccessMessage())
  }

  func dismiss() {
    navigationController.popToViewController(
      baseController,
      animated: true
    )
  }
}
