//
//  EmailPrimaryChangeEmailFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailPrimaryChangeEmailFlowViewModel: EmailPrimaryChangeEmailFlowViewModelProtocol {
  private(set) var verificationToken: String!
  private(set) var email: String!
}

// MARK: - Methods

extension EmailPrimaryChangeEmailFlowViewModel {
  func save(verificationToken: String) {
    self.verificationToken = verificationToken
  }

  func save(email: String) {
    self.email = email
  }
}

// MARK: - Getters

extension EmailPrimaryChangeEmailFlowViewModel {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol {
    VerifyPasswordViewModel(
      messageText: S.verifyPasswordChangeEmailMessage()
    )
  }

  var changeEmailVM: ChangeEmailViewModelProtocol {
    ChangeEmailViewModel(
      token: verificationToken
    )
  }

  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol {
    VerifyChangeEmailViewModel(
      email: email,
      verificationToken: verificationToken
    )
  }
}
