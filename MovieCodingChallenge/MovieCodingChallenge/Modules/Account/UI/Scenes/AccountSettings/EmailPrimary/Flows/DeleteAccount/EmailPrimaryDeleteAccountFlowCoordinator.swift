//
//  EmailPrimaryDeleteAccountFlowCoordinator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class EmailPrimaryDeleteAccountFlowCoordinator {
  lazy var viewModel: EmailPrimaryDeleteAccountFlowViewModelProtocol! = { EmailPrimaryDeleteAccountFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: StaticTableViewController

  private var verificationToken: String!

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last as? StaticTableViewController else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension EmailPrimaryDeleteAccountFlowCoordinator {
  func start() {
    navigateToVerifyPasswordPage()
  }
}

// MARK: - VerifyPassword

private extension EmailPrimaryDeleteAccountFlowCoordinator {
  func navigateToVerifyPasswordPage() {
    let vm = viewModel.verifyPasswordVM
    vm.onComplete = handleVerifyPassword()

    let vc = R.storyboard.accountSettings.verifyPasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyPassword() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let self = self else { return }
      self.baseController.progressPresenter.presentIndefiniteProgress(
        from: self.baseController
      )
      self.viewModel.deleteAccount(
        with: token,
        onSuccess: self.baseController.handleSuccess(),
        onError: self.baseController.handleError()
      )
    }
  }
}
