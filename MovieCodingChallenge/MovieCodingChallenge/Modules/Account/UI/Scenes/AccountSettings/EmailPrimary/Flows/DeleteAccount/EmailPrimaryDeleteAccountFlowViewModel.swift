//
//  EmailPrimaryDeleteAccountFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailPrimaryDeleteAccountFlowViewModel: EmailPrimaryDeleteAccountFlowViewModelProtocol {
  private let service: AccountDeletionServiceProtocol

  init(service: AccountDeletionServiceProtocol = App.shared.accountDeletion) {
    self.service = service
  }
}

// MARK: - Methods

extension EmailPrimaryDeleteAccountFlowViewModel {
  func deleteAccount(
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.deleteAccount(
      token: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Getters

extension EmailPrimaryDeleteAccountFlowViewModel {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol {
    VerifyPasswordViewModel(
      messageText: S.verifyPasswordDeleteAccountMessage()
    )
  }
}
