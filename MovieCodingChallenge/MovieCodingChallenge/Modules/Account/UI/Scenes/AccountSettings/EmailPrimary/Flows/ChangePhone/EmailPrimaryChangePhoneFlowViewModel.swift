//
//  EmailPrimaryChangePhoneFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailPrimaryChangePhoneFlowViewModel: EmailPrimaryChangePhoneFlowViewModelProtocol {
  private(set) var verificationToken: String!
  private(set) var countryCode: String!
  private(set) var phoneNumber: String!
}

// MARK: - Methods

extension EmailPrimaryChangePhoneFlowViewModel {
  func save(verificationToken: String) {
    self.verificationToken = verificationToken
  }

  func save(
    countryCode: String,
    phoneNumber: String
  ) {
    self.countryCode = countryCode
    self.phoneNumber = phoneNumber
  }
}

// MARK: - Getters

extension EmailPrimaryChangePhoneFlowViewModel {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol {
    VerifyPasswordViewModel(
      messageText: S.verifyPasswordChangePhoneMessage()
    )
  }

  var changePhoneVM: ChangePhoneViewModelProtocol {
    ChangePhoneViewModel(
      token: verificationToken
    )
  }

  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol {
    VerifyChangePhoneViewModel(
      phoneNumber: phoneNumber,
      verificationToken: verificationToken
    )
  }
}
