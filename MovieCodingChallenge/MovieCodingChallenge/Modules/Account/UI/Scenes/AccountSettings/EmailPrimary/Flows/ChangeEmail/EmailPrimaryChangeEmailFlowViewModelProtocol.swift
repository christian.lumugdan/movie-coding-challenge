//
//  EmailPrimaryChangeEmailFlowViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol EmailPrimaryChangeEmailFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol { get }
  var changeEmailVM: ChangeEmailViewModelProtocol { get }
  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol { get }
  
  func save(verificationToken: String)
  func save(email: String)
}
