//
//  EmailPrimaryChangeEmailFlowCoordinator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class EmailPrimaryChangeEmailFlowCoordinator {
  lazy var viewModel: EmailPrimaryChangeEmailFlowViewModelProtocol! = { EmailPrimaryChangeEmailFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: UIViewController

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension EmailPrimaryChangeEmailFlowCoordinator {
  func start() {
    navigateToVerifyPasswordPage()
  }
}

// MARK: - VerifyPassword

private extension EmailPrimaryChangeEmailFlowCoordinator {
  func navigateToVerifyPasswordPage() {
    let vm = viewModel.verifyPasswordVM
    vm.onComplete = handleVerifyPassword()

    let vc = R.storyboard.accountSettings.verifyPasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyPassword() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let self = self else { return }
      self.viewModel.save(verificationToken: token)
      self.navigateToChangeEmailPage()
    }
  }
}

// MARK: - ChangeEmail

private extension EmailPrimaryChangeEmailFlowCoordinator {
  func navigateToChangeEmailPage() {
    let vm = viewModel.changeEmailVM
    vm.onComplete = handleChangeEmail()

    let vc = R.storyboard.accountSettings.changeEmailController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleChangeEmail() -> SingleResult<String> {
    return { [weak self] email in
      guard let self = self else { return }
      self.viewModel.save(email: email)
      self.navigateToVerifyChangeEmailPage()
    }
  }
}

// MARK: - VerifyChangeEmail

private extension EmailPrimaryChangeEmailFlowCoordinator {
  func navigateToVerifyChangeEmailPage() {
    var vm = viewModel.verifyChangeCredentialVM
    vm.onComplete = handleVerifyChangeEmail()

    let vc = R.storyboard.accountSettings.verifyChangeCredentialController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyChangeEmail() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.presentSuccessMessage()
      self.dismiss()
    }
  }
}

// MARK: - Dismiss

private extension EmailPrimaryChangeEmailFlowCoordinator {
  func presentSuccessMessage() {
    guard let vc = baseController as? PresentersProviderProtocol else { return }
    vc.infoPresenter.presentSuccessInfo(message: S.verifyCredentialChangeEmailSuccessMessage())
  }

  func dismiss() {
    navigationController.popToViewController(
      baseController,
      animated: true
    )
  }
}
