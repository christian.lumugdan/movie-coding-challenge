//
//  EmailPrimaryChangePasswordFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailPrimaryChangePasswordFlowViewModel: EmailPrimaryChangePasswordFlowViewModelProtocol {
  private(set) var password: String!
}

// MARK: - Methods

extension EmailPrimaryChangePasswordFlowViewModel {
  func save(password: String) {
    self.password = password
  }
}

// MARK: - Getters

extension EmailPrimaryChangePasswordFlowViewModel {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol {
    VerifyPasswordViewModel(
      messageText: S.verifyPasswordChangePasswordMessage()
    )
  }

  var changePasswordVM: ChangePasswordViewModelProtocol {
    ChangePasswordViewModel(
      password: password
    )
  }
}
