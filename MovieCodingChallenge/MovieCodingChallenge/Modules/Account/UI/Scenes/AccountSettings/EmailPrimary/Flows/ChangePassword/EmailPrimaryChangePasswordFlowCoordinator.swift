//
//  EmailPrimaryChangePasswordFlowCoordinator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class EmailPrimaryChangePasswordFlowCoordinator {
  lazy var viewModel: EmailPrimaryChangePasswordFlowViewModelProtocol! = { EmailPrimaryChangePasswordFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: UIViewController

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension EmailPrimaryChangePasswordFlowCoordinator {
  func start() {
    navigateToVerifyPasswordPage()
  }
}

// MARK: - VerifyPassword

private extension EmailPrimaryChangePasswordFlowCoordinator {
  func navigateToVerifyPasswordPage() {
    let vm = viewModel.verifyPasswordVM
    vm.onComplete = handleVerifyPassword()

    let vc = R.storyboard.accountSettings.verifyPasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleVerifyPassword() -> DoubleResult<String, String> {
    return { [weak self] _, password in
      guard let self = self else { return }
      self.viewModel.save(password: password)
      self.navigateToChangePasswordPage()
    }
  }
}

// MARK: - ChangePassword

private extension EmailPrimaryChangePasswordFlowCoordinator {
  func navigateToChangePasswordPage() {
    let vm = viewModel.changePasswordVM
    vm.onComplete = handleChangePassword()

    let vc = R.storyboard.accountSettings.changePasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func handleChangePassword() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.presentSuccessMessage()
      self.dismiss()
    }
  }
}

// MARK: - Dismiss

private extension EmailPrimaryChangePasswordFlowCoordinator {
  func presentSuccessMessage() {
    guard let vc = baseController as? PresentersProviderProtocol else { return }
    vc.infoPresenter.presentSuccessInfo(message: S.changePasswordSuccessMessage())
  }

  func dismiss() {
    navigationController.popToViewController(
      baseController,
      animated: true
    )
  }
}
