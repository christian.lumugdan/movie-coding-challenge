//
//  EditProfileViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol EditProfileViewModelProtocol {
  var initialPictureURL: URL? { get }
  var initialFullName: String? { get }
  var initialDateOfBirth: Date? { get }
  var initialDescription: String? { get }
  
  // swiftlint:disable:next function_parameter_count
  func save(
    picture: Data?,
    fullName: String,
    birthdate: Date?,
    description: String?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  )
}
