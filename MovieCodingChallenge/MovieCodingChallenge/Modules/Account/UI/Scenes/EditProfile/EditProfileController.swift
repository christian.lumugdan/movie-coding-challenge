//
//  EditProfileController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import MaterialComponents.MDCTextField
import RxSwift

class EditProfileController: ScrollViewController, ScrollableController, ImagePickerPresenter {
  var viewModel: EditProfileViewModelProtocol! = EditProfileViewModel()
  var imagePicker: UIImagePickerController! = .init()
  lazy var imagePickerPresenter: ImagePickerPresenter! = self
  lazy var dirtyFormChecker: DirtyFormCheckerProtocol! = {
    let allFormViews = formViews + [descriptionField.textView!]
    return DirtyFormChecker(views: allFormViews)
  }()

  @IBOutlet private(set) var pictureImageView: UIImageView!
  @IBOutlet private(set) var fullnameField: MDCTextField!
  @IBOutlet private(set) var dateOfBirthField: APDateField!
  @IBOutlet private(set) var descriptionCharLimitLabel: UILabel!
  @IBOutlet private(set) var descriptionField: APMultilineTextField!
  @IBOutlet private(set) var formViews: [UIView]!

  private(set) var fullnameFieldInputController: MDCTextInputControllerBase!
  private(set) var dateOfBirthFieldInputController: MDCTextInputControllerBase!
  private(set) var descriptionFieldInputController: MDCTextInputControllerBase!

  private var initialHash: Int?

  override func backButtonTapped(_ sender: AnyObject) {
    if !dirtyFormChecker.isFormDirty() {
      super.backButtonTapped(sender)
    }
    presentConfirmDiscardDialog()
  }
}

// MARK: - Lifecycle

extension EditProfileController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
}

// MARK: - Setup

private extension EditProfileController {
  func setup() {
    setupFullnameField()
    setupDateOfBirthField()
    setupDescriptionField()
    setupImagePicker()
    setupInitialValues()

    dirtyFormChecker.resetHash()
  }

  func setupFullnameField() {
    fullnameField.applyAttribute(.fullname)
    fullnameFieldInputController =
      MDCHelper.inputController(for: fullnameField)
  }

  func setupDateOfBirthField() {
    dateOfBirthFieldInputController =
      MDCHelper.inputController(for: dateOfBirthField)
    dateOfBirthField.datePicker.maximumDate = .now
  }

  func setupDescriptionField() {
    descriptionFieldInputController =
      MDCHelper.inputController(for: descriptionField)

    descriptionField.charLimitLabel = descriptionCharLimitLabel

    descriptionField.textColor = .black
    descriptionField.font = .systemFont(ofSize: 17, weight: .regular)
  }

  func setupImagePicker() {
    imagePicker.delegate = self
  }

  func setupInitialValues() {
    pictureImageView.setImageWithURL(
      viewModel.initialPictureURL,
      onSuccess: handlePictureLoadSuccess()
    )
    fullnameField.text = viewModel.initialFullName
    if let dob = viewModel.initialDateOfBirth {
      dateOfBirthField.setupFromDate(dob)
    }
    descriptionField.text = viewModel.initialDescription
  }
}

// MARK: - Handlers

private extension EditProfileController {
  func handlePictureLoadSuccess() -> SingleResult<UIImage> {
    return { [weak self] _ in
      guard let s = self else { return }
      s.dirtyFormChecker.resetHash()
    }
  }

  func handleDiscardButtonTap() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.navigationController?.popViewController(animated: true)
    }
  }
}

// MARK: - Actions

private extension EditProfileController {
  @IBAction
  func editPictureButtonTapped(_ sender: Any) {
    imagePickerPresenter.presentImagePickerOptions()
  }

  @IBAction
  func saveButtonTapped(_ sender: Any) {
    view.endEditing(true)

    progressPresenter.presentIndefiniteProgress(from: self)
    let birthdate = dateOfBirthField.text.isNilOrEmpty ? nil : dateOfBirthField.datePicker.date
    viewModel.save(
      picture: pictureImageView.image?.looselyCompressedJpegData,
      fullName: fullnameField.text!,
      birthdate: birthdate,
      description: descriptionField.text,
      onSuccess: handleSuccessWithMessage(
        then: handleSaveSuccess()
      ),
      onError: handleError()
    )
  }
}

// MARK: - Utils

private extension EditProfileController {
  func presentConfirmDiscardDialog() {
    let dialog = EditProfileConfirmDiscardDialog(
      onDiscard: handleDiscardButtonTap()
    )
    dialogPresenter.presentDialog(dialog, from: self)
  }
}

// MARK: - Handlers

extension EditProfileController {
  func handleSaveSuccess() -> SingleResult<String> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.dirtyFormChecker.resetHash()
    }
  }
}

// MARK: - UIImagePickerControllerDelegate & UINavigationControllerDelegate

extension EditProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(
    _ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
  ) {
    if let image = info[.editedImage] as? UIImage {
      pictureImageView.image = image
    }

    picker.dismiss(animated: true)
  }
}
