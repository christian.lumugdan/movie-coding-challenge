//
//  EditProfileViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EditProfileViewModel: EditProfileViewModelProtocol {
  private let session: SessionServiceProtocol
  private let service: ProfileServiceProtocol

  init(
    session: SessionServiceProtocol = App.shared.session,
    service: ProfileServiceProtocol = App.shared.profile
  ) {
    self.session = session
    self.service = service
  }
}

// MARK: - Methods

extension EditProfileViewModel {
  // swiftlint:disable:next function_parameter_count
  func save(
    picture: Data?,
    fullName: String,
    birthdate: Date?,
    description: String?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    service.setInfo(
      fullName: fullName,
      birthdate: birthdate,
      description: description,
      onSuccess: savePicture(
        picture,
        onSuccess: onSuccess,
        onError: onError
      ),
      onError: onError
    )
  }
}

// MARK: - Chain Handlers

private extension EditProfileViewModel {
  func savePicture(
    _ picture: Data?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }

      guard let data = picture else {
        return onSuccess(self.successMessage)
      }

      self.service.setPicture(
        with: data,
        onSuccess: { onSuccess(self.successMessage) },
        onError: onError
      )
    }
  }
}

// MARK: - Getters

extension EditProfileViewModel {
  var initialPictureURL: URL? { user.avatarThumbURL }
  var initialFullName: String? { user.fullName }
  var initialDateOfBirth: Date? { user.birthdateValue }
  var initialDescription: String? { user.description }
  
  private var user: User { session.user! }
  private var successMessage: String { S.editProfileSaveSuccess() }
}
