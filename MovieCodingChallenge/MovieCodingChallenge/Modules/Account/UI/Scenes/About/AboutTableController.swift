//
//  AboutTableController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class AboutTableController: StaticTableViewController {
  lazy var appDocumentPresenter: AppDocumentPresenterProtocol = AppDocumentPresenter()
}

// MARK: - Routing

private extension AboutTableController {
  func navigateToPage(at row: Row) {
    switch row {
    case .terms:
      appDocumentPresenter.presentTermsOfServicePage(
        fromController: self
      )
    case .privacy:
      appDocumentPresenter.presentPrivacyPolicyPage(
        fromController: self
      )
    }
  }
}

// MARK: - UITableViewDelegate

extension AboutTableController {
  override func tableView(
    _ tableView: UITableView,
    didSelectRowAt indexPath: IndexPath
  ) {
    tableView.deselectRow(at: indexPath, animated: true)

    guard let row = Row(rawValue: indexPath.row) else { return }
    navigateToPage(at: row)
  }
}

// MARK: - Types

private enum Row: Int {
  case terms
  case privacy
}
