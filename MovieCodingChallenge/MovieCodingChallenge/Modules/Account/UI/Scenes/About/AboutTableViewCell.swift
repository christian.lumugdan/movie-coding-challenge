//
//  AboutTableViewCell.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import UIKit

class AboutTableViewCell: UITableViewCell {
  
  static let identifier = "AboutTableViewCell"
  
  @IBOutlet var titleLabel: UILabel?

  override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
  }
}
