//
//  HomeViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

class HomeViewModel: HomeViewModelProtocol {
//  var onSomeCallbackEvent: VoidResult?
//
//  private(set) var someVariable1: Int = 0
//
//  private let model: SomeModel
//  private let service: SomeService
//
//  init(
//    model: SomeModel,
//    service: SomeService = App.shared.service
//  ) {
//    self.model = model
//    self.service = service
//  }
}

// MARK: - Methods

extension HomeViewModel {
//  func someFunction1(param1: Int) {
//
//  }
//
//  func someFunction2(
//    param1: Int,
//    param2: String,
//    onSuccess: @escaping VoidResult,
//    onError: @escaping ErrorResult
//  ) {
//
//  }
}

// MARK: - Getters

extension HomeViewModel {
//  var someVariable2: String { model.property }
}
