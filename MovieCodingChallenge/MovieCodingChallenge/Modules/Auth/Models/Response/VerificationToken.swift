//
//  VerificationToken.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

struct VerificationToken: APIModel, Codable {
  let token: String
  let expiresAt: Date
}
