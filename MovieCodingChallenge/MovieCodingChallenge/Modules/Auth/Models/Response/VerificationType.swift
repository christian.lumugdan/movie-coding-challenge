//
//  User.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

enum VerificationType: String, Codable {
  case email
  case phone = "phone_number"
}
