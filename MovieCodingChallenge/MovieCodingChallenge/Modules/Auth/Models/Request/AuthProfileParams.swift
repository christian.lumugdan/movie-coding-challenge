//
//  AuthProfileParams.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct AuthProfileParams: APIRequestParameters {
  var fullName: String?
  var phoneNumber: String?
  var email: String?
  var birthdate: String?
  var description: String?
}

extension AuthProfileParams: Codable {
  enum CodingKeys: String, CodingKey {
    case fullName = "full_name"
    case phoneNumber = "phone_number"
    case email
    case birthdate
    case description
  }
}
