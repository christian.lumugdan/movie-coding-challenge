//
//  UpdatePasswordRequestParams.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct UpdatePasswordRequestParams: APIRequestParameters {
  /// The user's username
  var username: String

  /// The token sent to user's email when he requested for password reset.
  var token: String

  /// The new password.
  var password: String
  var passwordConfirmation: String
}

extension UpdatePasswordRequestParams: Codable {
  enum CodingKeys: String, CodingKey {
    case username, token, password
    case passwordConfirmation = "password_confirmation"
  }
}

extension UpdatePasswordRequestParams: Equatable {}
