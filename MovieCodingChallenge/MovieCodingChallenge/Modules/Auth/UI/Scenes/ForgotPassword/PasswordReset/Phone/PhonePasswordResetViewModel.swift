//
//  PhonePasswordResetViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhonePasswordResetViewModel: PasswordResetViewModelProtocol {
  let initialUsername: String

  private let service: ForgotPasswordServiceProtocol

  init(
    fullPhoneNumber: String,
    service: ForgotPasswordServiceProtocol = App.shared.forgotPassword
  ) {
    initialUsername = fullPhoneNumber
    self.service = service
  }
}

// MARK: - Methods

extension PhonePasswordResetViewModel {
  func sendPasswordResetRequest(
    for username: String?,
    onSuccess: @escaping SingleResult<VerifyPasswordResetViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    let result = validate(username)
    switch result {
    case let .success(username):
      service.sendPasswordResetRequest(
        for: username,
        onSuccess: handleSuccess(
          with: username,
          thenExecute: onSuccess
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension PhonePasswordResetViewModel {
  var usernameAttribute: TextFieldAttributeType { .phoneNumber }
}

// MARK: - SingleFormInputViewModelProtocol

extension PhonePasswordResetViewModel {
  func validate(_ input: String?) -> Result<String, Error> {
    PhoneNumberInputValidator.validate(input).genericResult
  }
}
