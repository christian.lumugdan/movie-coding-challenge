//
//  PasswordResetViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PasswordResetViewModelProtocol: SingleFormInputViewModelProtocol {
  var initialUsername: String { get }
  var usernameAttribute: TextFieldAttributeType { get }

  func sendPasswordResetRequest(
    for username: String?,
    onSuccess: @escaping SingleResult<VerifyPasswordResetViewModelProtocol>,
    onError: @escaping ErrorResult
  )

  func handleSuccess(
    with username: String,
    thenExecute handler: @escaping SingleResult<VerifyPasswordResetViewModelProtocol>
  ) -> VoidResult
}

// MARK: - Default Handlers

extension PasswordResetViewModelProtocol {
  func handleSuccess(
    with username: String,
    thenExecute handler: @escaping SingleResult<VerifyPasswordResetViewModelProtocol>
  ) -> VoidResult {
    return {
      let vm = VerifyPasswordResetViewModel(username: username)
      handler(vm)
    }
  }
}
