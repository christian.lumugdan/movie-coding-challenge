//
//  PasswordResetController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift

class PasswordResetController: ScrollViewController {
  @IBOutlet private(set) var usernameField: MDCTextField!
  @IBOutlet private(set) var continueButton: MDCButton!

  var viewModel: PasswordResetViewModelProtocol!

  private(set) var usernameInputController: MDCTextInputControllerBase!

  override var preferredStatusBarStyle: UIStatusBarStyle {
    if #available(iOS 13.0, *) {
      return .darkContent
    } else {
      return .default
    }
  }

  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension PasswordResetController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension PasswordResetController {
  func setup() {
    setupUsernameField()
  }

  func setupUsernameField() {
    usernameField.applyAttribute(.email)
    usernameField.placeholder = S.emailFieldPlaceholder()
    usernameField.text = viewModel.initialUsername

    usernameInputController = MDCHelper.inputController(for: usernameField)
  }
}

// MARK: - Bind

private extension PasswordResetController {
  func bind() {
    usernameField.rx.text.orEmpty
      .map { !$0.isEmpty }
      .bind(to: continueButton.rx.isEnabled)
      .disposed(by: rx.disposeBag)
  }
}

// MARK: Actions

extension PasswordResetController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.sendPasswordResetRequest(
      for: usernameField.text,
      onSuccess: handleResetPasswordRequestSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

private extension PasswordResetController {
  func handleResetPasswordRequestSuccess() -> SingleResult<VerifyPasswordResetViewModelProtocol> {
    return { [weak self] codeVM in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.presentVerificationPage(with: codeVM)
    }
  }
}

// MARK: - Routing

private extension PasswordResetController {
  func presentVerificationPage(with viewModel: VerifyPasswordResetViewModelProtocol) {
    let vc = R.storyboard.authForgotPassword.verifyPasswordResetController()!
    vc.viewModel = viewModel
    navigationController?.pushViewController(vc, animated: true)
  }
}
