//
//  EmailPasswordResetViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright (c) 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailPasswordResetViewModel: PasswordResetViewModelProtocol {
  let initialUsername: String

  private let service: ForgotPasswordServiceProtocol

  init(
    email: String,
    service: ForgotPasswordServiceProtocol = App.shared.forgotPassword
  ) {
    initialUsername = email
    self.service = service
  }
}

// MARK: - Methods

extension EmailPasswordResetViewModel {
  func sendPasswordResetRequest(
    for username: String?,
    onSuccess: @escaping SingleResult<VerifyPasswordResetViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    let result = validate(username)
    switch result {
    case let .success(username):
      service.sendPasswordResetRequest(
        for: username,
        onSuccess: handleSuccess(
          with: username,
          thenExecute: onSuccess
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension EmailPasswordResetViewModel {
  var usernameAttribute: TextFieldAttributeType { .email }
}

// MARK: - SingleFormInputViewModelProtocol

extension EmailPasswordResetViewModel {
  func validate(_ input: String?) -> Result<String, Error> {
    EmailAddressInputValidator.validate(input).genericResult
  }
}
