//
//  VerifyPasswordResetViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class VerifyPasswordResetViewModel: VerifyPasswordResetViewModelProtocol {
  private let username: String
  private let service: ForgotPasswordServiceProtocol

  init(
    username: String,
    service: ForgotPasswordServiceProtocol = App.shared.forgotPassword
  ) {
    self.username = username
    self.service = service
  }
}

// MARK: - Methods

extension VerifyPasswordResetViewModel {
  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.sendPasswordResetRequest(
      for: username,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func verify(
    with token: String,
    onSuccess: @escaping SingleResult<NewPasswordViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    service.confirmPasswordReset(
      for: username,
      with: token,
      onSuccess: handleVerifySuccess(
        for: username,
        with: token,
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }
}

// MARK: - Handlers

private extension VerifyPasswordResetViewModel {
  func handleVerifySuccess(
    for username: String,
    with token: String,
    thenExecute handler: @escaping SingleResult<NewPasswordViewModelProtocol>
  ) -> VoidResult {
    return {
      let vm = NewPasswordViewModel(
        username: username,
        token: token
      )
      handler(vm)
    }
  }
}

// MARK: - Getters

extension VerifyPasswordResetViewModel {
  var titleText: String { S.accountResetPasswordViaUsernameLabelsTitle() }
  var messageText: String { S.accountResetPasswordViaUsernameLabelsDescription() }
  var subMessageText: String? { username }
}
