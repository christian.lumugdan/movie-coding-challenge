//
//  VerifyPasswordResetViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol VerifyPasswordResetViewModelProtocol: CodeVerificationViewModelProtocol {
  func verify(
    with token: String,
    onSuccess: @escaping SingleResult<NewPasswordViewModelProtocol>,
    onError: @escaping ErrorResult
  )
}
