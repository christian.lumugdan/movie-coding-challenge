//
//  VerifyPasswordResetController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class VerifyPasswordResetController: ScrollViewController {
  var viewModel: VerifyPasswordResetViewModelProtocol!

  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var subMessageLabel: UILabel!
  @IBOutlet private(set) var resendButtonView: DebouncedButtonView!

  // Make sure that each field has its own designated Tag value (from 1 to N, left to right.)
  @IBOutlet var textFields: [VerificationCodeField]!

  private(set) var inputControllers: [MDCTextInputControllerUnderline] = []

  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - View LifeCycle

extension VerifyPasswordResetController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    textFields.first!.becomeFirstResponder()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    view.endEditing(true)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension VerifyPasswordResetController {
  func setup() {
    setupLabels()
    setupFields()
    setupFieldInputControllers()
    setupResendButtonView()
  }

  func setupLabels() {
    titleLabel.text = viewModel.titleText
    messageLabel.text = viewModel.messageText
    subMessageLabel.text = viewModel.subMessageText
  }
}

// MARK: - Bind

private extension VerifyPasswordResetController {
  func bind() {
    bindFields()
    bindResendButtonView()
  }
}

// MARK: - Handlers

private extension VerifyPasswordResetController {
  func handleVerificationSuccess() -> SingleResult<NewPasswordViewModelProtocol> {
    return { [weak self] newPasswordVM in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.presentNewPasswordPage(with: newPasswordVM)
    }
  }
}

// MARK: - Routing

private extension VerifyPasswordResetController {
  func presentNewPasswordPage(with viewModel: NewPasswordViewModelProtocol) {
    let vc = R.storyboard.authForgotPassword.newPasswordController()!
    vc.viewModel = viewModel
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - CodeVerificationControllerProtocol

extension VerifyPasswordResetController: CodeVerificationControllerProtocol {
  var codeVerificationVM: CodeVerificationViewModelProtocol { viewModel }

  func setupFieldInputControllers() {
    inputControllers = generateInputControllers(for: textFields)
  }

  func handleCodeFillCompletion() -> SingleResult<String> {
    return { [weak self] code in
      guard let self = self else { return }
      self.progressPresenter.presentIndefiniteProgress(from: self)
      self.viewModel.verify(
        with: code,
        onSuccess: self.handleVerificationSuccess(),
        onError: self.handleVerificationError()
      )
    }
  }
}

// MARK: - VerificationCodeFieldDelegate

extension VerifyPasswordResetController: VerificationCodeFieldDelegate {}

// MARK: - UITextFieldDelegate

extension VerifyPasswordResetController: UITextFieldDelegate {
  func textField(
    _ textField: UITextField,
    shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    shouldChangeCharacters(
      for: textField,
      in: range,
      replacementString: string
    )
  }
}
