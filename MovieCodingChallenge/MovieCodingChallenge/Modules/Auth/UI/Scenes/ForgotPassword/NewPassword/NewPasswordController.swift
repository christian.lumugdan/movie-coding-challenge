//
//  NewPasswordController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class NewPasswordController: ScrollViewController {
  var viewModel: NewPasswordViewModelProtocol!
  var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }

  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var passwordField: APPasswordField!
  @IBOutlet private(set) var continueButton: MDCButton!

  private(set) var fieldInputController: MDCTextInputControllerBase!

  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension NewPasswordController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    passwordField.becomeFirstResponder()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension NewPasswordController {
  func setup() {
    setupTitleAndMessage()
    setupPasswordField()

    continueButton.applyStyle(.primary)
  }

  func setupTitleAndMessage() {
    titleLabel.text = viewModel.titleText
    messageLabel.text = viewModel.messageText
  }

  func setupPasswordField() {
    fieldInputController = MDCHelper.inputController(for: passwordField)
  }
}

// MARK: - Bind

private extension NewPasswordController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

private extension NewPasswordController {
  @IBAction
  func doneButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.resetPassword(
      with: passwordField.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

extension NewPasswordController {
  func handleSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.dismiss()
    }
  }
}

// MARK: - Routing

private extension NewPasswordController {
  func dismiss() {
    // TODO: Move this logic in AppDelegate+RootViewController

    // If flow is modally presented
    if isPresentedModally {
      return dismiss(animated: true)
    }

    // If flow is pushed

    // If LoginFormController is in the flow
    if let loginVC = navigationController?.viewControllers.first(where: { $0 is LoginFormController }) {
      navigationController?.popToViewController(loginVC, animated: true)
    }

    navigationController?.popToRootViewController(animated: true)
  }
}

// MARK: - SingleFormInputControllerProtocol

extension NewPasswordController: SingleFormInputControllerProtocol {
  var field: MDCTextField! { passwordField }
}
