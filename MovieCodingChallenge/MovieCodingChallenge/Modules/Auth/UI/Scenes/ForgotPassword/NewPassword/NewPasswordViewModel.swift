//
//  NewPasswordViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class NewPasswordViewModel: NewPasswordViewModelProtocol {
  private let username: String
  private let token: String
  private let service: ForgotPasswordServiceProtocol
  private let config: AppConfigProtocol

  init(
    username: String,
    token: String,
    service: ForgotPasswordServiceProtocol = App.shared.forgotPassword,
    config: AppConfigProtocol = App.shared.config
  ) {
    self.username = username
    self.token = token
    self.service = service
    self.config = config
  }
}

// MARK: - Methods

extension NewPasswordViewModel {
  func resetPassword(
    with newPassword: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(newPassword)
    switch result {
    case let .success(newPassword):
      let params = UpdatePasswordRequestParams(
        username: username,
        token: token,
        password: newPassword,
        passwordConfirmation: newPassword
      )

      service.resetPassword(
        with: params,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension NewPasswordViewModel {
  var titleText: String { S.accountNewPasswordViaUsernameLabelsTitle() }
  var messageText: String { S.accountNewPasswordViaUsernameLabelsDescription(config.minPasswordLength) }
}
