//
//  NewPasswordViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol NewPasswordViewModelProtocol: SingleFormInputViewModelProtocol {
  var titleText: String { get }
  var messageText: String { get }

  func resetPassword(
    with newPassword: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension NewPasswordViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return NewPasswordInputValidator.validate(input).genericResult
  }
}
