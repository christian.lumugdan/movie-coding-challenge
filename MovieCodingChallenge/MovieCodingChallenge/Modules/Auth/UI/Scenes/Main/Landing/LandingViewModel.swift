//
//  LandingViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class LandingViewModel: LandingViewModelProtocol {
  private let socialAuth: SocialAuthServiceProtocol
  private let usernameChecker: UsernameCheckerServiceProtocol

  init(
    socialAuth: SocialAuthServiceProtocol = App.shared.socialAuth,
    usernameChecker: UsernameCheckerServiceProtocol = App.shared.usernameChecker
  ) {
    self.socialAuth = socialAuth
    self.usernameChecker = usernameChecker
  }
}

// MARK: Methods

extension LandingViewModel {
  func checkUsernameAvailability(
    username: String,
    onEmailAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onEmailUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onPhoneNumberResult: @escaping SingleResult<OTPAuthViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    let result = validate(username)
    switch result {
    case let .success(username):
      if InputValidatorUtil.isValidEmail(username) {
        checkEmailAvailability(
          email: username,
          onEmailAvailable: onEmailAvailable,
          onEmailUnavailable: onEmailUnavailable,
          onError: onError
        )
      } else {
        checkPhoneNumberAvailability(
          phoneNumber: username,
          onPhoneNumberResult: onPhoneNumberResult,
          onError: onError
        )
      }
    case let .failure(error):
      onError(error)
    }
  }

  func authenticateWithApple(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    socialAuth.authenticateWithApple(
      using: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func authenticateWithGoogle(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    socialAuth.authenticateWithGoogle(
      using: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func authenticateWithFacebook(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    socialAuth.authenticateWithFacebook(
      using: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Helpers

private extension LandingViewModel {
  func checkPhoneNumberAvailability(
    phoneNumber: String,
    onPhoneNumberResult: @escaping SingleResult<OTPAuthViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    usernameChecker.checkUsernameAvailability(
      phoneNumber,
      onAvailable: handlePhoneNumberAvailable(
        with: phoneNumber,
        thenExecute: onPhoneNumberResult
      ),
      onUnavailable: handlePhoneNumberUnavailable(
        with: phoneNumber,
        thenExecute: onPhoneNumberResult
      ),
      onError: onError
    )
  }

  func checkEmailAvailability(
    email: String,
    onEmailAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onEmailUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    usernameChecker.checkUsernameAvailability(
      email,
      onAvailable: handleEmailAvailable(
        with: email,
        thenExecute: onEmailAvailable
      ),
      onUnavailable: handleEmailUnavailable(
        with: email,
        thenExecute: onEmailUnavailable
      ),
      onError: onError
    )
  }
}

// MARK: - Util Handlers

private extension LandingViewModel {
  func handleEmailAvailable(
    with email: String,
    thenExecute handler: @escaping SingleResult<CreatePasswordViewModelProtocol>
  ) -> VoidResult {
    return {
      let vm = EmailCreatePasswordViewModel(email: email)
      handler(vm)
    }
  }

  func handleEmailUnavailable(
    with email: String,
    thenExecute handler: @escaping SingleResult<LoginFormViewModelProtocol>
  ) -> VoidResult {
    return {
      let vm = EmailLoginFormViewModel(email: email)
      handler(vm)
    }
  }

  func handlePhoneNumberAvailable(
    with fullPhoneNumber: String,
    thenExecute handler: @escaping SingleResult<OTPAuthViewModelProtocol>
  ) -> VoidResult {
    return {
      let vm = OTPSignupViewModel(fullPhoneNumber: fullPhoneNumber)
      handler(vm)
    }
  }

  func handlePhoneNumberUnavailable(
    with fullPhoneNumber: String,
    thenExecute handler: @escaping SingleResult<OTPAuthViewModelProtocol>
  ) -> VoidResult {
    return {
      let vm = OTPLoginViewModel(fullPhoneNumber: fullPhoneNumber)
      handler(vm)
    }
  }
}
