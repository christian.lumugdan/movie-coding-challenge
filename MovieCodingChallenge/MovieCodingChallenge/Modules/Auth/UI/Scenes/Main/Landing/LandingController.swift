//
//  LandingController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import ActiveLabel
import AuthenticationServices
import GoogleSignIn
import Material
import MaterialComponents.MDCButton
import PureLayout
import RxSwift
import SVProgressHUD
import UIKit

class LandingController: ScrollViewController, KitchenSinkPresenter {
  var viewModel: LandingViewModelProtocol!
  var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }

  lazy var appDocumentPresenter: AppDocumentPresenterProtocol = AppDocumentPresenter()
  lazy var applePresenter: AppleSignInPresenterProtocol = AppleSignInPresenter()
  lazy var googlePresenter: GoogleSignInPresenterProtocol = GoogleSignInPresenter()
  lazy var fbPresenter: FacebookLoginPresenterProtocol = FacebookLoginPresenter()

  @IBOutlet private(set) var appNameLabel: UILabel!
  @IBOutlet private(set) var appDescriptionLabel: UILabel!

  @IBOutlet private(set) var field: MDCTextField!

  @IBOutlet private(set) var continueButton: MDCButton!
  @IBOutlet private(set) var signInWithSocialButton: MDCButton!
  @IBOutlet private(set) var appleSignInButton: LeftAlignedIconMDCButton!
  @IBOutlet private(set) var googleSignInButton: LeftAlignedIconMDCButton!

  @IBOutlet private(set) var facebookSignInButton: LeftAlignedIconMDCButton!
  @IBOutlet private(set) var tosAcceptanceLabel: ActiveLabel!
  @IBOutlet private(set) var versionInfoView: VersionInfoView!
  @IBOutlet private(set) var legalStatementStackView: UIStackView!

  @IBOutlet private(set) var contentHeight: NSLayoutConstraint!
  @IBOutlet private(set) var logoTopSpace: NSLayoutConstraint!
  @IBOutlet private(set) var logoHeight: NSLayoutConstraint!
  @IBOutlet private(set) var bgViewHeight: NSLayoutConstraint!

  private(set) var fieldInputController: MDCTextInputControllerBase!
  private(set) var onSuccess: SingleResult<String>?
  private(set) var onError: SingleResult<GoogleSignInPresenterError>?
  private(set) var doneIntro: Bool = false

  override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
}

// MARK: - View LifeCycle

extension LandingController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(true, animated: animated)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    progressPresenter.dismiss()
  }
}

// MARK: - View Setups & Bindings

private extension LandingController {
  func setup() {
    setupHandlers()
    setupViews()

    setupKitchensinkTrigger(
      forView: continueButton,
      action: #selector(kitchensinkTriggerViewLongPressed(sender:))
    )
  }

  func setupHandlers() {
    applePresenter.presentationAnchorProvider = { [weak self] in self?.view.window }
    applePresenter.onSuccess = handleAppleSignInSuccess()
    applePresenter.onError = handleError()
    applePresenter.onCancel = DefaultClosure.voidResult()

    googlePresenter.controllerProvider = { [unowned self] in self }
    googlePresenter.onSuccess = handleGoogleSignInSuccess()
    googlePresenter.onError = handleError()
    googlePresenter.onCancel = DefaultClosure.voidResult()
  }

  func setupViews() {
    appNameLabel.text = S.appName()
    scrollView.delegate = self
    if !UIScreen.hasTopNotch {
      logoTopSpace.constant = 30
      logoHeight.constant = 250
    }
    bgViewHeight.constant = Screen.height


    setupUsernameField()
    setupButtons()
    setupTermsOfServiceLabel()
    setupVersionInfo()
  }

  func setupButtons() {
    continueButton.applyStyle(.base)

    appleSignInButton.applyStyle(.black)
    facebookSignInButton.applyStyle(.primary)
    googleSignInButton.applyStyle(.baseWithShadow)
  }

  func setupUsernameField() {
    field.applyAttribute(.username)
    field.placeholder = S.usernameFieldPlaceholder()
    field.returnKeyType = .continue
    field.enablesReturnKeyAutomatically = true
    fieldInputController = MDCHelper.inputController(for: field)
  }
  
  func setupVersionInfo() {
    versionInfoView.versionLabel.textColor = R.color.textOnPrimaryRegular()
  }

  func setupContinueLayout() {
    doneIntro = true
    logoTopSpace.constant = 55
    logoHeight.constant = 145
    bgViewHeight.constant = 188
    legalStatementStackView.isHidden = true

    appNameLabel.alpha = 0
    appNameLabel.textColor = R.color.textOnLightRegular()!
    appDescriptionLabel.alpha = 0
    appDescriptionLabel.textColor = R.color.textOnLightRegular()!
    appDescriptionLabel.text = S.landingLabelsInputInstruction()

    UIView.animate(
      withDuration: 0.5,
      delay: 0,
      usingSpringWithDamping: 0.75,
      initialSpringVelocity: 4,
      options: .curveEaseInOut,
      animations: { [unowned self] in
        self.view.layoutIfNeeded()

        appDescriptionLabel.alpha = 1.0
        appNameLabel.alpha = 1.0

        field.alpha = 1.0
        signInWithSocialButton.alpha = 1.0

        if let continueButton = continueButton as? APCustomButton {
          continueButton.backgroundColour = R.color.primaryFull()!
          continueButton.foregroundColor = R.color.textOnPrimaryRegular()!
          continueButton.rippleColor = R.color.textOnPrimaryRegular()!
        }
        continueButton.setTitle(S.continue(), for: .normal)
      }
    )
  }

  func toggleSocialButtons(shouldShow: Bool) {
    let titleColor: UIColor = shouldShow ? R.color.textOnLightSecondary()! : R.color.primaryFull()!
    signInWithSocialButton.setTitleColor(titleColor, for: .normal)
    UIView.animate(withDuration: 0.3) { [unowned self] in
      self.appleSignInButton.alpha = shouldShow.cgFloatValue()
      self.facebookSignInButton.alpha = shouldShow.cgFloatValue()
      self.googleSignInButton.alpha = shouldShow.cgFloatValue()
      if !UIScreen.hasTopNotch {
        self.contentHeight.constant = shouldShow ? 140 : 0
      }
    }
  }
}

// MARK: - Actions

private extension LandingController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    doneIntro ? handleInputUsernameContinue() : setupContinueLayout()
  }

  @IBAction
  func socialSignInButtonTapped(_ sender: AnyObject) {
    toggleSocialButtons(shouldShow: appleSignInButton.alpha == 0)
  }

  @IBAction
  func appleSignInButtonTapped(_ sender: AnyObject) {
    if #available(iOS 13.0, *) {
      applePresenter.presentAppleSignIn()
    }
  }

  @IBAction
  func googleSignInButtonTapped(_ sender: AnyObject) {
    googlePresenter.presentGoogleSignIn()
  }

  @IBAction
  func facebookLoginButtonTapped(_ sender: AnyObject) {
    fbPresenter.presentFacebookLogin(
      onSuccess: handleFBLoginSuccess(),
      onError: handleError(),
      onCancel: DefaultClosure.voidResult()
    )
  }
}

// MARK: - UIScrollViewDelegate

extension LandingController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    scrollView.contentOffset.y = max(scrollView.contentOffset.y, 0)
  }
}

// MARK: - Handlers

private extension LandingController {
  func handleInputUsernameContinue() {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.checkUsernameAvailability(
      username: field.text ?? "",
      onEmailAvailable: handleEmailAvailable(),
      onEmailUnavailable: handleEmailUnavailable(),
      onPhoneNumberResult: handlePhoneNumberResult(),
      onError: handleError()
    )
  }

  func handleAppleSignInSuccess() -> SingleResult<String> {
    return { [weak self] token in
      guard let self = self else { return }
      self.viewModel.authenticateWithApple(
        using: token,
        onSuccess: self.handleSocialAuth(),
        onError: self.handleError()
      )
    }
  }

  func handleGoogleSignInSuccess() -> SingleResult<String> {
    return { [weak self] token in
      guard let self = self else { return }
      self.viewModel.authenticateWithGoogle(
        using: token,
        onSuccess: self.handleSocialAuth(),
        onError: self.handleError()
      )
    }
  }

  func handleFBLoginSuccess() -> SingleResult<String> {
    return { [weak self] token in
      guard let self = self else { return }
      self.viewModel.authenticateWithFacebook(
        using: token,
        onSuccess: self.handleSocialAuth(),
        onError: self.handleError()
      )
    }
  }

  func handleSocialAuth() -> VoidResult {
    return {
      // noop
    }
  }
}

// MARK: - Terms And Conditions

private extension LandingController {
  func setupTermsOfServiceLabel() {
    let tosType = ActiveType.custom(pattern: "\\s\(S.legalTermsOfService())\\b")
    let ppType = ActiveType.custom(pattern: "\\s\(S.legalPrivacyPolicy())\\b")

    tosAcceptanceLabel.enabledTypes.append(tosType)
    tosAcceptanceLabel.enabledTypes.append(ppType)

    let attrStr = tosAcceptanceLabel.attributedText

    tosAcceptanceLabel.customize { [weak self] label in
      guard let self = self else { return }

      label.attributedText = attrStr
      label.textColor = R.color.textOnPrimaryRegular()!

      label.configureLinkAttribute = { type, attributes, _ in
        var _attrs = attributes
        if [tosType, ppType].contains(type) {
          _attrs[.font] = T.font.semibold(ofSize: label.font.pointSize)
          _attrs[.foregroundColor] = R.color.textOnPrimaryRegular()!
        }
        return _attrs
      }

      label.handleCustomTap(for: tosType) { _ in
        self.appDocumentPresenter.presentTermsOfServicePage(
          fromController: self
        )
      }

      label.handleCustomTap(for: ppType) { [unowned self] _ in
        self.appDocumentPresenter.presentPrivacyPolicyPage(
          fromController: self
        )
      }
    }
  }
}

// MARK: - SingleFormInputControllerProtocol

extension LandingController: SingleFormInputControllerProtocol {}

// MARK: - UsernameCheckerControllerProtocol

extension LandingController: UsernameCheckerControllerProtocol {}

// MARK: - Kitchensink extra

private extension LandingController {
  @objc
  func kitchensinkTriggerViewLongPressed(sender: UILongPressGestureRecognizer) {
    debugLog("Triggreed")
    presentKitchenSinkController()
  }
}
