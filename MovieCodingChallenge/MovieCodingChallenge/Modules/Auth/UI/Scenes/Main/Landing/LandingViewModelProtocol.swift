//
//  LandingViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol LandingViewModelProtocol: SingleFormInputViewModelProtocol {
  func checkUsernameAvailability(
    username: String,
    onEmailAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onEmailUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onPhoneNumberResult: @escaping SingleResult<OTPAuthViewModelProtocol>,
    onError: @escaping ErrorResult
  )

  func authenticateWithApple(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func authenticateWithGoogle(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func authenticateWithFacebook(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

extension LandingViewModelProtocol {
  func validate(_ input: String) -> Result<String, Error> {
    UsernameInputValidator.validate(input).genericResult
  }
}
