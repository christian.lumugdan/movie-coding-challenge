//
//  UsernameCheckerControllerProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import SVProgressHUD

protocol UsernameCheckerControllerProtocol where Self: SingleFormInputControllerProtocol {
  func presentCreatePasswordPage(with viewModel: CreatePasswordViewModelProtocol)
  func presentLoginPage(with viewModel: LoginFormViewModelProtocol)
  func presentOTPAuthPage(with viewModel: OTPAuthViewModelProtocol)

  func handleEmailAvailable() -> SingleResult<CreatePasswordViewModelProtocol>
  func handleEmailUnavailable() -> SingleResult<LoginFormViewModelProtocol>
  func handlePhoneNumberResult() -> SingleResult<OTPAuthViewModelProtocol>
}

// MARK: - Handlers Defaults

extension UsernameCheckerControllerProtocol {
  func handleEmailAvailable() -> SingleResult<CreatePasswordViewModelProtocol> {
    return { [weak self] passwordVM in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.clearFormErrors()
      self.presentCreatePasswordPage(with: passwordVM)
    }
  }

  func handleEmailUnavailable() -> SingleResult<LoginFormViewModelProtocol> {
    return { [weak self] loginVM in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.clearFormErrors()
      self.presentLoginPage(with: loginVM)
    }
  }

  func handlePhoneNumberResult() -> SingleResult<OTPAuthViewModelProtocol> {
    return { [weak self] otpAuthVM in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.clearFormErrors()
      self.presentOTPAuthPage(with: otpAuthVM)
    }
  }

}

// MARK: - Utils

extension UsernameCheckerControllerProtocol {
  func clearFormErrors() {
    fieldInputController.setErrorText(nil, errorAccessibilityValue: nil)
  }
}

// MARK: - Routing Defaults

extension UsernameCheckerControllerProtocol {
  func presentCreatePasswordPage(with viewModel: CreatePasswordViewModelProtocol) {
    let vc = R.storyboard.authSignup.createPasswordController()!
    vc.viewModel = viewModel
    navigationController?.pushViewController(vc, animated: true)
  }

  func presentLoginPage(with viewModel: LoginFormViewModelProtocol) {
    let vc = R.storyboard.authLogin.loginFormController()!
    vc.viewModel = viewModel
    navigationController?.pushViewController(vc, animated: true)
  }

  func presentOTPAuthPage(with viewModel: OTPAuthViewModelProtocol) {
    let vc = R.storyboard.otpAuth.otpAuthController()!
    vc.viewModel = viewModel
    navigationController?.pushViewController(vc, animated: true)
  }
}
