//
//  EmailCheckerViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol EmailCheckerViewModelProtocol: SingleFormInputViewModelProtocol {
  var initialEmail: String { get }
  
  func checkEmailAvailability(
    email: String?,
    onAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension EmailCheckerViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return EmailAddressInputValidator.validate(input).genericResult
  }
}
