//
//  OTPAddEmailViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class OTPAddEmailViewModel: OTPAddEmailViewModelProtocol {
  private let session: SessionServiceProtocol
  private let service: ProfileServiceProtocol

  init(
    session: SessionServiceProtocol = App.shared.session,
    service: ProfileServiceProtocol = App.shared.profile
  ) {
    self.session = session
    self.service = service
  }
}

// MARK: - Methods

extension OTPAddEmailViewModel {
  func skip() {
    session.recordAddOTPEmailComplete()
  }

  func submit(
    email: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(email)
    switch result {
    case let .success(email):
      service.addOnboardingEmail(
        email,
        onSuccess: { [weak self] in
          guard let self = self else { return }
          onSuccess()
          self.session.recordAddOTPEmailComplete()
        },
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}
