//
//  OTPAddEmailViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol OTPAddEmailViewModelProtocol: SingleFormInputViewModelProtocol {
  func skip()
  
  func submit(
    email: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

extension OTPAddEmailViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return EmailAddressInputValidator.validate(input).genericResult
  }
}
