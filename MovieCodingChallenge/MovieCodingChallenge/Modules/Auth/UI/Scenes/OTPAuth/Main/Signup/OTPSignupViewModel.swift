//
//  OTPSignupViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class OTPSignupViewModel: OTPAuthViewModelProtocol {
  private let fullPhoneNumber: String
  private let usernameChecker: UsernameCheckerServiceProtocol
  private let service: RegisterServiceProtocol

  init(
    fullPhoneNumber: String,
    usernameChecker: UsernameCheckerServiceProtocol = App.shared.usernameChecker,
    service: RegisterServiceProtocol = App.shared.register
  ) {
    self.fullPhoneNumber = fullPhoneNumber
    self.usernameChecker = usernameChecker
    self.service = service
  }
}

// MARK: - Methods

extension OTPSignupViewModel {
  func processOTP(
    _ otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.registerWithPhoneNumber(
      fullPhoneNumber,
      otp: otp,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    usernameChecker.checkUsernameAvailability(
      fullPhoneNumber,
      onAvailable: onSuccess,
      onUnavailable: DefaultClosure.voidResult(),
      onError: onError
    )
  }
}

// MARK: - Getters

extension OTPSignupViewModel {
  var titleText: String { S.otpAuthSignupTitle() }
  var messageText: String { S.otpAuthSignupMessage() }
  var subMessageText: String? { fullPhoneNumber.formattedPhoneNumber }
}
