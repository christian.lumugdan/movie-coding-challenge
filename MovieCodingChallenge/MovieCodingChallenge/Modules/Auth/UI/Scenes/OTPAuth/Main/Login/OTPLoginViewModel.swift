//
//  OTPLoginViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class OTPLoginViewModel: OTPAuthViewModelProtocol {
  private let fullPhoneNumber: String
  private let usernameChecker: UsernameCheckerServiceProtocol
  private let service: LoginServiceProtocol

  init(
    fullPhoneNumber: String,
    usernameChecker: UsernameCheckerServiceProtocol = App.shared.usernameChecker,
    service: LoginServiceProtocol = App.shared.login
  ) {
    self.fullPhoneNumber = fullPhoneNumber
    self.usernameChecker = usernameChecker
    self.service = service
  }
}

// MARK: - Methods

extension OTPLoginViewModel {
  func processOTP(
    _ otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.loginWithPhoneNumber(
      fullPhoneNumber,
      otp: otp,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    usernameChecker.checkUsernameAvailability(
      fullPhoneNumber,
      onAvailable: DefaultClosure.voidResult(),
      onUnavailable: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Getters

extension OTPLoginViewModel {
  var titleText: String { S.otpAuthLoginTitle() }
  var messageText: String { S.otpAuthLoginMessage() }
  var subMessageText: String? { fullPhoneNumber.formattedPhoneNumber }
}
