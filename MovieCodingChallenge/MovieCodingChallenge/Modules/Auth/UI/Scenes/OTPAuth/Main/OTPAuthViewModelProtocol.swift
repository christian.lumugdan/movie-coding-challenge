//
//  OTPAuthViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol OTPAuthViewModelProtocol: CodeVerificationViewModelProtocol {
  func processOTP(
    _ otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
