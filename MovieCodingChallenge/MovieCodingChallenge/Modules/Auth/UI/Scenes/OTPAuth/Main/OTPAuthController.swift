//
//  OTPAuthController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields

class OTPAuthController: ScrollViewController {
  var viewModel: OTPAuthViewModelProtocol!

  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var subMessageLabel: UILabel!
  @IBOutlet private(set) var resendButtonView: DebouncedButtonView!

  @IBOutlet private(set) var textFields: [VerificationCodeField]!

  private(set) var inputControllers: [MDCTextInputControllerUnderline] = []

  // MARK: Overrides

  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - View LifeCycle

extension OTPAuthController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    textFields.first?.becomeFirstResponder()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    view.endEditing(true)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension OTPAuthController {
  func setup() {
    setupLabels()
    setupFields()
    setupFieldInputControllers()
    setupResendButtonView()
  }

  func setupLabels() {
    titleLabel.text = viewModel.titleText
    messageLabel.text = viewModel.messageText
    subMessageLabel.text = viewModel.subMessageText
  }
}

// MARK: - Bind

private extension OTPAuthController {
  func bind() {
    bindFields()
    bindResendButtonView()
  }
}

// MARK: - Handlers

private extension OTPAuthController {
}

// MARK: - CodeVerificationControllerProtocol

extension OTPAuthController: CodeVerificationControllerProtocol {
  var codeVerificationVM: CodeVerificationViewModelProtocol { viewModel }

  func setupFieldInputControllers() {
    inputControllers = generateInputControllers(for: textFields)
  }

  func handleCodeFillCompletion() -> SingleResult<String> {
    return { [weak self] code in
      guard let self = self else { return }
      self.progressPresenter.presentIndefiniteProgress(from: self)
      self.viewModel.processOTP(
        code,
        onSuccess: self.handleSuccess(),
        onError: self.handleVerificationError()
      )
    }
  }
}

// MARK: - VerificationCodeFieldDelegate

extension OTPAuthController: VerificationCodeFieldDelegate {}

// MARK: - UITextFieldDelegate

extension OTPAuthController: UITextFieldDelegate {
  func textField(
    _ textField: UITextField,
    shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    shouldChangeCharacters(
      for: textField,
      in: range,
      replacementString: string
    )
  }
}
