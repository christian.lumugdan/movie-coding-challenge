//
//  LoginFormController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright (c) 2020 Appetiser Pty Ltd. All rights reserved.
//

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD
import UIKit

class LoginFormController: ScrollViewController {
  var viewModel: LoginFormViewModelProtocol!
  var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }

  @IBOutlet private(set) var passwordField: APPasswordField!
  @IBOutlet private(set) var continueButton: MDCButton!

  @IBOutlet private(set) var passwordResetButton: UIButton!
  @IBOutlet private(set) var usernameLabel: UILabel!

  private(set) var fieldInputController: MDCTextInputControllerBase!
  
  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - View LifeCycle

extension LoginFormController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    if viewWillAppearCallCount == 1 {
      passwordField.becomeFirstResponder()
    }
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension LoginFormController {
  func setup() {
    usernameLabel.text = viewModel.usernameText
    setupPasswordField()
  }

  func setupPasswordField() {
    passwordField.returnKeyType = .continue

    fieldInputController = MDCHelper.inputController(for: passwordField)
  }
}

// MARK: - Bind

private extension LoginFormController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

private extension LoginFormController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.login(
      with: passwordField.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }

  @IBAction
  func passwordResetButtonTapped(_ sender: AnyObject) {
    let vc = R.storyboard.authForgotPassword.passwordResetController()!
    vc.viewModel = viewModel.passwordResetVM
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - SingleFormInputControllerProtocol

extension LoginFormController: SingleFormInputControllerProtocol {
  var field: MDCTextField! { passwordField }
}
