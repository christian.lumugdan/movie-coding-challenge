//
//  PhoneLoginFormViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhoneLoginFormViewModel: LoginFormViewModelProtocol {
  private let countryCode: String
  private let phoneNumber: String
  private let service: LoginServiceProtocol
  
  init(
    countryCode: String,
    phoneNumber: String,
    service: LoginServiceProtocol = App.shared.login
  ) {
    self.countryCode = countryCode
    self.phoneNumber = phoneNumber
    self.service = service
  }
}

// MARK: - Methods

extension PhoneLoginFormViewModel {
  func login(
    with password: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(password)
    switch result {
    case let .success(password):
      break
    // TODO: Class to be removed
//      service.loginWithPhoneNumber(
//        fullPhoneNumber,
//        password: password,
//        onSuccess: onSuccess,
//        onError: onError
//      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension PhoneLoginFormViewModel {
  var usernameText: String { fullPhoneNumber }
  
  var passwordResetVM: PasswordResetViewModelProtocol {
    PhonePasswordResetViewModel(fullPhoneNumber: fullPhoneNumber)
  }
  
  private var fullPhoneNumber: String { countryCode + phoneNumber }
}
