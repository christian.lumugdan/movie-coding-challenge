//
//  EmailLoginFormViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailLoginFormViewModel: LoginFormViewModelProtocol {
  private let email: String
  private let service: LoginServiceProtocol

  init(
    email: String,
    service: LoginServiceProtocol = App.shared.login
  ) {
    self.email = email
    self.service = service
  }
}

// MARK: - Methods

extension EmailLoginFormViewModel {
  func login(
    with password: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(password)
    switch result {
    case let .success(password):
      service.loginWithEmail(
        email,
        password: password,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension EmailLoginFormViewModel {
  var usernameText: String { email }
  
  var passwordResetVM: PasswordResetViewModelProtocol {
    EmailPasswordResetViewModel(email: email)
  }
}
