//
//  LoginFormViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright (c) 2020 Appetiser Pty Ltd. All rights reserved.
//

import NSObject_Rx
import RxSwift

protocol LoginFormViewModelProtocol: SingleFormInputViewModelProtocol {
  var usernameText: String { get }
  var passwordResetVM: PasswordResetViewModelProtocol { get }

  func login(
    with password: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension LoginFormViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return PasswordInputValidator.validate(input).genericResult
  }
}
