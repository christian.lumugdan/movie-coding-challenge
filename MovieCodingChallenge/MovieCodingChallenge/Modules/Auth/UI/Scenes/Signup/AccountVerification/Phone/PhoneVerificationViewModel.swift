//
//  PhoneVerificationViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhoneVerificationViewModel: AccountVerificationViewModelProtocol {
  private(set) var shouldLogOutOnBackTap: Bool

  private let service: AccountVerificationServiceProtocol
  private let user: User

  init(
    shouldLogOutOnBackTap: Bool = false,
    service: AccountVerificationServiceProtocol = App.shared.accountVerification,
    user: User = App.shared.session.user!
  ) {
    self.shouldLogOutOnBackTap = shouldLogOutOnBackTap
    self.service = service
    self.user = user
  }
}

// MARK: - Methods

extension PhoneVerificationViewModel {
  func verify(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.verify(
      for: .phone,
      using: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.resendCode(
      for: .phone,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func skip() {
    // noop
  }
}

// MARK: - Getters

extension PhoneVerificationViewModel {
  var titleText: String { S.accountVerifViaPhoneLabelsTitle() }
  var messageText: String { S.accountVerifViaPhoneLabelsDescription() }
  var subMessageText: String? { user.phoneNumber?.formattedPhoneNumber }

  var isSkippable: Bool { false }
}
