//
//  AccountVerificationController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class AccountVerificationController: ScrollViewController, LogoutTriggerControllerProtocol {
  var viewModel: AccountVerificationViewModelProtocol!
  var logoutTriggerVM: LogoutTriggerViewModelProtocol! = LogoutTriggerViewModel()

  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var subMessageLabel: UILabel!
  @IBOutlet private(set) var resendButtonView: DebouncedButtonView!

  // Make sure that each field has its own designated Tag value (from 1 to N, left to right.)
  @IBOutlet var textFields: [VerificationCodeField]!

  var inputControllers: [MDCTextInputControllerUnderline] = []

  // MARK: Overrides

  override var shouldSetNavBarTransparent: Bool { true }

  override func backButtonTapped(_ sender: AnyObject) {
    super.backButtonTapped(sender)
    logoutTriggerVM.logout()
  }
}

// MARK: - View LifeCycle

extension AccountVerificationController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    textFields.first!.becomeFirstResponder()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    view.endEditing(true)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension AccountVerificationController {
  func setup() {
    setupNavBar()
    setupLabels()
    setupFields()
    setupFieldInputControllers()
    setupResendButtonView()
  }

  func setupNavBar() {
    if viewModel.isSkippable {
      addSkipButton()
    }
  }

  func addSkipButton() {
    let skipButton = UIBarButtonItem(
      title: S.skip(),
      style: .done,
      target: self,
      action: #selector(skipButtonTapped)
    )
    navigationItem.rightBarButtonItem = skipButton
  }

  func setupLabels() {
    titleLabel.text = viewModel.titleText
    messageLabel.text = viewModel.messageText
    subMessageLabel.text = viewModel.subMessageText
  }
}

// MARK: - Bind

private extension AccountVerificationController {
  func bind() {
    bindFields()
    bindResendButtonView()
  }
}

// MARK: - Actions

private extension AccountVerificationController {
  @objc
  func skipButtonTapped() {
    viewModel.skip()
  }
}

// MARK: - Handlers

private extension AccountVerificationController {
  func handleVerificationError() -> ErrorResult {
    return { [weak self] error in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.infoPresenter.presentErrorInfo(error: error)
      self.clearFields(makeFirstFieldFirstResponder: true)
    }
  }

  func handleBackTapSuccess(_ sender: AnyObject) -> VoidResult {
    return { [sender] in
      super.backButtonTapped(sender)
    }
  }
}

// MARK: - CodeVerificationControllerProtocol

extension AccountVerificationController: CodeVerificationControllerProtocol {
  var codeVerificationVM: CodeVerificationViewModelProtocol { viewModel }

  func setupFieldInputControllers() {
    inputControllers = generateInputControllers(for: textFields)
  }

  func handleCodeFillCompletion() -> SingleResult<String> {
    return { [weak self] code in
      guard let self = self else { return }
      self.progressPresenter.presentIndefiniteProgress(from: self)
      self.viewModel.verify(
        using: code,
        onSuccess: self.handleSuccess(),
        onError: self.handleVerificationError()
      )
    }
  }
}

// MARK: - VerificationCodeFieldDelegate

extension AccountVerificationController: VerificationCodeFieldDelegate {}

// MARK: - UITextFieldDelegate

extension AccountVerificationController: UITextFieldDelegate {
  func textField(
    _ textField: UITextField,
    shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    shouldChangeCharacters(
      for: textField,
      in: range,
      replacementString: string
    )
  }
}
