//
//  EmailVerificationViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailVerificationViewModel: AccountVerificationViewModelProtocol {
  private(set) var shouldLogOutOnBackTap: Bool

  private let session: SessionServiceProtocol
  private let service: AccountVerificationServiceProtocol
  private let config: AppConfigProtocol
  private let user: User

  init(
    shouldLogOutOnBackTap: Bool = false,
    session: SessionServiceProtocol = App.shared.session,
    service: AccountVerificationServiceProtocol = App.shared.accountVerification,
    config: AppConfigProtocol = App.shared.config,
    user: User = App.shared.session.user!
  ) {
    self.shouldLogOutOnBackTap = shouldLogOutOnBackTap
    self.session = session
    self.service = service
    self.config = config
    self.user = user
  }
}

// MARK: - Methods

extension EmailVerificationViewModel {
  func verify(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.verify(
      for: .email,
      using: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.resendCode(
      for: .email,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func skip() {
    session.recordEmailVerificationSkip()
  }
}

// MARK: - Getters

extension EmailVerificationViewModel {
  var titleText: String { S.accountVerifViaEmailLabelsTitle() }
  var messageText: String { S.accountVerifViaEmailLabelsDescription() }
  var subMessageText: String? { user.email }

  var isSkippable: Bool { config.emailVerificationSkippable }
}
