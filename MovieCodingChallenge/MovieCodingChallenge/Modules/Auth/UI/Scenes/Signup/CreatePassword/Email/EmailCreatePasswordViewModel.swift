//
//  EmailCreatePasswordViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailCreatePasswordViewModel: CreatePasswordViewModelProtocol {
  private let email: String
  private let service: RegisterServiceProtocol

  init(
    email: String,
    service: RegisterServiceProtocol = App.shared.register
  ) {
    self.email = email
    self.service = service
  }
}

// MARK: - Methods

extension EmailCreatePasswordViewModel {
  func register(
    with password: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(password)
    switch result {
    case let .success(password):
      service.registerWithEmail(
        email,
        password: password,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension EmailCreatePasswordViewModel {
  var usernameText: String { email }
}
