//
//  CreatePasswordViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol CreatePasswordViewModelProtocol: SingleFormInputViewModelProtocol {
  var usernameText: String { get }

  func register(
    with password: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - Defaults

extension CreatePasswordViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return NewPasswordInputValidator.validate(input).genericResult
  }
}
