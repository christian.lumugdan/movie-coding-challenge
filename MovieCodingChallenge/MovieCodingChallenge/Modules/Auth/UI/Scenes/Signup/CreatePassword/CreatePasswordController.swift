//
//  CreatePasswordController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class CreatePasswordController: ViewController {
  var viewModel: CreatePasswordViewModelProtocol!
  var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }

  @IBOutlet private(set) var scrollContentView: UIView!
  @IBOutlet private(set) var accountLabel: UILabel!

  @IBOutlet private(set) var passwordField: APPasswordField!
  @IBOutlet private(set) var continueButton: MDCButton!

  private(set) var fieldInputController: MDCTextInputControllerBase!
  
  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension CreatePasswordController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension CreatePasswordController {
  func setup() {
    setupAccountLabel()
    setupPasswordField()

    continueButton.applyStyle(.primary)
  }

  func setupAccountLabel() {
    accountLabel.text = viewModel.usernameText
  }

  func setupPasswordField() {
    passwordField.returnKeyType = .continue
    passwordField.delegate = self

    fieldInputController = MDCHelper.inputController(for: passwordField)
    passwordField.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension CreatePasswordController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

extension CreatePasswordController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.register(
      with: passwordField.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - UITextFieldDelegate

extension CreatePasswordController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    guard !textField.text.isNilOrEmpty else { return false }

    if textField == passwordField {
      continueButtonTapped(self)
    }

    return true
  }
}

// MARK: - SingleFormInputControllerProtocol

extension CreatePasswordController: SingleFormInputControllerProtocol {
  var field: MDCTextField! { passwordField }
}
