//
//  OnboardingPictureViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class OnboardingPictureViewModel: OnboardingPictureViewModelProtocol {
  private let service: ProfileServiceProtocol

  init(service: ProfileServiceProtocol = App.shared.profile) {
    self.service = service
  }
}

// MARK: - Methods

extension OnboardingPictureViewModel {
  func submitPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.setPicture(
      with: data,
      onSuccess: handleSubmissionSuccess(thenExecute: onSuccess),
      onError: onError
    )
  }

  func skip() {
    finish()
  }
}

// MARK: - Utils

private extension OnboardingPictureViewModel {
  func finish() {
    service.markOnboardingAsComplete(
      onSuccess: DefaultClosure.voidResult(),
      onError: DefaultClosure.singleResult()
    )
  }
}

// MARK: - Handlers

private extension OnboardingPictureViewModel {
  func handleSubmissionSuccess(thenExecute handler: @escaping VoidResult) -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.finish()
      handler()
    }
  }
}
