//
//  OnboardingNameViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import NSObject_Rx
import RxSwift

class OnboardingNameViewModel: OnboardingNameViewModelProtocol {
  private let validate: OnboardingNameInputValidator.ValidateMethod
  private let service: ProfileServiceProtocol

  init(
    validate: @escaping OnboardingNameInputValidator.ValidateMethod = OnboardingNameInputValidator.validate,
    service: ProfileServiceProtocol = App.shared.profile
  ) {
    self.validate = validate
    self.service = service
  }
}

// MARK: - Methods

extension OnboardingNameViewModel {
  func submit(
    name: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(name)
    switch result {
    case let .success(name):
      service.setName(
        name,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension OnboardingNameViewModel {
  var nextPageVM: OnboardingPictureViewModelProtocol {
    OnboardingPictureViewModel()
  }
}
