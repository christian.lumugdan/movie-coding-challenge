//
//  OnboardingNameInputValidator.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct OnboardingNameInputValidator: InputValidator {
  typealias ValidateMethod = (Inputs) -> Result<ValidInputs, ValidationError>

  static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let name = inputs, !name.isEmpty else {
      return .failure(.requiredName)
    }

    let validInputs = ValidInputs(name)

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

extension OnboardingNameInputValidator {
  typealias Inputs = String?

  typealias ValidInputs = String

  enum ValidationError: LocalizedError {
    case requiredName

    var errorDescription: String? {
      switch self {
      case .requiredName:
        return S.nameFormErrorNameRequired()
      }
    }
  }
}
