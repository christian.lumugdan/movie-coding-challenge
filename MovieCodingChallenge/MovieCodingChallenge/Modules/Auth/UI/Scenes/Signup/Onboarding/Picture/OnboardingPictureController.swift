//
//  OnboardingPictureController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class OnboardingPictureController: ViewController, ImagePickerPresenter {
  var viewModel: OnboardingPictureViewModelProtocol!
  var inputCache: OnboardingInputCacheProtocol!
  var imagePickerPresenter: ImagePickerPresenter!

  lazy var shouldHideBackButton: Bool = false

  // MARK: ImagePickerPresenter

  var imagePicker: UIImagePickerController! = .init()

  @IBOutlet private(set) var firstStepsView: StepsView!
  @IBOutlet private(set) var secondStepsView: StepsView!
  @IBOutlet private(set) var pictureImageView: UIImageView!
  @IBOutlet private(set) var pictureButton: UIButton!
  @IBOutlet private(set) var primaryButton: MDCButton!

  private(set) weak var skipButton: UIBarButtonItem!

  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - View LifeCycle

extension OnboardingPictureController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    progressPresenter.dismiss()

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true

    inputCache.cachePicture(pictureImageView.image)
  }
}

// MARK: - Setup

private extension OnboardingPictureController {
  func setup() {
    setupNavBar()
    setupStepsViews()

    pictureImageView.image = inputCache.picture

    imagePicker.delegate = self

    primaryButton.layer.cornerRadius = 8
  }

  func setupNavBar() {
    setupBackButton()
    setupSkipButton()
  }

  func setupBackButton() {
    if shouldHideBackButton {
      navigationItem.leftBarButtonItem = nil
    }
  }

  func setupSkipButton() {
    let skipButton = UIBarButtonItem(
      title: S.skip(),
      style: .done,
      target: self,
      action: #selector(skipButtonTapped(_:))
    )
    skipButton.tintColor = R.color.textOnLightRegular()!.withAlphaComponent(0.3)
    navigationItem.rightBarButtonItem = skipButton
    self.skipButton = skipButton
  }

  func setupStepsViews() {
    firstStepsView.isHidden = false
    secondStepsView.isHidden = true
  }
}

// MARK: - Bind

private extension OnboardingPictureController {
  func bind() {
    pictureImageView.rx.observe(UIImage.self, "image")
      .map { $0 == nil }
      .bind(to: primaryButton.rx.isHidden)
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Actions

private extension OnboardingPictureController {
  @IBAction
  func pictureButtonTapped(_ sender: Any) {
    imagePickerPresenter.presentImagePickerOptions()
  }

  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    guard let picture = pictureImageView.image else {
      preconditionFailure("pictureImageView.image should not be nil at this point")
    }

    guard let data = picture.looselyCompressedJpegData else {
      preconditionFailure("pictureImageView.image.jpegData should not be nil at this point")
    }

    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.submitPicture(
      with: data,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }

  @objc
  func skipButtonTapped(_ sender: AnyObject) {
    viewModel.skip()
  }
}

// MARK: - Helpers

extension OnboardingPictureController {
  func switchToSecondStepsView() {
    firstStepsView.isHidden = true
    secondStepsView.isHidden = false
  }
}

// MARK: - UIImagePickerControllerDelegate & UINavigationControllerDelegate

extension OnboardingPictureController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(
    _ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
  ) {
    if let image = info[.editedImage] as? UIImage {
      pictureImageView.image = image
      switchToSecondStepsView()
    }

    picker.dismiss(animated: true)
  }
}
