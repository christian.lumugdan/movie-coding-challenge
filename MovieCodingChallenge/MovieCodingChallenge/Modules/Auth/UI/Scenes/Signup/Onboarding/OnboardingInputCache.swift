//
//  OnboardingInputCache.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class OnboardingInputCache: OnboardingInputCacheProtocol {
  private(set) var name: String?
  private(set) var picture: UIImage?
}

// MARK: - Methods

extension OnboardingInputCache {
  func cacheName(_ name: String?) {
    self.name = name
  }

  func cachePicture(_ picture: UIImage?) {
    self.picture = picture
  }
}
