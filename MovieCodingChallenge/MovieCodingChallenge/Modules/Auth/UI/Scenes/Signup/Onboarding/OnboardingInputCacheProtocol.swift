//
//  OnboardingInputCacheProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol OnboardingInputCacheProtocol {
  var name: String? { get }
  var picture: UIImage? { get }

  func cacheName(_ name: String?)
  func cachePicture(_ picture: UIImage?)
}
