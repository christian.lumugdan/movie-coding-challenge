//
//  OnboardingNameController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class OnboardingNameController: ViewController, LogoutTriggerControllerProtocol {
  var viewModel: OnboardingNameViewModelProtocol!
  var logoutTriggerVM: LogoutTriggerViewModelProtocol!
  var inputCache: OnboardingInputCacheProtocol!

  @IBOutlet private(set) var nameField: MDCTextField!
  @IBOutlet private(set) var primaryButton: MDCButton!

  private(set) var nameInputController: MDCTextInputControllerBase!

  // MARK: Overrides

  override var shouldSetNavBarTransparent: Bool { true }

  override func backButtonTapped(_ sender: AnyObject) {
    super.backButtonTapped(sender)
    logoutTriggerVM.logout()
  }
}

// MARK: - LifeCycle

extension OnboardingNameController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    progressPresenter.dismiss()

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true

    view.endEditing(true)

    inputCache.cacheName(nameField.text)
  }
}

// MARK: - Setup

private extension OnboardingNameController {
  func setup() {
    setupNavBar()
    setupNameField()

    primaryButton.layer.cornerRadius = 8
  }

  func setupNavBar() {
    navigationItem.leftBarButtonItem = nil
  }

  func setupNameField() {
    nameField.text = inputCache.name
    nameField.applyAttribute(.givenName)
    nameField.returnKeyType = .continue
    nameField.enablesReturnKeyAutomatically = true

    nameInputController = MDCHelper.inputController(for: nameField)

    nameField.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension OnboardingNameController {
  func bind() {
    nameField.rx.controlEvent(.editingDidEndOnExit)
      .withLatestFrom(nameField.rx.text)
      .filter { !$0.isNilOrEmpty }
      .bind(to: primaryButton.rx.sendActions(for: .touchUpInside))
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Actions

private extension OnboardingNameController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.submit(
      name: nameField.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

private extension OnboardingNameController {
  func handleSuccess() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.clearFormErrors()
      self.presentNextPage()
    }
  }

  func handleError() -> ErrorResult {
    return { [weak self] error in
      guard let self = self else { return }
      self.progressPresenter.dismiss()
      self.nameInputController.setErrorText(
        error.localizedDescription,
        errorAccessibilityValue: nil
      )
    }
  }
}

// MARK: - Utils

private extension OnboardingNameController {
  func clearFormErrors() {
    nameInputController.setErrorText(nil, errorAccessibilityValue: nil)
  }
}

// MARK: - Routing

private extension OnboardingNameController {
  func presentNextPage() {
    let vc = R.storyboard.authSignup.onboardingPictureController()!
    vc.viewModel = viewModel.nextPageVM
    vc.inputCache = inputCache
    vc.imagePickerPresenter = vc
    navigationController?.pushViewController(vc, animated: true)
  }
}
