//
//  ProfileService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ProfileService: ProfileServiceProtocol {
  var onUserResult: SingleResult<User>?
  var onAvatarResult: SingleResult<Photo>?

  private let onboardingAPI: AuthOnboardingAPI
  private let profileAPI: AuthProfileAPI

  init(
    onboardingAPI: AuthOnboardingAPI,
    profileAPI: AuthProfileAPI
  ) {
    self.onboardingAPI = onboardingAPI
    self.profileAPI = profileAPI
  }
}

// MARK: - Profile

extension ProfileService {
  func refreshProfile(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    profileAPI.getAuthProfile(
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func addOnboardingEmail(
    _ email: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    onboardingAPI.postOnboardingEmail(
      email: email,
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func markOnboardingAsComplete(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    onboardingAPI.postOnboardingComplete(
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func setName(
    _ name: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let params = AuthProfileParams(fullName: name)
    profileAPI.putAuthProfile(
      with: params,
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    profileAPI.postAuthProfileAvatar(
      with: data,
      onSuccess: handleAvatarResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func setInfo(
    fullName: String,
    birthdate: Date?,
    description: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    var birthdateText: String? = ""
    if let bd = birthdate {
      birthdateText = Constants.Formatters.birthdateFormatter.string(from: bd)
    }

    let params = AuthProfileParams(
      fullName: fullName,
      birthdate: birthdateText,
      description: description
    )
    profileAPI.putAuthProfile(
      with: params,
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }
}
