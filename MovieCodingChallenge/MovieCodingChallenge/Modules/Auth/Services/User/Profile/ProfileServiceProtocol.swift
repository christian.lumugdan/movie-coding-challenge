//
//  ProfileServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ProfileServiceProtocol: UserServiceProtocol {
  func refreshProfile(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func addOnboardingEmail(
    _ email: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func markOnboardingAsComplete(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func setName(
    _ name: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func setInfo(
    fullName: String,
    birthdate: Date?,
    description: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
