//
//  AccountVerificationService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class AccountVerificationService: AccountVerificationServiceProtocol {
  var onUserResult: SingleResult<User>?
  var onAvatarResult: SingleResult<Photo>?

  private let api: AuthVerificationAPI

  init(api: AuthVerificationAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension AccountVerificationService {
  func verify(
    for type: VerificationType,
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthVerificationVerify(
      token: token,
      type: type,
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func resendCode(
    for type: VerificationType,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthVerificationResend(
      type: type,
      onSuccess: { _ in onSuccess() },
      onError: onError
    )
  }
}
