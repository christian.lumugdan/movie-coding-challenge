//
//  CredentialsServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol CredentialsServiceProtocol {
  var onUserResult: SingleResult<User>? { get set }

  func requestVerificationTokenWithPassword(
    _ password: String,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  )
  
  func requestVerificationTokenWithOTP(
    _ otp: String,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  )

  func requestChangeEmail(
    with newEmail: String,
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func verifyChangeEmail(
    with token: String,
    verificationToken: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func requestChangePhoneNumber(
    with newPhoneNumber: String,
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func verifyChangePhoneNumber(
    with token: String,
    verificationToken: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func changePassword(
    to newPassword: String,
    oldPassword: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

extension CredentialsServiceProtocol {
  func handleVerifySuccess(thenExecute handler: @escaping VoidResult) -> SingleResult<User> {
    return { [onUserResult] user in
      onUserResult?(user)
      handler()
    }
  }
}
