//
//  UsernameCheckerService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class UsernameCheckerService: UsernameCheckerServiceProtocol {
  private let api: AuthCheckAPI

  init(api: AuthCheckAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension UsernameCheckerService {
  func checkUsernameAvailability(
    _ username: String,
    onAvailable: @escaping VoidResult,
    onUnavailable: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthCheckUsername(
      username,
      onSuccess: { isAvailable in
        if isAvailable {
          onAvailable()
        } else {
          onUnavailable()
        }
      },
      onError: onError
    )
  }
}
