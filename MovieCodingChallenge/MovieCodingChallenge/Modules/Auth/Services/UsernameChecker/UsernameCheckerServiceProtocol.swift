//
//  UsernameCheckerServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol UsernameCheckerServiceProtocol {
  func checkUsernameAvailability(
    _ username: String,
    onAvailable: @escaping VoidResult,
    onUnavailable: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
