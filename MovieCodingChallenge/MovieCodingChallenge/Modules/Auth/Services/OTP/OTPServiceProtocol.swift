//
//  OTPServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol OTPServiceProtocol {
  func generateOTP(
    for phoneNumber: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
