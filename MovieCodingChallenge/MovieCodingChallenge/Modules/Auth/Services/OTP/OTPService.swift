//
//  OTPService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class OTPService: OTPServiceProtocol {
  private let api: AuthOTPAPI

  init(
    api: AuthOTPAPI
  ) {
    self.api = api
  }
}

// MARK: - Methods

extension OTPService {
  func generateOTP(
    for phoneNumber: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postOTPGenerate(
      for: phoneNumber,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
