//
//  RegisterService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class RegisterService: RegisterServiceProtocol {
  var onAuth: SingleResult<UserAuthResponse>?

  private let api: AuthRegisterAPI

  init(api: AuthRegisterAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension RegisterService {
  func registerWithEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthRegisterEmail(
      email,
      password: password,
      onSuccess: handleAuthResponseResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func registerWithPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthRegisterPhoneNumber(
      phoneNumber,
      otp: otp,
      onSuccess: handleAuthResponseResult(thenExecute: onSuccess),
      onError: onError
    )
  }
}
