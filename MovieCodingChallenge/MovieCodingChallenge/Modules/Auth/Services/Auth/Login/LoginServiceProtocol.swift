//
//  SocialAuthServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol LoginServiceProtocol: AuthServiceProtocol {
  func loginWithEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func loginWithPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
