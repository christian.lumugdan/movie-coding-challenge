//
//  LoginService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class LoginService: LoginServiceProtocol {
  var onAuth: SingleResult<UserAuthResponse>?

  private let api: AuthLoginAPI

  init(api: AuthLoginAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension LoginService {
  func loginWithEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthLoginEmail(
      email,
      password: password,
      onSuccess: handleAuthResponseResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func loginWithPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthLoginPhoneNumber(
      phoneNumber,
      otp: otp,
      onSuccess: handleAuthResponseResult(thenExecute: onSuccess),
      onError: onError
    )
  }
}
