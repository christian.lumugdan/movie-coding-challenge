//
//  AuthServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AuthServiceProtocol {
  var onAuth: SingleResult<UserAuthResponse>? { get set }
}

extension AuthServiceProtocol {
  func handleAuthResponseResult(thenExecute handler: @escaping VoidResult) -> SingleResult<UserAuthResponse> {
    return { [onAuth] authResponse in
      onAuth?(authResponse)
      handler()
    }
  }
}
