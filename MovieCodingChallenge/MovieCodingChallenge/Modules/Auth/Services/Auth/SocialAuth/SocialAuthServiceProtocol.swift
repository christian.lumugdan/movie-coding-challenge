//
//  SocialAuthServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

enum SocialLoginType: String {
  case apple
  case google
  case facebook
}

protocol SocialAuthServiceProtocol: AuthServiceProtocol {
  func authenticateWithApple(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func authenticateWithGoogle(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func authenticateWithFacebook(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
