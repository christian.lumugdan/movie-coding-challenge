//
//  SocialAuthService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class SocialAuthService: SocialAuthServiceProtocol {
  var onAuth: SingleResult<UserAuthResponse>?

  private let api: AuthSocialAPI

  init(api: AuthSocialAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension SocialAuthService {
  func authenticateWithApple(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthSocial(
      with: token,
      type: .apple,
      onSuccess: handleAuthResponseResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func authenticateWithGoogle(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthSocial(
      with: token,
      type: .google,
      onSuccess: handleAuthResponseResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func authenticateWithFacebook(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthSocial(
      with: token,
      type: .facebook,
      onSuccess: handleAuthResponseResult(thenExecute: onSuccess),
      onError: onError
    )
  }
}
