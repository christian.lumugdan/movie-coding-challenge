//
//  LogoutServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol LogoutServiceProtocol: DeAuthServiceProtocol {
  func logout(
    shouldBroadcast: Bool,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
