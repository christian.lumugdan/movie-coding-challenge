//
//  LogoutService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class LogoutService: LogoutServiceProtocol {
  var onDeAuth: BoolResult?
  var onError: ErrorResult?

  private let api: AuthLogoutAPI

  init(api: AuthLogoutAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension LogoutService {
  func logout(
    shouldBroadcast: Bool,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthLogout(
      onSuccess: handleDeAuth(
        shouldBroadcast: shouldBroadcast,
        thenExecute: onSuccess
      ),
      onError: handleDeAuthError(thenExecute: onError)
    )
  }
}
