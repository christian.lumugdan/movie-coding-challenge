//
//  AccountDeletionService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class AccountDeletionService: AccountDeletionServiceProtocol {
  var onDeAuth: BoolResult?
  var onError: ErrorResult?

  private let api: AuthAccountAPI

  init(api: AuthAccountAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension AccountDeletionService {
  func deleteAccount(
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.deleteAccount(
      token: token,
      onSuccess: handleDeAuth(
        shouldBroadcast: true,
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }
}
