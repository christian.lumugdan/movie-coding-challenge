//
//  DeAuthServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol DeAuthServiceProtocol {
  var onDeAuth: BoolResult? { get set }
  var onError: ErrorResult? { get set }
}

extension DeAuthServiceProtocol {
  func handleDeAuth(
    shouldBroadcast: Bool,
    thenExecute handler: @escaping VoidResult
  ) -> VoidResult {
    return { [onDeAuth] in
      onDeAuth?(shouldBroadcast)
      handler()
    }
  }

  func handleDeAuthError(thenExecute handler: @escaping ErrorResult) -> ErrorResult {
    return { [onError] error in
      onError?(error)
      handler(error)
    }
  }
}
