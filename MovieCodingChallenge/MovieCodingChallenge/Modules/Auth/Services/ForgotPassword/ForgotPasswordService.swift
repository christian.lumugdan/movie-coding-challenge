//
//  ForgotPasswordService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ForgotPasswordService: ForgotPasswordServiceProtocol {
  private let api: AuthPasswordAPI

  init(api: AuthPasswordAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension ForgotPasswordService {
  func sendPasswordResetRequest(
    for username: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthForgotPassword(
      for: username,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func confirmPasswordReset(
    for username: String,
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthResetPasswordCheck(
      for: username,
      with: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func resetPassword(
    with params: UpdatePasswordRequestParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postResetPassword(
      with: params,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
