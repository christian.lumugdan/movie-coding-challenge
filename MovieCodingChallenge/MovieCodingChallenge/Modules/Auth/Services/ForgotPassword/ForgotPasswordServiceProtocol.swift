//
//  ForgotPasswordServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ForgotPasswordServiceProtocol {
  func sendPasswordResetRequest(
    for username: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func confirmPasswordReset(
    for username: String,
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func resetPassword(
    with params: UpdatePasswordRequestParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
