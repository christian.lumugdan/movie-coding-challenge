//
//  APIClient+AuthRegister.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//
import Foundation

import Alamofire

extension APIClient: AuthRegisterAPI {
  @discardableResult
  func postAuthRegisterEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "email": email,
      "password": password,
      "password_confirmation": password,
    ]

    return request(
      "auth/register",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postAuthRegisterPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "phone_number": phoneNumber,
      "otp": otp,
    ]

    return request(
      "auth/register",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
