//
//  AuthRegisterAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AuthRegisterAPI {
  @discardableResult
  func postAuthRegisterEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postAuthRegisterPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
