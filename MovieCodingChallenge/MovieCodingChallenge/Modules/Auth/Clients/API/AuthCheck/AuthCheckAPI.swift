//
//  AuthCheckAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AuthCheckAPI {
  /// This checks whether the specified email is still available for a new account or
  /// is already paired to an existing account.
  ///
  /// - parameter username: The email address or phone number we're trying to check.
  /// - parameter onSuccess: This closure receives True only if the `email` is still available.
  /// - parameter onError: An error handler.
  ///
  @discardableResult
  func postAuthCheckUsername(
    _ username: String,
    onSuccess: @escaping BoolResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
