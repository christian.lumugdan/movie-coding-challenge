//
//  APIClient+AuthCheck.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//
import Foundation

import Alamofire

extension APIClient: AuthCheckAPI {
  @discardableResult
  func postAuthCheckEmail(
    _ email: String,
    onSuccess: @escaping BoolResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/check-email",
      method: .post,
      parameters: ["email": email],
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: { _ in onSuccess(false) },
      failure: { error in
        if let error = error as? APIClientError,
          case let .failedRequest(info) = error,
          info.errorCode == .usernameNotFound {
          // Email is available.
          return onSuccess(true)
        }

        onError(error)
      }
    )
  }

  @discardableResult
  func postAuthCheckUsername(
    _ username: String,
    onSuccess: @escaping BoolResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/check-username",
      method: .post,
      parameters: ["username": username],
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: { _ in onSuccess(false) },
      failure: { error in
        if let error = error as? APIClientError,
          case let .failedRequest(info) = error,
          info.errorCode == .usernameNotFound {
          // Email is available.
          return onSuccess(true)
        }

        onError(error)
      }
    )
  }
}
