//
//  APIClient+AuthOnboarding.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthOnboardingAPI {
  @discardableResult
  func postOnboardingEmail(
    email: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/onboarding/email",
      method: .post,
      parameters: ["email": email],
      encoding: JSONEncoding.default,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postOnboardingComplete(
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/onboarding/complete",
      method: .post,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
