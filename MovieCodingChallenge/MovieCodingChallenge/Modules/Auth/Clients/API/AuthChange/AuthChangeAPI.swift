//
//  AuthChangeAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AuthChangeAPI {
  @discardableResult
  func postAuthChangeEmail(
    email: String,
    token: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postAuthChangeEmailVerify(
    token: String,
    verificationToken: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postAuthChangePhoneNumber(
    phoneNumber: String,
    token: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postAuthChangePhoneNumberVerify(
    token: String,
    verificationToken: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postAuthChangePassword(
    newPassword: String,
    oldPassword: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
