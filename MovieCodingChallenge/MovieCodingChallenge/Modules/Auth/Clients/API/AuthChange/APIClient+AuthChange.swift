//
//  APIClient+AuthChange.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthChangeAPI {
  @discardableResult
  func postAuthChangeEmail(
    email: String,
    token: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "email": email
    ]

    let additionalHeaders = [
      "Verification-Token": token
    ]

    return request(
      "auth/change/email",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(
        additionalHeaders: additionalHeaders
      ),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postAuthChangeEmailVerify(
    token: String,
    verificationToken: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "token": token
    ]

    let additionalHeaders = [
      "Verification-Token": verificationToken
    ]

    return request(
      "auth/change/email/verify",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(
        additionalHeaders: additionalHeaders
      ),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postAuthChangePhoneNumber(
    phoneNumber: String,
    token: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "phone_number": phoneNumber
    ]

    let additionalHeaders = [
      "Verification-Token": token
    ]

    return request(
      "auth/change/phone-number",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(
        additionalHeaders: additionalHeaders
      ),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postAuthChangePhoneNumberVerify(
    token: String,
    verificationToken: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "token": token
    ]

    let additionalHeaders = [
      "Verification-Token": verificationToken
    ]

    return request(
      "auth/change/phone-number/verify",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(
        additionalHeaders: additionalHeaders
      ),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postAuthChangePassword(
    newPassword: String,
    oldPassword: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "old_password": oldPassword,
      "new_password": newPassword,
      "new_password_confirmation": newPassword
    ]

    return request(
      "auth/change/password",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
