//
//  APIClient+AuthLogin.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthLoginAPI {
  @discardableResult
  func postAuthLoginEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "username": email,
      "password": password
    ]

    return request(
      "auth/login",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postAuthLoginPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "username": phoneNumber,
      "otp": otp
    ]

    return request(
      "auth/login",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
