//
//  AuthPasswordAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AuthPasswordAPI {
  /// Used to grab a token to be used for resetting the user's password.
  /// This endpoint is for the password reset flow.
  @discardableResult
  func postAuthForgotPassword(
    for username: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  /// Checks the validity of the given token.
  /// This endpoint is for the password reset flow.
  @discardableResult
  func postAuthResetPasswordCheck(
    for username: String,
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  /// Reset the user's password
  @discardableResult
  func postResetPassword(
    with params: UpdatePasswordRequestParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
