//
//  APIClient+AuthPassword.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthPasswordAPI {
  @discardableResult
  func postAuthForgotPassword(
    for username: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/forgot-password",
      method: .post,
      parameters: ["username": username],
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: { _ in onSuccess() },
      failure: onError
    )
  }

  @discardableResult
  func postAuthResetPasswordCheck(
    for username: String,
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/reset-password/check",
      method: .post,
      parameters: [
        "token": token,
        "username": username,
      ],
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: { _ in onSuccess() },
      failure: onError
    )
  }

  @discardableResult
  func postResetPassword(
    with params: UpdatePasswordRequestParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/reset-password",
      method: .post,
      parameters: params.dictionary(),
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
