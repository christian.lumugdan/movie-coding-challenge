//
//  APIClient+AuthSocial.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthSocialAPI {
  @discardableResult
  func postAuthSocial(
    with token: String,
    type: SocialLoginType,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/social",
      method: .post,
      parameters: ["token": token, "provider": type.rawValue],
      encoding: URLEncoding.queryString,
      headers: httpRequestHeaders(withAuth: false),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
