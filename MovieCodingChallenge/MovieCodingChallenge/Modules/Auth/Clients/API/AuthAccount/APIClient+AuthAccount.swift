//
//  APIClient+AuthAccount.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthAccountAPI {
  @discardableResult
  func deleteAccount(
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let additionalHeaders = [
      "Verification-Token": token
    ]

    return request(
      "auth/account",
      method: .delete,
      headers: httpRequestHeaders(
        additionalHeaders: additionalHeaders
      ),
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
