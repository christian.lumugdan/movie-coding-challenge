//
//  AuthOTPAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol AuthOTPAPI {
  @discardableResult
  func postOTPGenerate(
    for phoneNumber: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
