//
//  APIClient+AuthOTP.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthOTPAPI {
  @discardableResult
  func postOTPGenerate(
    for phoneNumber: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/otp/generate",
      method: .post,
      parameters: ["phone_number": phoneNumber],
      encoding: JSONEncoding.default,
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
