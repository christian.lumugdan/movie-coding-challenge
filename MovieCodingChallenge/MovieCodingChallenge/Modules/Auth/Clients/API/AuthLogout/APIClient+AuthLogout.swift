//
//  APIClient+AuthLogout.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthLogoutAPI {
  @discardableResult
  func postAuthLogout(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/logout",
      method: .post,
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
