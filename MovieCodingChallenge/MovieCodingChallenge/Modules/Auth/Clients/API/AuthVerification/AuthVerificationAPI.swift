//
//  AuthVerificationAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AuthVerificationAPI {
  @discardableResult
  func postAuthVerificationVerify(
    token: String,
    type: VerificationType,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postAuthVerificationResend(
    type: VerificationType,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postAuthVerificationToken(
    params: PostAuthVerificationTokenParams,
    onSuccess: @escaping SingleResult<VerificationToken>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
