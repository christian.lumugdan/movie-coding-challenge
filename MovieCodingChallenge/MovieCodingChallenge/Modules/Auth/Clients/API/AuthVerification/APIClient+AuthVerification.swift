//
//  APIClient+AccountVerification.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthVerificationAPI {
  @discardableResult
  func postAuthVerificationVerify(
    token: String,
    type: VerificationType,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "token": token,
      "via": type.rawValue
    ]

    return request(
      "auth/verification/verify",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postAuthVerificationResend(
    type: VerificationType,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "via": type.rawValue
    ]

    return request(
      "auth/verification/resend",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postAuthVerificationToken(
    params: PostAuthVerificationTokenParams,
    onSuccess: @escaping SingleResult<VerificationToken>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/account/verification-token",
      method: .post,
      parameters: params.dictionary(),
      encoding: JSONEncoding.default,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
