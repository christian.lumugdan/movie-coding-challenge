//
//  AuthProfileAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AuthProfileAPI {
  @discardableResult
  func putAuthProfile(
    with params: AuthProfileParams,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  func postAuthProfileAvatar(
    with data: Data,
    onSuccess: @escaping SingleResult<Photo>,
    onError: @escaping ErrorResult
  )

  @discardableResult
  func getAuthProfile(
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
