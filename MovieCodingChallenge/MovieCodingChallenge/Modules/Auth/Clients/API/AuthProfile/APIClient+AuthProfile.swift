//
//  APIClient+AuthProfile.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthProfileAPI {
  @discardableResult
  func putAuthProfile(
    with params: AuthProfileParams,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/profile",
      method: .put,
      parameters: params.dictionary(),
      encoding: JSONEncoding.default,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  func postAuthProfileAvatar(
    with data: Data,
    onSuccess: @escaping SingleResult<Photo>,
    onError: @escaping ErrorResult
  ) {
    let params = [
      "avatar": data
    ]

    let multipartFormData: (MultipartFormData) -> Void = { multipartFormData in
      multipartFormData.append(params)
    }

    upload(
      to: "auth/profile/avatar",
      with: multipartFormData,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func getAuthProfile(
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/profile",
      method: .get,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
