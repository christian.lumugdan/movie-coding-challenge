//
//  AppIntroStepOneViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppIntroStepOneViewModel: PageContentViewModelProtocol {}

// MARK: - Getters

extension AppIntroStepOneViewModel {
  var title: String { S.appIntroLabelsStep(1) }
  var description: String { S.appIntroLabelsDescriptions() }
  var image: UIImage { R.image.intro1()! }
  var imageBackgroundColor: UIColor { R.color.appIntroStep1BackgroundColor()! }
}
