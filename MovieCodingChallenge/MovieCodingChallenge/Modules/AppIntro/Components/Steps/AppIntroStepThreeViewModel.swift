//
//  AppIntroStepThreeViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppIntroStepThreeViewModel: PageContentViewModelProtocol {}

// MARK: - Getters

extension AppIntroStepThreeViewModel {
  var title: String { S.appIntroLabelsStep(3) }
  var description: String { S.appIntroLabelsDescriptions() }
  var image: UIImage { R.image.intro3()! }
  var imageBackgroundColor: UIColor { R.color.appIntroStep3BackgroundColor()! }
}
