//
//  AppIntroStepFourViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppIntroStepFourViewModel: PageContentViewModelProtocol {}

// MARK: - Getters

extension AppIntroStepFourViewModel {
  var title: String { S.appIntroLabelsStep(4) }
  var description: String { S.appIntroLabelsDescriptions() }
  var image: UIImage { R.image.intro4()! }
  var imageBackgroundColor: UIColor { R.color.appIntroStep4BackgroundColor()! }
}
