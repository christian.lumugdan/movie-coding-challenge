//
//  AppIntroStepTwoViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppIntroStepTwoViewModel: PageContentViewModelProtocol {}

// MARK: - Getters

extension AppIntroStepTwoViewModel {
  var title: String { S.appIntroLabelsStep(2) }
  var description: String { S.appIntroLabelsDescriptions() }
  var image: UIImage { R.image.intro2()! }
  var imageBackgroundColor: UIColor { R.color.appIntroStep2BackgroundColor()! }
}
