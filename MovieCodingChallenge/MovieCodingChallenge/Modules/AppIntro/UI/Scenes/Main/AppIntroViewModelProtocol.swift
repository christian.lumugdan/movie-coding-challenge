//
//  AppIntroViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol AppIntroViewModelProtocol {
  var onContinue: VoidResult? { get set }
  var onSkip: VoidResult? { get set }
  
  var pageVMs: [PageContentViewModelProtocol] { get }
  
  var shouldHideNextButton: Bool { get }
  var shouldHideSkipButton: Bool { get }
}
