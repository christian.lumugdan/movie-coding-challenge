//
//  AppIntroViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class AppIntroViewModel: AppIntroViewModelProtocol {
  var onContinue: VoidResult?
  var onSkip: VoidResult?
}

// MARK: - Getters

extension AppIntroViewModel {
  var pageVMs: [PageContentViewModelProtocol] {
    [
      AppIntroStepOneViewModel(),
      AppIntroStepTwoViewModel(),
      AppIntroStepThreeViewModel(),
      AppIntroStepFourViewModel()
    ]
  }
  
  var shouldHideNextButton: Bool { false }
  var shouldHideSkipButton: Bool { false }
}
