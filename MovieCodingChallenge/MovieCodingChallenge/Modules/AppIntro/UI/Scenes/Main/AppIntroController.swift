//
//  AppIntroController.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Pageboy
import UIKit

class AppIntroController: ViewController {
  var viewModel: AppIntroViewModelProtocol!

  @IBOutlet private(set) var pageContentView: UIView!
  @IBOutlet private(set) var pageControl: PageControl!
  @IBOutlet private(set) var skipButton: UIButton!
  @IBOutlet private(set) var nextButton: UIButton!
  @IBOutlet private(set) var continueButton: APButtonPrimary!
  @IBOutlet private(set) var continueBtnHeight: NSLayoutConstraint!

  private(set) var pageController: PageController!
  private var origContinueBtnHeight: CGFloat = 0
}

// MARK: - Lifecycle

extension AppIntroController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)
  }
}

// MARK: - Setup

private extension AppIntroController {
  func setupViews() {
    guard viewModel != nil else { return }

    setupPageControl()
    setupPageController()

    skipButton.isHidden = viewModel.shouldHideSkipButton
    nextButton.isHidden = viewModel.shouldHideNextButton

    origContinueBtnHeight = continueButton.frame.size.height
    continueBtnHeight.constant = 0.0
  }

  func setupPageControl() {
    pageControl.numberOfPages = viewModel.pageVMs.count
    pageControl.addTarget(
      self,
      action: #selector(pageControlValueChanged(_:)),
      for: .valueChanged
    )
  }

  func setupPageController() {
    pageController = PageController()
    pageController.delegate = self
    pageController.dataSource = self
    pageController.transition = .init(style: .fade, duration: 0.4)

    pageContentView.backgroundColor = .clear
    addChild(pageController)
    pageContentView.addSubview(pageController.view)
    pageController.view.autoPinEdgesToSuperviewEdges()
    pageController.didMove(toParent: self)
  }

  func setupUIForPage(index: CGFloat, currentPosition: CGFloat) {
    guard let pageCount = pageController.pageCount else { return }
    let lastPageIndex = CGFloat(pageCount - 1)

    guard index > lastPageIndex - 2,
          currentPosition < lastPageIndex
    else { return }

    DispatchQueue.main.async {
      guard currentPosition > lastPageIndex - 1.0,
            currentPosition != lastPageIndex
      else { return }

      let percentage = currentPosition.truncatingRemainder(dividingBy: 1.0)
      self.continueBtnHeight.constant = CGFloat(self.origContinueBtnHeight * percentage)
      self.continueButton.alpha = percentage
      self.skipButton.alpha = 1.0 - percentage
      self.nextButton.alpha = 1.0 - percentage
    }
  }
}

// MARK: - Actions

private extension AppIntroController {
  @objc
  func pageControlValueChanged(_ sender: AnyObject) {
    pageController.scrollToPage(
      .at(index: pageControl.currentPage),
      animated: true
    )
  }

  @IBAction
  func onSkipTapped(_ sender: AnyObject) {
    viewModel.onSkip?()
  }

  @IBAction
  func onNextTapped(_ sender: AnyObject) {
    let nextIndex = pageControl.currentPage + 1
    guard nextIndex < pageControl.numberOfPages else { return }

    pageControl.setCurrentPage(nextIndex, animated: true)
    pageController.scrollToPage(.at(index: pageControl.currentPage), animated: true)
  }

  @IBAction
  func onContinueTapped(_ sender: AnyObject) {
    viewModel.onContinue?()
  }
}

// MARK: - PageboyViewControllerDataSource

extension AppIntroController: PageboyViewControllerDataSource {
  func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
    viewModel.pageVMs.count
  }

  func viewController(
    for pageboyViewController: PageboyViewController,
    at index: PageboyViewController.PageIndex
  ) -> UIViewController? {
    let vc = R.storyboard.appIntro.pageContentController()!
    if let vm = viewModel.pageVMs[safe: index] {
      vc.viewModel = vm
    }
    return vc
  }

  func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
    .first
  }
}

// MARK: - PageboyViewControllerDelegate

extension AppIntroController: PageboyViewControllerDelegate {
  func pageboyViewController(
    _ pageboyViewController: PageboyViewController,
    didScrollTo position: CGPoint,
    direction: PageboyViewController.NavigationDirection,
    animated: Bool
  ) {
    guard pageControl.isUserInteractionEnabled else { return }

    let targetOffset = UIScreen.main.bounds.width * position.x
    let index = Int((targetOffset / UIScreen.main.bounds.width).rounded())

    pageControl.setCurrentPage(index, animated: animated)

    setupUIForPage(index: CGFloat(index), currentPosition: position.x)
  }

  func pageboyViewController(
    _ pageboyViewController: PageboyViewController,
    willScrollToPageAt index: PageboyViewController.PageIndex,
    direction: PageboyViewController.NavigationDirection,
    animated: Bool
  ) {}

  func pageboyViewController(
    _ pageboyViewController: PageboyViewController,
    didScrollToPageAt index: PageboyViewController.PageIndex,
    direction: PageboyViewController.NavigationDirection,
    animated: Bool
  ) {}

  func pageboyViewController(
    _ pageboyViewController: PageboyViewController,
    didReloadWith currentViewController: UIViewController,
    currentPageIndex: PageboyViewController.PageIndex
  ) {}
}
