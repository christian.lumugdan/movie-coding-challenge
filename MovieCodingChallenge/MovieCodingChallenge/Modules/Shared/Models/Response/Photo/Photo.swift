//
//  Photo.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct Photo: APIModel, Codable {
  let id: Int
  let url: URL
  let thumbUrl: URL
}
