//
//  Photo.Update.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension Photo {
  struct Update: APIModel, Codable {
    let id: Int
    var delete: Bool? = nil
  }
}
