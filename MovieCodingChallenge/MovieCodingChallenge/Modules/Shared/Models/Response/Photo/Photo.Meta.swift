//
//  Photo.Meta.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension Photo {
  struct Meta {
    var id: Int? = nil
    var data: Data? = nil
  }
}
