//
//  GenericCategory.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct GenericCategory: APIModel, Codable, Equatable {
  let id: Int
  let label: String
}
