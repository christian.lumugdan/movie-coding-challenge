//
//  Gender.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

enum Gender: String, Codable, Defaultable {
  case male, female, other

  static var defaultValue: Gender {
    return .other
  }
}
