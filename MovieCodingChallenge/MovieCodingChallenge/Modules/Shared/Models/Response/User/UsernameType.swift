//
//  UsernameType.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

enum UsernameType: String, Codable {
  case email
  case phone = "phone_number"
}
