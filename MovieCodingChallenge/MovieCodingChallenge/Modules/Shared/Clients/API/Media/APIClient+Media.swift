//
//  APIClient+Media.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: MediaAPI {
  func postUploadMedia(
    with data: Data,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping SingleResult<Photo>,
    onError: @escaping ErrorResult
  ) {
    let multipartFormData: SingleResult<MultipartFormData> = { multipartFormData in
      multipartFormData.append(["file": data])
    }
    upload(
      to: "media",
      with: multipartFormData,
      uploadProgress: onProgress,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
