//
//  MediaAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol MediaAPI {
  func postUploadMedia(
    with data: Data,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping SingleResult<Photo>,
    onError: @escaping ErrorResult
  )
}
