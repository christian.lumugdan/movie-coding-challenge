//
//  APIClient+PushNotification.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: DevicesAPI {
  @discardableResult
  func postDevices(
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    let parameters = [
      "device_token": token,
      "device_id": UUID().uuidString
    ]

    return request(
      "devices",
      method: .post,
      parameters: parameters,
      encoding: JSONEncoding.default,
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
