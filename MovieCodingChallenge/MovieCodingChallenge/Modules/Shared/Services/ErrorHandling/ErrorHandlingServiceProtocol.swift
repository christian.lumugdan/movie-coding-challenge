//
//  ErrorHandlingServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ErrorHandlingServiceProtocol {
  var onUnauthorizedError: VoidResult? { get set }
  
  func processAPIError(_ apiError: APIClientError)
  
  func processError(_ error: Error)

  func processError(
    _ error: Error,
    info: [String: Any]?
  )
}
