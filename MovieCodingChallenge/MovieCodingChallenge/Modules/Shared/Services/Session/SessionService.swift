//
//  SessionService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire
import GoogleSignIn

class SessionService: SessionServiceProtocol {
  var onResumeSessionError: ErrorResult?

  private(set) var user: User?

  private let api: APIClientProtocol
  private let userDefaults: UserDefaults

  init(
    api: APIClientProtocol,
    userDefaults: UserDefaults = .standard
  ) {
    self.api = api

    self.userDefaults = userDefaults

    setup()
  }
}

// MARK: - Setup

private extension SessionService {
  func setup() {
    resumeSession()
  }

  func resumeSession() {
    guard let userInfo = encodedUserData else { return }
    do {
      debugLog("<-- \(try JSONSerialization.jsonObject(with: userInfo))")
      user = try User.decode(userInfo)
    } catch DecodingError.typeMismatch {
      // Force remove cached user info since we can't decode it anymore.
      encodedUserData = nil
    } catch {
      onResumeSessionError?(error)
    }
  }
}

// MARK: - Handlers

extension SessionService {
  func handleAuthResult() -> SingleResult<UserAuthResponse> {
    return { [unowned self] authResult in
      self.cacheUser(authResult.user)
      self.api.accessToken = authResult.accessToken
      self.postSesssionNotification(name: .didLogin)
    }
  }

  func handleUserResult() -> SingleResult<User> {
    return { [unowned self] updatedUser in
      let oldUser = self.user

      self.cacheUser(updatedUser)
      self.postSesssionNotification(name: .didRefreshUser)

      if let oldUser = oldUser,
         !oldUser.hasCompletedOnboarding,
         updatedUser.hasCompletedOnboarding {
        self.postSesssionNotification(name: .didCompleteOnboarding)
      }
    }
  }

  func handleVerifyAccountResult() -> SingleResult<User> {
    return { [unowned self] user in
      self.cacheUser(user)
      self.postSesssionNotification(name: .didVerifyAccount)
      self.postSesssionNotification(name: .didRefreshUser)
    }
  }

  func handleAvatarResult() -> SingleResult<Photo> {
    return { [unowned self] avatar in
      guard let currentUser = self.user else { return }
      let updatedUser = User(user: currentUser, avatar: avatar)
      self.cacheUser(updatedUser)
      self.postSesssionNotification(name: .didRefreshUser)
    }
  }

  func handleUnauthorizedError() -> VoidResult {
    return { [unowned self] in
      self.clearSession(shouldBroadcast: true)
    }
  }

  func handleDeAuth() -> BoolResult {
    return { [unowned self] shouldBroadcast in
      self.clearSession(shouldBroadcast: shouldBroadcast)
    }
  }

  func handleDeAuthError() -> ErrorResult {
    return { error in
      if case let .failedRequest(info) = error as? APIClientError,
         info.status == .unauthorized {
        // Just let the user log out since they no longer have valid session anyway.
      }
    }
  }
}

// MARK: - Methods

extension SessionService {
  func clearSession(shouldBroadcast: Bool = false) {
    resetUser()
    api.reset()

    if shouldBroadcast {
      postSesssionNotification(name: .didLogout)
    }
  }
}

// MARK: - Email Flags

extension SessionService {
  func recordEmailVerificationSkip() {
    hasSkippedEmailVerification = true
    postSesssionNotification(name: .didSkipEmailVerification)
  }
  
  func recordAddOTPEmailComplete() {
    hasCompletedOTPAddEmail = true
    postSesssionNotification(name: .didCompleteOTPOnboarding)
  }

  private(set) var hasSkippedEmailVerification: Bool {
    set { userDefaults.set(newValue, forKey: #function) }
    get { userDefaults.bool(forKey: #function) }
  }
  
  private(set) var hasCompletedOTPAddEmail: Bool {
    set { userDefaults.set(newValue, forKey: #function) }
    get { userDefaults.bool(forKey: #function) }
  }
}

// MARK: - User Data Management

private extension SessionService {
  var encodedUserData: Data? {
    set { userDefaults.set(newValue, forKey: #function) }
    get { userDefaults.data(forKey: #function) }
  }

  func cacheUser(_ user: User) {
    self.user = user

    if let newEncodedUserData = try? user.encode() {
      encodedUserData = newEncodedUserData
    }
  }

  func resetUser() {
    user = nil
    encodedUserData = nil
    hasSkippedEmailVerification = false
    hasCompletedOTPAddEmail = false

    resetGoogleUserIfNeeded()
    // TODO: Add more social logouts here
  }

  /// Clears the Google user from Keychain.
  /// See: https://developers.google.com/identity/sign-in/ios/sign-in?ver=swift#sign_out_the_user
  func resetGoogleUserIfNeeded() {
    guard
      let gidSignIn = GIDSignIn.sharedInstance(),
      gidSignIn.hasPreviousSignIn()
    else { return }

    gidSignIn.signOut()
  }
}

// MARK: - Utils

private extension SessionService {
  func postSesssionNotification(name: Notification.Name) {
    NotificationCenter.default.post(name: name, object: self)
  }
}

// MARK: - Getters

extension SessionService {
  var isActive: Bool { (user != nil) && (api.accessToken != nil) }
}
