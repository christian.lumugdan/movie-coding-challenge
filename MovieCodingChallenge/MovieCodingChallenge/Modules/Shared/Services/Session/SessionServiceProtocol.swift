//
//  SessionServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol SessionServiceProtocol: AppServiceProtocol {
  var onResumeSessionError: ErrorResult? { get set }
  
  var isActive: Bool { get }
  var user: User? { get }
  var hasSkippedEmailVerification: Bool { get }

  func handleAuthResult() -> SingleResult<UserAuthResponse>
  func handleUserResult() -> SingleResult<User>
  func handleAvatarResult() -> SingleResult<Photo>
  func handleDeAuth() -> BoolResult
  func handleDeAuthError() -> ErrorResult
  func handleUnauthorizedError() -> VoidResult
  
  func clearSession(shouldBroadcast: Bool)
  func recordEmailVerificationSkip()
  func recordAddOTPEmailComplete()
}
