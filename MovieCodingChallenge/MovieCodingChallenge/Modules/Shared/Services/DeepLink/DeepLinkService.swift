//
//  DeepLinkService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class DeepLinkService: DeepLinkServiceProtocol {
  private var deepLink: Deeplink?

  private let pushNotifParser: PushNotificationParserProtocol
  private let dynamicLinkParser: DynamicLinkParserProtocol
  private let shortcutParser: ShortcutParserProtocol

  init(
    pushNotifParser: PushNotificationParserProtocol = PushNotificationParser(),
    dynamicLinkParser: DynamicLinkParserProtocol = DynamicLinkParser(),
    shortcutParser: ShortcutParserProtocol = ShortcutParser(),
    launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) {
    self.pushNotifParser = pushNotifParser
    self.dynamicLinkParser = dynamicLinkParser
    self.shortcutParser = shortcutParser

    setup(from: launchOptions)
  }
}

// MARK: - Setup

private extension DeepLinkService {
  func setup(from launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
    handlePushNotification(from: launchOptions)
  }

  func handlePushNotification(from launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
    guard let remoteUserInfo = launchOptions?[.remoteNotification] as? [AnyHashable: Any] else {
      return
    }
    handlePushNotification(with: remoteUserInfo)
  }
}

// MARK: - Methods

extension DeepLinkService {
  func shouldPresentPushNotificationInForeground(
    with userInfo: [AnyHashable: Any]
  ) -> Bool {
    // Add checking here if should present push notification in foreground
    // Example case: New chat message push notif, but chat thread is currently open in the app

    // let deepLink = pushNotifParser.parseNotification(with: userInfo)

    return true
  }

  func handlePushNotification(with userInfo: [AnyHashable: Any]) {
    deepLink = pushNotifParser.parseNotification(with: userInfo)
  }

  func handleShortcut(from item: UIApplicationShortcutItem) {
    deepLink = shortcutParser.parseShortcut(from: item)
  }

  func handleDynamicLink(from url: URL) {
    deepLink = dynamicLinkParser.parseDynamicLink(from: url)
  }

  func executeDeepLink() {
    guard let deepLink = deepLink else { return }

    switch deepLink {
    case .sample:
      // Navigate to specific page
      break
    }

    reset()
  }
}

// MARK: - Utils

private extension DeepLinkService {
  func reset() {
    deepLink = nil
  }
}

// MARK: - Getters

extension DeepLinkService {
  var hasDeepLinkToExecute: Bool { deepLink != nil }
}
