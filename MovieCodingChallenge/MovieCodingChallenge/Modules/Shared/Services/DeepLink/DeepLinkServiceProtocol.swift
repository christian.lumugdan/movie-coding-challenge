//
//  DeepLinkServiceProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol DeepLinkServiceProtocol {
  var hasDeepLinkToExecute: Bool { get }
  
  func shouldPresentPushNotificationInForeground(
    with userInfo: [AnyHashable: Any]
  ) -> Bool
  
  func handlePushNotification(with userInfo: [AnyHashable: Any])
  func handleShortcut(from item: UIApplicationShortcutItem)
  func handleDynamicLink(from url: URL)

  func executeDeepLink()
}
