//
//  PushNotificationParser.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PushNotificationParser: PushNotificationParserProtocol {
  private let jsonDecoder: JSONDecoder

  init(
    jsonDecoder: JSONDecoder = .init()
  ) {
    self.jsonDecoder = jsonDecoder
  }
}

// MARK: - Methods

extension PushNotificationParser {
  func parseNotification(with userInfo: [AnyHashable: Any]) -> Deeplink? {
    do {
      guard let pushNotif = try decodeNotification(from: userInfo) else {
        return nil
      }

      switch pushNotif.type {
      case .sample:
        return .sample
      }
    } catch {
      return nil
    }
  }
}

// MARK: - Utils

private extension PushNotificationParser {
  func decodeNotification(from userInfo: [AnyHashable: Any]) throws -> PushNotification? {
    let optionalJSON = try? JSONSerialization.data(
      withJSONObject: userInfo,
      options: .prettyPrinted
    )
    guard let json = optionalJSON else { return nil }
    return try? jsonDecoder.decode(
      PushNotification.self,
      from: json
    )
  }
}
