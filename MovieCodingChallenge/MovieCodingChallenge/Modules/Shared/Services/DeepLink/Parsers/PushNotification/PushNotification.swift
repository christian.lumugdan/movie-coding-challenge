//
//  v.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct PushNotification {
  let type: Type
  
  private var payload: Any?
}

// MARK: - Decodable

extension PushNotification: Decodable {
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)

    let payloadString = try container.decode(String.self, forKey: .payload)
    if let data = payloadString.data(using: .utf8) {
      do {
        payload = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
      } catch {
        debugPrint(error.localizedDescription)
      }
    }

    type = try container.decode(Type.self, forKey: .type)
  }

  enum CodingKeys: String, CodingKey {
    case payload
    case type = "code"
  }

  func decodedValue<T>(forKeyPath: String? = nil, decoder: JSONDecoder? = nil) -> T? where T: Decodable {
    guard var payload = payload else { return nil }

    if let keyPath = forKeyPath {
      guard let d = nestedData(keyPath) else { return nil }
      payload = d
    }

    do {
      let json = try JSONSerialization.data(withJSONObject: payload)
      return try (decoder ?? GenericAPIModel.decoder()).decode(T.self, from: json)

    } catch {
//      App.shared.errorHandling.processError(error, info: ["keyPath": forKeyPath ?? "n/a"])
      return nil
    }
  }

  /// Returns the data at the given `keyPath`. Nil if path doesn't exist.
  private func nestedData(_ keyPath: String) -> Any? {
    guard let payload = payload, !keyPath.isEmpty else { return nil }
    guard let dict = payload as? [String: Any] else { return nil }
    return dict[keyPath: keyPath] as Any
  }
}

// MARK: - Types

extension PushNotification {
  enum `Type`: String, Codable {
    case sample
  }
}
