//
//  DynamicLinkParserProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol DynamicLinkParserProtocol {
  func parseDynamicLink(from url: URL) -> Deeplink?
}
