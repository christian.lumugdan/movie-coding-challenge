//
//  DynamicLinkPath.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

enum DynamicLinkPath: String {
  case sample = "/sample"
}
