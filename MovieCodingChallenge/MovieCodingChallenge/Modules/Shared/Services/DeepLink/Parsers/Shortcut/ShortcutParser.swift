//
//  ShortcutParser.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class ShortcutParser: ShortcutParserProtocol {}

// MARK: - Methods

extension ShortcutParser {
  func parseShortcut(from item: UIApplicationShortcutItem) -> Deeplink? {
    guard let key = ShortcutKey(rawValue: item.type) else {
      return nil
    }

    switch key {
    case .sample:
      break
    }

    return nil
  }
}
