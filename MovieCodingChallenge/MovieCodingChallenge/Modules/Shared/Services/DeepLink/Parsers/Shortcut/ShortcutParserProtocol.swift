//
//  ShortcutParserProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol ShortcutParserProtocol {
  func parseShortcut(from item: UIApplicationShortcutItem) -> Deeplink?
}
