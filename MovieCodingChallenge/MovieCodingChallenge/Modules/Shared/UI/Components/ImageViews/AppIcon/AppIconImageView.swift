//
//  AppIconImageView.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class AppIconImageView: ImageView {
  override func awakeFromNib() {
    super.awakeFromNib()
    onInit()
  }

  func onInit() {
    image = App.icon
  }
}
