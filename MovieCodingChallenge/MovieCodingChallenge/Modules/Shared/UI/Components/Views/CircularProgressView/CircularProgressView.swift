//
//  CircularProgressView.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class CircularProgressView: UIView {
  @IBInspectable var progressWidth: CGFloat = 1 {
    didSet { refreshProgressLayer() }
  }

  @IBInspectable var progressColor: UIColor = .white {
    didSet { refreshProgressLayer() }
  }

  @IBInspectable var outlineWidth: CGFloat = 1 {
    didSet { refreshOutlineLayer() }
  }

  @IBInspectable var outlineColor: UIColor = .black {
    didSet { refreshOutlineLayer() }
  }

  private var outlineLayer = CAShapeLayer()
  private var progressLayer = CAShapeLayer()

  private var onAnimationComplete: VoidResult?

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }

  func animate(
    fromValue: CGFloat = 0,
    toValue: CGFloat = 1,
    duration: TimeInterval,
    removedOnComplete: Bool = false,
    onComplete: @escaping VoidResult
  ) {
    onAnimationComplete = onComplete

    CATransaction.begin()

    let circularProgressAnimation = CABasicAnimation(keyPath: "strokeEnd")
    circularProgressAnimation.duration = duration
    circularProgressAnimation.fromValue = fromValue
    circularProgressAnimation.toValue = toValue

    let reversed = fromValue > toValue
    circularProgressAnimation.fillMode = reversed ? .backwards : .forwards

    circularProgressAnimation.isRemovedOnCompletion = removedOnComplete

    CATransaction
      .setCompletionBlock { [weak self] in
        guard let self = self else { return }
        self.onAnimationComplete?()
        self.onAnimationComplete = nil
      }

    progressLayer.add(
      circularProgressAnimation,
      forKey: "progressAnimation"
    )

    CATransaction.commit()
  }
  
  func stopAnimation() {
    CATransaction.setDisableActions(true)
  }

  func setup() {
    setupCircleLayer()
    setupProgressLayer()
  }
}

// MARK: - Setup

private extension CircularProgressView {
  func setupCircleLayer() {
    outlineLayer.path = circularPath.cgPath
    outlineLayer.fillColor = UIColor.clear.cgColor
    outlineLayer.lineCap = .round
    outlineLayer.lineWidth = outlineWidth
    outlineLayer.strokeColor = outlineColor.cgColor
    layer.addSublayer(outlineLayer)
  }

  func setupProgressLayer() {
    progressLayer.path = circularPath.cgPath
    progressLayer.fillColor = UIColor.clear.cgColor
    progressLayer.lineCap = .round
    progressLayer.lineWidth = progressWidth
    progressLayer.strokeEnd = 0
    progressLayer.strokeColor = progressColor.cgColor
    layer.addSublayer(progressLayer)
  }
}

// MARK: - Refresh

private extension CircularProgressView {
  func refreshOutlineLayer() {
    outlineLayer.lineWidth = outlineWidth
    outlineLayer.strokeColor = outlineColor.cgColor
  }

  func refreshProgressLayer() {
    progressLayer.lineWidth = progressWidth
    progressLayer.strokeColor = progressColor.cgColor
  }
}

// MARK: - Getters

private extension CircularProgressView {
  var circularPath: UIBezierPath {
    let center = CGPoint(
      x: frame.size.width / 2.0,
      y: frame.size.height / 2.0
    )

    return UIBezierPath(
      arcCenter: center,
      radius: max(center.x, center.y),
      startAngle: -.pi / 2,
      endAngle: 3 * .pi / 2,
      clockwise: true
    )
  }
}
