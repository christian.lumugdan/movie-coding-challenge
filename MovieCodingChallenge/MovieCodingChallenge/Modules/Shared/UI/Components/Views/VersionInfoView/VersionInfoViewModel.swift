//
//  VersionInfoViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class VersionInfoViewModel: VersionInfoViewModelProtocol {
}

// MARK: - Getters

extension VersionInfoViewModel {
  var versionText: String {
    S.versionInfo(
      App.releaseVersion,
      App.buildNumber
    )
  }
}
