//
//  UserContactDetailsViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol UserContactDetailsViewModelProtocol {
  var imageURL: URL { get }
  var nameText: String { get }
  var emailText: String? { get }
  var mobileNumberText: String? { get }
}
