//
//  UserContactDetailsViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class UserContactDetailsViewModel: UserContactDetailsViewModelProtocol {
  private let user: User

  init(user: User) {
    self.user = user
  }
}

// MARK: - Getters

extension UserContactDetailsViewModel {
  var imageURL: URL { user.avatarPermanentThumbUrl }
  var nameText: String { user.fullName ?? "" }
  var emailText: String? { user.email }
  var mobileNumberText: String? { user.phoneNumber }
}
