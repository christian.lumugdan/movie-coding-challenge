//
//  DebouncedButtonView.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import MaterialComponents.MDCButton
import RxCocoa
import RxSwift

class DebouncedButtonView: BaseView, NibLoadable {
  @IBInspectable var debounceInSeconds: Float = 0

  @IBOutlet private(set) var contentView: UIView!
  @IBOutlet private(set) var button: APButtonText!
  @IBOutlet private(set) var timerProgressView: CircularProgressView!

  private var animationEndsAt: Date?

  override func prepare() {
    loadNib()

    setup()
    bind()
  }
}

// MARK: - Methods

extension DebouncedButtonView {
  func startTimer() {
    let duration = TimeInterval(debounceInSeconds)

    animationEndsAt = Date().addingTimeInterval(duration)

    button.isEnabled = false
    timerProgressView.isHidden = false

    timerProgressView.animate(
      fromValue: 1,
      toValue: 0,
      duration: duration,
      onComplete: trigger(type(of: self).reset)
    )
  }

  func resumeTimer() {
    guard let animationEndsAt = animationEndsAt else { return }

    let remainingSecondsInAnimation = animationEndsAt.timeIntervalSince1970 - Date().timeIntervalSince1970

    guard remainingSecondsInAnimation > 0 else {
      return reset()
    }

    let resumeValue = CGFloat(Float(remainingSecondsInAnimation) / debounceInSeconds)

    button.isEnabled = false
    timerProgressView.isHidden = false

    timerProgressView.animate(
      fromValue: resumeValue,
      toValue: 0,
      duration: remainingSecondsInAnimation,
      onComplete: trigger(type(of: self).reset)
    )
  }

  func stopTimer() {
    timerProgressView.stopAnimation()
  }
}

// MARK: - Setup

private extension DebouncedButtonView {
  func setup() {
    setupTimerProgressView()
  }

  func setupTimerProgressView() {
    // Hide initially
    timerProgressView.isHidden = true

    timerProgressView.progressWidth = 2

    // TODO: Maybe set directly to R.color.disabled or R.color.inactive
    button.isEnabled = false
    let progressColor = button.titleColor(for: .disabled) ?? R.color.primaryFull()!
    timerProgressView.progressColor = progressColor.withAlphaComponent(button.alpha)
    button.isEnabled = true

    timerProgressView.outlineWidth = 0

    timerProgressView.autoPinEdge(
      .leading,
      to: .trailing,
      of: button.titleLabel!,
      withOffset: 8
    )
  }
}

// MARK: - Bind

private extension DebouncedButtonView {
  func bind() {
    bindAppActivity()
  }

  func bindAppActivity() {
    let nc = NotificationCenter.default

    nc.rx.notification(UIApplication.didEnterBackgroundNotification)
      .mapToVoid()
      .bind(onNext: trigger(type(of: self).stopTimer))
      .disposed(by: rx.disposeBag)

    nc.rx.notification(UIApplication.willEnterForegroundNotification)
      .mapToVoid()
      .bind(onNext: trigger(type(of: self).resumeTimer))
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Helpers

private extension DebouncedButtonView {
  func reset() {
    button.isEnabled = true
    timerProgressView.isHidden = true

    if hasPreviousAnimationEnded {
      animationEndsAt = nil
    }
  }
}

// MARK: - Private Getters

private extension DebouncedButtonView {
  var hasPreviousAnimationEnded: Bool {
    guard
      let animationEndsAt = animationEndsAt
    else {
      return true
    }

    return animationEndsAt.isInPast
  }
}
