//
//  PhotoSelectionCellViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhotoSelectionCellViewModel: PhotoSelectionCellViewModelProtocol {
  private(set) var imageData: Data?
  let imageURL: URL?
  
  private let photo: Photo?

  init(imageData: Data) {
    self.imageData = imageData
    imageURL = nil
    photo = nil
  }

  init(imageURL: URL) {
    self.imageURL = imageURL
    imageData = nil
    photo = nil
  }
  
  init(photo: Photo) {
    self.photo = photo
    imageURL = photo.thumbUrl
    imageData = nil
  }
}

// MARK: - Methods

extension PhotoSelectionCellViewModel {
  func setImageData(_ imageData: Data) {
    self.imageData = imageData
  }
}

// MARK: - Getters

extension PhotoSelectionCellViewModel {
  var id: Int? { photo?.id }
}
