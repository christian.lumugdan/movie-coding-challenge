//
//  PhotoSelectionCellViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PhotoSelectionCellViewModelProtocol {
  var id: Int? { get }
  var imageData: Data? { get }
  var imageURL: URL? { get }

  func setImageData(_ imageData: Data)
}

// MARK: - Helpers

extension Array where Element == PhotoSelectionCellViewModelProtocol {
  var metaArray: [Photo.Meta] {
    map {
      Photo.Meta(
        id: $0.id,
        data: $0.imageData
      )
    }
  }
}
