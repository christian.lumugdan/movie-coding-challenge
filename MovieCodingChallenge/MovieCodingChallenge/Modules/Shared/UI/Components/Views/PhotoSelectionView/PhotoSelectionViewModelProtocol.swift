//
//  PhotoSelectionViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol PhotoSelectionViewModelProtocol {
  var onRefresh: VoidResult? { get set }
  
  var dataSource: [PhotoSelectionCellViewModelProtocol] { get }
  var maxNumberOfPhotos: Int? { get }
  var allowRearrange: Bool { get }
  
  func addPhotos(_ photosData: [Data])
  func removePhoto(at index: Int)
  func movePhoto(
    fromIndex: Int,
    toIndex: Int
  )
}

// MARK: - Helpers

extension PhotoSelectionViewModelProtocol {
  var remainingNumberOfItems: Int? {
    guard let maxNumberOfPhotos = maxNumberOfPhotos else {
      return nil
    }
    return maxNumberOfPhotos - dataSource.count
  }
  
  var canAddMore: Bool {
    remainingNumberOfItems != 0
  }
}
