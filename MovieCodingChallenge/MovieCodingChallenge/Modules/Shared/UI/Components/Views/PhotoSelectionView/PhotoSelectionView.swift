//
//  PhotoSelectionView.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import RxCocoa
import RxSwift

class PhotoSelectionView: BaseView, NibLoadable {
  var onAddTap: VoidResult?
  var onPhotoTap: SingleResult<UIImage?>?
  var onPhotoLongPress: SingleResult<Int>?

  var viewModel: PhotoSelectionViewModelProtocol! {
    didSet {
      viewModel.onRefresh = trigger(type(of: self).refresh)
      setupVM()
    }
  }

  @IBOutlet private(set) var contentView: UIView!

  @IBOutlet private(set) var collectionView: UICollectionView!
  @IBOutlet private(set) var collectionViewHeightALC: NSLayoutConstraint!

  private(set) var control: UIControl = .init()
  private(set) var longPressRecognizer: UILongPressGestureRecognizer!

  override func prepare() {
    loadNib()
    setup()
    bind()
  }
}

// MARK: - Method

extension PhotoSelectionView {
  func setPrimaryPhoto(at index: Int) {
    collectionView.performBatchUpdates({
      viewModel.movePhoto(
        fromIndex: index,
        toIndex: 0
      )
      collectionView.moveItem(
        at: IndexPath(row: adjustedIndex(at: index, operation: +), section: 0),
        to: IndexPath(row: firstPhotoIndex, section: 0)
      )
    })
  }
}

// MARK: - Setup

private extension PhotoSelectionView {
  func setup() {
    setupCollectionView()
    setupLongPress()
  }

  func setupCollectionView() {
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.dragDelegate = self
    collectionView.dropDelegate = self

    collectionView.register(
      UINib(resource: R.nib.addPhotoSelectionCell),
      forCellWithReuseIdentifier: R.nib.addPhotoSelectionCell.name
    )
    collectionView.register(
      UINib(resource: R.nib.photoSelectionCell),
      forCellWithReuseIdentifier: R.nib.photoSelectionCell.name
    )
  }

  func setupLongPress() {
    longPressRecognizer = UILongPressGestureRecognizer(
      target: self,
      action: #selector(collectionViewLongPressed)
    )
    collectionView.addGestureRecognizer(longPressRecognizer)
  }

  func setupVM() {
    setupCollectionViewRearrange()
    refresh()
  }

  func setupCollectionViewRearrange() {
    collectionView.dragInteractionEnabled = viewModel.allowRearrange
  }
}

// MARK: - Bindings

private extension PhotoSelectionView {
  func bind() {
    bindCollectionView()
  }

  func bindCollectionView() {
    collectionView.rx.contentSize
      .map { $0.height }
      .bind(to: collectionViewHeightALC.rx.constant)
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Actions

private extension PhotoSelectionView {
  @objc
  func collectionViewLongPressed(gesture: UILongPressGestureRecognizer) {
    if gesture.state != .began {
      return
    }

    let point = gesture.location(in: collectionView)

    guard let indexPath = collectionView.indexPathForItem(at: point) else {
      return
    }

    onPhotoLongPress?(adjustedIndex(at: indexPath))
  }
}

// MARK: - Refresh

private extension PhotoSelectionView {
  func refresh() {
    control.sendActions(for: .valueChanged)
    collectionView.reloadData()
  }
}

// MARK: - Handlers

private extension PhotoSelectionView {
  func handleRemoveButtonTap(inCell cell: UICollectionViewCell) -> VoidResult {
    return { [weak self, cell] in
      guard let self = self else { return }
      guard let indexPath = self.collectionView.indexPath(for: cell) else { return }

      let isAddButtonHiddenBeforeRemove = !self.viewModel.canAddMore

      self.viewModel.removePhoto(
        at: self.adjustedIndex(at: indexPath)
      )

      self.collectionView.performBatchUpdates({
        if isAddButtonHiddenBeforeRemove {
          self.collectionView.insertItems(at: [IndexPath(row: 0, section: 0)])
        }
        self.collectionView.deleteItems(at: [indexPath])
        self.control.sendActions(for: .valueChanged)
      })
    }
  }
}

// MARK: - Helpers

private extension PhotoSelectionView {
  func adjustedIndex(at indexPath: IndexPath) -> Int {
    adjustedIndex(at: indexPath.row, operation: -)
  }

  func adjustedIndex(
    at index: Int,
    operation: DoubleResultWithReturn<Int, Int, Int>
  ) -> Int {
    let offset = viewModel.canAddMore ? 1 : 0
    let adjustedIndex = operation(index, offset)
    return adjustedIndex
  }

  var firstPhotoIndex: Int {
    viewModel.canAddMore ? 1 : 0
  }
}

// MARK: - UICollectionViewDataSource

extension PhotoSelectionView: UICollectionViewDataSource {
  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    if viewModel.canAddMore {
      // Add 1 for AddPhotoSelectionCell
      return viewModel.dataSource.count + 1
    } else {
      return viewModel.dataSource.count
    }
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    if viewModel.canAddMore && indexPath.row == 0 {
      return addPhotoSelectionCell(at: indexPath)
    } else {
      return photoSelectionCell(at: indexPath)
    }
  }

  private func addPhotoSelectionCell(at indexPath: IndexPath) -> UICollectionViewCell {
    collectionView.dequeueReusableCell(
      withReuseIdentifier: R.nib.addPhotoSelectionCell.name,
      for: indexPath
    )
  }

  private func photoSelectionCell(at indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(
      withReuseIdentifier: R.nib.photoSelectionCell.name,
      for: indexPath
    ) as! PhotoSelectionCell

    cell.onRemoveButtonTap = handleRemoveButtonTap(inCell: cell)
    cell.viewModel = viewModel.dataSource[adjustedIndex(at: indexPath)]

    return cell
  }
}

// MARK: - UICollectionViewDelegate

extension PhotoSelectionView: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    if indexPath.row == 0 {
      onAddTap?()
    } else if let cell = collectionView.cellForItem(at: indexPath) as? PhotoSelectionCell {
      onPhotoTap?(cell.imageView.image)
    }
  }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension PhotoSelectionView: UICollectionViewDelegateFlowLayout {
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    let minimumItemSpacing = self.collectionView(
      collectionView,
      layout: collectionViewLayout,
      minimumInteritemSpacingForSectionAt: indexPath.section
    )
    let cellsPerRow: CGFloat = 3
    let spacingPerRow = (cellsPerRow - 1) * minimumItemSpacing
    let offset: CGFloat = 1
    let dimension = ((bounds.width - spacingPerRow) / cellsPerRow) - offset
    return CGSize(width: dimension, height: dimension)
  }

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumInteritemSpacingForSectionAt section: Int
  ) -> CGFloat {
    7
  }

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumLineSpacingForSectionAt section: Int
  ) -> CGFloat {
    7
  }
}

// MARK: - UICollectionViewDragDelegate

extension PhotoSelectionView: UICollectionViewDragDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    itemsForBeginning session: UIDragSession,
    at indexPath: IndexPath
  ) -> [UIDragItem] {
    if viewModel.canAddMore && indexPath.row == 0 {
      return []
    }

    let itemProvider = NSItemProvider(object: "\(indexPath)" as NSString)
    let dragItem = UIDragItem(itemProvider: itemProvider)
    dragItem.localObject = viewModel.dataSource[adjustedIndex(at: indexPath)]
    return [dragItem]
  }

  func collectionView(
    _ collectionView: UICollectionView,
    dragPreviewParametersForItemAt indexPath: IndexPath
  ) -> UIDragPreviewParameters? {
    let previewParams = UIDragPreviewParameters()

    let cellSize = self.collectionView(
      collectionView,
      layout: collectionView.collectionViewLayout,
      sizeForItemAt: indexPath
    )
    let path = UIBezierPath(
      roundedRect: CGRect(origin: .zero, size: cellSize),
      cornerRadius: 8
    )

    previewParams.visiblePath = path

    return previewParams
  }
}

// MARK: - UICollectionViewDropDelegate

extension PhotoSelectionView: UICollectionViewDropDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    dropSessionDidUpdate session: UIDropSession,
    withDestinationIndexPath destinationIndexPath: IndexPath?
  ) -> UICollectionViewDropProposal {
    if collectionView.hasActiveDrag {
      return .init(operation: .move, intent: .insertAtDestinationIndexPath)
    }
    return .init(operation: .forbidden)
  }

  func collectionView(
    _ collectionView: UICollectionView,
    performDropWith coordinator: UICollectionViewDropCoordinator
  ) {
    var destinationIndexPath: IndexPath
    if let indexPath = coordinator.destinationIndexPath {
      destinationIndexPath = indexPath
    } else {
      let row = collectionView.numberOfItems(inSection: 0)
      destinationIndexPath = IndexPath(row: row - 1, section: 0)
    }

    if coordinator.proposal.operation == .move {
      reorderItems(
        coordinator: coordinator,
        destinationIndexPath: destinationIndexPath,
        collectionView: collectionView
      )
    }
  }

  private func reorderItems(
    coordinator: UICollectionViewDropCoordinator,
    destinationIndexPath: IndexPath,
    collectionView: UICollectionView
  ) {
    if viewModel.canAddMore && destinationIndexPath.row == 0 { return }

    guard let item = coordinator.items.first,
          let sourceIndexPath = item.sourceIndexPath else { return }
    collectionView.performBatchUpdates({
      self.viewModel.movePhoto(
        fromIndex: self.adjustedIndex(at: sourceIndexPath),
        toIndex: self.adjustedIndex(at: destinationIndexPath)
      )
      collectionView.deleteItems(at: [sourceIndexPath])
      collectionView.insertItems(at: [destinationIndexPath])
    })
    coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
  }
}
