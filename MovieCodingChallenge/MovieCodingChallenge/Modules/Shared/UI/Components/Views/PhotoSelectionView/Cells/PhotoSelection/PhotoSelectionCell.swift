//
//  PhotoSelectionCell.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import ImageViewer_swift

class PhotoSelectionCell: UICollectionViewCell {
  var viewModel: PhotoSelectionCellViewModelProtocol! {
    didSet {
      refresh()
    }
  }

  var onRemoveButtonTap: VoidResult?

  @IBOutlet private(set) var imageView: ImageView!
  @IBOutlet private(set) var removeButton: UIButton!
  
  override func awakeFromNib() {
    setup()
  }
}

// MARK: - Setup

private extension PhotoSelectionCell {
  func setup() {
    // noop
  }
}

// MARK: - Actions

private extension PhotoSelectionCell {
  @IBAction func removeButtonTapped(_ sender: Any) {
    onRemoveButtonTap?()
  }
}

// MARK: - Refresh

private extension PhotoSelectionCell {
  func refresh() {
    if let imageData = viewModel.imageData {
      refresh(with: imageData)
    } else if let imageURL = viewModel.imageURL {
      refresh(with: imageURL)
    }
  }

  func refresh(with imageData: Data) {
    guard let image = UIImage(data: imageData) else { return }

    imageView.image = image

    // TODO: Extract logic outside of this cell
    imageView.setupImageViewer(images: [image])
  }

  func refresh(with imageURL: URL) {
    imageView.setImageWithURL(
      imageURL,
      onSuccess: handleSetImageWithURLSuccess()
    )

    // TODO: Extract logic outside of this cell
    imageView.setupImageViewer(url: imageURL)
  }
}

// MARK: - Handlers

private extension PhotoSelectionCell {
  func handleSetImageWithURLSuccess() -> SingleResult<UIImage> {
    return { [weak self] image in
      guard
        let self = self,
        let imageData = image.looselyCompressedJpegData
      else { return }

      self.viewModel.setImageData(imageData)
    }
  }
}
