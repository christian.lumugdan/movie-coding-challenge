//
//  VerificationCodeField.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents.MaterialTextFields
import UIKit

protocol VerificationCodeFieldDelegate: AnyObject {
  /// This delegate method will be called when a backspace key was pressed regardless if there's a
  /// text or not.
  ///
  /// The other delegate method that's called before this
  /// method is `textField:shouldChangeCharactersIn:replacementString:`.
  ///
  /// There's also a discussion about this in SO:
  /// - https://stackoverflow.com/questions/1977934/detect-backspace-in-empty-uitextfield
  ///
  func verificationCodeFieldDidDeleteBackward(_ codeField: VerificationCodeField, oldText: String?)
}

class VerificationCodeField: APTextField {
  weak var fieldDelegate: VerificationCodeFieldDelegate?

  override func deleteBackward() {
    let oldText = text

    // This implicitly calls the method `textField:shouldChangeCharactersIn:replacementString:`.
    super.deleteBackward()

    fieldDelegate?.verificationCodeFieldDidDeleteBackward(self, oldText: oldText)
  }
}
