//
//  APDateField.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import MaterialComponents.MaterialTextFields
import NKVPhonePicker
import RxCocoa
import RxSwift

class APDateField: APTextField {
  @IBInspectable
  var dateTextFormat: String = defaultDateFormat

  private(set) var datePicker: UIDatePicker!
  private(set) var doneButton: UIBarButtonItem!

  convenience init() {
    self.init(frame: .zero)
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    onInit()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
    onInit()
  }

  private func onInit() {
    setup()
    bind()
  }
}

// MARK: - Methods

extension APDateField {
  func setupFromDateText(_ dateText: String) {
    let df = dateFormatter
    df.dateFormat = dateTextFormat

    guard let date = df.date(from: dateText) else { return }
    datePicker.date = date
    text = dateText
  }
  
  func setupFromDate(_ date: Date) {
    let df = dateFormatter
    df.dateFormat = dateTextFormat
    
    let dateText = df.string(from: date)
    datePicker.date = date
    text = dateText
  }
}

// MARK: - Setup

private extension APDateField {
  func setup() {
    setupInputView()
    setupInputAccessoryView()
  }

  func setupInputView() {
    datePicker = UIDatePicker()
    datePicker.datePickerMode = .date
    datePicker.preferredDatePickerStyle = .wheels
    inputView = datePicker
  }

  func setupInputAccessoryView() {
    let toolBarSize = CGSize(width: 0, height: 40)
    let toolBarFrame = CGRect(origin: .zero, size: toolBarSize)
    let toolbar = UIToolbar(frame: toolBarFrame)

    doneButton = UIBarButtonItem(
      barButtonSystemItem: .done,
      target: nil,
      action: nil
    )

    let space = UIBarButtonItem(
      barButtonSystemItem: .flexibleSpace,
      target: nil,
      action: nil
    )

    toolbar.setItems([space, doneButton], animated: false)
    inputAccessoryView = toolbar
  }
}

// MARK: - Bind

private extension APDateField {
  func bind() {
    bindDatePicker()
    bindDoneButton()
  }

  func bindDatePicker() {
    datePicker.rx.date
      .skip(1)
      .withLatestFrom(
        Observable.just(dateFormatter),
        resultSelector: { $1.string(from: $0) }
      )
      .bind(to: rx.text)
      .disposed(by: rx.disposeBag)
  }

  func bindDoneButton() {
    doneButton.rx.tap
      .bind(onNext: handleDoneButtonTap())
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Handlers

private extension APDateField {
  func handleDoneButtonTap() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.resignFirstResponder()
    }
  }
}

// MARK: - Getters

private extension APDateField {
  static var defaultDateFormat: String {
    DateFormatter.dateFormat(
      fromTemplate: "yyyyMMdd",
      options: 0,
      locale: .current
    ) ?? "dd/MM/yyyy"
  }

  var dateFormatter: DateFormatter {
    let df = DateFormatter()
    df.timeZone = .utc
    df.dateFormat = dateTextFormat
    return df
  }
}
