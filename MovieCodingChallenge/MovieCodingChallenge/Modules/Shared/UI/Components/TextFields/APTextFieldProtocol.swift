//
//  APTextFieldProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import MaterialComponents.MDCTextField

protocol APTextFieldProtocol {
  var inputController: MDCTextInputControllerBase? { get set }

  func setErrorText(_ errorText: String?)
  
  func reset()
}

// MARK: - Defaults

extension APTextFieldProtocol {
  func setErrorText(_ errorText: String?) {
    inputController?.setErrorText(errorText, errorAccessibilityValue: nil)
  }
  
  func reset() {
    setErrorText(nil)
  }
}
