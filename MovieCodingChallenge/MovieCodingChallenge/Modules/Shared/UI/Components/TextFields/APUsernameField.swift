//
//  APUsernameField.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents.MaterialTextFields
import PhoneNumberKit
import PureLayout
import UIKit

class APUsernameField: APTextField {
  private var donePhoneSuggestion = false
  private lazy var phoneCountryCodePrefix: String = {
    let regionCode = Locale.current.regionCode ?? ""
    let countryCode = PhoneNumberKit().countryCode(for: regionCode)
    return S.countryCodeFormat(String(countryCode ?? 0))
  }()

  override func prepare() {
    super.prepare()

    keyboardType = .emailAddress
    placeholder = S.usernameFieldPlaceholder()
    autocorrectionType = .no
    clearButtonMode = .whileEditing

    setupDelegate()
  }

  private func setupDelegate() {
    rx
      .controlEvent([.editingChanged])
      .asObservable()
      .subscribe(onNext: handleTextChange())
      .disposed(by: rx.disposeBag)
  }

  func handleTextChange() -> VoidResult {
    return { [weak self] in
      guard let self = self, var text = self.text else { return }

      if !self.donePhoneSuggestion && text.isNumber {
        if Int(text) == 0 {
          text = ""
        }
        self.donePhoneSuggestion = true

        text = S.phoneNumberFormat(self.phoneCountryCodePrefix, text)
        let prefixRange = NSRange(location: 0, length: self.phoneCountryCodePrefix.count)

        self.text = text
        let attributedString = NSMutableAttributedString(attributedString: self.attributedText!)
        attributedString.addAttribute(
          NSAttributedString.Key.foregroundColor,
          value: R.color.primaryFull()!,
          range: prefixRange
        )

        self.attributedText = attributedString
      } else if self.donePhoneSuggestion {
        let originalText = String(text.dropFirst(self.phoneCountryCodePrefix.count))

        if originalText.count == 0 {
          self.text = ""
          self.donePhoneSuggestion = false
        } else if Int(originalText) == nil {
          self.text = originalText
          self.donePhoneSuggestion = false
        } else {          
          let attributedString = NSMutableAttributedString(attributedString: self.attributedText!)
          let prefixLength = self.phoneCountryCodePrefix.count
          let prefixRange = NSRange(location: prefixLength, length: text.count - prefixLength)
          attributedString.addAttribute(
            NSAttributedString.Key.foregroundColor,
            value: R.color.textOnLightRegular()!,
            range: prefixRange
          )
          self.attributedText = attributedString
        }
      }
    }
  }
}
