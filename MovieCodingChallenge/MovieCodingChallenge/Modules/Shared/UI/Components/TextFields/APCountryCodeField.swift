//
//  APCountryCodeField.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import MaterialComponents.MaterialTextFields
import NKVPhonePicker
import RxCocoa
import RxSwift

class APCountryCodeField: APTextField {
  weak var pickerDelegate: UIViewController? {
    didSet {
      countryCodePicker.phonePickerDelegate = pickerDelegate
    }
  }

  private var maskButton: UIButton!
  private var countryFlagImageContainerView: UIView!
  private var countryFlagImageView: UIImageView!
  private var countryCodePicker: NKVPhonePickerTextField!

  override func prepare() {
    super.prepare()

    setup()
    bind()
  }
}

// MARK: - Setup

private extension APCountryCodeField {
  func setup() {
    setupChevron()
    setupMaskButton()
    setupCountryFlagViews()
    setupCountryCodePickerField()
    setupLeftView()

    resetCountryCodeFlag()
  }

  func setupChevron() {
    let chevron = UIImageView(image: R.image.chevronDown()!)
    chevron.tintColor = R.color.form.textField.placeholderTextColor()!
    addSubview(chevron)

    chevron.autoSetDimensions(to: CGSize(width: 10, height: 7))
    chevron.autoPinEdge(toSuperviewEdge: .trailing)
    chevron.autoAlignAxis(toSuperviewAxis: .horizontal)
  }

  func setupMaskButton() {
    // Catch all touches intended for TextField and route them to FlagView to trigger
    // the presentation of the picker screen.
    let mb = UIButton()
    superview!.addSubview(mb)
    mb.autoMatch(.height, to: .height, of: self)
    mb.autoMatch(.width, to: .width, of: self)
    mb.autoAlignAxis(.horizontal, toSameAxisOf: self, withOffset: 0)
    mb.autoAlignAxis(.vertical, toSameAxisOf: self, withOffset: 0)
    maskButton = mb
  }

  func setupCountryFlagViews() {
    let sidePadding: CGFloat = 4
    let flagIconSize: CGSize = CGSize(width: 30, height: 20)

    // Image View
    let imageView = UIImageView()
    imageView.autoSetDimensions(to: flagIconSize)

    // Container
    let outerView = UIView()
    outerView.addSubview(imageView)

    let insets = UIEdgeInsets(
      top: 0,
      left: sidePadding,
      bottom: 0,
      right: sidePadding
    )
    imageView.autoPinEdgesToSuperviewEdges(with: insets)

    countryFlagImageContainerView = outerView
    countryFlagImageView = imageView
  }

  func setupCountryCodePickerField() {
    countryCodePicker = NKVPhonePickerTextField(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    countryCodePicker.keyboardType = .phonePad
    countryCodePicker.isHidden = true
    superview!.addSubview(countryCodePicker)
  }

  func setupLeftView() {
    leftView = countryFlagImageContainerView
    leftViewMode = .unlessEditing
  }
}

// MARK: - Bind

private extension APCountryCodeField {
  func bind() {
    bindMaskButton()
    bindPhonePicker()
  }

  func bindMaskButton() {
    maskButton.rx.tap
      .subscribe(onNext: handleMaskButtonTap())
      .disposed(by: rx.disposeBag)
  }

  func bindPhonePicker() {
    var initialCountryCode: Observable<String>!
    var updatedCountryCode: Observable<String>!

    initialCountryCode = countryCodePicker.rx
      .text.orEmpty
      .asObservable()

    updatedCountryCode = countryCodePicker.rx
      .methodInvoked(#selector(setter: NKVPhonePickerTextField.text))
      .compactMap { $0.first as? String }
      .share(replay: 1)

    Observable
      .merge(
        initialCountryCode,
        updatedCountryCode
      )
      .map { "+" + $0 }
      .bind(to: rx.text)
      .disposed(by: rx.disposeBag)

    updatedCountryCode
      .subscribe(onNext: handleCountryCodeChange())
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Handlers

private extension APCountryCodeField {
  func handleCountryCodeChange<T>() -> SingleResult<T> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.resetCountryCodeFlag()
    }
  }

  func handleMaskButtonTap() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.countryCodePicker.flagView.flagButton.sendActions(for: .touchUpInside)
    }
  }
}

// MARK: - Utils

private extension APCountryCodeField {
  func resetCountryCodeFlag() {
    guard let flagIcon = countryCodePicker.flagView.flagButton.imageView?.image else {
      return
    }
    countryFlagImageView.image = flagIcon
  }
}
