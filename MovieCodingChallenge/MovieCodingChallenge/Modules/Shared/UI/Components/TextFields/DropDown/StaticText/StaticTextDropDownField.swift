//
//  APDropDownField.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import DropDown
import MaterialComponents.MaterialTextFields

class StaticTextDropDownField: APTextField, APDropDownFieldProtocol {
  var viewModel: StaticTextDropDownFieldViewModelProtocol! {
    didSet {
      viewModel.onRefresh = trigger(type(of: self).refresh)
      setupVM()
    }
  }
  var onSelect: VoidResult?

  @IBInspectable var shouldAllowMultipleSelection: Bool = false {
    didSet { setupMultipleSelection() }
  }

  private(set) var dropDown: DropDown = .init()

  override func prepare() {
    super.prepare()

    setup()
    bind()
  }
}

// MARK: - APDropDownFieldProtocol

extension StaticTextDropDownField {
  var dropDownFieldVM: APDropDownFieldViewModelProtocol { viewModel }
}
