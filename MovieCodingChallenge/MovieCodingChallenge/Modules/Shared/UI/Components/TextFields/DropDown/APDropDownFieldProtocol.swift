//
//  APDropDownFieldProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import DropDown
import RxCocoa
import RxSwift

protocol APDropDownFieldProtocol where Self: APTextField {
  // MARK: Required

  var onSelect: VoidResult? { get set }
  var dropDown: DropDown { get } 
  var dropDownFieldVM: APDropDownFieldViewModelProtocol { get }

  // MARK: Customizable

  var customCellNib: UINib? { get }
  var customCellHeight: CGFloat? { get }
  var shouldAllowMultipleSelection: Bool { get }

  func handleCustomCellConfig() -> TripleResult<Index, String, DropDownCell>

  func handleSelection() -> DoubleResult<Index, String>
  func handleMultipleSelection() -> DoubleResult<[Index], [String]>

  func refresh()
}

// MARK: - Setup

extension APDropDownFieldProtocol {
  func setup() {
    setupDropDown()
    clearButtonMode = .always
  }

  func setupDropDown() {
    setupDropDownAnchor()
    
    dropDown.direction = .bottom
    dropDown.topOffset = .init(x: 0, y: bounds.height)

    dropDown.selectionAction = handleSelection()
    dropDown.cancelAction = handleCancel()

    if let customCellNib = customCellNib {
      dropDown.cellNib = customCellNib
      dropDown.customCellConfiguration = handleCustomCellConfig()
    }

    if let customCellHeight = customCellHeight {
      dropDown.cellHeight = customCellHeight
    }
  }

  func setupDropDownAnchor() {
    let anchorView = UIView()
    addSubview(anchorView)
    anchorView.autoPinEdge(toSuperviewEdge: .bottom)
    anchorView.autoPinEdge(toSuperviewEdge: .leading)
    anchorView.autoPinEdge(toSuperviewEdge: .trailing)
    dropDown.anchorView = anchorView
  }

  func setupVM() {
    bindVM()
    refresh()
  }

  func setupMultipleSelection() {
    if shouldAllowMultipleSelection {
      dropDown.multiSelectionAction = handleMultipleSelection()
    } else {
      dropDown.multiSelectionAction = nil
    }
  }
}

// MARK: - Bind

extension APDropDownFieldProtocol {
  func bind() {
    bindTextInput()
  }

  func bindTextInput() {
    rx.controlEvent(.editingDidBegin)
      .bind(onNext: handleTextFieldFocus())
      .disposed(by: rx.disposeBag)

    rx.controlEvent(.editingDidEnd)
      .bind(onNext: handleTextFieldUnfocus())
      .disposed(by: rx.disposeBag)

    rx.text.orEmpty
      .distinctUntilChanged()
      .debounce(.seconds(1), scheduler: MainScheduler.instance)
      .bind(onNext: handleDebouncedTextChange())
      .disposed(by: rx.disposeBag)

    rx.text.orEmpty
      .filter { $0.isEmpty }
      .skip(2)
      .mapToVoid()
      .bind(onNext: handleTextClear())
      .disposed(by: rx.disposeBag)

    clearButton.rx.tap
      .bind(onNext: handleClearButtonTap())
      .disposed(by: rx.disposeBag)
  }

  func bindVM() {
    Observable
      .of(
        rx.controlEvent(.editingDidBegin)
          .map { true },
        rx.controlEvent(.editingDidEnd)
          .map { false }
          .withLatestFrom(Observable.just(dropDownFieldVM))
          .map { $0.selectionText.isNilOrEmpty }
      )
      .merge()
      .bind(to: clearButton.rx.isHidden)
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Reset

extension APDropDownFieldProtocol {
  func reset() {
    clearText()
    dropDown.clearSelection()
    dropDownFieldVM.reset()
  }
}

// MARK: - Refresh

extension APDropDownFieldProtocol {
  func refresh() {
    dropDown.dataSource = dropDownFieldVM.dataSource
  }
}

// MARK: - Handlers

private extension APDropDownFieldProtocol {
  func handleTextFieldFocus() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.dropDown.show()
    }
  }

  func handleTextFieldUnfocus() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.text = self.dropDownFieldVM.selectionText
      self.dropDown.hide()
    }
  }

  func handleDebouncedTextChange() -> SingleResult<String> {
    return { [weak self] query in
      guard let self = self else { return }
      self.dropDownFieldVM.refresh(withQuery: query)
    }
  }

  func handleTextClear() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.dropDownFieldVM.reset()
    }
  }

  func handleClearButtonTap() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.becomeFirstResponder()
      self.dropDown.show()
    }
  }
}

// MARK: - Defaults

extension APDropDownFieldProtocol {
  var customCellNib: UINib? { nil }
  var customCellHeight: CGFloat? { nil }

  func handleCustomCellConfig() -> TripleResult<Index, String, DropDownCell> {
    return { _, _, _ in
      // noop
    }
  }

  func handleSelection() -> DoubleResult<Index, String> {
    return { [weak self] index, _ in
      guard let self = self else { return }
      self.dropDown.hide()
      self.dropDownFieldVM.selectItems(atIndices: [index])

      self.resignFirstResponder()

      self.text = self.dropDownFieldVM.selectionText
      self.onSelect?()
    }
  }

  func handleCancel() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      if self.dropDownFieldVM.selectionText.isNilOrEmpty {
        self.reset()
      }
      self.resignFirstResponder()
    }
  }

  func handleMultipleSelection() -> DoubleResult<[Index], [String]> {
    return { [weak self] indices, _ in
      guard let self = self else { return }
      self.dropDownFieldVM.selectItems(atIndices: indices)
    }
  }
}
