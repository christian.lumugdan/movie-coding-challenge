//
//  APDropDownFieldViewModelProtocol.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol APDropDownFieldViewModelProtocol {
  var onRefresh: VoidResult? { get set }
  
  var dataSource: [String] { get }
  var selectedIndices: [Int] { get }
  var selectionText: String? { get }
  
  func refresh(withQuery query: String)
  func selectItems(atIndices indices: [Int])
  func reset()
}
