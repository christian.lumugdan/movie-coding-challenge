//
//  APPasswordField.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents.MaterialTextFields
import PureLayout
import UIKit

class APPasswordField: APTextField {
  let rightButton = UIButton(type: .custom)

  override var isSecureTextEntry: Bool {
    willSet {
      let image = newValue ? R.image.iconEyeShow() : R.image.iconEyeHide()
      self.rightButton.setImage(image, for: .normal)
    }
  }

  override func prepare() {
    super.prepare()

    textContentType = .password
    keyboardType = .default
    placeholder = S.password()
    autocorrectionType = .no
    isSecureTextEntry = true
    clearButtonMode = .never

    setupRightButton()
  }

  private func setupRightButton() {
    rightButton.autoSetDimensions(to: CGSize(width: 32, height: 32))
    rightButton.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)

    rightView = rightButton
    rightViewMode = .always
  }

  @IBAction
  func rightButtonTapped(_ sender: AnyObject) {
    isSecureTextEntry.toggle()
  }
}
