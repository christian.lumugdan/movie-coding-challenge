//
//  LeftAlignedIconMDCButton.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents.MDCButton
import UIKit

@IBDesignable
class LeftAlignedIconMDCButton: MDCButton {
  override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
    let titleRect = super.titleRect(forContentRect: contentRect)
    let imageSize = currentImage?.size ?? .zero
    let availableWidth = contentRect.width - imageEdgeInsets.right - imageSize.width - titleRect.width
    return titleRect.offsetBy(dx: round(availableWidth / 2), dy: 0)
  }
}
