//
//  EmailAddressInputValidatorTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailAddressInputValidatorTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("EmailAddressInputValidator") {
      typealias Inputs = EmailAddressInputValidator.Inputs
      typealias ValidInputs = EmailAddressInputValidator.ValidInputs
      typealias ValidationError = EmailAddressInputValidator.ValidationError

      var inputs: EmailAddressInputValidator.Inputs!
      var expectedResult: Result<ValidInputs, ValidationError>!

      describe("ValidationError") {
        context("requiredEmail") {
          it("should have correct errorDescription") {
            let expectedValue = S.emailFieldErrorMissingValue()
            expect(ValidationError.requiredEmail.localizedDescription).to(equal(expectedValue))
          }
        }

        context("invalidEmail") {
          it("should have correct errorDescription") {
            let expectedValue = S.emailFieldErrorInvalidValue()
            expect(ValidationError.invalidEmail.localizedDescription).to(equal(expectedValue))
          }
        }
      }

      context("when validate method called with nil input") {
        beforeEach {
          inputs = Inputs(nil)
        }

        it("should return failure with requiredEmail error") {
          expectedResult = .failure(ValidationError.requiredEmail)

          let result = EmailAddressInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      context("when validate method called with empty input") {
        beforeEach {
          inputs = Inputs("")
        }

        it("should return failure with requiredEmail error") {
          expectedResult = .failure(ValidationError.requiredEmail)

          let result = EmailAddressInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      context("when validate method called with invalid email input") {
        beforeEach {
          inputs = Inputs("KJFNU(#MDNB")
        }

        it("should return failure with invalidEmail error") {
          expectedResult = .failure(ValidationError.invalidEmail)

          let result = EmailAddressInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      context("when validate method called with valid email input") {
        let testEmail = "hello@example.com"

        beforeEach {
          inputs = Inputs(testEmail)
        }

        it("should return success") {
          expectedResult = .success(testEmail)

          let result = EmailAddressInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      afterEach {
        inputs = nil
      }
    }
  }
}
