//
//  MockLogoutTriggerViewModel.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockLogoutTriggerViewModel: LogoutTriggerViewModelProtocol {
  private(set) var logoutCallCount: Int = 0

  func logout() {
    logoutCallCount += 1
  }
}
