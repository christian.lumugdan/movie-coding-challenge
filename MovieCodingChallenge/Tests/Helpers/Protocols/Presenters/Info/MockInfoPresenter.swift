//
//  MockInfoPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockInfoPresenter: InfoPresenterProtocol {
  private(set) var presentInfoCallCount: Int = 0
  private(set) var presentInfoValue: InfoProtocol?
  private(set) var presentInfoOnComplete: VoidResult?

  private(set) var presentSuccessInfoCallCount: Int = 0
  private(set) var presentSuccessInfoMessage: String?
  private(set) var presentSuccessInfoOnComplete: VoidResult?

  private(set) var presentErrorInfoCallCount: Int = 0
  private(set) var presentErrorInfoValue: Error?
  private(set) var presentErrorInfoOnComplete: VoidResult?

  private(set) var presentErrorMessageCallCount: Int = 0
  private(set) var presentErrorMessageValue: String?

  private(set) var dismissCallCount: Int = 0
  private(set) var dismissOnComplete: VoidResult?
}

// MARK: - Methods

extension MockInfoPresenter {
  func presentInfo(
    _ info: InfoProtocol,
    onComplete: @escaping VoidResult
  ) {
    presentInfoCallCount += 1
    presentInfoValue = info
    presentInfoOnComplete = onComplete
  }

  func presentSuccessInfo(message: String) {
    presentSuccessInfoCallCount += 1
    presentSuccessInfoMessage = message
  }

  func presentSuccessInfo(
    message: String,
    onComplete: @escaping VoidResult
  ) {
    presentSuccessInfoCallCount += 1
    presentSuccessInfoMessage = message
    presentSuccessInfoOnComplete = onComplete
  }

  func presentErrorInfo(error: Error) {
    presentErrorInfoCallCount += 1
    presentErrorInfoValue = error
  }

  func presentErrorMessage(message: String) {
    presentErrorMessageCallCount += 1
    presentErrorMessageValue = message
  }

  func presentErrorInfo(
    error: Error,
    onComplete: @escaping VoidResult
  ) {
    presentErrorInfoCallCount += 1
    presentErrorInfoValue = error
    presentErrorInfoOnComplete = onComplete
  }
}
