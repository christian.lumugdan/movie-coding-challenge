//
//  MockFacebookLoginPresenter.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import FBSDKLoginKit
@testable import MovieCodingChallenge

class MockFacebookLoginPresenter: FacebookLoginPresenterProtocol {
  var loginManagerProvider: EmptyResult<LoginManager>!
  var controllerProvider: EmptyResult<UIViewController>?

  var errorToReturn: Error?
  var shouldCancel: Bool = false

  private(set) var presentFacebookLoginCallCount: Int = 0

  func presentFacebookLogin(
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult,
    onCancel: @escaping VoidResult
  ) {
    presentFacebookLoginCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else if shouldCancel {
      onCancel()
    } else {
      onSuccess("")
    }
  }
}
