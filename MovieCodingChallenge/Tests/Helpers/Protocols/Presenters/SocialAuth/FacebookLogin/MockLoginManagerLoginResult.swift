//
//  MockLoginManagerLoginResult.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import FBSDKLoginKit

class MockLoginManagerLoginResult: LoginManagerLoginResult {
  init(
    mockToken: MockAccessToken = .init(),
    isCancelled: Bool = false
  ) {
    super.init(
      token: mockToken,
      isCancelled: isCancelled,
      grantedPermissions: Set(),
      declinedPermissions: Set()
    )
  }
}
