//
//  FacebookLoginPresenterTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class FacebookLoginPresenterTests: QuickSpec {
  override func spec() {
    describe("FacebookLoginPresenter") {
      var sut: FacebookLoginPresenter!
      var loginManager: MockLoginManager!
      var controller: UIViewController!

      beforeEach {
        loginManager = MockLoginManager()
        controller = UIViewController()

        sut = FacebookLoginPresenter()
        sut.loginManagerProvider = { loginManager }
        sut.controllerProvider = { controller }
      }

      afterEach {
        sut = nil
      }

      it("should access loginManagerProvider and controllerProvider on presentFacebookLogin") {
        var loginManagerProviderAccessed = false
        sut.loginManagerProvider = {
          loginManagerProviderAccessed = true
          return MockLoginManager()
        }
        var controllerProviderAccessed = false
        sut.controllerProvider = {
          controllerProviderAccessed = true
          return controller
        }

        sut.presentFacebookLogin(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult(),
          onCancel: DefaultClosure.voidResult()
        )

        expect(loginManagerProviderAccessed).toEventually(beTrue())
        expect(controllerProviderAccessed).toEventually(beTrue())
      }

      it("should pass correct permissions and fromController to loginManager on presentFacebookLogin") {
        sut.presentFacebookLogin(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult(),
          onCancel: DefaultClosure.voidResult()
        )

        expect(loginManager.logInPermissions).to(equal(["public_profile", "email"]))
        expect(loginManager.logInFromViewController).to(be(controller))
      }

      context("when initialized with successful loginManager") {
        var accessToken: MockAccessToken!

        beforeEach {
          accessToken = MockAccessToken(mockTokenString: "Token")
          loginManager.mockToken = accessToken
        }

        afterEach {
          accessToken = nil
        }

        it("should call onSuccess closure and pass expected token on presentFacebookLogin") {
          var onSuccessCalled = false
          var passedToken: String!

          sut.presentFacebookLogin(
            onSuccess: { token in
              onSuccessCalled = true
              passedToken = token
            },
            onError: DefaultClosure.singleResult(),
            onCancel: DefaultClosure.voidResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
          expect(passedToken).toEventually(equal("Token"))
        }
      }

      context("when initialized with failing loginManager") {
        var logInError: Error!

        beforeEach {
          logInError = NSError(domain: #function, code: 0, userInfo: nil)
          loginManager.errorToReturn = logInError
        }

        it("should call onError and pass expected logInError on presentFacebookLogin") {
          var onErrorCalled = false
          var passedError: Error!

          sut.presentFacebookLogin(
            onSuccess: DefaultClosure.singleResult(),
            onError: { error in
              onErrorCalled = true
              passedError = error
            },
            onCancel: DefaultClosure.voidResult()
          )

          expect(onErrorCalled).toEventually(beTrue())
          expect(passedError).toEventually(be(logInError))
        }
      }

      context("when initialized with cancelling loginManager") {
        beforeEach {
          loginManager.shouldCancel = true
        }

        it("should call onCancel on presentFacebookLogin") {
          var onCancelCalled = false

          sut.presentFacebookLogin(
            onSuccess: DefaultClosure.singleResult(),
            onError: DefaultClosure.singleResult(),
            onCancel: { onCancelCalled = true }
          )

          expect(onCancelCalled).toEventually(beTrue())
        }
      }
    }
  }
}
