//
//  MockGIDGoogleUser.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import GoogleSignIn

class MockGIDGoogleUser: GIDGoogleUser {
  var mockAuthentication: MockGIDAuthentication!

  override var authentication: GIDAuthentication! { mockAuthentication }
}

class MockGIDAuthentication: GIDAuthentication {
  var mockIdToken: String!

  override var idToken: String! { mockIdToken }
}
