//
//  GoogleSignInPresenterTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import GoogleSignIn
@testable import MovieCodingChallenge

class GoogleSignInPresenterTests: QuickSpec {
  override func spec() {
    describe("GoogleSignInPresenter") {
      var sut: GoogleSignInPresenter!

      beforeEach {
        sut = GoogleSignInPresenter()
      }

      afterEach {
        sut = nil
      }

      context("when sessionProvider returns nil") {
        beforeEach {
          sut.sessionProvider = { nil }
        }

        it("should call onError and pass .noSession error on presentGoogleSignIn") {
          var onErrorCalled = false
          var passedError: GoogleSignInPresenterError!
          sut.onError = { error in
            onErrorCalled = true
            passedError = error
          }

          sut.presentGoogleSignIn()

          expect(onErrorCalled).toEventually(beTrue())
          expect(passedError).toEventually(equal(.noSession))
        }
      }

      context("when signIn is assumed to fail") {
        var signInError: Error!

        beforeEach {
          signInError = NSError(domain: #function, code: 0, userInfo: nil)
        }

        it("should call onError and pass expected error when onSignInError is called") {
          var onErrorCalled = false
          var passedError: Error!
          sut.onError = { error in
            onErrorCalled = true
            guard case let .wrapped(error) = error else {
              fail("Expected GoogleSignInPresenterError.wrappedError, got something else")
              return
            }
            passedError = error
          }

          sut.onSignInError(signInError)

          expect(onErrorCalled).toEventually(beTrue())
          expect(passedError).toEventually(be(signInError))
        }
      }

      context("when signIn is assumed to succeed") {
        var authentication: MockGIDAuthentication!
        var user: MockGIDGoogleUser!

        beforeEach {
          authentication = MockGIDAuthentication()
          user = MockGIDGoogleUser()
          user.mockAuthentication = authentication
        }

        afterEach {
          authentication = nil
          user = nil
        }

        it("should call onSuccess and pass expected token when onSignInSuccess is called") {
          var onSuccessCalled = false
          var passedToken: String!
          sut.onSuccess = { token in
            onSuccessCalled = true
            passedToken = token
          }
          authentication.mockIdToken = "Token"

          sut.onSignInSuccess(user)

          expect(onSuccessCalled).toEventually(beTrue())
          expect(passedToken).toEventually(equal("Token"))
        }
      }
    }
  }
}
