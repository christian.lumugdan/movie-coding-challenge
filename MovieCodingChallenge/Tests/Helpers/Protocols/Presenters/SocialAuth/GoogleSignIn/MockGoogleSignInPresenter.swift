//
//  MockGoogleSignInPresenter.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

@testable import GoogleSignIn
@testable import MovieCodingChallenge

class MockGoogleSignInPresenter: NSObject, GoogleSignInPresenterProtocol {
  var onSignInSuccess: SingleResult<GIDGoogleUser>!
  var onSignInError: ErrorResult!

  var controllerProvider: EmptyResult<UIViewController>?
  var sessionProvider: EmptyResult<GIDSignIn?>!

  var onSuccess: SingleResult<String>?
  var onError: SingleResult<GoogleSignInPresenterError>?
  var onCancel: VoidResult?

  var errorToReturn: GoogleSignInPresenterError?
  var shouldCancel: Bool = false

  private(set) var presentGoogleSignInCallCount: Int = 0
}

// MARK: - Methods

extension MockGoogleSignInPresenter {
  func presentGoogleSignIn() {
    presentGoogleSignInCallCount += 1

    if let e = errorToReturn {
      onError?(e)
    } else if shouldCancel {
      onCancel?()
    } else {
      onSuccess?("")
    }
  }
}
