//
//  MockASAuthorizationController.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import AuthenticationServices
import Foundation

class MockASAuthorizationController: ASAuthorizationController {
  var errorToReturn: Error?

  private(set) var performRequestsCallCount: Int = 0

  init(dummy: Any? = nil) {
    let request = ASAuthorizationAppleIDProvider().createRequest()
    super.init(authorizationRequests: [request])
  }

  func reset() {
    errorToReturn = nil
    performRequestsCallCount = 0
  }

  override func performRequests() {
    performRequestsCallCount += 1

    if let e = errorToReturn {
      delegate?.authorizationController?(
        controller: self,
        didCompleteWithError: e
      )
    } else {
      // Can't test this part. Can't mock ASAuthorization
      //      delegate?.authorizationController?(
      //        controller: self,
      //        didCompleteWithAuthorization: // CANT MOCK THIS
      //      )
    }
  }
}
