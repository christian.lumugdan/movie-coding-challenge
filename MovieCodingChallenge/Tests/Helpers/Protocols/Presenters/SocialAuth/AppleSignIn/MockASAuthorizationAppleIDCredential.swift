//
//  MockASAuthorization.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import AuthenticationServices
import Foundation

class MockASAuthorizationAppleIDCredential: ASAuthorizationAppleIDCredential {
  var token: String!

  override var identityToken: Data? { token.data(using: .utf8) }

  init(dummy: Any? = nil) {
    super.init(coder: MockNSCoder())!
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func encode(with coder: NSCoder) {}
}
