//
//  MockAppleSignInPresenter.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import AuthenticationServices
import Foundation
import UIKit

@testable import MovieCodingChallenge

class MockAppleSignInPresenter: NSObject, AppleSignInPresenterProtocol {
  var onAuthorizationComplete: SingleResult<ASAuthorizationCredential>!
  var controllerProvider: EmptyResult<ASAuthorizationController>!
  var presentationAnchorProvider: EmptyResult<ASPresentationAnchor?>?

  var onSuccess: SingleResult<String>?
  var onError: ErrorResult?
  var onCancel: VoidResult?

  var errorToReturn: Error?
  var shouldCancel: Bool = false

  private(set) var presentAppleSignInCallCount: Int = 0
}

// MARK: - Methods

extension MockAppleSignInPresenter {
  func presentAppleSignIn() {
    presentAppleSignInCallCount += 1

    if let e = errorToReturn {
      onError?(e)
    } else if shouldCancel {
      onCancel?()
    } else {
      onSuccess?("")
    }
  }
}
