//
//  AppleSignInPresenterTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import AuthenticationServices
@testable import MovieCodingChallenge

class AppleSignInPresenterTests: QuickSpec {
  override func spec() {
    describe("AppleSignInPresenter") {
      var sut: AppleSignInPresenter!

      var credential: MockASAuthorizationAppleIDCredential!
      var controller: MockASAuthorizationController!

      beforeEach {
        credential = MockASAuthorizationAppleIDCredential()
        controller = MockASAuthorizationController()
        sut = AppleSignInPresenter()
      }

      afterEach {
        credential = nil
        controller = nil
        sut = nil
      }

      it("should return expected default authorizationController properties when controllerProvider is called") {
        let returnedController = sut.controllerProvider()

        expect(returnedController.authorizationRequests.count).to(equal(1))
        let returnedRequest = returnedController.authorizationRequests.first as! ASAuthorizationAppleIDRequest
        expect(returnedRequest.provider).to(beAKindOf(ASAuthorizationAppleIDProvider.self))
        expect(returnedRequest.requestedScopes).to(equal([.fullName, .email]))
      }

      context("when mock controllerProvider is assigned") {
        beforeEach {
          sut.controllerProvider = { controller }
        }

        it("should set controller.delegate and controller.presentationContextProvider to sut on presentAppleSignIn") {
          expect(controller.delegate).to(beNil())
          expect(controller.presentationContextProvider).to(beNil())

          sut.presentAppleSignIn()

          expect(controller.delegate).to(be(sut))
          expect(controller.presentationContextProvider).to(be(sut))
        }

        it("should call controller.performRequests on presentAppleSignIn") {
          expect(controller.performRequestsCallCount).to(equal(0))

          sut.presentAppleSignIn()

          expect(controller.performRequestsCallCount).to(equal(1))
        }

        it("should call presentationAnchorProvider and pass expected ASPresentationAnchor on presentAppleSignIn then controller.presentationContextProvider.presentationContextProvider(for:)") {
          var presentationAnchorProviderCalled = false
          let expectedAnchor = UIWindow()
          sut.presentationAnchorProvider = {
            presentationAnchorProviderCalled = true
            return expectedAnchor
          }

          sut.presentAppleSignIn()
          let returnedAnchor = controller.presentationContextProvider?.presentationAnchor(for: controller)

          expect(presentationAnchorProviderCalled).toEventually(beTrue())
          expect(expectedAnchor).to(be(returnedAnchor))
        }

        context("and assumed to have successful controller") {
          it("should call onSuccess and pass expected token when onAuthorizationComplete is called") {
            var onSuccessCalled = false
            var passedToken: String?
            sut.onSuccess = { token in
              onSuccessCalled = true
              passedToken = token
            }
            credential.token = "Token"

            sut.onAuthorizationComplete(credential)

            expect(onSuccessCalled).toEventually(beTrue())
            expect(passedToken).toEventually(equal("Token"))
          }
        }

        context("and initialized with failing controller") {
          var controllerError: Error?

          it("should call onError and pass expected error on presentAppleSignIn") {
            var onErrorCalled = false
            var passedError: Error?
            sut.onError = { error in
              onErrorCalled = true
              passedError = error
            }
            controllerError = NSError(domain: #function, code: 0, userInfo: nil)
            controller.errorToReturn = controllerError

            sut.presentAppleSignIn()

            expect(onErrorCalled).toEventually(beTrue())
            expect(passedError).toEventually(be(controllerError))
          }

          it("should call onCancel when canceled error is encountered on presentAppleSignIn") {
            var onCancelCalled = false
            sut.onCancel = {
              onCancelCalled = true
            }
            controller.errorToReturn = ASAuthorizationError(.canceled)

            sut.presentAppleSignIn()

            expect(onCancelCalled).toEventually(beTrue())
          }

          it("should call onCancel when unknown error is encountered on presentAppleSignIn") {
            var onCancelCalled = false
            sut.onCancel = {
              onCancelCalled = true
            }
            controller.errorToReturn = ASAuthorizationError(.unknown)

            sut.presentAppleSignIn()

            expect(onCancelCalled).toEventually(beTrue())
          }
        }
      }
    }
  }
}
