//
//  MockProgressPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

@testable import MovieCodingChallenge

class MockProgressPresenter: ProgressPresenterProtocol {
  private(set) var presentIndefiniteProgressCallCount: Int = 0
  private(set) var presentIndefiniteProgressMessage: String?
  private(set) var presentIndefiniteProgressSource: UIViewController?

  private(set) var presentProgressCallCount: Int = 0
  private(set) var presentProgressValue: Float?
  private(set) var presentProgressMessage: String?
  private(set) var presentProgressSource: UIViewController?

  private(set) var dismissCallCount: Int = 0
  private(set) var dismissOnComplete: VoidResult?
}

// MARK: - Methods

extension MockProgressPresenter {
  func presentIndefiniteProgress(
    from source: UIViewController
  ) {
    presentIndefiniteProgressCallCount += 1
    presentIndefiniteProgressSource = source
  }

  func presentIndefiniteProgress(
    message: String?,
    from source: UIViewController
  ) {
    presentIndefiniteProgressCallCount += 1
    presentIndefiniteProgressMessage = message
    presentIndefiniteProgressSource = source
  }

  func presentProgress(
    value: Float,
    from source: UIViewController
  ) {
    presentProgressCallCount += 1
    presentProgressValue = value
    presentProgressSource = source
  }

  func presentProgress(
    value: Float,
    message: String?,
    from source: UIViewController
  ) {
    presentProgressCallCount += 1
    presentProgressValue = value
    presentProgressMessage = message
    presentProgressSource = source
  }

  func dismiss() {
    dismissCallCount += 1
  }

  func dismiss(
    onComplete: @escaping VoidResult
  ) {
    dismissCallCount += 1
    dismissOnComplete = onComplete
  }
}
