//
//  MockDialogPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

@testable import MovieCodingChallenge

class MockDialogPresenter: DialogPresenterProtocol {
  private(set) var presentDialogCallCount: Int = 0
  private(set) var presentDialogValue: DialogProtocol?
  private(set) var presentDialogSource: UIViewController?
}

// MARK: - Methods

extension MockDialogPresenter {
  func presentDialog(
    _ dialog: DialogProtocol,
    from source: UIViewController
  ) {
    presentDialogCallCount += 1
    presentDialogValue = dialog
    presentDialogSource = source
  }
}
