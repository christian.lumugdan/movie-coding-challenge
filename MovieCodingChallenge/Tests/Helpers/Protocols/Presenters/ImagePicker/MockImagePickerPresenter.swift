//
//  MockImagePickerPresenter.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

@testable import MovieCodingChallenge

class MockImagePresenter: UIViewController, ImagePickerPresenter {
  var imagePicker: UIImagePickerController! = .init()

  private(set) var presentImagePickerOptionsCallCount: Int = 0

  func presentImagePickerOptions() {
    presentImagePickerOptionsCallCount += 1
  }
}
