//
//  MockAppDocumentPresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

@testable import MovieCodingChallenge

class MockAppDocumentPresenter: AppDocumentPresenterProtocol {
  private(set) var presentTermsOfServicePageCallCount: Int = 0
  private(set) var presentTermsOfServicePageController: UIViewController?

  private(set) var presentPrivacyPolicyPageCallCount: Int = 0
  private(set) var presentPrivacyPolicyPageController: UIViewController?
}

// MARK: - Methods

extension MockAppDocumentPresenter {
  func presentTermsOfServicePage(
    fromController controller: UIViewController
  ) {
    presentTermsOfServicePageCallCount += 1
    presentTermsOfServicePageController = controller
  }

  func presentPrivacyPolicyPage(
    fromController controller: UIViewController
  ) {
    presentPrivacyPolicyPageCallCount += 1
    presentPrivacyPolicyPageController = controller
  }
}
