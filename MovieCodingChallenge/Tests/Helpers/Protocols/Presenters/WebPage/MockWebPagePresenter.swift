//
//  MockWebPagePresenter.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

@testable import MovieCodingChallenge

class MockWebPagePresenter: WebPagePresenterProtocol {
  private(set) var presentWebPageCallCount: Int = 0
  private(set) var presentWebPageMetadata: WebPageMetadata?
  private(set) var presentWebPageController: UIViewController?
}

// MARK: - Methods

extension MockWebPagePresenter {
  func presentWebPage(
    withMetadata metadata: WebPageMetadata,
    fromController controller: UIViewController
  ) {
    presentWebPageCallCount += 1
    presentWebPageMetadata = metadata
    presentWebPageController = controller
  }
}
