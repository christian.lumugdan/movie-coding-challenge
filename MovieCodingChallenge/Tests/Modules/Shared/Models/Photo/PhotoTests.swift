//
//  PhotoTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhotoTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("Photo") {
      var sut: Photo!

      it("should be initializable") {
        sut = Photo()
      }

      afterEach {
        sut = nil
      }
    }
  }
}
