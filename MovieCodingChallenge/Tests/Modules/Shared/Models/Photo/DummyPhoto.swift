//
//  DummyAvatar.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

extension Photo {
  init(
    testURL: URL = .dummy,
    testThumbURL: URL = .dummyThumb
  ) {
    self.init(
      id: 0,
      url: testURL,
      thumbUrl: testThumbURL
    )
  }
}
