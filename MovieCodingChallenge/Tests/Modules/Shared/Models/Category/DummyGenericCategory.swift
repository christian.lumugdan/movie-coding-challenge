//
//  DummyGenericCategory.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

extension GenericCategory {
  init(
    testId: Int = 0,
    testLabel: String = ""
  ) {
    self.init(
      id: testId,
      label: testLabel
    )
  }
}
