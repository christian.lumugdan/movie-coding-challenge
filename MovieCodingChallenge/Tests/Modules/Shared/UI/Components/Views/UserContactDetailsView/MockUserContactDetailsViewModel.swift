//
//  MockUserContactDetailsViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockUserContactDetailsViewModel: UserContactDetailsViewModelProtocol {
  var imageURL: URL = .dummy
  var nameText: String = ""
  var emailText: String?
  var mobileNumberText: String?
}
