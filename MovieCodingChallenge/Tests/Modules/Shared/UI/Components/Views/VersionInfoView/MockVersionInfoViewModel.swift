//
//  MockVersionInfoViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockVersionInfoViewModel: VersionInfoViewModelProtocol {
  var versionText: String = ""
}
