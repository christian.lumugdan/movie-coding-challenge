//
//  MockPhotoSelectionViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockPhotoSelectionCellViewModel: PhotoSelectionCellViewModelProtocol {
  var id: Int?
  var imageData: Data?
  var imageURL: URL?

  private(set) var setImageDataCallCount: Int = 0
  private(set) var setImageDataValue: Data?
}

// MARK: - Methods

extension MockPhotoSelectionCellViewModel {
  func setImageData(_ imageData: Data) {
    setImageDataCallCount += 1
    setImageDataValue = imageData
  }
}
