//
//  MockPageContentViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

@testable import MovieCodingChallenge

class MockPageContentViewModel: PageContentViewModelProtocol {
  var title: String = ""
  var description: String = ""
  var image: UIImage = UIImage()

  var titleColor: UIColor = .clear
  var descriptionColor: UIColor = .clear
  var imageBackgroundColor: UIColor = .clear

  var imageContentMode: UIView.ContentMode = .bottom
  var imageClipsToBounds: Bool = false
}
