//
//  GenerateOTPViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class GenerateOTPViewModelTests: QuickSpec {
  override func spec() {
    describe("GenerateOTPViewModel") {
      var sut: GenerateOTPViewModel!
      var session: MockSessionService!
      var service: MockOTPService!

      beforeEach {
        let user = User(phoneNumber: "PhoneNumber")

        session = MockSessionService()
        session.user = user

        service = MockOTPService()
        sut = GenerateOTPViewModel(
          session: session,
          service: service
        )
      }

      afterEach {
        sut = nil
        session = nil
        service = nil
      }

      it("should pass session.user.phoneNumber and call service.generateOTP once on generateOTP") {
        expect(service.generateOTPCallCount).to(equal(0))
        expect(service.generateOTPPhoneNumber).to(beNil())

        sut.generateOTP(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.generateOTPCallCount).to(equal(1))
        expect(service.generateOTPPhoneNumber).to(equal("PhoneNumber"))
      }
    }
  }
}
