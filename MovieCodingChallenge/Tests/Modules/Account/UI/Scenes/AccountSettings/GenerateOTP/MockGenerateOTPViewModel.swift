//
//  MockGenerateOTPViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockGenerateOTPViewModel: GenerateOTPViewModelProtocol {
  var errorToReturn: Error?

  private(set) var generateOTPCallCount: Int = 0
}

// MARK: - Methods

extension MockGenerateOTPViewModel {
  func generateOTP(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    generateOTPCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
