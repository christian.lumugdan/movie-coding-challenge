//
//  EmailPrimaryDeleteAccountFlowViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailPrimaryDeleteAccountFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("EmailPrimaryDeleteAccountFlowViewModel") {
      var sut: EmailPrimaryDeleteAccountFlowViewModel!
      
      beforeEach {
        sut = EmailPrimaryDeleteAccountFlowViewModel()
      }
      
      afterEach {
        sut = nil
      }
      
      it("should return verifyPasswordVM with correct messageText") {
        let expectedText = S.verifyPasswordDeleteAccountMessage()
        expect(sut.verifyPasswordVM.messageText).to(equal(expectedText))
      }
    }
  }
}
