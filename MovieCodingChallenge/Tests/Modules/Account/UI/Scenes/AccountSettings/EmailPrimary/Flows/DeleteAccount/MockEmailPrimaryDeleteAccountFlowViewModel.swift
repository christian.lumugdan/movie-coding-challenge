//
//  MockDeleteAccountViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockEmailPrimaryDeleteAccountFlowViewModel: EmailPrimaryDeleteAccountFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol = MockVerifyPasswordViewModel()
  
  var errorToReturn: Error?

  private(set) var deleteAccountCallCount: Int = 0
  private(set) var deleteAccountToken: String?
}

// MARK: - Methods

extension MockEmailPrimaryDeleteAccountFlowViewModel {
  func deleteAccount(
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    deleteAccountCallCount += 1
    deleteAccountToken = token

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
