//
//  MockEmailPrimaryChangeEmailFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import MovieCodingChallenge

class MockEmailPrimaryChangeEmailFlowViewModel: EmailPrimaryChangeEmailFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol = MockVerifyPasswordViewModel()
  var changeEmailVM: ChangeEmailViewModelProtocol = MockChangeEmailViewModel()
  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol = MockVerifyChangeCredentialViewModel()
  
  var saveVerificationTokenCallCount: Int = 0
  var saveVerificationTokenValue: String?
  
  var saveEmailCallCount: Int = 0
  var saveEmailValue: String?
}

// MARK: - Methods

extension MockEmailPrimaryChangeEmailFlowViewModel {
  func save(verificationToken: String) {
    saveVerificationTokenCallCount += 1
    saveVerificationTokenValue = verificationToken
  }
  
  func save(email: String) {
    saveEmailCallCount += 1
    saveEmailValue = email
  }
}
