//
//  MockEmailPrimaryChangePhoneFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import MovieCodingChallenge

class MockEmailPrimaryChangePhoneFlowViewModel: EmailPrimaryChangePhoneFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol = MockVerifyPasswordViewModel()
  var changePhoneVM: ChangePhoneViewModelProtocol = MockChangePhoneViewModel()
  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol = MockVerifyChangeCredentialViewModel()
  
  var saveVerificationTokenCallCount: Int = 0
  var saveVerificationTokenValue: String?
  
  var saveCountryCodePhoneNumberCallCount: Int = 0
  var saveCountryCodeValue: String?
  var savePhoneNumberValue: String?
}

// MARK: - Methods

extension MockEmailPrimaryChangePhoneFlowViewModel {
  func save(verificationToken: String) {
    saveVerificationTokenCallCount += 1
    saveVerificationTokenValue = verificationToken
  }
  
  func save(
    countryCode: String,
    phoneNumber: String
  ) {
    saveCountryCodePhoneNumberCallCount += 1
    saveCountryCodeValue = countryCode
    savePhoneNumberValue = phoneNumber
  }
}
