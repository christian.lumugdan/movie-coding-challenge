//
//  EmailPrimaryChangePasswordFlowViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailPrimaryChangePasswordFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("EmailPrimaryChangePasswordFlowViewModel") {
      var sut: EmailPrimaryChangePasswordFlowViewModel!
      
      beforeEach {
        sut = EmailPrimaryChangePasswordFlowViewModel()
      }
      
      afterEach {
        sut = nil
      }
      
      it("should return verifyPasswordVM with correct messageText") {
        let expectedText = S.verifyPasswordChangePasswordMessage()
        expect(sut.verifyPasswordVM.messageText).to(equal(expectedText))
      }
    }
  }
}

