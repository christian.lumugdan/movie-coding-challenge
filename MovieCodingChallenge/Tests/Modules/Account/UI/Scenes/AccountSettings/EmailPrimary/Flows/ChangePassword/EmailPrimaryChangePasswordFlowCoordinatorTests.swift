//
//  EmailPrimaryChangePasswordFlowControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailPrimaryChangePasswordFlowCoordinatorTests: QuickSpec {
  override func spec() {
    describe("EmailPrimaryChangePasswordFlowCoordinator") {
      var sut: EmailPrimaryChangePasswordFlowCoordinator!
      var viewModel: MockEmailPrimaryChangePasswordFlowViewModel!
      var navigationController: MockNavigationController!

      beforeEach {
        navigationController = MockNavigationController(
          rootViewController: UIViewController()
        )

        viewModel = MockEmailPrimaryChangePasswordFlowViewModel()

        sut = EmailPrimaryChangePasswordFlowCoordinator(
          navigationController: navigationController
        )
        sut.viewModel = viewModel
      }

      afterEach {
        navigationController = nil
        viewModel = nil
        sut = nil
      }

      it("should push VerifyPasswordController as first page in flow on start") {
        expect(navigationController.viewControllers.count).to(equal(1))

        sut.start()

        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyPasswordController.self))
      }

      it("should call viewModel.save(password:) once on viewModel.verifyPasswordVM.onComplete") {
        expect(viewModel.savePasswordCallCount).to(equal(0))
        expect(viewModel.savePasswordValue).to(beNil())

        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")

        expect(viewModel.savePasswordCallCount).to(equal(1))
        expect(viewModel.savePasswordValue).to(equal("Password"))
      }

      it("should push ChangePasswordController as second page in flow on viewModel.verifyPasswordVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))

        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")

        expect(navigationController.viewControllers.count).to(equal(3))
        expect(navigationController.pushedViewController).to(beAKindOf(ChangePasswordController.self))
      }

      it("should pop to controller before the flow on viewModel.changePasswordVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))

        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        viewModel.changePasswordVM.onComplete?()

        expect(navigationController.viewControllers.count).to(equal(1))
      }
    }
  }
}
