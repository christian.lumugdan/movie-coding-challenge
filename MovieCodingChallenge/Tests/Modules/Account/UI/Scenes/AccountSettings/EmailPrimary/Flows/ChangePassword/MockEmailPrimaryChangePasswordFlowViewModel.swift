//
//  MockEmailPrimaryChangePasswordFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockEmailPrimaryChangePasswordFlowViewModel: EmailPrimaryChangePasswordFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol = MockVerifyPasswordViewModel()
  var changePasswordVM: ChangePasswordViewModelProtocol = MockChangePasswordViewModel()

  var savePasswordCallCount: Int = 0
  var savePasswordValue: String?
}

// MARK: - Methods

extension MockEmailPrimaryChangePasswordFlowViewModel {
  func save(password: String) {
    savePasswordCallCount += 1
    savePasswordValue = password
  }
}
