//
//  EmailPrimaryDeleteAccountFlowCoordinatorTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailPrimaryDeleteAccountFlowCoordinatorTests: QuickSpec {
  override func spec() {
    describe("EmailPrimaryDeleteAccountFlowCoordinator") {
      var sut: EmailPrimaryDeleteAccountFlowCoordinator!
      var viewModel: MockEmailPrimaryDeleteAccountFlowViewModel!
      var navigationController: MockNavigationController!
      
      beforeEach {
        navigationController = MockNavigationController(
          rootViewController: StaticTableViewController()
        )
        
        viewModel = MockEmailPrimaryDeleteAccountFlowViewModel()
        
        sut = EmailPrimaryDeleteAccountFlowCoordinator(
          navigationController: navigationController
        )
        sut.viewModel = viewModel
      }
      
      afterEach {
        navigationController = nil
        viewModel = nil
        sut = nil
      }
      
      it("should push VerifyPasswordController as first page in flow on start") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        
        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyPasswordController.self))
      }
    }
  }
}
