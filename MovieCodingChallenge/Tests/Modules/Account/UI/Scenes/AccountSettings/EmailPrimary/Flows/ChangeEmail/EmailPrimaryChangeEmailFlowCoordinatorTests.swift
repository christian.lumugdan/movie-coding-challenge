//
//  EmailPrimaryChangeEmailFlowCoordinatorTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailPrimaryChangeEmailFlowCoordinatorTests: QuickSpec {
  override func spec() {
    describe("EmailPrimaryChangeEmailFlowCoordinator") {
      var sut: EmailPrimaryChangeEmailFlowCoordinator!
      var viewModel: MockEmailPrimaryChangeEmailFlowViewModel!
      var navigationController: MockNavigationController!

      beforeEach {
        navigationController = MockNavigationController(
          rootViewController: UIViewController()
        )

        viewModel = MockEmailPrimaryChangeEmailFlowViewModel()

        sut = EmailPrimaryChangeEmailFlowCoordinator(
          navigationController: navigationController
        )
        sut.viewModel = viewModel
      }

      afterEach {
        navigationController = nil
        viewModel = nil
        sut = nil
      }

      it("should push VerifyPasswordController as first page in flow on start") {
        expect(navigationController.viewControllers.count).to(equal(1))

        sut.start()

        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyPasswordController.self))
      }

      it("should call viewModel.save(verificationToken:) once on viewModel.verifyPasswordVM.onComplete") {
        expect(viewModel.saveVerificationTokenCallCount).to(equal(0))
        expect(viewModel.saveVerificationTokenValue).to(beNil())

        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")

        expect(viewModel.saveVerificationTokenCallCount).to(equal(1))
        expect(viewModel.saveVerificationTokenValue).to(equal("Token"))
      }

      it("should push ChangeEmailController as second page in flow on viewModel.verifyPasswordVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))

        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")

        expect(navigationController.viewControllers.count).to(equal(3))
        expect(navigationController.pushedViewController).to(beAKindOf(ChangeEmailController.self))
      }

      it("should call viewModel.save(email:) once on viewModel.changeEmailVM.onComplete") {
        expect(viewModel.saveEmailCallCount).to(equal(0))
        expect(viewModel.saveEmailValue).to(beNil())

        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        viewModel.changeEmailVM.onComplete?("Email")

        expect(viewModel.saveEmailCallCount).to(equal(1))
        expect(viewModel.saveEmailValue).to(equal("Email"))
      }

      it("should push VerifyChangeCredentialController as third page in flow on viewModel.changeEmailVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))

        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        viewModel.changeEmailVM.onComplete?("Email")

        expect(navigationController.viewControllers.count).to(equal(4))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyChangeCredentialController.self))
      }
      
      
      it("should pop to controller before the flow on viewModel.verifyChangeCredentialVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        viewModel.changeEmailVM.onComplete?("Email")
        viewModel.verifyChangeCredentialVM.onComplete?()
        
        expect(navigationController.viewControllers.count).to(equal(1))
      }
    }
  }
}
