//
//  EmailPrimaryChangeEmailFlowViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailPrimaryChangeEmailFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("EmailPrimaryChangeEmailFlowViewModel") {
      var sut: EmailPrimaryChangeEmailFlowViewModel!
      
      beforeEach {
        sut = EmailPrimaryChangeEmailFlowViewModel()
      }
      
      afterEach {
        sut = nil
      }
      
      it("should return verifyPasswordVM with correct messageText") {
        let expectedText = S.verifyPasswordChangeEmailMessage()
        expect(sut.verifyPasswordVM.messageText).to(equal(expectedText))
      }
      
      it("should return verifyChangeCredentialVM with correct type") {
        sut.save(verificationToken: "Token")
        sut.save(email: "Email")
        
        let expectedType = VerifyChangeEmailViewModel.self
        expect(sut.verifyChangeCredentialVM).to(beAKindOf(expectedType))
      }
    }
  }
}


