//
//  MockVerifyPasswordViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import MovieCodingChallenge

class MockVerifyPasswordViewModel: VerifyPasswordViewModelProtocol {
  var onComplete: DoubleResult<String, String>?
  
  var messageText: String = ""
  
  var errorToReturn: Error?
  
  private(set) var verifyPasswordValue: String?
  private(set) var verifyPasswordCallCount: Int = 0
}

// MARK: - Methods

extension MockVerifyPasswordViewModel {
  func verifyPassword(
    _ password: String?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    verifyPasswordValue = password
    verifyPasswordCallCount += 1
    
    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess("")
    }
  }
}
