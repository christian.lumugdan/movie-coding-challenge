//
//  VerifyPasswordViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class VerifyPasswordViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("VerifyPasswordViewModel") {
      var sut: VerifyPasswordViewModel!
      var service: MockCredentialsService!

      beforeEach {
        service = MockCredentialsService()
        sut = VerifyPasswordViewModel(
          messageText: "MessageText",
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }
      
      it("should use init messageText as messageText") {
        expect(sut.messageText).to(equal("MessageText"))
      }

      context("when valid password is used") {
        var validPassword: String!

        beforeEach {
          validPassword = "Password"
        }

        afterEach {
          validPassword = nil
        }

        it("should pass correct parameters and call service.requestVerificationToken on verifyPassword") {
          expect(service.requestVerificationTokenWithPasswordCallCount).to(equal(0))
          expect(service.requestVerificationTokenWithPasswordValue).to(beNil())

          sut.verifyPassword(
            validPassword,
            onSuccess: DefaultClosure.singleResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.requestVerificationTokenWithPasswordCallCount).to(equal(1))
          expect(service.requestVerificationTokenWithPasswordValue).to(equal("Password"))
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure once on verifyPassword") {
            var onSuccessCallCount = 0
            var onCompleteCallCount = 0
            sut.onComplete = { _, _ in
              onCompleteCallCount += 1
            }

            sut.verifyPassword(
              validPassword,
              onSuccess: { _ in onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
            expect(onCompleteCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            service.errorToReturn = expectedError
          }

          it("should pass service error and call onError closure once on verifyPassword") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.verifyPassword(
              validPassword,
              onSuccess: DefaultClosure.singleResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }

      context("when invalid password is used") {
        var invalidPassword: String!

        beforeEach {
          invalidPassword = ""
        }

        afterEach {
          invalidPassword = nil
        }

        it("should pass back PasswordInputValidator.ValidationError to onError closure once on verifyPassword") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.verifyPassword(
            invalidPassword,
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(beAKindOf(PasswordInputValidator.ValidationError.self))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
