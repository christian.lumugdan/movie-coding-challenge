//
//  ChangePhoneControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class ChangePhoneControllerTests: QuickSpec {
  override func spec() {
    describe("ChangePhoneController") {
      var sut: ChangePhoneController!
      var viewModel: MockChangePhoneViewModel!
      
      beforeEach {
        viewModel = MockChangePhoneViewModel()
        
        sut = R.storyboard.accountSettings.changePhoneController()!
        sut.viewModel = viewModel
      }
      
      afterEach {
        sut = nil
        viewModel = nil
      }
      
      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }
        
        it("should have non-nil outlets") {
          expect(sut.field).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
        }
        
        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }
        
        it("should return shouldSetNavBarTransparent equal to true") {
          expect(sut.shouldSetNavBarTransparent).to(beTrue())
        }
        
        it("should return singleFormInputVM equal to viewModel instance") {
          expect(sut.singleFormInputVM).to(be(viewModel))
        }
        
        it("should pass field.text and call viewModel.changePhone once on tap of continueButton") {
          expect(viewModel.changePhoneCallCount).to(equal(0))
          expect(viewModel.changePhoneValue).to(beNil())
          
          sut.field.text = "NewPhone"
          sut.continueButton.tap()
          
          expect(viewModel.changePhoneCallCount).to(equal(1))
          expect(viewModel.changePhoneValue).to(equal("NewPhone"))
        }
      }
    }
  }
}
