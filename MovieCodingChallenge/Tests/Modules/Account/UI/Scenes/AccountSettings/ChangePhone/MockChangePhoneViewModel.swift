//
//  MockChangePhoneViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockChangePhoneViewModel: ChangePhoneViewModelProtocol {
  var onComplete: SingleResult<String>?

  var errorToReturn: Error?

  private(set) var changePhoneValue: String?
  private(set) var changePhoneCallCount: Int = 0
}

// MARK: - Methods

extension MockChangePhoneViewModel {
  func changePhone(
    phoneNumber: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    changePhoneValue = phoneNumber
    changePhoneCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
