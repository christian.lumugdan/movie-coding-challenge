//
//  ChangePhoneViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class ChangePhoneViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("ChangePhoneViewModel") {
      var sut: ChangePhoneViewModel!
      var service: MockCredentialsService!

      beforeEach {
        service = MockCredentialsService()
        sut = ChangePhoneViewModel(
          token: "ChangePhoneToken",
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      context("when valid email is used") {
        var validPhoneNumber: String!

        beforeEach {
          validPhoneNumber = "+639123456789"
        }

        afterEach {
          validPhoneNumber = nil
        }

        it("should pass correct parameters and call service.requestChangePhone on changePhone") {
          expect(service.requestChangePhoneNumberCallCount).to(equal(0))
          expect(service.requestChangePhoneNumberValue).to(beNil())
          expect(service.requestChangePhoneNumberToken).to(beNil())

          sut.changePhone(
            phoneNumber: validPhoneNumber,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.requestChangePhoneNumberCallCount).to(equal(1))
          expect(service.requestChangePhoneNumberValue).to(equal(validPhoneNumber))
          expect(service.requestChangePhoneNumberToken).to(equal("ChangePhoneToken"))
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure once on changePhone") {
            var onSuccessCallCount = 0
            var onCompleteCallCount = 0
            sut.onComplete = { _ in
              onCompleteCallCount += 1
            }

            sut.changePhone(
              phoneNumber: validPhoneNumber,
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
            expect(onCompleteCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            service.errorToReturn = expectedError
          }

          it("should pass service error and call onError closure once on changePhone") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.changePhone(
              phoneNumber: validPhoneNumber,
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }

      context("when invalid email is used") {
        var invalidPhoneNumber: String!

        beforeEach {
          invalidPhoneNumber = ""
        }

        afterEach {
          invalidPhoneNumber = nil
        }

        it("should pass back PasswordInputValidator.ValidationError to onError closure once on changePhone") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.changePhone(
            phoneNumber: invalidPhoneNumber,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(beAKindOf(PhoneNumberInputValidator.ValidationError.self))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
