//
//  ChangePasswordViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class ChangePasswordViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("ChangePasswordViewModel") {
      var sut: ChangePasswordViewModel!
      var service: MockCredentialsService!
      var oldPassword: String!

      beforeEach {
        oldPassword = "OldPassword"
        service = MockCredentialsService()
        sut = ChangePasswordViewModel(
          password: oldPassword,
          service: service
        )
      }

      afterEach {
        oldPassword = nil
        service = nil
        sut = nil
      }

      context("when valid new password is used") {
        var validNewPassword: String!

        beforeEach {
          validNewPassword = "NewPassword"
        }

        afterEach {
          validNewPassword = nil
        }

        it("should pass correct parameters and call service.changePassword on changePassword") {
          expect(service.changePasswordCallCount).to(equal(0))
          expect(service.changePasswordNewValue).to(beNil())
          expect(service.changePasswordOldValue).to(beNil())

          sut.changePassword(
            with: validNewPassword,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.changePasswordCallCount).to(equal(1))
          expect(service.changePasswordNewValue).to(equal(validNewPassword))
          expect(service.changePasswordOldValue).to(equal(oldPassword))
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess and onComplete closures once on changePassword") {
            var onSuccessCallCount = 0
            var onCompleteCallCount = 0
            sut.onComplete = {
              onCompleteCallCount += 1
            }

            sut.changePassword(
              with: validNewPassword,
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
            expect(onCompleteCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            service.errorToReturn = expectedError
          }

          it("should pass service error and call onError closure once on changePassword") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.changePassword(
              with: validNewPassword,
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }

      context("when invalid email is used") {
        var invalidNewPassword: String!

        beforeEach {
          invalidNewPassword = ""
        }

        afterEach {
          invalidNewPassword = nil
        }

        it("should pass back PasswordInputValidator.ValidationError to onError closure once on changePassword") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.changePassword(
            with: invalidNewPassword,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(beAKindOf(NewPasswordInputValidator.ValidationError.self))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
