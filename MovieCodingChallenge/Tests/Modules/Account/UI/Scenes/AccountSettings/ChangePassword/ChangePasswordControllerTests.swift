//
//  ChangePasswordControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class ChangePasswordControllerTests: QuickSpec {
  override func spec() {
    describe("ChangePasswordController") {
      var sut: ChangePasswordController!
      var viewModel: MockChangePasswordViewModel!
      
      beforeEach {
        viewModel = MockChangePasswordViewModel()
        
        sut = R.storyboard.accountSettings.changePasswordController()!
        sut.viewModel = viewModel
      }
      
      afterEach {
        sut = nil
        viewModel = nil
      }
      
      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }
        
        it("should have non-nil outlets") {
          expect(sut.passwordField).toNot(beNil())
          expect(sut.field).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
        }
        
        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }
        
        it("should return shouldSetNavBarTransparent equal to true") {
          expect(sut.shouldSetNavBarTransparent).to(beTrue())
        }
        
        it("should return singleFormInputVM equal to viewModel instance") {
          expect(sut.singleFormInputVM).to(be(viewModel))
        }
        
        it("should pass field.text and call viewModel.changePassword once on tap of continueButton") {
          expect(viewModel.changePasswordCallCount).to(equal(0))
          expect(viewModel.changePasswordValue).to(beNil())
          
          sut.field.text = "NewPassword"
          sut.continueButton.tap()
          
          expect(viewModel.changePasswordCallCount).to(equal(1))
          expect(viewModel.changePasswordValue).to(equal("NewPassword"))
        }
      }
    }
  }
}
