//
//  MockChangePasswordViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import MovieCodingChallenge

class MockChangePasswordViewModel: ChangePasswordViewModelProtocol {
  var onComplete: VoidResult?
  
  var titleText: String = ""
  var messageText: String = ""
  
  var errorToReturn: Error?
  
  private(set) var changePasswordValue: String?
  private(set) var changePasswordCallCount: Int = 0
}

// MARK: - Methods

extension MockChangePasswordViewModel {
  func changePassword(
    with newPassword: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    changePasswordValue = newPassword
    changePasswordCallCount += 1
    
    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
