//
//  AccountControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class AccountControllerTests: QuickSpec {
  override func spec() {
    describe("AccountController") {
      var sut: AccountController!
      var viewModel: MockAccountViewModel!

      beforeEach {
        viewModel = MockAccountViewModel()
        viewModel.fullNameText = "FullNameText"
        viewModel.usernameText = "christian.lumugdanText"

        sut = R.storyboard.account.accountController()!
        sut.viewModel = viewModel
      }

      afterEach {
        sut = nil
        viewModel = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.profileImageView).toNot(beNil())
          expect(sut.fullnameLabel).toNot(beNil())
          expect(sut.usernameLabel).toNot(beNil())
        }
      }

      context("when view is loaded and will appear") {
        beforeEach {
          sut.loadViewIfNeeded()
          sut.viewWillAppear(false)
        }

        it("set fullnameLabel.text to viewModel.fullNameText") {
          expect(sut.fullnameLabel.text).to(equal("FullNameText"))
        }

        it("set usernameLabel.text to viewModel.userNameText") {
          expect(sut.usernameLabel.text).to(equal("christian.lumugdanText"))
        }
      }

      context("when initialized in nav flow") {
        var nc: MockNavigationController!

        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)

          sut.loadViewIfNeeded()
        }

        afterEach {
          nc = nil
        }

        it("should present EditProfileController on tap of section 0 row 0") {
          let currentCallCount = nc.pushViewControllerCallCount

          sut.tableView.tap(row: 0)

          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
          expect(nc.pushedViewController).to(beAKindOf(EditProfileController.self))
        }

        it("should present AboutController on tap of section 0 row 1") {
          let currentCallCount = nc.pushViewControllerCallCount

          sut.tableView.delegate?.tableView?(
            sut.tableView,
            didSelectRowAt: IndexPath(row: 1, section: 0)
          )

          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
          expect(nc.pushedViewController).to(beAKindOf(AboutController.self))
        }

        it("should present EmailPrimaryAccountSettingsController on tap of section 0 row 2") {
          let currentCallCount = nc.pushViewControllerCallCount

          sut.tableView.delegate?.tableView?(
            sut.tableView,
            didSelectRowAt: IndexPath(row: 2, section: 0)
          )

          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
          expect(nc.pushedViewController).to(beAKindOf(EmailPrimaryAccountSettingsController.self))
        }
      }
    }
  }
}
