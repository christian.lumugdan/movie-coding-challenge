//
//  VerifyChangeCredentialControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class VerifyChangeCredentialControllerTests: QuickSpec {
  override func spec() {
    describe("VerifyChangeCredentialController") {
      var sut: VerifyChangeCredentialController!
      var viewModel: MockVerifyChangeCredentialViewModel!
      
      beforeEach {
        viewModel = MockVerifyChangeCredentialViewModel()
        
        sut = R.storyboard.accountSettings.verifyChangeCredentialController()!
        sut.viewModel = viewModel
      }
      
      afterEach {
        sut = nil
        viewModel = nil
      }
      
      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }
        
        it("should have non-nil outlets") {
          expect(sut.titleLabel).toNot(beNil())
          expect(sut.messageLabel).toNot(beNil())
          expect(sut.subMessageLabel).toNot(beNil())
          expect(sut.resendButtonView).toNot(beNil())
          expect(sut.textFields).toNot(beNil())
        }
        
        it("should have correct textFields and inputControllers count") {
          expect(sut.textFields.count).to(equal(5))
          expect(sut.inputControllers.count).to(equal(5))
        }
        
        it("should return shouldSetNavBarTransparent equal to true") {
          expect(sut.shouldSetNavBarTransparent).to(beTrue())
        }
        
        it("should pass call viewModel.resendCode once on tap of resendCodeButton") {
          expect(viewModel.resendCodeCallCount).to(equal(0))
          
          sut.resendButtonView.button.tap()
          
          expect(viewModel.resendCodeCallCount).to(equal(1))
        }
        
        it("should pass textFields texts and call viewModel.verify when all fields are filled") {
          expect(viewModel.verifyCallCount).to(equal(0))
          expect(viewModel.verifyToken).to(beNil())
          
          sut.textFields.forEach { $0.setText("0") }
          
          expect(viewModel.verifyCallCount).toEventually(equal(1))
          expect(viewModel.verifyToken).toEventually(equal("00000"))
        }
      }
    }
  }
}
