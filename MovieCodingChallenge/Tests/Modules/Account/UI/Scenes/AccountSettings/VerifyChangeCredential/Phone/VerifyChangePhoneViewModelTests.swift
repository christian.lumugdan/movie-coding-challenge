//
//  VerifyChangePhoneViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class VerifyChangePhoneViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("VerifyChangePhoneViewModel") {
      var sut: VerifyChangePhoneViewModel!
      var service: MockCredentialsService!

      var phoneNumber: String!
      var verificationToken: String!

      beforeEach {
        phoneNumber = "+639123456789"
        verificationToken = "VerificationToken"

        service = MockCredentialsService()
        sut = VerifyChangePhoneViewModel(
          phoneNumber: phoneNumber,
          verificationToken: verificationToken,
          service: service
        )
      }

      afterEach {
        phoneNumber = nil
        verificationToken = nil
        service = nil
        sut = nil
      }

      it("should pass correct parameters and call service.verifyChangePhoneNumber on verify") {
        expect(service.verifyChangePhoneNumberCallCount).to(equal(0))
        expect(service.verifyChangePhoneNumberToken).to(beNil())
        expect(service.verifyChangePhoneNumberVerificationToken).to(beNil())

        sut.verify(
          using: "Token",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.verifyChangePhoneNumberCallCount).to(equal(1))
        expect(service.verifyChangePhoneNumberToken).to(equal("Token"))
        expect(service.verifyChangePhoneNumberVerificationToken).to(equal(verificationToken))
      }

      it("should pass correct parameters and call service.requestChangePhoneNumber on resendCode") {
        expect(service.requestChangePhoneNumberCallCount).to(equal(0))
        expect(service.requestChangePhoneNumberToken).to(beNil())
        expect(service.requestChangePhoneNumberToken).to(beNil())

        sut.resendCode(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.requestChangePhoneNumberCallCount).to(equal(1))
        expect(service.requestChangePhoneNumberValue).to(equal(phoneNumber))
        expect(service.requestChangePhoneNumberToken).to(equal(verificationToken))
      }

      context("and service is set to succeed") {
        beforeEach {
          service.errorToReturn = nil
        }

        it("should call onSuccess and onComplete closures once on verify") {
          var onSuccessCallCount = 0
          var onCompleteCallCount = 0
          sut.onComplete = {
            onCompleteCallCount += 1
          }

          sut.verify(
            using: "Token",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
          expect(onCompleteCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on resendCode") {
          var onSuccessCallCount = 0

          sut.resendCode(
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("and service is set to fail") {
        let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          service.errorToReturn = expectedError
        }

        it("should pass service error and call onError closure once on verify") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.verify(
            using: "Token",
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
