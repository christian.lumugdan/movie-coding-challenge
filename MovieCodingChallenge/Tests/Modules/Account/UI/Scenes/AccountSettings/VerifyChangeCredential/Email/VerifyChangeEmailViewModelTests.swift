//
//  VerifyChangeEmailViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class VerifyChangeEmailViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("VerifyChangeEmailViewModel") {
      var sut: VerifyChangeEmailViewModel!
      var service: MockCredentialsService!

      var email: String!
      var verificationToken: String!

      beforeEach {
        email = "Email"
        verificationToken = "VerificationToken"

        service = MockCredentialsService()
        sut = VerifyChangeEmailViewModel(
          email: email,
          verificationToken: verificationToken,
          service: service
        )
      }

      afterEach {
        email = nil
        verificationToken = nil
        service = nil
        sut = nil
      }

      it("should pass correct parameters and call service.verifyChangeEmail on verify") {
        expect(service.verifyChangeEmailCallCount).to(equal(0))
        expect(service.verifyChangeEmailToken).to(beNil())
        expect(service.verifyChangeEmailVerificationToken).to(beNil())

        sut.verify(
          using: "Token",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.verifyChangeEmailCallCount).to(equal(1))
        expect(service.verifyChangeEmailToken).to(equal("Token"))
        expect(service.verifyChangeEmailVerificationToken).to(equal(verificationToken))
      }

      it("should pass correct parameters and call service.requestChangeEmail on resendCode") {
        expect(service.requestChangeEmailCallCount).to(equal(0))
        expect(service.requestChangeEmailToken).to(beNil())
        expect(service.requestChangeEmailToken).to(beNil())

        sut.resendCode(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.requestChangeEmailCallCount).to(equal(1))
        expect(service.requestChangeEmailValue).to(equal(email))
        expect(service.requestChangeEmailToken).to(equal(verificationToken))
      }

      context("and service is set to succeed") {
        beforeEach {
          service.errorToReturn = nil
        }

        it("should call onSuccess and onComplete closures once on verify") {
          var onSuccessCallCount = 0
          var onCompleteCallCount = 0
          sut.onComplete = {
            onCompleteCallCount += 1
          }

          sut.verify(
            using: "Token",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
          expect(onCompleteCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on resendCode") {
          var onSuccessCallCount = 0

          sut.resendCode(
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("and service is set to fail") {
        let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          service.errorToReturn = expectedError
        }

        it("should pass service error and call onError closure once on verify") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.verify(
            using: "Token",
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
