//
//  ChangeEmailControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class ChangeEmailControllerTests: QuickSpec {
  override func spec() {
    describe("ChangeEmailController") {
      var sut: ChangeEmailController!
      var viewModel: MockChangeEmailViewModel!

      beforeEach {
        viewModel = MockChangeEmailViewModel()

        sut = R.storyboard.accountSettings.changeEmailController()!
        sut.viewModel = viewModel
      }

      afterEach {
        sut = nil
        viewModel = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.field).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }
        
        it("should return shouldSetNavBarTransparent equal to true") {
          expect(sut.shouldSetNavBarTransparent).to(beTrue())
        }
        
        it("should return singleFormInputVM equal to viewModel instance") {
          expect(sut.singleFormInputVM).to(be(viewModel))
        }
        
        it("should pass field.text and call viewModel.changeEmail once on tap of continueButton") {
          expect(viewModel.changeEmailCallCount).to(equal(0))
          expect(viewModel.changeEmailValue).to(beNil())
          
          sut.field.text = "NewEmail"
          sut.continueButton.tap()
          
          expect(viewModel.changeEmailCallCount).to(equal(1))
          expect(viewModel.changeEmailValue).to(equal("NewEmail"))
        }
      }
    }
  }
}
