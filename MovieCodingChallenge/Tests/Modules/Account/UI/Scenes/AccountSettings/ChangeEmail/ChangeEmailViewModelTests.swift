//
//  ChangeEmailViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class ChangeEmailViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("ChangeEmailViewModel") {
      var sut: ChangeEmailViewModel!
      var service: MockCredentialsService!

      beforeEach {
        service = MockCredentialsService()
        sut = ChangeEmailViewModel(
          token: "ChangeEmailToken",
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }
      
      context("when valid email is used") {
        var validEmail: String!

        beforeEach {
          validEmail = "ios@appetiser.com.au"
        }

        afterEach {
          validEmail = nil
        }

        it("should pass correct parameters and call service.requestChangeEmail on changeEmail") {
          expect(service.requestChangeEmailCallCount).to(equal(0))
          expect(service.requestChangeEmailValue).to(beNil())
          expect(service.requestChangeEmailToken).to(beNil())

          sut.changeEmail(
            with: validEmail,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.requestChangeEmailCallCount).to(equal(1))
          expect(service.requestChangeEmailValue).to(equal(validEmail))
          expect(service.requestChangeEmailToken).to(equal("ChangeEmailToken"))
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure once on changeEmail") {
            var onSuccessCallCount = 0
            var onCompleteCallCount = 0
            sut.onComplete = { _ in
              onCompleteCallCount += 1
            }

            sut.changeEmail(
              with: validEmail,
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
            expect(onCompleteCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            service.errorToReturn = expectedError
          }

          it("should pass service error and call onError closure once on changeEmail") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.changeEmail(
              with: validEmail,
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }

      context("when invalid email is used") {
        var invalidEmail: String!

        beforeEach {
          invalidEmail = ""
        }

        afterEach {
          invalidEmail = nil
        }

        it("should pass back PasswordInputValidator.ValidationError to onError closure once on changeEmail") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.changeEmail(
            with: invalidEmail,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(beAKindOf(EmailAddressInputValidator.ValidationError.self))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
