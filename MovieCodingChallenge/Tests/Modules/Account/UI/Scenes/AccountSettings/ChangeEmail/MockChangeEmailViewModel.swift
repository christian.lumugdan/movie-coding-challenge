//
//  MockChangeEmailViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockChangeEmailViewModel: ChangeEmailViewModelProtocol {
  var onComplete: SingleResult<String>?

  var errorToReturn: Error?

  private(set) var changeEmailValue: String?
  private(set) var changeEmailCallCount: Int = 0
}

// MARK: - Methods

extension MockChangeEmailViewModel {
  func changeEmail(
    with newEmail: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    changeEmailValue = newEmail
    changeEmailCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
