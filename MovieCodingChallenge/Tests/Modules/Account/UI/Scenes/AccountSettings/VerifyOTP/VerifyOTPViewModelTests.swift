//
//  VerifyOTPViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class VerifyOTPViewModelTests: QuickSpec {
  override func spec() {
    describe("VerifyOTPViewModel") {
      var sut: VerifyOTPViewModel!
      var session: MockSessionService!
      var credentialsService: MockCredentialsService!
      var otpService: MockOTPService!

      beforeEach {
        let user = User(phoneNumber: "+639123456789")

        session = MockSessionService()
        session.user = user

        credentialsService = MockCredentialsService()
        otpService = MockOTPService()

        sut = VerifyOTPViewModel(
          session: session,
          credentialsService: credentialsService,
          otpService: otpService
        )
      }

      afterEach {
        sut = nil
        session = nil
        credentialsService = nil
        otpService = nil
      }

      it("should return correct titleText") {
        expect(sut.titleText).to(equal(S.verifyOtpTitle()))
      }

      it("should return correct messageText") {
        expect(sut.messageText).to(equal(S.verifyOtpMessage()))
      }

      it("should return correct subMessageText") {
        expect(sut.subMessageText).to(equal("+639123456789".formattedPhoneNumber))
      }

      it("should call credentialsService.requestVerificationTokenWithOTP once on verifyOTP") {
        expect(credentialsService.requestVerificationTokenWithOTPCallCount).to(equal(0))
        expect(credentialsService.requestVerificationTokenWithOTPValue).to(beNil())

        sut.verifyOTP(
          "OTP",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(credentialsService.requestVerificationTokenWithOTPCallCount).to(equal(1))
        expect(credentialsService.requestVerificationTokenWithOTPValue).to(equal("OTP"))
      }

      it("should call otpService.generateOTP once on verifyOTP") {
        expect(otpService.generateOTPCallCount).to(equal(0))
        expect(otpService.generateOTPPhoneNumber).to(beNil())

        sut.resendCode(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(otpService.generateOTPCallCount).to(equal(1))
        expect(otpService.generateOTPPhoneNumber).to(equal("+639123456789"))
      }
    }
  }
}
