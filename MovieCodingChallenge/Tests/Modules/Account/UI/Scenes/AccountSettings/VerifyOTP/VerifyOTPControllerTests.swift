//
//  VerifyOTPControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class VerifyOTPControllerTests: QuickSpec {
  override func spec() {
    describe("VerifyOTPController") {
      var sut: VerifyOTPController!
      var viewModel: MockVerifyOTPViewModel!

      beforeEach {
        viewModel = MockVerifyOTPViewModel()
        viewModel.titleText = "TitleText"
        viewModel.messageText = "MessageText"
        viewModel.subMessageText = "SubMessageText"

        sut = R.storyboard.accountSettings.verifyOTPController()!
        sut.viewModel = viewModel
      }

      afterEach {
        sut = nil
        viewModel = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.titleLabel).toNot(beNil())
          expect(sut.messageLabel).toNot(beNil())
          expect(sut.subMessageLabel).toNot(beNil())
          expect(sut.resendButtonView).toNot(beNil())
          expect(sut.textFields).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.inputControllers).toNot(beNil())
        }

        it("should set text labels from viewModel texts") {
          expect(sut.titleLabel.text).to(equal("TitleText"))
          expect(sut.messageLabel.text).to(equal("MessageText"))
          expect(sut.subMessageLabel.text).to(equal("SubMessageText"))
        }
      }
    }
  }
}
