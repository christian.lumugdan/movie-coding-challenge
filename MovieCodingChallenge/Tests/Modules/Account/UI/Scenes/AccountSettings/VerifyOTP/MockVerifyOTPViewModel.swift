//
//  MockVerifyOTPViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockVerifyOTPViewModel: VerifyOTPViewModelProtocol {
  var onComplete: DoubleResult<String, String>?

  var titleText: String = ""
  var messageText: String = ""
  var subMessageText: String?

  var errorToReturn: Error?

  private(set) var verifyOTPCallCount: Int = 0
  private(set) var verifyOTPValue: String?

  private(set) var resendCodeCallCount: Int = 0
}

// MARK: - Methods

extension MockVerifyOTPViewModel {
  func verifyOTP(
    _ otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    verifyOTPCallCount += 1
    verifyOTPValue = otp

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    resendCodeCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
