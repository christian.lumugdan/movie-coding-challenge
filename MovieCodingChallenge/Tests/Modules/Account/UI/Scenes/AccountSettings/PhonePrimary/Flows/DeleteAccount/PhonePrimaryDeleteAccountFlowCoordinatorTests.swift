//
//  PhonePrimaryDeleteAccountFlowCoordinatorTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhonePrimaryDeleteAccountFlowCoordinatorTests: QuickSpec {
  override func spec() {
    describe("PhonePrimaryDeleteAccountFlowCoordinator") {
      var sut: PhonePrimaryDeleteAccountFlowCoordinator!
      var viewModel: MockPhonePrimaryDeleteAccountFlowViewModel!
      var navigationController: MockNavigationController!
      
      beforeEach {
        navigationController = MockNavigationController(
          rootViewController: StaticTableViewController()
        )
        
        viewModel = MockPhonePrimaryDeleteAccountFlowViewModel()
        
        sut = PhonePrimaryDeleteAccountFlowCoordinator(
          navigationController: navigationController
        )
        sut.viewModel = viewModel
      }
      
      afterEach {
        navigationController = nil
        viewModel = nil
        sut = nil
      }
      
      it("should push VerifyOTPController as first page in flow on start") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        
        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyOTPController.self))
      }
    }
  }
}
