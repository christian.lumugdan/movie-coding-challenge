//
//  PhonePrimaryDeleteAccountFlowViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhonePrimaryDeleteAccountFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhonePrimaryDeleteAccountFlowViewModel") {
      var sut: PhonePrimaryDeleteAccountFlowViewModel!
      
      beforeEach {
        sut = PhonePrimaryDeleteAccountFlowViewModel()
      }
      
      afterEach {
        sut = nil
      }
      
      // No tests
    }
  }
}
