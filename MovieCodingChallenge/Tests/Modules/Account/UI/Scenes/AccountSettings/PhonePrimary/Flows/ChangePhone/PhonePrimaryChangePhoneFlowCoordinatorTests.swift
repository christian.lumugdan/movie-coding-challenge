//
//  PhonePrimaryChangePhoneFlowCoordinatorTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhonePrimaryChangePhoneFlowCoordinatorTests: QuickSpec {
  override func spec() {
    describe("PhonePrimaryChangePhoneFlowCoordinator") {
      var sut: PhonePrimaryChangePhoneFlowCoordinator!
      var viewModel: MockPhonePrimaryChangePhoneFlowViewModel!
      var navigationController: MockNavigationController!
      
      beforeEach {
        navigationController = MockNavigationController(
          rootViewController: StaticTableViewController()
        )
        
        viewModel = MockPhonePrimaryChangePhoneFlowViewModel()
        
        sut = PhonePrimaryChangePhoneFlowCoordinator(
          navigationController: navigationController
        )
        sut.viewModel = viewModel
      }
      
      afterEach {
        navigationController = nil
        viewModel = nil
        sut = nil
      }
      
      it("should push VerifyOTPController as first page in flow on start") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        
        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyOTPController.self))
      }
      
      it("should call viewModel.save(verificationToken:) once on viewModel.verifyOTPVM.onComplete") {
        expect(viewModel.saveVerificationTokenCallCount).to(equal(0))
        expect(viewModel.saveVerificationTokenValue).to(beNil())
        
        sut.start()
        viewModel.verifyOTPVM.onComplete?("Token", "OTP")
        
        expect(viewModel.saveVerificationTokenCallCount).to(equal(1))
        expect(viewModel.saveVerificationTokenValue).to(equal("Token"))
      }
      
      it("should replace OTP page with ChangePhoneController as second page in flow on viewModel.verifyOTPVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        viewModel.verifyOTPVM.onComplete?("Token", "OTP")
        
        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.viewControllers.last).toEventually(beAKindOf(ChangePhoneController.self))
      }
      
      it("should pop to controller before the flow on viewModel.verifyChangeCredentialVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        viewModel.verifyOTPVM.onComplete?("Token", "OTP")
        viewModel.changePhoneVM.onComplete?("PhoneNumber")
        viewModel.verifyChangeCredentialVM.onComplete?()
        
        expect(navigationController.viewControllers.count).to(equal(1))
      }
    }
  }
}
