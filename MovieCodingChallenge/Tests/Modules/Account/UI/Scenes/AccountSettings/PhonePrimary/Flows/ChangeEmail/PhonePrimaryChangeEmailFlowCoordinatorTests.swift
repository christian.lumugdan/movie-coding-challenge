//
//  PhonePrimaryChangeEmailFlowCoordinatorTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhonePrimaryChangeEmailFlowCoordinatorTests: QuickSpec {
  override func spec() {
    describe("PhonePrimaryChangeEmailFlowCoordinator") {
      var sut: PhonePrimaryChangeEmailFlowCoordinator!
      var viewModel: MockPhonePrimaryChangeEmailFlowViewModel!
      var navigationController: MockNavigationController!

      beforeEach {
        navigationController = MockNavigationController(
          rootViewController: StaticTableViewController()
        )

        viewModel = MockPhonePrimaryChangeEmailFlowViewModel()

        sut = PhonePrimaryChangeEmailFlowCoordinator(
          navigationController: navigationController
        )
        sut.viewModel = viewModel
      }

      afterEach {
        navigationController = nil
        viewModel = nil
        sut = nil
      }

      it("should push VerifyOTPController as first page in flow on start") {
        expect(navigationController.viewControllers.count).to(equal(1))

        sut.start()

        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyOTPController.self))
      }

      it("should call viewModel.save(verificationToken:) once on viewModel.verifyOTPVM.onComplete") {
        expect(viewModel.saveVerificationTokenCallCount).to(equal(0))
        expect(viewModel.saveVerificationTokenValue).to(beNil())

        sut.start()
        viewModel.verifyOTPVM.onComplete?("Token", "OTP")

        expect(viewModel.saveVerificationTokenCallCount).to(equal(1))
        expect(viewModel.saveVerificationTokenValue).to(equal("Token"))
      }

      it("should replace OTP page with ChangeEmailController as first page in flow on viewModel.verifyOTPVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))

        sut.start()
        viewModel.verifyOTPVM.onComplete?("Token", "OTP")

        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.viewControllers.last).toEventually(beAKindOf(ChangeEmailController.self))
      }
      
      it("should pop to controller before the flow on viewModel.verifyChangeCredentialVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        viewModel.verifyOTPVM.onComplete?("Token", "OTP")
        viewModel.changeEmailVM.onComplete?("Email")
        viewModel.verifyChangeCredentialVM.onComplete?()
        
        expect(navigationController.viewControllers.count).to(equal(1))
      }
    }
  }
}
