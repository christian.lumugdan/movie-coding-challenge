//
//  MockPhonePrimaryChangePhoneFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import MovieCodingChallenge

class MockPhonePrimaryChangePhoneFlowViewModel: PhonePrimaryChangePhoneFlowViewModelProtocol {
  
  var generateOTPVM: GenerateOTPViewModelProtocol = MockGenerateOTPViewModel()
  var verifyOTPVM: VerifyOTPViewModelProtocol = MockVerifyOTPViewModel()
  var changePhoneVM: ChangePhoneViewModelProtocol = MockChangePhoneViewModel()
  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol = MockVerifyChangeCredentialViewModel()
  
  var saveVerificationTokenCallCount: Int = 0
  var saveVerificationTokenValue: String?
  
  var savePhoneNumberCallCount: Int = 0
  var savePhoneNumberValue: String?
}

// MARK: - Methods

extension MockPhonePrimaryChangePhoneFlowViewModel {
  func save(verificationToken: String) {
    saveVerificationTokenCallCount += 1
    saveVerificationTokenValue = verificationToken
  }
  
  func save(phoneNumber: String) {
    savePhoneNumberCallCount += 1
    savePhoneNumberValue = phoneNumber
  }
}
