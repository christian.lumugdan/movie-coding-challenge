//
//  PhonePrimaryChangePhoneFlowViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhonePrimaryChangePhoneFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhonePrimaryChangePhoneFlowViewModel") {
      var sut: PhonePrimaryChangePhoneFlowViewModel!

      beforeEach {
        sut = PhonePrimaryChangePhoneFlowViewModel()
      }

      afterEach {
        sut = nil
      }

      it("should return verifyChangeCredentialVM with correct type") {
        sut.save(verificationToken: "Token")
        sut.save(phoneNumber: "PhoneNumber")

        let expectedType = VerifyChangePhoneViewModel.self
        expect(sut.verifyChangeCredentialVM).to(beAKindOf(expectedType))
      }
    }
  }
}
