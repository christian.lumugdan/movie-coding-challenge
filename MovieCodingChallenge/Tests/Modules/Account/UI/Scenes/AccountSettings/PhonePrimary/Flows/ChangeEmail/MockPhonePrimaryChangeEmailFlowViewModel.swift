//
//  MockPhonePrimaryChangeEmailFlowViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import MovieCodingChallenge

class MockPhonePrimaryChangeEmailFlowViewModel: PhonePrimaryChangeEmailFlowViewModelProtocol {
  var generateOTPVM: GenerateOTPViewModelProtocol = MockGenerateOTPViewModel()
  var verifyOTPVM: VerifyOTPViewModelProtocol = MockVerifyOTPViewModel()
  var changeEmailVM: ChangeEmailViewModelProtocol = MockChangeEmailViewModel()
  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol = MockVerifyChangeCredentialViewModel()
  
  var saveVerificationTokenCallCount: Int = 0
  var saveVerificationTokenValue: String?
  
  var saveEmailCallCount: Int = 0
  var saveEmailValue: String?
}

// MARK: - Methods

extension MockPhonePrimaryChangeEmailFlowViewModel {
  func save(verificationToken: String) {
    saveVerificationTokenCallCount += 1
    saveVerificationTokenValue = verificationToken
  }
  
  func save(email: String) {
    saveEmailCallCount += 1
    saveEmailValue = email
  }
}
