//
//  PhonePrimaryChangeEmailFlowViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhonePrimaryChangeEmailFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhonePrimaryChangeEmailFlowViewModel") {
      var sut: PhonePrimaryChangeEmailFlowViewModel!
      
      beforeEach {
        sut = PhonePrimaryChangeEmailFlowViewModel()
      }
      
      afterEach {
        sut = nil
      }
      
      it("should return verifyChangeCredentialVM with correct type") {
        sut.save(verificationToken: "Token")
        sut.save(email: "Email")
        
        let expectedType = VerifyChangeEmailViewModel.self
        expect(sut.verifyChangeCredentialVM).to(beAKindOf(expectedType))
      }
    }
  }
}


