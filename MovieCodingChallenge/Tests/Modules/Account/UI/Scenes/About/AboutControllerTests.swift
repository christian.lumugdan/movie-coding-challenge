//
//  AboutControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class AboutControllerTests: QuickSpec {
  override func spec() {
    describe("AboutController") {
      var sut: AboutController!

      beforeEach {
        sut = R.storyboard.about.aboutController()!
      }

      afterEach {
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.appIconContainerView).toNot(beNil())
          expect(sut.appIconImageView).toNot(beNil())
          expect(sut.versionInfoView).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.tableController).toNot(beNil())
        }
      }
    }
  }
}
