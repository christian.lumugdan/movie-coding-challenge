//
//  AboutTableControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class AboutTableControllerTests: QuickSpec {
  override func spec() {
    describe("AboutTableController") {
      var sut: AboutTableController!
      var nc: MockNavigationController!
      var appDocumentPresenter: MockAppDocumentPresenter!

      beforeEach {
        appDocumentPresenter = MockAppDocumentPresenter()
        sut = R.storyboard.about.aboutTableController()!
        sut.appDocumentPresenter = appDocumentPresenter
      }

      afterEach {
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("tableView.dataSource should return correct number of section and rows") {
          let sectionsCount = sut.tableView.dataSource?.numberOfSections?(in: sut.tableView)
          let rowsCount = sut.tableView.dataSource?.tableView(sut.tableView, numberOfRowsInSection: 0)

          expect(sectionsCount).to(equal(1))
          expect(rowsCount).to(equal(2))
        }
      }

      context("when initialized in nav flow") {
        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)

          sut.loadViewIfNeeded()
        }

        afterEach {
          nc = nil
        }

        it("should pass self and call appDocumentPresenter.presentTermsOfServicePage once on tap of section 0 row 0") {
          expect(appDocumentPresenter.presentTermsOfServicePageCallCount).to(equal(0))
          expect(appDocumentPresenter.presentTermsOfServicePageController).to(beNil())

          sut.tableView.delegate?.tableView?(
            sut.tableView,
            didSelectRowAt: IndexPath(row: 0, section: 0)
          )

          expect(appDocumentPresenter.presentTermsOfServicePageCallCount).to(equal(1))
          expect(appDocumentPresenter.presentTermsOfServicePageController).to(be(sut))
        }

        it("should pass self and call appDocumentPresenter.presentPrivacyPolicyPage once on tap of section 0 row 1") {
          expect(appDocumentPresenter.presentPrivacyPolicyPageCallCount).to(equal(0))
          expect(appDocumentPresenter.presentPrivacyPolicyPageController).to(beNil())

          sut.tableView.delegate?.tableView?(
            sut.tableView,
            didSelectRowAt: IndexPath(row: 1, section: 0)
          )

          expect(appDocumentPresenter.presentPrivacyPolicyPageCallCount).to(equal(1))
          expect(appDocumentPresenter.presentPrivacyPolicyPageController).to(be(sut))
        }
      }
    }
  }
}
