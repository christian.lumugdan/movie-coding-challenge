//
//  AppIntroViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import MovieCodingChallenge

class AppIntroViewModelTests: QuickSpec {
  override func spec() {
    describe("AppIntroViewModel") {
      var sut: AppIntroViewModel!

      beforeEach {
        sut = AppIntroViewModel()
      }

      afterEach {
        sut = nil
      }
      
      it("should return correct pageVMs") {
        expect(sut.pageVMs.count).to(equal(4))
        
        expect(sut.pageVMs[0]).to(beAKindOf(AppIntroStepOneViewModel.self))
        expect(sut.pageVMs[1]).to(beAKindOf(AppIntroStepTwoViewModel.self))
        expect(sut.pageVMs[2]).to(beAKindOf(AppIntroStepThreeViewModel.self))
        expect(sut.pageVMs[3]).to(beAKindOf(AppIntroStepFourViewModel.self))
      }
      
      it("should return false for shouldHideNextButton") {
        expect(sut.shouldHideNextButton).to(beFalse())
      }
      
      it("should return false for shouldHideSkipButton") {
        expect(sut.shouldHideSkipButton).to(beFalse())
      }
    }
  }
}
