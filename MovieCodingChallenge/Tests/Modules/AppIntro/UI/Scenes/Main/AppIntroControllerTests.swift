//
//  AppIntroControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class AppIntroControllerTests: QuickSpec {
  override func spec() {
    describe("AppIntroController") {
      var sut: AppIntroController!
      var viewModel: MockAppIntroViewModel!

      beforeEach {
        viewModel = MockAppIntroViewModel()
        viewModel.pageVMs = [
          MockPageContentViewModel(),
          MockPageContentViewModel()
        ]

        sut = R.storyboard.appIntro.appIntroController()!
        sut.viewModel = viewModel
      }

      afterEach {
        sut = nil
        viewModel = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.pageContentView).toNot(beNil())
          expect(sut.pageControl).toNot(beNil())
          expect(sut.skipButton).toNot(beNil())
          expect(sut.nextButton).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
          expect(sut.continueBtnHeight).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.pageController).toNot(beNil())
        }

        it("should set pageController delegate and dataSource to self") {
          expect(sut.pageController.delegate).to(be(sut))
          expect(sut.pageController.dataSource).to(be(sut))
        }

        it("should call viewModel.onSkip once on tap of skipButton") {
          var onSkipCallCount = 0
          viewModel.onSkip = { onSkipCallCount += 1 }

          sut.skipButton.tap()

          expect(onSkipCallCount).to(equal(1))
        }

        it("should call viewModel.onContinue once on tap of continueButton") {
          var onContinueCallCount = 0
          viewModel.onContinue = { onContinueCallCount += 1 }

          sut.continueButton.tap()

          expect(onContinueCallCount).to(equal(1))
        }

        it("should move to next page on tap of nextButton") {
          expect(sut.pageControl.currentPage).to(equal(0))
          expect(sut.pageController.currentIndex).to(equal(0))

          sut.nextButton.tap()

          expect(sut.pageControl.currentPage).toEventually(equal(1))
          expect(sut.pageController.currentIndex).toNotEventually(
            equal(1),
            timeout: .seconds(2)
          )
        }

        it("should return viewModel.pageVMs.count when pageController.dataSource.numberOfViewControllers is called") {
          expect(sut.pageController.dataSource?.numberOfViewControllers(in: sut.pageController))
            .to(equal(2))
        }

        it("should unhide continueButton when scrolled to last page") {
          expect(sut.continueBtnHeight.constant).to(equal(0))
          sut.pageController.scrollToPage(.at(index: 1), animated: false)
          sut.pageController.scrollToPage(.at(index: 1), animated: false) { _, _, _ in
            expect(sut.continueBtnHeight.constant).toEventually(beGreaterThan(0))
          }
        }
      }

      it("should hide/unhide skipButton and nextButton based on viewModel values") {
        viewModel.shouldHideNextButton = true
        viewModel.shouldHideSkipButton = true

        sut.loadViewIfNeeded()

        expect(sut.nextButton.isHidden).to(beTrue())
        expect(sut.skipButton.isHidden).to(beTrue())
      }
    }
  }
}

