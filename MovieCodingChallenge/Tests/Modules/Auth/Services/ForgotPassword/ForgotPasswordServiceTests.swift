//
//  ForgotPasswordServiceTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class ForgotPasswordServiceTests: QuickSpec {
  override func spec() {
    describe("ForgotPasswordServiceTests") {
      var sut: ForgotPasswordService!
      var api: MockAuthPasswordAPI!
      let username = "test@sample.com"
      let token = "123456"

      beforeEach {
        api = MockAuthPasswordAPI()
        sut = ForgotPasswordService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      context("when sending password reset request") {
        it("should call api.postAuthForgotPassword on sendPasswordResetRequest") {
          sut.sendPasswordResetRequest(
            for: username,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(api.postAuthForgotPasswordUsername).to(equal(username))
          expect(api.postAuthForgotPasswordCallCount).to(equal(1))
        }

        context("when initialized with failing api") {
          var apiError: Error!

          beforeEach {
            apiError = NSError(domain: #function, code: 1, userInfo: nil)
            api.errorToReturn = apiError
          }

          afterEach {
            apiError = nil
            api.reset()
          }

          it("should call onError closure and pass error from api on sendPasswordResetRequest") {
            var passedError: Error?

            sut.sendPasswordResetRequest(
              for: username,
              onSuccess: DefaultClosure.voidResult(),
              onError: { error in
                passedError = error
              }
            )

            expect(passedError).toEventually(be(apiError))
          }
        }
      }

      context("when confirming password reset") {
        it("should call api.postAuthResetPasswordCheck on confirmPasswordReset") {
          sut.confirmPasswordReset(
            for: username,
            with: token,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(api.postAuthResetPasswordCheckUsername).to(equal(username))
          expect(api.postAuthResetPasswordCheckToken).to(equal(token))
          expect(api.postAuthResetPasswordCheckCallCount).to(equal(1))
        }

        context("when initialized with failing api") {
          var apiError: Error!

          beforeEach {
            apiError = NSError(domain: #function, code: 1, userInfo: nil)
            api.errorToReturn = apiError
          }

          afterEach {
            apiError = nil
            api.reset()
          }

          it("should call onError closure and pass error from api on confirmPasswordReset") {
            var passedError: Error?

            sut.confirmPasswordReset(
              for: username,
              with: token,
              onSuccess: DefaultClosure.voidResult(),
              onError: { error in
                passedError = error
              }
            )

            expect(passedError).toEventually(be(apiError))
          }
        }
      }

      context("when resetting password") {
        var params: UpdatePasswordRequestParams!

        beforeEach {
          params = UpdatePasswordRequestParams(
            username: username,
            token: token,
            password: "samplepassword",
            passwordConfirmation: "samplepassword"
          )
        }

        afterEach {
          params = nil
        }

        it("should call api.postResetPassword on resetPassword") {
          sut.resetPassword(
            with: params,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(api.postResetPasswordParams).to(equal(params))
          expect(api.postResetPasswordCallCount).to(equal(1))
        }

        context("when initialized with failing api") {
          var apiError: Error!

          beforeEach {
            apiError = NSError(domain: #function, code: 1, userInfo: nil)
            api.errorToReturn = apiError
          }

          afterEach {
            apiError = nil
            api.reset()
          }

          it("should call onError closure and pass error from api on sendPasswordResetRequest") {
            var passedError: Error?

            sut.resetPassword(
              with: params,
              onSuccess: DefaultClosure.voidResult(),
              onError: { error in
                passedError = error
              }
            )

            expect(passedError).toEventually(be(apiError))
          }
        }
      }
    }
  }
}
