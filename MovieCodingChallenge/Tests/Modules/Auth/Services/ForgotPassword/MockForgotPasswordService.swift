//
//  MockForgotPasswordService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockForgotPasswordService: ForgotPasswordServiceProtocol {
  private(set) var sendPasswordResetRequestCallCount: Int = 0
  private(set) var confirmPasswordResetCallCount: Int = 0
  private(set) var resetPasswordCallCount: Int = 0

  private(set) var resetPasswordParams: UpdatePasswordRequestParams?

  var errorToReturn: Error?
}

// MARK: - Methods

extension MockForgotPasswordService {
  func sendPasswordResetRequest(
    for username: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    sendPasswordResetRequestCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func confirmPasswordReset(
    for username: String,
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    confirmPasswordResetCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func resetPassword(
    with params: UpdatePasswordRequestParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    resetPasswordCallCount += 1
    resetPasswordParams = params

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
