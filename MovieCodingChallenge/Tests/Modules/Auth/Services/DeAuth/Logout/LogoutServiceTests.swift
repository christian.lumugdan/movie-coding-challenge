//
//  LogoutServiceTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class LogoutServiceTests: QuickSpec {
  override func spec() {
    describe("LogoutService") {
      var sut: LogoutService!
      var api: MockAuthLogoutAPI!

      beforeEach {
        api = MockAuthLogoutAPI()
        sut = LogoutService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.postAuthLogout on logout") {
        expect(api.postAuthLogoutCallCount).to(equal(0))

        sut.logout(
          shouldBroadcast: false,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthLogoutCallCount).to(equal(1))
      }

      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

        afterEach {
          api.reset()
        }

        it("should call onDeAuth and onSuccess closure on logout") {
          var onSuccessCalled = false
          var onDeAuthCalled = false
          sut.onDeAuth = { _ in
            onDeAuthCalled = true
          }

          sut.logout(
            shouldBroadcast: false,
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onDeAuthCalled).toEventually(beTrue())
          expect(onSuccessCalled).toEventually(beTrue())
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
          api.reset()
        }

        it("should call onError and sut.onError closures and pass error from api on logout") {
          var passedError: Error?
          var onErrorCalled = false
          var onSutErrorCalled = false
          sut.onError = { _ in
            onSutErrorCalled = true
          }

          sut.logout(
            shouldBroadcast: false,
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCalled = true
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCalled).toEventually(beTrue())
          expect(onSutErrorCalled).toEventually(beTrue())
        }
      }
    }
  }
}
