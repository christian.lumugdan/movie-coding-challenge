//
//  MockLogoutService.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockLogoutService: LogoutServiceProtocol {
  var onDeAuth: BoolResult?
  var onError: ErrorResult?

  var errorToReturn: Error?

  private(set) var logoutShouldBroadcast: Bool?
  private(set) var logoutCallCount: Int = 0

  func reset() {
    errorToReturn = nil

    logoutCallCount = 0
  }
}

// MARK: - Methods

extension MockLogoutService {
  func logout(
    shouldBroadcast: Bool,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    logoutShouldBroadcast = shouldBroadcast
    logoutCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
