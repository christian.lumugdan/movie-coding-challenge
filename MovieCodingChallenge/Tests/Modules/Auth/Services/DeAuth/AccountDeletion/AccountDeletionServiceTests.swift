//
//  AccountDeletionServiceTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class AccountDeletionServiceTests: QuickSpec {
  override func spec() {
    describe("AccountDeletionService") {
      var sut: AccountDeletionService!
      var api: MockAuthAccountAPI!

      beforeEach {
        api = MockAuthAccountAPI()
        sut = AccountDeletionService(
          api: api
        )
      }

      afterEach {
        api = nil
        sut = nil
      }

      it("should pass correct params and call api.deleteUsers once on deleteAccount") {
        expect(api.deleteAccountCallCount).to(equal(0))
        expect(api.deleteAccountToken).to(beNil())

        sut.deleteAccount(
          token: "Token",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.deleteAccountCallCount).to(equal(1))
        expect(api.deleteAccountToken).to(equal("Token"))
      }
    }
  }
}
