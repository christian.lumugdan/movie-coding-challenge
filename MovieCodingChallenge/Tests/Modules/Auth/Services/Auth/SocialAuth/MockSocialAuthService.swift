//
//  MockSocialAuthService.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockSocialAuthService: SocialAuthServiceProtocol {
  var onAuth: SingleResult<UserAuthResponse>?

  var errorToReturn: Error?

  private(set) var authenticateWithAppleCallCount: Int = 0
  private(set) var authenticateWithAppleToken: String?

  private(set) var authenticateWithGoogleCallCount: Int = 0
  private(set) var authenticateWithGoogleToken: String?

  private(set) var authenticateWithFacebookCallCount: Int = 0
  private(set) var authenticateWithFacebookToken: String?

  func reset() {
    errorToReturn = nil

    authenticateWithAppleCallCount = 0
    authenticateWithGoogleCallCount = 0
    authenticateWithFacebookCallCount = 0
  }
}

// MARK: - Methods

extension MockSocialAuthService {
  func authenticateWithApple(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authenticateWithAppleCallCount += 1
    authenticateWithAppleToken = token

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func authenticateWithGoogle(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authenticateWithGoogleCallCount += 1
    authenticateWithGoogleToken = token

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func authenticateWithFacebook(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authenticateWithFacebookCallCount += 1
    authenticateWithFacebookToken = token

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
