//
//  MockLoginService.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockLoginService: LoginServiceProtocol {
  var onAuth: SingleResult<UserAuthResponse>?

  var errorToReturn: Error?

  private(set) var loginWithEmailValue: String?
  private(set) var loginWithEmailPassword: String?
  private(set) var loginWithEmailCallCount: Int = 0

  private(set) var loginWithPhoneNumberValue: String?
  private(set) var loginWithPhoneNumberOTP: String?
  private(set) var loginWithPhoneNumberCallCount: Int = 0

  func reset() {
    errorToReturn = nil

    loginWithEmailCallCount = 0
    loginWithPhoneNumberCallCount = 0
  }
}

// MARK: - Methods

extension MockLoginService {
  func loginWithEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    loginWithEmailValue = email
    loginWithEmailPassword = password
    loginWithEmailCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func loginWithPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    loginWithPhoneNumberValue = phoneNumber
    loginWithPhoneNumberOTP = otp
    loginWithPhoneNumberCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
