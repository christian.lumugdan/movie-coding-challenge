//
//  LoginServiceTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class LoginServiceTests: QuickSpec {
  override func spec() {
    describe("LoginService") {
      var sut: LoginService!
      var api: MockAuthLoginAPI!

      beforeEach {
        api = MockAuthLoginAPI()
        sut = LoginService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should pass parameters and call api.postAuthLogin on loginWithEmail") {
        expect(api.postAuthLoginEmailCallCount).to(equal(0))
        expect(api.postAuthLoginEmailValue).to(beNil())
        expect(api.postAuthLoginEmailPassword).to(beNil())

        sut.loginWithEmail(
          "Email",
          password: "Password",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthLoginEmailCallCount).to(equal(1))
        expect(api.postAuthLoginEmailValue).to(equal("Email"))
        expect(api.postAuthLoginEmailPassword).to(equal("Password"))
      }

      it("should pass parameters and call api.postAuthLogin on loginWithPhoneNumber") {
        expect(api.postAuthLoginPhoneNumberCallCount).to(equal(0))
        expect(api.postAuthLoginPhoneNumberValue).to(beNil())
        expect(api.postAuthLoginPhoneNumberOTP).to(beNil())

        sut.loginWithPhoneNumber(
          "PhoneNumber",
          otp: "OTP",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthLoginPhoneNumberCallCount).to(equal(1))
        expect(api.postAuthLoginPhoneNumberValue).to(equal("PhoneNumber"))
        expect(api.postAuthLoginPhoneNumberOTP).to(equal("OTP"))
      }
    }
  }
}
