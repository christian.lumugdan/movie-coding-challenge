//
//  RegisterServiceTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class RegisterServiceTests: QuickSpec {
  override func spec() {
    describe("RegisterService") {
      var sut: RegisterService!
      var api: MockAuthRegisterAPI!

      beforeEach {
        api = MockAuthRegisterAPI()
        sut = RegisterService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.postAuthRegister on registerWithEmail") {
        expect(api.postAuthRegisterEmailCallCount).to(equal(0))

        sut.registerWithEmail(
          "Email",
          password: "Password",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthRegisterEmailCallCount).to(equal(1))
        
        expect(api.postAuthRegisterEmailValue).to(equal("Email"))
        expect(api.postAuthRegisterEmailPassword).to(equal("Password"))
      }

      it("should call api.postAuthRegisterPhoneNumber on registerWithPhoneNumber") {
        expect(api.postAuthRegisterPhoneNumberCallCount).to(equal(0))

        sut.registerWithPhoneNumber(
          "PhoneNumber",
          otp: "OTP",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthRegisterPhoneNumberCallCount).to(equal(1))
        expect(api.postAuthRegisterPhoneNumberValue).to(equal("PhoneNumber"))
        expect(api.postAuthRegisterPhoneNumberOTP).to(equal("OTP"))
      }

    }
  }
}
