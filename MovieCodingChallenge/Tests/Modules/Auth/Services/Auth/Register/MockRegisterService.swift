//
//  MockRegisterService.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockRegisterService: RegisterServiceProtocol {
  var onAuth: SingleResult<UserAuthResponse>?

  var errorToReturn: Error?

  private(set) var registerWithEmailValue: String?
  private(set) var registerWithEmailPassword: String?
  private(set) var registerWithEmailCallCount: Int = 0

  private(set) var registerWithPhoneNumberValue: String?
  private(set) var registerWithPhoneNumberOTP: String?
  private(set) var registerWithPhoneNumberCallCount: Int = 0

  func reset() {
    errorToReturn = nil

    registerWithEmailCallCount = 0
    registerWithPhoneNumberCallCount = 0
  }
}

// MARK: - Methods

extension MockRegisterService {
  func registerWithEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    registerWithEmailValue = email
    registerWithEmailPassword = password
    registerWithEmailCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func registerWithPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    registerWithPhoneNumberValue = phoneNumber
    registerWithPhoneNumberOTP = otp
    registerWithPhoneNumberCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
