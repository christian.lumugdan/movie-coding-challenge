//
//  OTPServiceTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OTPServiceTests: QuickSpec {
  override func spec() {
    describe("OTPService") {
      var sut: OTPService!
      var api: MockAuthOTPAPI!

      beforeEach {
        api = MockAuthOTPAPI()
        sut = OTPService(
          api: api
        )
      }

      afterEach {
        api = nil
        sut = nil
      }

      it("should call api.postOTPGenerate once on generateOTP") {
        expect(api.postOTPGenerateCallCount).to(equal(0))
        expect(api.postOTPGeneratePhoneNumber).to(beNil())

        sut.generateOTP(
          for: "PhoneNumber",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postOTPGenerateCallCount).to(equal(1))
        expect(api.postOTPGeneratePhoneNumber).to(equal("PhoneNumber"))
      }
    }
  }
}
