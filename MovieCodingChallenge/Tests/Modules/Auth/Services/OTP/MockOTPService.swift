//
//  MockOTPService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockOTPService: OTPServiceProtocol {
  var errorToReturn: Error?

  private(set) var generateOTPCallCount: Int = 0
  private(set) var generateOTPPhoneNumber: String?
}

// MARK: - Methods

extension MockOTPService {
  func generateOTP(
    for phoneNumber: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    generateOTPCallCount += 1
    generateOTPPhoneNumber = phoneNumber

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
