//
//  CredentialsServiceTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class CredentialsServiceTests: QuickSpec {
  override func spec() {
    describe("CredentialsService") {
      var sut: CredentialsService!
      var authChangeAPI: MockAuthChangeAPI!
      var authVerificationAPI: MockAuthVerificationAPI!

      beforeEach {
        authChangeAPI = MockAuthChangeAPI()
        authVerificationAPI = MockAuthVerificationAPI()
        sut = CredentialsService(
          authChangeAPI: authChangeAPI,
          authVerificationAPI: authVerificationAPI
        )
      }

      afterEach {
        sut = nil
        authChangeAPI = nil
        authVerificationAPI = nil
      }

      it("should pass token and call api.postAuthVerificationToken once on requestVerificationToken") {
        expect(authVerificationAPI.postAuthVerificationTokenCallCount).to(equal(0))

        sut.requestVerificationTokenWithPassword(
          "Password",
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        let paramsDictionary = authVerificationAPI.postAuthVerificationTokenParams?.dictionary()
        expect(paramsDictionary?[stringOrNil: "password"]).to(equal("Password"))
      }

      it("should pass email and call api.postAuthChangeEmail once on requestChangeEmail") {
        expect(authChangeAPI.postAuthChangeEmailCallCount).to(equal(0))

        sut.requestChangeEmail(
          with: "NewEmail",
          token: "EmailToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangeEmailValue).to(equal("NewEmail"))
        expect(authChangeAPI.postAuthChangeEmailToken).to(equal("EmailToken"))
        expect(authChangeAPI.postAuthChangeEmailCallCount).to(equal(1))
      }

      it("should pass token and call api.postAuthChangeEmailVerify once on verifyChangeEmail") {
        expect(authChangeAPI.postAuthChangeEmailVerifyCallCount).to(equal(0))

        sut.verifyChangeEmail(
          with: "EmailToken",
          verificationToken: "EmailVerificationToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangeEmailVerifyToken).to(equal("EmailToken"))
        expect(authChangeAPI.postAuthChangeEmailVerifyVerificationToken).to(equal("EmailVerificationToken"))
        expect(authChangeAPI.postAuthChangeEmailVerifyCallCount).to(equal(1))
      }

      it("should pass phoneNumber and call api.postAuthChangePhoneNumber once on requestChangePhoneNumber") {
        expect(authChangeAPI.postAuthChangePhoneNumberCallCount).to(equal(0))

        sut.requestChangePhoneNumber(
          with: "NewPhoneNumber",
          token: "PhoneNumberToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangePhoneNumberValue).to(equal("NewPhoneNumber"))
        expect(authChangeAPI.postAuthChangePhoneNumberToken).to(equal("PhoneNumberToken"))
        expect(authChangeAPI.postAuthChangePhoneNumberCallCount).to(equal(1))
      }

      it("should pass token and call api.postAuthChangePhoneNumberVerify once on verifyChangePhoneNumber") {
        expect(authChangeAPI.postAuthChangePhoneNumberVerifyCallCount).to(equal(0))

        sut.verifyChangePhoneNumber(
          with: "PhoneNumberToken",
          verificationToken: "PhoneNumberVerificationToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangePhoneNumberVerifyToken).to(equal("PhoneNumberToken"))
        expect(authChangeAPI.postAuthChangePhoneNumberVerifyVerificationToken).to(equal("PhoneNumberVerificationToken"))
        expect(authChangeAPI.postAuthChangePhoneNumberVerifyCallCount).to(equal(1))
      }

      it("should pass newPassword, oldPassword and call api.postAuthChangePassword once on changePassword") {
        expect(authChangeAPI.postAuthChangePasswordCallCount).to(equal(0))

        sut.changePassword(
          to: "NewPassword",
          oldPassword: "OldPassword",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangePasswordNewValue).to(equal("NewPassword"))
        expect(authChangeAPI.postAuthChangePasswordOldValue).to(equal("OldPassword"))
        expect(authChangeAPI.postAuthChangePasswordCallCount).to(equal(1))
      }
    }
  }
}
