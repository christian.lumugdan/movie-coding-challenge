//
//  MockCredentialsService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockCredentialsService: CredentialsServiceProtocol {
  var onUserResult: SingleResult<User>?

  var errorToReturn: Error?

  private(set) var requestVerificationTokenWithPasswordValue: String?
  private(set) var requestVerificationTokenWithPasswordCallCount: Int = 0

  private(set) var requestVerificationTokenWithOTPValue: String?
  private(set) var requestVerificationTokenWithOTPCallCount: Int = 0

  private(set) var requestChangeEmailValue: String?
  private(set) var requestChangeEmailToken: String?
  private(set) var requestChangeEmailCallCount: Int = 0

  private(set) var verifyChangeEmailToken: String?
  private(set) var verifyChangeEmailVerificationToken: String?
  private(set) var verifyChangeEmailCallCount: Int = 0

  private(set) var requestChangePhoneNumberValue: String?
  private(set) var requestChangePhoneNumberToken: String?
  private(set) var requestChangePhoneNumberCallCount: Int = 0

  private(set) var verifyChangePhoneNumberToken: String?
  private(set) var verifyChangePhoneNumberVerificationToken: String?
  private(set) var verifyChangePhoneNumberCallCount: Int = 0

  private(set) var changePasswordNewValue: String?
  private(set) var changePasswordOldValue: String?
  private(set) var changePasswordCallCount: Int = 0
}

// MARK: - Methods

extension MockCredentialsService {
  func requestVerificationTokenWithPassword(
    _ password: String,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    requestVerificationTokenWithPasswordValue = password
    requestVerificationTokenWithPasswordCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess("")
    }
  }

  func requestVerificationTokenWithOTP(
    _ otp: String,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    requestVerificationTokenWithOTPValue = otp
    requestVerificationTokenWithOTPCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess("")
    }
  }

  func requestChangeEmail(
    with newEmail: String,
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    requestChangeEmailValue = newEmail
    requestChangeEmailToken = token
    requestChangeEmailCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func verifyChangeEmail(
    with token: String,
    verificationToken: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    verifyChangeEmailToken = token
    verifyChangeEmailVerificationToken = verificationToken
    verifyChangeEmailCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func requestChangePhoneNumber(
    with newPhoneNumber: String,
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    requestChangePhoneNumberValue = newPhoneNumber
    requestChangePhoneNumberToken = token
    requestChangePhoneNumberCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func verifyChangePhoneNumber(
    with token: String,
    verificationToken: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    verifyChangePhoneNumberToken = token
    verifyChangePhoneNumberVerificationToken = verificationToken
    verifyChangePhoneNumberCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func changePassword(
    to newPassword: String,
    oldPassword: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    changePasswordNewValue = newPassword
    changePasswordOldValue = oldPassword
    changePasswordCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
