//
//  MockAccountVerificationService.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAccountVerificationService: AccountVerificationServiceProtocol {
  var onUserResult: SingleResult<User>?
  var onAvatarResult: SingleResult<Photo>?

  var errorToReturn: Error?

  private(set) var verifyType: VerificationType?
  private(set) var verifyToken: String?
  private(set) var verifyCallCount: Int = 0

  private(set) var resendCodeType: VerificationType?
  private(set) var resendCodeCallCount: Int = 0

  func reset() {
    errorToReturn = nil

    verifyCallCount = 0
    resendCodeCallCount = 0
  }
}

// MARK: - Methods

extension MockAccountVerificationService {
  func verify(
    for type: VerificationType,
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    verifyType = type
    verifyToken = token
    verifyCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func resendCode(
    for type: VerificationType,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    resendCodeType = type
    resendCodeCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
