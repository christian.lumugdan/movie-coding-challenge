//
//  AccountVerificationServiceTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class AccountVerificationServiceTests: QuickSpec {
  override func spec() {
    describe("AccountVerificationService") {
      var sut: AccountVerificationService!
      var api: MockAuthVerificationAPI!

      beforeEach {
        api = MockAuthVerificationAPI()
        sut = AccountVerificationService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.postAuthVerificationVerify on verify") {
        expect(api.postAuthVerificationVerifyCallCount).to(equal(0))

        sut.verify(
          for: .email,
          using: "Token",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthVerificationVerifyCallCount).to(equal(1))
      }

      it("should pass the type and token to api on verify 1") {
        sut.verify(
          for: .email,
          using: "Token",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthVerificationVerifyType).to(equal(.email))
        expect(api.postAuthVerificationVerifyToken).to(equal("Token"))
      }

      it("should pass the type and token to api on verify 2") {
        sut.verify(
          for: .phone,
          using: "Test",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthVerificationVerifyType).to(equal(.phone))
        expect(api.postAuthVerificationVerifyToken).to(equal("Test"))
      }

      it("should pass the type to api on resendCode 1") {
        sut.resendCode(
          for: .email,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthVerificationResendType).to(equal(.email))
      }

      it("should pass the type to api on resendCode 2") {
        sut.resendCode(
          for: .phone,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthVerificationResendType).to(equal(.phone))
      }

      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

        afterEach {
          api.reset()
        }

        it("should call onSuccess and onUserResult closures on verify") {
          var onSuccessCalled = false
          var onUserResultCalled = false
          sut.onUserResult = { _ in
            onUserResultCalled = true
          }

          sut.verify(
            for: .email,
            using: "Token",
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
          expect(onUserResultCalled).toEventually(beTrue())
        }

        it("should call onSuccess closures on resendCode") {
          var onSuccessCalled = false

          sut.resendCode(
            for: .email,
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
          api.reset()
        }

        it("should call onError closure and pass back error from api on verify") {
          var passedError: Error?
          var onErrorCalled = false

          sut.verify(
            for: .email,
            using: "Token",
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCalled = true
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCalled).toEventually(beTrue())
        }

        it("should call onError closure and pass back error from api on resendCode") {
          var passedError: Error?
          var onErrorCalled = false

          sut.resendCode(
            for: .email,
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCalled = true
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCalled).toEventually(beTrue())
        }
      }
    }
  }
}
