//
//  MockProfileService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockProfileService: ProfileServiceProtocol {
  var onUserResult: SingleResult<User>?
  var onAvatarResult: SingleResult<Photo>?

  var errorToReturn: Error?

  private(set) var refreshProfileCallCount: Int = 0

  private(set) var addOnboardingEmailCallCount: Int = 0
  private(set) var addOnboardingEmailValue: String?

  private(set) var markOnboardingAsCompleteCallCount: Int = 0

  private(set) var setNameCallCount: Int = 0
  private(set) var setNameValue: String?

  private(set) var setPictureCallCount: Int = 0
  private(set) var setPictureValue: Data?

  private(set) var setInfoFullName: String?
  private(set) var setInfoBirthdate: Date?
  private(set) var setInfoDescription: String?
  private(set) var setInfoCallCount: Int = 0
}

// MARK: - Methods

extension MockProfileService {
  func refreshProfile(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    refreshProfileCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func addOnboardingEmail(
    _ email: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    addOnboardingEmailCallCount += 1
    addOnboardingEmailValue = email

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func markOnboardingAsComplete(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    markOnboardingAsCompleteCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func setName(
    _ name: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    setNameCallCount += 1
    setNameValue = name

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    setPictureCallCount += 1
    setPictureValue = data

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func setInfo(
    fullName: String,
    birthdate: Date?,
    description: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    setInfoFullName = fullName
    setInfoBirthdate = birthdate
    setInfoDescription = description
    setInfoCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
