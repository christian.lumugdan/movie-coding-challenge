//
//  ProfileServiceTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class ProfileServiceTests: QuickSpec {
  override func spec() {
    describe("ProfileService") {
      var sut: ProfileService!
      var onboardingAPI: MockAuthOnboardingAPI!
      var profileAPI: MockAuthProfileAPI!

      beforeEach {
        onboardingAPI = MockAuthOnboardingAPI()
        profileAPI = MockAuthProfileAPI()
        sut = ProfileService(
          onboardingAPI: onboardingAPI,
          profileAPI: profileAPI
        )
      }

      afterEach {
        sut = nil
        onboardingAPI = nil
        profileAPI = nil
      }

      it("should call profileAPI.getAuthProfile on refreshProfile") {
        expect(profileAPI.getAuthProfileCallCount).to(equal(0))

        sut.refreshProfile(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(profileAPI.getAuthProfileCallCount).to(equal(1))
      }

      it("should call onboardingAPI.postOnboardingEmail on addOnboardingEmail") {
        expect(onboardingAPI.postOnboardingEmailCallCount).to(equal(0))
        expect(onboardingAPI.postOnboardingEmailValue).to(beNil())

        sut.addOnboardingEmail(
          "Email",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(onboardingAPI.postOnboardingEmailCallCount).to(equal(1))
        expect(onboardingAPI.postOnboardingEmailValue).to(equal("Email"))
      }

      it("should call onboardingAPI.postOnboardingComplete on markOnboardingAsComplete") {
        expect(onboardingAPI.postOnboardingCompleteCallCount).to(equal(0))

        sut.markOnboardingAsComplete(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(onboardingAPI.postOnboardingCompleteCallCount).to(equal(1))
      }

      it("should call profileAPI.putAuthProfile on setName") {
        expect(profileAPI.putAuthProfileCallCount).to(equal(0))

        sut.setName(
          "Test",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(profileAPI.putAuthProfileCallCount).to(equal(1))
        expect(profileAPI.putAuthProfileParams?.fullName).to(equal("Test"))
      }

      it("should call profileAPI.postAuthProfileAvatar on setPicture") {
        expect(profileAPI.postAuthProfileAvatarCallCount).to(equal(0))

        sut.setPicture(
          with: Data(),
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(profileAPI.postAuthProfileAvatarCallCount).to(equal(1))
      }

      it("should call profileAPI.putAuthProfile on setInfo") {
        expect(profileAPI.putAuthProfileCallCount).to(equal(0))

        sut.setInfo(
          fullName: "FullName",
          birthdate: "2021-03-23".toDate()?.date,
          description: "Description",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(profileAPI.putAuthProfileCallCount).to(equal(1))

        let paramsDictionary = profileAPI.putAuthProfileParams?.dictionary()
        expect(paramsDictionary?[stringOrNil: "full_name"]).to(equal("FullName"))
        expect(paramsDictionary?[stringOrNil: "birthdate"]).to(equal("2021-03-23"))
        expect(paramsDictionary?[stringOrNil: "description"]).to(equal("Description"))
      }
    }
  }
}
