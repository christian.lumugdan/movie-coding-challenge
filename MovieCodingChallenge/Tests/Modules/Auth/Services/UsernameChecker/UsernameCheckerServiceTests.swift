//
//  UsernameCheckerServiceTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class UsernameCheckerServiceTests: QuickSpec {
  override func spec() {
    describe("UsernameCheckerService") {
      var sut: UsernameCheckerService!
      var api: MockAuthCheckAPI!
      let email = "test@sample.com"

      beforeEach {
        api = MockAuthCheckAPI()
        sut = UsernameCheckerService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.postAuthCheckEmail on checkUsernameAvailability") {
        sut.checkUsernameAvailability(
          "",
          onAvailable: DefaultClosure.voidResult(),
          onUnavailable: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthCheckUsernameCallCount).to(equal(1))
      }

      context("when api.usernameExists is set to true") {
        beforeEach {
          api.usernameExists = true
        }

        it("should call onSuccess closure on checkUsernameAvailability") {
          var onAvailableCalled = false

          sut.checkUsernameAvailability(
            email,
            onAvailable: { onAvailableCalled = true },
            onUnavailable: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(onAvailableCalled).toEventually(beTrue())
        }
      }

      context("when api.usernameExists is set to false") {
        beforeEach {
          api.usernameExists = false
        }

        it("should call onUnavailable closure on checkUsernameAvailability") {
          var onUnavailableCalled = false

          sut.checkUsernameAvailability(
            email,
            onAvailable: DefaultClosure.voidResult(),
            onUnavailable: { onUnavailableCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onUnavailableCalled).toEventually(beTrue())
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
          api.reset()
        }

        it("should call onError closure and pass error from api on checkUsernameAvailability") {
          var passedError: Error?

          sut.checkUsernameAvailability(
            email,
            onAvailable: DefaultClosure.voidResult(),
            onUnavailable: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
            }
          )

          expect(passedError).toEventually(be(apiError))
        }
      }
    }
  }
}
