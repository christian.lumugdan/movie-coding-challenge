//
//  MockUsernameCheckerService.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockUsernameCheckerService: UsernameCheckerServiceProtocol {
  var isUsernameAvailable: Bool = true
  var errorToReturn: Error?

  private(set) var checkUsernameParamUsername: String?
  private(set) var checkUsernameCallCount: Int = 0
}

// MARK: - Methods

extension MockUsernameCheckerService {
  func checkUsernameAvailability(
    _ username: String,
    onAvailable: @escaping VoidResult,
    onUnavailable: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    checkUsernameParamUsername = username
    checkUsernameCallCount += 1

    if let e = errorToReturn {
      return onError(e)
    }

    if isUsernameAvailable {
      onAvailable()
    } else {
      onUnavailable()
    }
  }
}
