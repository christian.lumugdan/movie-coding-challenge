//
//  DummyUserAuthResponse.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

extension UserAuthResponse {
  init() {
    self.init(
      user: User(),
      accessToken: "",
      tokenType: "",
      expiresIn: ""
    )
  }
}
