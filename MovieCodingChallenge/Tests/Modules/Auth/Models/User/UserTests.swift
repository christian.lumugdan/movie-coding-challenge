//
//  UserTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class UserTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("User") {
      var sut: User!

      context("hasName") {
        context("when initialized with non-nil and non-empty fullName") {
          beforeEach {
            sut = User(fullName: "Test Name")
          }

          it("should be true") {
            expect(sut.hasName).to(beTrue())
          }
        }

        context("when initialized with nil fullName") {
          beforeEach {
            sut = User(fullName: nil)
          }

          it("should be false") {
            expect(sut.hasName).to(beFalse())
          }
        }

        context("when initialized with empty fullName") {
          beforeEach {
            sut = User(fullName: "")
          }

          it("should be false") {
            expect(sut.hasName).to(beFalse())
          }
        }
      }

      context("isVerified") {
        context("when initialized with true verified") {
          beforeEach {
            sut = User(emailVerified: true)
          }

          it("should be true") {
            expect(sut.isEmailVerified).to(beTrue())
          }
        }

        context("when initialized with false verified") {
          beforeEach {
            sut = User(emailVerified: false)
          }

          it("should be false") {
            expect(sut.isEmailVerified).to(beFalse())
          }
        }
      }

      context("isPhoneNumberVerified") {
        context("when initialized with true phoneNumberVerified") {
          beforeEach {
            sut = User(phoneNumberVerified: true)
          }

          it("should be true") {
            expect(sut.isPhoneNumberVerified).to(beTrue())
          }
        }

        context("when initialized with false phoneNumberVerified") {
          beforeEach {
            sut = User(phoneNumberVerified: false)
          }

          it("should be false") {
            expect(sut.isPhoneNumberVerified).to(beFalse())
          }
        }
      }

      context("hasAvatar") {
        context("when initialized with non-nil avatar") {
          beforeEach {
            sut = User(avatar: Photo())
          }

          it("should be true") {
            expect(sut.hasAvatar).to(beTrue())
          }
        }

        context("when initialized with nil avatar") {
          beforeEach {
            sut = User(avatar: nil)
          }

          it("should be false") {
            expect(sut.hasAvatar).to(beFalse())
          }
        }
      }

      afterEach {
        sut = nil
      }
    }
  }
}
