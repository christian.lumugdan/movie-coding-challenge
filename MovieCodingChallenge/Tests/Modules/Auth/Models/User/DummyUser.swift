//
//  DummyUser.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

extension User {
  init(
    testId: Int = 0,
    primaryUsername: UsernameType = .email,
    email: String? = nil,
    phoneNumber: String? = nil,
    fullName: String? = nil,
    emailVerified: Bool = false,
    phoneNumberVerified: Bool = false,
    avatarPermanentUrl: URL = .dummy,
    avatarPermanentThumbUrl: URL = .dummyThumb,
    avatar: Photo? = nil,
    birthdate: String? = nil,
    description: String? = nil,
    createdAt: Date = .init(),
    updatedAt: Date = .init(),
    onboardedAt: Date = .init()
  ) {
    self.init(
      id: testId,
      primaryUsername: primaryUsername,
      email: email,
      phoneNumber: phoneNumber,
      fullName: fullName,
      gender: .male,
      emailVerified: emailVerified,
      phoneNumberVerified: phoneNumberVerified,
      avatarPermanentUrl: avatarPermanentUrl,
      avatarPermanentThumbUrl: avatarPermanentThumbUrl,
      avatar: avatar,
      birthdate: birthdate,
      description: description,
      createdAt: createdAt,
      updatedAt: updatedAt,
      onboardedAt: onboardedAt
    )
  }
}
