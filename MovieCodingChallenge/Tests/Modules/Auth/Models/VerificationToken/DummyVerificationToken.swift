//
//  DummyVerificationToken.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

extension VerificationToken {
  init(
    testToken: String = "",
    testExpiresAt: Date = .now
  ) {
    self.init(
      token: testToken,
      expiresAt: testExpiresAt
    )
  }
}
