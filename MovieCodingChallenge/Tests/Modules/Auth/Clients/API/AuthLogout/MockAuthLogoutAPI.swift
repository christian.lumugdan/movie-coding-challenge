//
//  MockAuthLogoutAPI.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAuthLogoutAPI: AuthLogoutAPI {
  var errorToReturn: Error?

  private(set) var postAuthLogoutEmail: String?
  private(set) var postAuthLogoutPassword: String?
  private(set) var postAuthLogoutPhoneNumber: String?
  private(set) var postAuthLogoutCallCount: Int = 0

  func reset() {
    errorToReturn = nil

    postAuthLogoutEmail = nil
    postAuthLogoutPassword = nil
    postAuthLogoutPhoneNumber = nil
    postAuthLogoutCallCount = 0
  }
}

extension MockAuthLogoutAPI {
  @discardableResult
  func postAuthLogout(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthLogoutCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }
}
