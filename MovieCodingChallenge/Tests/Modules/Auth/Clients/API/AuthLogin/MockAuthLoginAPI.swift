//
//  MockAuthLoginAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAuthLoginAPI: AuthLoginAPI {
  var errorToReturn: Error?

  private(set) var postAuthLoginEmailValue: String?
  private(set) var postAuthLoginEmailPassword: String?
  private(set) var postAuthLoginEmailCallCount: Int = 0

  private(set) var postAuthLoginPhoneNumberValue: String?
  private(set) var postAuthLoginPhoneNumberOTP: String?
  private(set) var postAuthLoginPhoneNumberCallCount: Int = 0
}

extension MockAuthLoginAPI {
  @discardableResult
  func postAuthLoginEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthLoginEmailValue = email
    postAuthLoginEmailPassword = password
    postAuthLoginEmailCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthLoginPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthLoginPhoneNumberValue = phoneNumber
    postAuthLoginPhoneNumberOTP = otp
    postAuthLoginPhoneNumberCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }
}
