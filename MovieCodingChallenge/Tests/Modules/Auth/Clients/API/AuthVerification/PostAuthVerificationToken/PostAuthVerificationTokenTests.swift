//
//  PostAuthVerificationTokenTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PostAuthVerificationTokenTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PostAuthVerificationToken") {
      var apiResponse: APIResponse!
      var data: VerificationToken!

      context("when decoding status 200 response") {
        beforeEach {
          apiResponse = self.decodeResponseValue(statusCode: .ok)
        }

        it("should have non-nil decoded response") {
          expect(apiResponse).toNot(beNil())
        }

        it("should have non-nil decoded response.data") {
          data = apiResponse.decodedValue()

          expect(data).toNot(beNil())
        }
      }

      afterEach {
        apiResponse = nil
        data = nil
      }
    }
  }
}
