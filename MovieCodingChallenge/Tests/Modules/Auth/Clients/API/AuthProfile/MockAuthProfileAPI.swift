//
//  MockAuthProfileAPI.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAuthProfileAPI: AuthProfileAPI {
  var errorToReturn: Error?

  private(set) var putAuthProfileParams: AuthProfileParams?
  private(set) var putAuthProfileCallCount: Int = 0

  private(set) var postAuthProfileAvatarCallCount: Int = 0
  private(set) var getAuthProfileCallCount: Int = 0

  func reset() {
    errorToReturn = nil

    putAuthProfileParams = nil
    putAuthProfileCallCount = 0

    postAuthProfileAvatarCallCount = 0
    getAuthProfileCallCount = 0
  }
}

extension MockAuthProfileAPI {
  func putAuthProfile(
    with params: AuthProfileParams,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    putAuthProfileParams = params
    putAuthProfileCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  func postAuthProfileAvatar(
    with data: Data,
    onSuccess: @escaping SingleResult<Photo>,
    onError: @escaping ErrorResult
  ) {
    postAuthProfileAvatarCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }
  }

  func getAuthProfile(
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getAuthProfileCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }
}
