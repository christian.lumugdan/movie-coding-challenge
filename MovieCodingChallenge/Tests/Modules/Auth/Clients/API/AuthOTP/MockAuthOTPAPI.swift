//
//  MockAuthOTPAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAuthOTPAPI: AuthOTPAPI {
  var errorToReturn: Error?

  private(set) var postOTPGenerateCallCount: Int = 0
  private(set) var postOTPGeneratePhoneNumber: String?
}

// MARK: - Methods

extension MockAuthOTPAPI {
  @discardableResult
  func postOTPGenerate(
    for phoneNumber: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postOTPGenerateCallCount += 1
    postOTPGeneratePhoneNumber = phoneNumber

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }
}
