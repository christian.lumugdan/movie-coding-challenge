//
//  MockAuthRegisterAPI.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAuthRegisterAPI: AuthRegisterAPI {
  var errorToReturn: Error?

  private(set) var postAuthRegisterEmailCallCount: Int = 0
  private(set) var postAuthRegisterEmailValue: String?
  private(set) var postAuthRegisterEmailPassword: String?

  private(set) var postAuthRegisterPhoneNumberCallCount: Int = 0
  private(set) var postAuthRegisterPhoneNumberOTP: String?
  private(set) var postAuthRegisterPhoneNumberValue: String?
}

extension MockAuthRegisterAPI {
  @discardableResult
  func postAuthRegisterEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthRegisterEmailCallCount += 1
    postAuthRegisterEmailValue = email
    postAuthRegisterEmailPassword = password

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthRegisterPhoneNumber(
    _ phoneNumber: String,
    otp: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthRegisterPhoneNumberCallCount += 1
    postAuthRegisterPhoneNumberValue = phoneNumber
    postAuthRegisterPhoneNumberOTP = otp

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }
}
