//
//  PostAuthRegisterTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PostAuthRegisterTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PostAuthRegister") {
      var apiResponse: APIResponse! // We have this in in all endpoint spec classes
      var data: UserAuthResponse! // The type here will depend on the API model you want to decode for

      context("when decoding status 201 response") {
        beforeEach {
          apiResponse = self.decodeResponseValue(statusCode: .created)
        }

        it("should have non-nil decoded response") {
          expect(apiResponse).toNot(beNil())
        }

        it("should have non-nil decoded response.data") {
          data = apiResponse.decodedValue()

          expect(data).toNot(beNil())
        }
      }

      afterEach {
        apiResponse = nil
        data = nil
      }
    }
  }
}
