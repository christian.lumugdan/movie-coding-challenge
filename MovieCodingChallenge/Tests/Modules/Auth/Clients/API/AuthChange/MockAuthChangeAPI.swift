//
//  MockAuthChangeAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAuthChangeAPI: AuthChangeAPI {
  var errorToReturn: Error?

  private(set) var postAuthChangeEmailValue: String?
  private(set) var postAuthChangeEmailToken: String?
  private(set) var postAuthChangeEmailCallCount: Int = 0

  private(set) var postAuthChangeEmailVerifyToken: String?
  private(set) var postAuthChangeEmailVerifyVerificationToken: String?
  private(set) var postAuthChangeEmailVerifyCallCount: Int = 0

  private(set) var postAuthChangePhoneNumberValue: String?
  private(set) var postAuthChangePhoneNumberToken: String?
  private(set) var postAuthChangePhoneNumberCallCount: Int = 0

  private(set) var postAuthChangePhoneNumberVerifyToken: String?
  private(set) var postAuthChangePhoneNumberVerifyVerificationToken: String?
  private(set) var postAuthChangePhoneNumberVerifyCallCount: Int = 0

  private(set) var postAuthChangePasswordNewValue: String?
  private(set) var postAuthChangePasswordOldValue: String?
  private(set) var postAuthChangePasswordCallCount: Int = 0
}

extension MockAuthChangeAPI {
  @discardableResult
  func postAuthChangeEmail(
    email: String,
    token: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthChangeEmailValue = email
    postAuthChangeEmailToken = token
    postAuthChangeEmailCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthChangeEmailVerify(
    token: String,
    verificationToken: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthChangeEmailVerifyToken = token
    postAuthChangeEmailVerifyVerificationToken = verificationToken
    postAuthChangeEmailVerifyCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthChangePhoneNumber(
    phoneNumber: String,
    token: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthChangePhoneNumberValue = phoneNumber
    postAuthChangePhoneNumberToken = token
    postAuthChangePhoneNumberCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthChangePhoneNumberVerify(
    token: String,
    verificationToken: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthChangePhoneNumberVerifyToken = token
    postAuthChangePhoneNumberVerifyVerificationToken = verificationToken
    postAuthChangePhoneNumberVerifyCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthChangePassword(
    newPassword: String,
    oldPassword: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthChangePasswordNewValue = newPassword
    postAuthChangePasswordOldValue = oldPassword
    postAuthChangePasswordCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }
}
