//
//  MockAuthCheckAPI.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAuthCheckAPI: AuthCheckAPI {
  var errorToReturn: Error?
  var usernameExists: Bool = false

  private(set) var postAuthCheckUsernameCallCount = 0

  func reset() {
    errorToReturn = nil
    usernameExists = false
  }
}

extension MockAuthCheckAPI {
  func postAuthCheckUsername(
    _ username: String,
    onSuccess: @escaping BoolResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthCheckUsernameCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(usernameExists)
    }

    return DummyRequest()
  }
}
