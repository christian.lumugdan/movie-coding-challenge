// 
//  PostOnboardingEmailTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import MovieCodingChallenge

class PostOnboardingEmailTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PostOnboardingEmail") {
      var apiResponse: APIResponse!
      var data: User!
      
      afterEach {
        apiResponse = nil
        data = nil
      }
      
      context("when decoding status 200 response") {
        beforeEach {
          apiResponse = self.decodeResponseValue(statusCode: .ok)
        }
        
        it("should have non-nil decoded response") {
          expect(apiResponse).toNot(beNil())
        }
        
        it("should have 200 status code") {
          data = apiResponse.decodedValue()
          
          expect(data).toNot(beNil())
        }
      }
    }
  }
}
