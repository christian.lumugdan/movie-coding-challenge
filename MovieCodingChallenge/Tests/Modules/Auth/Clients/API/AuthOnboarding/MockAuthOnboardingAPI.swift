//
//  MockAuthOnboardingAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAuthOnboardingAPI: AuthOnboardingAPI {
  var errorToReturn: Error?

  private(set) var postOnboardingEmailCallCount: Int = 0
  private(set) var postOnboardingEmailValue: String?

  private(set) var postOnboardingCompleteCallCount: Int = 0
}

// MARK: - Methods

extension MockAuthOnboardingAPI {
  @discardableResult
  func postOnboardingEmail(
    email: String,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postOnboardingEmailCallCount += 1
    postOnboardingEmailValue = email

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postOnboardingComplete(
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postOnboardingCompleteCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }
}
