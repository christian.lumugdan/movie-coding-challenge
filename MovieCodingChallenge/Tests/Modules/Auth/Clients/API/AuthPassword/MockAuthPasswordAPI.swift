//
//  MockAuthPasswordAPI.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import MovieCodingChallenge

class MockAuthPasswordAPI: AuthPasswordAPI {
  var errorToReturn: Error?

  private(set) var postAuthForgotPasswordUsername: String?
  private(set) var postAuthResetPasswordCheckUsername: String?
  private(set) var postAuthResetPasswordCheckToken: String?
  private(set) var postResetPasswordParams: UpdatePasswordRequestParams?

  private(set) var postAuthForgotPasswordCallCount = 0
  private(set) var postAuthResetPasswordCheckCallCount = 0
  private(set) var postResetPasswordCallCount = 0

  func reset() {
    errorToReturn = nil

    postAuthForgotPasswordUsername = nil
    postAuthResetPasswordCheckUsername = nil
    postAuthResetPasswordCheckToken = nil
    postResetPasswordParams = nil

    postAuthForgotPasswordCallCount = 0
    postAuthResetPasswordCheckCallCount = 0
    postResetPasswordCallCount = 0
  }
}

extension MockAuthPasswordAPI {
  func postAuthForgotPassword(
    for username: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthForgotPasswordUsername = username
    postAuthForgotPasswordCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthResetPasswordCheck(
    for username: String,
    with token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthResetPasswordCheckUsername = username
    postAuthResetPasswordCheckToken = token
    postAuthResetPasswordCheckCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }

  func postResetPassword(
    with params: UpdatePasswordRequestParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postResetPasswordParams = params
    postResetPasswordCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }
}
