//
//  PostAuthForgotPasswordTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PostAuthForgotPasswordTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PostAuthForgotPassword") {
      var apiResponse: APIResponse! // We have this in in all endpoint spec

      context("when decoding status 200 response") {
        beforeEach {
          apiResponse = self.decodeResponseValue(statusCode: .ok)
        }

        it("should have non-nil decoded response") {
          expect(apiResponse).toNot(beNil())
        }
      }

      afterEach {
        apiResponse = nil
      }
    }
  }
}
