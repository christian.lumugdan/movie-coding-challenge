//
//  MockVerifyPasswordResetViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockVerifyPasswordResetViewModel: VerifyPasswordResetViewModelProtocol {
  var titleText: String = ""
  var messageText: String = ""
  var subMessageText: String? = ""

  var errorToReturn: Error?

  private(set) var resendCodeCallCount: Int = 0
  private(set) var verifyCallCount: Int = 0
}

extension MockVerifyPasswordResetViewModel {
  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    resendCodeCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func verify(
    with token: String,
    onSuccess: @escaping SingleResult<NewPasswordViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    verifyCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(MockNewPasswordViewModel())
    }
  }
}
