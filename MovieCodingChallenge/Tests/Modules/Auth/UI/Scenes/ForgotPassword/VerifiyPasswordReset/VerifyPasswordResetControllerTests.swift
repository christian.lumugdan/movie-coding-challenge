//
//  VerifyPasswordResetControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class VerifyPasswordResetControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("VerifyPasswordResetController") {
      var sut: VerifyPasswordResetController!
      var nc: MockNavigationController!
      var viewModel: MockVerifyPasswordResetViewModel!

      beforeEach {
        viewModel = MockVerifyPasswordResetViewModel()
        sut = R.storyboard.authForgotPassword.verifyPasswordResetController()!
        sut.viewModel = viewModel
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.titleLabel).toNot(beNil())
          expect(sut.messageLabel).toNot(beNil())
          expect(sut.subMessageLabel).toNot(beNil())
          expect(sut.resendButtonView).toNot(beNil())
          expect(sut.textFields).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.inputControllers).toNot(beNil())
        }

        it("should have equal count of textFields and inputControllers") {
          expect(sut.inputControllers.count).to(equal(sut.textFields.count))
        }

        it("should have textFields with correct tags") {
          let expectedTags = [1, 2, 3, 4, 5]
          let tags = sut.textFields.map { $0.tag }

          expect(tags).to(equal(expectedTags))
        }

        it("should have labels reflecting viewModel values") {
          expect(sut.titleLabel.text).to(equal(viewModel.titleText))
          expect(sut.messageLabel.text).to(equal(viewModel.messageText))
          expect(sut.subMessageLabel.text).to(equal(viewModel.subMessageText))
        }

        describe("code verification") {
          context("when all textFields are filled") {
            it("should call viewModel.verify") {
              expect(viewModel.verifyCallCount).to(equal(0))

              self.fillInTextFields(sut.textFields)

              expect(viewModel.verifyCallCount).toEventually(equal(1))
            }
          }

          context("when initialized to succeed") {
            beforeEach {
              nc = MockNavigationController(rootViewController: UIViewController())
              nc.loadViewIfNeeded()
              nc.pushViewController(sut, animated: false)

              viewModel.errorToReturn = nil
              sut.loadViewIfNeeded()
            }

            afterEach {
              nc = nil
            }

            it("should navigate to NewPasswordController") {
              let currentCallCount = nc.pushViewControllerCallCount

              self.fillInTextFields(sut.textFields)

              expect(nc.pushViewControllerCallCount).toEventually(equal(currentCallCount + 1))
              expect(nc.pushedViewController).to(beAKindOf(NewPasswordController.self))
            }
          }

          context("when initialized to fail") {
            beforeEach {
              viewModel.errorToReturn = NSError(domain: #function, code: 1, userInfo: nil)
            }

            afterEach {
              viewModel.errorToReturn = nil
            }

            // TODO: Test SVProgressHUD is shown with error message

            it("should clear all textFields") {
              self.fillInTextFields(sut.textFields)

              expect(self.areTextFieldsEmpty(sut.textFields)).toEventually(beTrue())
            }
          }
        }

        describe("resend code") {
          context("when resend code button is tapped") {
            it("should call viewModel.resendCode") {
              expect(viewModel.resendCodeCallCount).to(equal(0))

              sut.resendButtonView.button.tap()

              expect(viewModel.resendCodeCallCount).to(equal(1))
            }
          }

          context("when initialized to succeed") {
            beforeEach {
              viewModel.errorToReturn = nil
            }

            it("should clear all textfields") {
              sut.resendButtonView.button.tap()

              expect(self.areTextFieldsEmpty(sut.textFields)).to(beTrue())
            }
          }

          // TODO: Test for failure. Test if SVProgressHUD with error message is shown
        }
      }
    }
  }

  private func areTextFieldsEmpty(_ textFields: [VerificationCodeField]) -> Bool {
    let code = textFields
      .map { $0.text ?? "" }
      .joined(separator: "")
      .trimmed

    return code.isEmpty
  }

  private func fillInTextFields(_ textFields: [VerificationCodeField]) {
    textFields.forEach { textField in
      let code = Int.random(in: 0 ... 9)
      textField.setText(String(code))
    }
  }
}
