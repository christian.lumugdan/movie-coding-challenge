//
//  VerifyPasswordResetViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class VerifyPasswordResetViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("VerifyPasswordResetViewModel") {
      var sut: VerifyPasswordResetViewModel!
      var service: MockForgotPasswordService!
      let username = "ios@appetiser.com.au"
      let token = "12345"

      beforeEach {
        service = MockForgotPasswordService()
        sut = VerifyPasswordResetViewModel(
          username: username,
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      it("should use init username as subMessageText") {
        expect(sut.subMessageText).to(equal(username))
      }

      describe("resendCode") {
        context("when requesting to resendCode") {
          it("should call service.sendPasswordResetRequest") {
            expect(service.sendPasswordResetRequestCallCount).to(equal(0))

            sut.resendCode(
              onSuccess: DefaultClosure.voidResult(),
              onError: DefaultClosure.singleResult()
            )

            expect(service.sendPasswordResetRequestCallCount).to(equal(1))
          }
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure") {
            var onSuccessCallCount = 0

            sut.resendCode(
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            service.errorToReturn = expectedError
          }

          it("should receive expected error via onError callback") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.resendCode(
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }

      describe("verify") {
        context("when requesting to verify") {
          it("should call service.confirmPasswordReset") {
            expect(service.confirmPasswordResetCallCount).to(equal(0))

            sut.verify(
              with: token,
              onSuccess: DefaultClosure.singleResult(),
              onError: DefaultClosure.singleResult()
            )

            expect(service.confirmPasswordResetCallCount).to(equal(1))
          }
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure") {
            var onSuccessCallCount = 0

            sut.verify(
              with: token,
              onSuccess: { _ in onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            service.errorToReturn = expectedError
          }

          it("should receive expected error via onError callback") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.verify(
              with: token,
              onSuccess: DefaultClosure.singleResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }
    }
  }
}
