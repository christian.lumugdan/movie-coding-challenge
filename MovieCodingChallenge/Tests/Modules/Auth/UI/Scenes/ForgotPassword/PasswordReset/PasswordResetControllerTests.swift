//
//  PasswordResetControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PasswordResetControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PasswordResetController") {
      var sut: PasswordResetController!
      var nc: MockNavigationController!
      var viewModel: MockPasswordResetViewModel!

      let initialUsername = "ios@appetiser.com.au"

      beforeEach {
        viewModel = MockPasswordResetViewModel()
        sut = R.storyboard.authForgotPassword.passwordResetController()!
        sut.viewModel = viewModel
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          viewModel.initialUsername = initialUsername
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.usernameField).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.usernameInputController).toNot(beNil())
        }

        it("should prefill the usernameField with initial username") {
          expect(sut.usernameField.text).to(equal(initialUsername))
        }

        describe("continue button") {
          beforeEach {
            sut.usernameField.text = ""
          }

          context("when usernameField is empty") {
            it("should be disabled") {
              sut.usernameField.setText("")
              expect(sut.continueButton.isEnabled).to(beFalse())
            }
          }

          context("when usernameField is not empty") {
            it("should be enabled") {
              sut.usernameField.setText(initialUsername)
              expect(sut.continueButton.isEnabled).to(beTrue())
            }
          }
        }

        describe("viewModel") {
          // TODO: Test for failure. Find way to test SVProgressHUD.

          context("when set to succeed") {
            beforeEach {
              viewModel.errorToReturn = nil

              nc = MockNavigationController(rootViewController: UIViewController())
              nc.loadViewIfNeeded()
              nc.pushViewController(sut, animated: false)

              sut.loadViewIfNeeded()
            }

            it("should push VerifyPasswordController once") {
              let currentCallCount = nc.pushViewControllerCallCount

              sut.continueButton.tap()

              expect(nc.pushedViewController).to(beAKindOf(VerifyPasswordResetController.self))
              expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
            }
          }
        }
      }
    }
  }
}
