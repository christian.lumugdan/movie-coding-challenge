//
//  MockPasswordResetViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockPasswordResetViewModel: PasswordResetViewModelProtocol {
  var initialUsername: String = ""
  var usernameAttribute: TextFieldAttributeType = .email

  var errorToReturn: Error?

  private(set) var sendPasswordResetRequestCallCount: Int = 0
}

extension MockPasswordResetViewModel {
  func sendPasswordResetRequest(
    for username: String?,
    onSuccess: @escaping SingleResult<VerifyPasswordResetViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    sendPasswordResetRequestCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(MockVerifyPasswordResetViewModel())
    }
  }
}
