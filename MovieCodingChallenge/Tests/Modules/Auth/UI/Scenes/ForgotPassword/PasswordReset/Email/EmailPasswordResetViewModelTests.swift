//
//  EmailPasswordResetViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailPasswordResetViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("EmailPasswordResetViewModel") {
      var sut: EmailPasswordResetViewModel!
      var service: MockForgotPasswordService!
      let email = "ios@appetiser.com.au"

      beforeEach {
        service = MockForgotPasswordService()
        sut = EmailPasswordResetViewModel(
          email: email,
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      it("should use init email as initialUsername") {
        expect(sut.initialUsername).to(equal(email))
      }

      it("should have usernameAttribute of .email") {
        expect(sut.usernameAttribute).to(equal(.email))
      }

      context("when sending password reset request") {
        it("should call service.sendPasswordResetRequest") {
          expect(service.sendPasswordResetRequestCallCount).to(equal(0))

          sut.sendPasswordResetRequest(
            for: email,
            onSuccess: DefaultClosure.singleResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.sendPasswordResetRequestCallCount).to(equal(1))
        }
      }

      context("when service is set to succeed") {
        beforeEach {
          service.errorToReturn = nil
        }

        it("should call onSuccess closure") {
          var onSuccessCallCount = 0

          sut.sendPasswordResetRequest(
            for: email,
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when service is set to fail") {
        let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          service.errorToReturn = expectedError
        }

        it("should receive expected error via onError callback") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.sendPasswordResetRequest(
            for: email,
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
