//
//  PhonePasswordResetViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhonePasswordResetViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhonePasswordResetViewModel") {
      var sut: PhonePasswordResetViewModel!
      var service: MockForgotPasswordService!
      let phoneNumber = "123456789"

      beforeEach {
        service = MockForgotPasswordService()
        sut = PhonePasswordResetViewModel(
          fullPhoneNumber: phoneNumber,
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      it("should use init email as initialUsername") {
        expect(sut.initialUsername).to(equal(phoneNumber))
      }

      it("should have usernameAttribute of .phoneNumber") {
        expect(sut.usernameAttribute).to(equal(.phoneNumber))
      }

      context("when sending password reset request") {
        it("should call service.sendPasswordResetRequest") {
          expect(service.sendPasswordResetRequestCallCount).to(equal(0))

          sut.sendPasswordResetRequest(
            for: phoneNumber,
            onSuccess: DefaultClosure.singleResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.sendPasswordResetRequestCallCount).to(equal(1))
        }
      }

      context("when service is set to succeed") {
        beforeEach {
          service.errorToReturn = nil
        }

        it("should call onSuccess closure") {
          var onSuccessCallCount = 0

          sut.sendPasswordResetRequest(
            for: phoneNumber,
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when service is set to fail") {
        let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          service.errorToReturn = expectedError
        }

        it("should receive expected error via onError callback") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.sendPasswordResetRequest(
            for: phoneNumber,
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
