//
//  MockNewPasswordViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockNewPasswordViewModel: NewPasswordViewModelProtocol {
  var titleText: String = ""
  var messageText: String = ""

  var errorToReturn: Error?

  private(set) var resetPasswordCallCount: Int = 0
}

extension MockNewPasswordViewModel {
  func resetPassword(
    with newPassword: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    resetPasswordCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
