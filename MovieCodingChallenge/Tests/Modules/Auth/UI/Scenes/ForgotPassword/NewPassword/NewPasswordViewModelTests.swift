//
//  NewPasswordViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class NewPasswordViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("NewPasswordViewModel") {
      var sut: NewPasswordViewModel!
      var service: MockForgotPasswordService!
      let username = "ios@appetiser.com.au"
      let token = "123456"

      beforeEach {
        service = MockForgotPasswordService()
        sut = NewPasswordViewModel(
          username: username,
          token: token,
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      context("when resetting password") {
        var newPassword: String!

        beforeEach {
          newPassword = "mynewpassword"
        }

        afterEach {
          newPassword = nil
        }

        it("should pass correct parameters and call service.resetPassword") {
          expect(service.resetPasswordCallCount).to(equal(0))
          expect(service.resetPasswordParams).to(beNil())

          sut.resetPassword(
            with: newPassword,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          let expectedParams = UpdatePasswordRequestParams(
            username: username,
            token: token,
            password: newPassword,
            passwordConfirmation: newPassword
          )
          expect(service.resetPasswordCallCount).to(equal(1))
          expect(service.resetPasswordParams).to(equal(expectedParams))
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure") {
            var onSuccessCallCount = 0

            sut.resetPassword(
              with: newPassword,
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            service.errorToReturn = expectedError
          }

          it("should receive expected error via onError callback") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.resetPassword(
              with: newPassword,
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }
    }
  }
}
