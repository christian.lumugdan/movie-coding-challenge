//
//  NewPasswordControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class NewPasswordControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("NewPasswordController") {
      var sut: NewPasswordController!
      var nc: MockNavigationController!

      var infoPresenter: MockInfoPresenter!
      var viewModel: MockNewPasswordViewModel!

      beforeEach {
        viewModel = MockNewPasswordViewModel()
        infoPresenter = MockInfoPresenter()

        sut = R.storyboard.authForgotPassword.newPasswordController()!
        sut.viewModel = viewModel
        sut.infoPresenter = infoPresenter
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.titleLabel).toNot(beNil())
          expect(sut.messageLabel).toNot(beNil())
          expect(sut.passwordField).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }

        describe("continue button") {
          context("when screen is loaded") {
            it("should be disabled") {
              expect(sut.continueButton.isEnabled).to(beFalse())
            }
          }

          context("when passwordField is empty") {
            it("should be disabled") {
              sut.passwordField.setText("")
              expect(sut.continueButton.isEnabled).to(beFalse())
            }
          }

          context("when passwordField text is less than 8 characters") {
            it("should be disabled") {
              sut.passwordField.setText("passw")
              expect(sut.continueButton.isEnabled).to(beFalse())
            }
          }

          context("when passwordField text is more than 32 characters") {
            it("should be disabled") {
              sut.passwordField.setText("thequickbrownfoxjumpsoverthelazydogmanytimes")
              expect(sut.continueButton.isEnabled).to(beFalse())
            }
          }

          context("when passwordField text is within 8-32 characters") {
            it("should be enabled") {
              sut.passwordField.setText("ios@appetiser.com.au")
              expect(sut.continueButton.isEnabled).to(beTrue())
            }
          }

          context("when tapped") {
            it("should call viewModel.resetPassword") {
              expect(viewModel.resetPasswordCallCount).to(equal(0))

              sut.continueButton.tap()

              expect(viewModel.resetPasswordCallCount).to(equal(1))
            }
          }
        }

        describe("keyboard return key") {
          context("when passwordField is empty") {
            it("should not call viewModel.resetPassword") {
              expect(viewModel.resetPasswordCallCount).to(equal(0))

              sut.passwordField.text = ""
              sut.passwordField.sendActions(for: .editingDidEndOnExit)

              expect(viewModel.resetPasswordCallCount).to(equal(0))
            }
          }

          context("when passwordField is not empty") {
            it("should call viewModel.resetPassword") {
              expect(viewModel.resetPasswordCallCount).to(equal(0))

              sut.passwordField.text = "newpassword"
              sut.passwordField.sendActions(for: .editingDidEndOnExit)

              expect(viewModel.resetPasswordCallCount).to(equal(1))
            }
          }
        }

        describe("viewModel") {
          context("when initialized to fail") {
            beforeEach {
              viewModel.errorToReturn = NSError(domain: #function, code: 1, userInfo: nil)
            }

            afterEach {
              viewModel.errorToReturn = nil
            }

            it("should call infoPresenter.presentErrorInfo once on tap of continueButton") {
              expect(infoPresenter.presentErrorInfoCallCount).to(equal(0))
              expect(infoPresenter.presentErrorInfoValue).to(beNil())

              sut.continueButton.tap()

              expect(infoPresenter.presentErrorInfoCallCount).to(equal(1))
              expect(infoPresenter.presentErrorInfoValue).to(be(viewModel.errorToReturn))
            }
          }

          // No test for viewModel success since controller is dismissed
        }

        describe("nav flow") {
          context("when LoginFormController is in the flow") {
            beforeEach {
              let loginController = R.storyboard.authLogin.loginFormController()!
              loginController.viewModel = MockLoginFormViewModel()
              nc = MockNavigationController(rootViewController: loginController)
              nc.loadViewIfNeeded()
              nc.pushViewController(sut, animated: false)

              sut.loadViewIfNeeded()
            }

            afterEach {
              nc = nil
            }

            it("should call navigationController.popToViewController once and go back to LoginFormController on tap of continue button") {
              let currentCallCount = nc.popToViewControllerCallCount

              sut.continueButton.tap()

              expect(nc.popToViewControllerCallCount).to(equal(currentCallCount + 1))
              expect(nc.viewControllers.first).to(beAnInstanceOf(LoginFormController.self))
            }
          }

          context("when LoginFormController is not in the flow") {
            beforeEach {
              nc = MockNavigationController(rootViewController: UIViewController())
              nc.loadViewIfNeeded()
              nc.pushViewController(sut, animated: false)

              sut.loadViewIfNeeded()
            }

            afterEach {
              nc = nil
            }

            it("should call navigationController.popToRootViewController once on tap of continue button") {
              let currentCallCount = nc.popToRootViewControllerCallCount

              sut.continueButton.tap()

              expect(nc.popToRootViewControllerCallCount).to(equal(currentCallCount + 1))
            }
          }
        }
      }
    }
  }
}
