//
//  MockOTPAuthViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockOTPAuthViewModel: OTPAuthViewModelProtocol {
  var titleText: String = ""
  var messageText: String = ""
  var subMessageText: String?

  var errorToReturn: Error?

  private(set) var processOTPCallCount: Int = 0
  private(set) var processOTPValue: String?

  private(set) var resendCodeCallCount: Int = 0
}

// MARK: - Methods

extension MockOTPAuthViewModel {
  func processOTP(
    _ otp: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    processOTPCallCount += 1
    processOTPValue = otp

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    resendCodeCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
