//
//  OTPAuthControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OTPAuthControllerTests: QuickSpec {
  override func spec() {
    describe("OTPAuthController") {
      var sut: OTPAuthController!
      var viewModel: MockOTPAuthViewModel!

      beforeEach {
        viewModel = MockOTPAuthViewModel()
        viewModel.titleText = "Title"
        viewModel.messageText = "Message"
        viewModel.subMessageText = "SubMessage"

        sut = R.storyboard.otpAuth.otpAuthController()!
        sut.viewModel = viewModel
      }

      afterEach {
        sut = nil
        viewModel = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.titleLabel).toNot(beNil())
          expect(sut.messageLabel).toNot(beNil())
          expect(sut.subMessageLabel).toNot(beNil())
          expect(sut.resendButtonView).toNot(beNil())
          expect(sut.textFields).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.inputControllers).toNot(beNil())
        }

        it("should set label values from viewModel") {
          expect(sut.titleLabel.text).to(equal("Title"))
          expect(sut.messageLabel.text).to(equal("Message"))
          expect(sut.subMessageLabel.text).to(equal("SubMessage"))
        }
      }
    }
  }
}
