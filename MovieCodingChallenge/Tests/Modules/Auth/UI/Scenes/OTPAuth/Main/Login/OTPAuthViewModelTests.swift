//
//  OTPLoginViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OTPLoginViewModelTests: QuickSpec {
  override func spec() {
    describe("OTPLoginViewModel") {
      var sut: OTPLoginViewModel!
      var usernameChecker: MockUsernameCheckerService!
      var service: MockLoginService!

      beforeEach {
        usernameChecker = MockUsernameCheckerService()
        service = MockLoginService()
        sut = OTPLoginViewModel(
          fullPhoneNumber: "+639123456789",
          usernameChecker: usernameChecker,
          service: service
        )
      }

      afterEach {
        sut = nil
        service = nil
        usernameChecker = nil
      }

      it("should return correct titleText") {
        expect(sut.titleText).to(equal(S.otpAuthLoginTitle()))
      }

      it("should return correct messageText") {
        expect(sut.messageText).to(equal(S.otpAuthLoginMessage()))
      }

      it("should return correct subMessageText") {
        expect(sut.subMessageText).to(equal("+639123456789".formattedPhoneNumber))
      }

      it("should call service.loginWithPhoneNumber once on processOTP") {
        expect(service.loginWithPhoneNumberCallCount).to(equal(0))
        expect(service.loginWithPhoneNumberValue).to(beNil())
        expect(service.loginWithPhoneNumberOTP).to(beNil())

        sut.processOTP(
          "OTP",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.loginWithPhoneNumberCallCount).to(equal(1))
        expect(service.loginWithPhoneNumberValue).to(equal("+639123456789"))
        expect(service.loginWithPhoneNumberOTP).to(equal("OTP"))
      }

      it("should call usernameChecker.checkUsernameAvailability once on processOTP") {
        expect(usernameChecker.checkUsernameCallCount).to(equal(0))
        expect(usernameChecker.checkUsernameParamUsername).to(beNil())

        sut.resendCode(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(usernameChecker.checkUsernameCallCount).to(equal(1))
        expect(usernameChecker.checkUsernameParamUsername).to(equal("+639123456789"))
      }
    }
  }
}
