//
//  OTPAddEmailControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OTPAddEmailControllerTests: QuickSpec {
  override func spec() {
    describe("OTPAddEmailController") {
      var sut: OTPAddEmailController!
      var viewModel: MockOTPAddEmailViewModel!

      beforeEach {
        viewModel = MockOTPAddEmailViewModel()
        sut = R.storyboard.otpAuth.otpAddEmailController()!
        sut.viewModel = viewModel
      }

      afterEach {
        sut = nil
        viewModel = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.field).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
          expect(sut.skipButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }

        it("should call viewModel.submit on tap of continueButton") {
          expect(viewModel.submitCallCount).to(equal(0))
          expect(viewModel.submitEmail).to(beNil())

          sut.field.text = "Email"
          sut.continueButton.tap()

          expect(viewModel.submitCallCount).to(equal(1))
          expect(viewModel.submitEmail).to(equal("Email"))
        }

        it("should call viewModel.skip on tap of skipButton") {
          expect(viewModel.skipCallCount).to(equal(0))

          sut.skipButton.tap()

          expect(viewModel.skipCallCount).to(equal(1))
        }
      }
    }
  }
}
