//
//  MockOTPAddEmailViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockOTPAddEmailViewModel: OTPAddEmailViewModelProtocol {
  var errorToReturn: Error?

  private(set) var skipCallCount: Int = 0

  private(set) var submitCallCount: Int = 0
  private(set) var submitEmail: String?
}

// MARK: - Methods

extension MockOTPAddEmailViewModel {
  func skip() {
    skipCallCount += 1
  }

  func submit(
    email: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    submitCallCount += 1
    submitEmail = email

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
