//
//  OTPAddEmailViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OTPAddEmailViewModelTests: QuickSpec {
  override func spec() {
    describe("OTPAddEmailViewModel") {
      var sut: OTPAddEmailViewModel!
      var session: MockSessionService!
      var service: MockProfileService!

      beforeEach {
        session = MockSessionService()
        service = MockProfileService()
        sut = OTPAddEmailViewModel(
          session: session,
          service: service
        )
      }

      afterEach {
        sut = nil
        service = nil
      }

      it("should call session.recordAddOTPEmailComplete on skip") {
        expect(session.recordAddOTPEmailCompleteCallCount).to(equal(0))

        sut.skip()

        expect(session.recordAddOTPEmailCompleteCallCount).to(equal(1))
      }

      it("should call session.recordAddOTPEmail on submit") {
        expect(service.addOnboardingEmailCallCount).to(equal(0))
        expect(service.addOnboardingEmailValue).to(beNil())

        sut.submit(
          email: "dev@appetiser.com.au",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.addOnboardingEmailCallCount).to(equal(1))
        expect(service.addOnboardingEmailValue).to(equal("dev@appetiser.com.au"))
      }
    }
  }
}
