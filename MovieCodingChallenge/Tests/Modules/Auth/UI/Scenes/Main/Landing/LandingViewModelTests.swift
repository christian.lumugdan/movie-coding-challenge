//
//  LandingViewModelTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Nimble
import Quick

@testable import MovieCodingChallenge

class LandingViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("LandingViewModel") {
      var sut: LandingViewModel!
      var socialAuth: MockSocialAuthService!

      beforeEach {
        socialAuth = MockSocialAuthService()
        sut = LandingViewModel(socialAuth: socialAuth)
      }

      afterEach {
        socialAuth = nil
        sut = nil
      }

      it("should call socialAuth.authenticateWithApple and pass token on authenticateWithApple") {
        expect(socialAuth.authenticateWithAppleCallCount).to(equal(0))
        expect(socialAuth.authenticateWithAppleToken).to(beNil())

        sut.authenticateWithApple(
          using: "AppleToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(socialAuth.authenticateWithAppleCallCount).to(equal(1))
        expect(socialAuth.authenticateWithAppleToken).to(equal("AppleToken"))
      }

      it("should call socialAuth.authenticateWithGoogle and pass token on authenticateWithGoogle") {
        expect(socialAuth.authenticateWithGoogleCallCount).to(equal(0))
        expect(socialAuth.authenticateWithGoogleToken).to(beNil())

        sut.authenticateWithGoogle(
          using: "GoogleToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(socialAuth.authenticateWithGoogleCallCount).to(equal(1))
        expect(socialAuth.authenticateWithGoogleToken).to(equal("GoogleToken"))
      }

      it("should call socialAuth.authenticateWithFacebook and pass token on authenticateWithFacebook") {
        expect(socialAuth.authenticateWithFacebookCallCount).to(equal(0))
        expect(socialAuth.authenticateWithFacebookToken).to(beNil())

        sut.authenticateWithFacebook(
          using: "FacebookToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(socialAuth.authenticateWithFacebookCallCount).to(equal(1))
        expect(socialAuth.authenticateWithFacebookToken).to(equal("FacebookToken"))
      }

      context("when initialized with successful socialAuth") {
        beforeEach {
          socialAuth.errorToReturn = nil
        }

        it("should call onSuccess callback on authenticateWithApple") {
          var onSuccessCalled = false

          sut.authenticateWithApple(
            using: "",
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }

        it("should call onSuccess callback on authenticateWithGoogle") {
          var onSuccessCalled = false

          sut.authenticateWithGoogle(
            using: "",
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }

        it("should call onSuccess callback on authenticateWithFacebook") {
          var onSuccessCalled = false

          sut.authenticateWithFacebook(
            using: "",
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }
      }

      context("when initialized with failing socialAuth") {
        var authError: Error!

        beforeEach {
          authError = NSError(domain: #function, code: 0, userInfo: nil)
          socialAuth.errorToReturn = authError
        }

        it("should call onError callback and pass expected error on authenticateWithApple") {
          var passedError: Error?

          sut.authenticateWithApple(
            using: "",
            onSuccess: DefaultClosure.voidResult(),
            onError: { passedError = $0 }
          )

          expect(passedError).toEventually(be(authError))
        }

        it("should call onError callback and pass expected error on authenticateWithGoogle") {
          var passedError: Error?

          sut.authenticateWithGoogle(
            using: "",
            onSuccess: DefaultClosure.voidResult(),
            onError: { passedError = $0 }
          )

          expect(passedError).toEventually(be(authError))
        }

        it("should call onError callback and pass expected error on authenticateWithFacebook") {
          var passedError: Error?

          sut.authenticateWithFacebook(
            using: "",
            onSuccess: DefaultClosure.voidResult(),
            onError: { passedError = $0 }
          )
          expect(passedError).toEventually(be(authError))
        }
      }
    }
  }
}
