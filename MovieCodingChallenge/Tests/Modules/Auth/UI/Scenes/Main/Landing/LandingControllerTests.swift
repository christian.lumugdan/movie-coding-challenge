//
//  LandingControllerTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Nimble
import Quick

@testable import MovieCodingChallenge

class LandingControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("LandingController") {
      var sut: LandingController!
      var viewModel: MockLandingViewModel!
      var applePresenter: MockAppleSignInPresenter!
      var googlePresenter: MockGoogleSignInPresenter!
      var fbPresenter: MockFacebookLoginPresenter!

      beforeEach {
        viewModel = MockLandingViewModel()
        applePresenter = MockAppleSignInPresenter()
        googlePresenter = MockGoogleSignInPresenter()
        fbPresenter = MockFacebookLoginPresenter()

        sut = R.storyboard.auth.landingController()!
        sut.viewModel = viewModel
        sut.applePresenter = applePresenter
        sut.googlePresenter = googlePresenter
        sut.fbPresenter = fbPresenter
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.appNameLabel).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
          expect(sut.appleSignInButton).toNot(beNil())
          expect(sut.googleSignInButton).toNot(beNil())
          expect(sut.facebookSignInButton).toNot(beNil())
          expect(sut.tosAcceptanceLabel).toNot(beNil())
          expect(sut.versionInfoView).toNot(beNil())
        }

        it("should set callbacks of presenters") {
          expect(applePresenter.onSuccess).toNot(beNil())
          expect(applePresenter.onError).toNot(beNil())
          expect(applePresenter.onCancel).toNot(beNil())
          expect(googlePresenter.onSuccess).toNot(beNil())
          expect(googlePresenter.onError).toNot(beNil())
          expect(googlePresenter.onCancel).toNot(beNil())
        }

        it("should call applePresenter.presentAppleSignIn on tap of appleSignInButton") {
          expect(applePresenter.presentAppleSignInCallCount).to(equal(0))

          sut.appleSignInButton.tap()

          expect(applePresenter.presentAppleSignInCallCount).to(equal(1))
        }

        it("should call googlePresenter.presentGoogleSignIn on tap of googleSignInButton") {
          expect(googlePresenter.presentGoogleSignInCallCount).to(equal(0))

          sut.googleSignInButton.tap()

          expect(googlePresenter.presentGoogleSignInCallCount).to(equal(1))
        }

        it("should call fbPresenter.presentFacebookLogin on tap of appleSignInButton") {
          expect(fbPresenter.presentFacebookLoginCallCount).to(equal(0))

          sut.facebookSignInButton.tap()

          expect(fbPresenter.presentFacebookLoginCallCount).to(equal(1))
        }
      }

      context("when initialized in nav flow") {
        var nc: MockNavigationController!

        beforeEach {
          nc = MockNavigationController(rootViewController: sut)
          nc.loadViewIfNeeded()
        }

        afterEach {
          nc = nil
        }

        it("should call navigationController.setNavigationBarHidden passing hidden == true") {
          expect(nc.setNavigationBarHiddenCallCount).to(equal(0))
          expect(nc.setNavigationBarHiddenValue).to(beNil())

          sut.viewWillAppear(true)

          expect(nc.setNavigationBarHiddenCallCount).to(equal(1))
          expect(nc.setNavigationBarHiddenValue).to(beTrue())
        }
      }
    }
  }
}
