//
//  MockLandingViewModel.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockLandingViewModel: LandingViewModelProtocol {
  var errorToReturn: Error?

  private(set) var checkUsernameAvailabilityCallCount: Int = 0
  private(set) var checkUsernameAvailabilityUsername: String?

  private(set) var authenticateWithAppleCallCount: Int = 0
  private(set) var authenticateWithFacebookCallCount: Int = 0
  private(set) var authenticateWithGoogleCallCount: Int = 0
}

extension MockLandingViewModel {
  func checkUsernameAvailability(
    username: String,
    onEmailAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onEmailUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onPhoneNumberResult: @escaping SingleResult<OTPAuthViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    checkUsernameAvailabilityCallCount += 1
    checkUsernameAvailabilityUsername = username
  }

  func authenticateWithApple(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authenticateWithAppleCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func authenticateWithGoogle(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authenticateWithGoogleCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func authenticateWithFacebook(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authenticateWithFacebookCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
