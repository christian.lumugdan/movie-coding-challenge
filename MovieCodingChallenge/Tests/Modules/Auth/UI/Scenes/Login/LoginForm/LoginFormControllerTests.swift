//
//  LoginFormControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class LoginFormControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("LoginFormController") {
      var sut: LoginFormController!
      var nc: MockNavigationController!

      var infoPresenter: MockInfoPresenter!
      var viewModel: MockLoginFormViewModel!

      beforeEach {
        viewModel = MockLoginFormViewModel()

        infoPresenter = MockInfoPresenter()

        sut = R.storyboard.authLogin.loginFormController()!
        sut.viewModel = viewModel
        sut.infoPresenter = infoPresenter
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.passwordField).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
          expect(sut.passwordResetButton).toNot(beNil())
          expect(sut.usernameLabel).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }

        it("should return viewModel as singleFormInputVM") {
          expect(sut.singleFormInputVM).to(be(viewModel))
        }

        it("should enable continueButton on set of field.text to non-empty password") {
          sut.field.setText("Password")

          expect(sut.continueButton.isEnabled).to(beTrue())
        }

        it("should not enable continueButton on set of field.text to empty password") {
          sut.field.setText("")

          expect(sut.continueButton.isEnabled).to(beFalse())
        }

        it("should pass field value and call viewModel.login on tap of continueButton") {
          sut.field.text = "Password1"
          expect(viewModel.loginPassword).to(beNil())
          expect(viewModel.loginCallCount).to(equal(0))

          sut.continueButton.tap()

          expect(viewModel.loginPassword).to(equal("Password1"))
          expect(viewModel.loginCallCount).to(equal(1))
        }

        it("should pass field value and call viewModel.login on tap of field keyboard return key") {
          sut.field.text = "Password2"
          expect(viewModel.loginPassword).to(beNil())
          expect(viewModel.loginCallCount).to(equal(0))

          sut.field.tapReturnKey()

          expect(viewModel.loginPassword).to(equal("Password2"))
          expect(viewModel.loginCallCount).to(equal(1))
        }
      }

      context("when initialized in nav flow") {
        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)

          sut.loadViewIfNeeded()
        }

        afterEach {
          nc = nil
        }

        it("should show the navigation bar on viewWillAppear") {
          sut.viewWillAppear(false)

          expect(sut.navigationController?.navigationBar.isHidden).to(beFalse())
        }

        it("should push PasswordResetController once on tap of passwordResetButton") {
          let currentCallCount = nc.pushViewControllerCallCount

          sut.passwordResetButton.tap()

          expect(nc.pushedViewController).to(beAKindOf(PasswordResetController.self))
          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
        }
      }

      context("when viewModel is set to fail") {
        var submitError: Error!

        beforeEach {
          submitError = NSError(domain: #function, code: 1, userInfo: nil)
          viewModel.errorToReturn = submitError

          sut.loadViewIfNeeded()
        }

        afterEach {
          submitError = nil
        }

        it("should call infoPresenter.presentErrorInfo once on tap of continueButton") {
          expect(infoPresenter.presentErrorInfoCallCount).to(equal(0))
          expect(infoPresenter.presentErrorInfoValue).to(beNil())

          sut.continueButton.tap()

          expect(infoPresenter.presentErrorInfoCallCount).to(equal(1))
          expect(infoPresenter.presentErrorInfoValue).to(be(submitError))
        }
      }

      context("when viewModel is set to succeed") {
        beforeEach {
          viewModel.errorToReturn = nil

          sut.loadViewIfNeeded()
        }

        it("should clear fieldInputController.errorText on tap of continueButton") {
          sut.fieldInputController.setErrorText("Existing error message", errorAccessibilityValue: nil)

          sut.continueButton.tap()

          expect(sut.fieldInputController.errorText).to(beNil())
        }
      }
    }
  }
}
