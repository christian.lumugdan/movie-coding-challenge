//
//  MockLoginFormViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockLoginFormViewModel: LoginFormViewModelProtocol {
  var usernameText: String = ""
  var passwordResetVM: PasswordResetViewModelProtocol = MockPasswordResetViewModel()

  var errorToReturn: Error?

  private(set) var loginPassword: String?
  private(set) var loginCallCount: Int = 0
}

extension MockLoginFormViewModel {
  func login(
    with password: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    loginPassword = password
    loginCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
