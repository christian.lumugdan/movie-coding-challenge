//
//  MockRegistrationViewModel.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockRegistrationViewModel: OnboardingNameViewModelProtocol {
  private(set) var submitCallCount: Int = 0
  private let submitError: Error?

  init(submitError: Error? = nil) {
    self.submitError = submitError
  }

  func submit(
    name: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    submitCallCount += 1

    if let e = submitError {
      onError(e)
    } else {
      onSuccess()
    }
  }
}

// MARK: - Getters

extension MockRegistrationViewModel {
  var nextPageVM: OnboardingPictureViewModelProtocol { MockOnboardingPictureViewModel() }
}
