//
//  OnboardingNameInputValidatorTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OnboardingNameInputValidatorTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("OnboardingNameInputValidator") {
      typealias Inputs = OnboardingNameInputValidator.Inputs
      typealias ValidInputs = OnboardingNameInputValidator.ValidInputs
      typealias ValidationError = OnboardingNameInputValidator.ValidationError

      var inputs: OnboardingNameInputValidator.Inputs!
      var expectedResult: Result<ValidInputs, ValidationError>!

      describe("ValidationError") {
        context("requiredName") {
          it("should have correct errorDescription") {
            let expectedValue = R.string.localizable.nameFormErrorNameRequired()
            expect(ValidationError.requiredName.localizedDescription).to(equal(expectedValue))
          }
        }
      }

      context("when validate method called with nil input") {
        beforeEach {
          inputs = Inputs(nil)
        }

        it("should return failure with requiredName error") {
          expectedResult = .failure(ValidationError.requiredName)

          let result = OnboardingNameInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      context("when validate method called with empty input") {
        beforeEach {
          inputs = Inputs("")
        }

        it("should return failure with requiredName error") {
          expectedResult = .failure(ValidationError.requiredName)

          let result = OnboardingNameInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      context("when validate method called with non-nil input") {
        let testName = "Test Name"

        beforeEach {
          inputs = Inputs(testName)
        }

        it("should return success") {
          expectedResult = .success(testName)

          let result = OnboardingNameInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      afterEach {
        inputs = nil
      }
    }
  }
}
