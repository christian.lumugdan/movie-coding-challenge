//
//  MockOnboardingPictureViewModel.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockOnboardingPictureViewModel: OnboardingPictureViewModelProtocol {
  private(set) var submitPictureCallCount: Int = 0
  private(set) var skipCallCount: Int = 0
  private let submitPictureError: Error?

  init(submitError: Error? = nil) {
    submitPictureError = submitError
  }

  func submitPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    submitPictureCallCount += 1

    if let e = submitPictureError {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func skip() {
    skipCallCount += 1
  }
}
