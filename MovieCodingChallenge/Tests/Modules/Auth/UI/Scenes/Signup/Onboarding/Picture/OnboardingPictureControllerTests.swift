//
//  OnboardingPictureControllerTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OnboardingPictureControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("OnboardingPictureController") {
      var sut: OnboardingPictureController!

      var viewModelSubmitError: Error!
      var viewModel: MockOnboardingPictureViewModel!

      var inputCache: MockOnboardingInputCache!

      var imagePickerPresenter: MockImagePresenter!

      beforeEach {
        viewModel = MockOnboardingPictureViewModel()

        inputCache = MockOnboardingInputCache()

        imagePickerPresenter = MockImagePresenter()

        sut = R.storyboard.authSignup.onboardingPictureController()!
        sut.viewModel = viewModel
        sut.inputCache = inputCache
        sut.imagePickerPresenter = imagePickerPresenter
      }

      afterEach {
        viewModel = nil
        inputCache = nil
        imagePickerPresenter = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.firstStepsView).toNot(beNil())
          expect(sut.secondStepsView).toNot(beNil())
          expect(sut.pictureImageView).toNot(beNil())
          expect(sut.pictureButton).toNot(beNil())
          expect(sut.primaryButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.skipButton).toNot(beNil())
        }

        it("should call inputCache.cachePicture on viewWillDisappear") {
          expect(inputCache.cachePictureCallCount).to(equal(0))

          sut.viewWillDisappear(false)

          expect(inputCache.cachePictureCallCount).to(equal(1))
        }

        it("should call viewModel.submitPicture on tap of primaryButton") {
          sut.pictureImageView.image = R.image.arrowLeftBlack()!
          expect(viewModel.submitPictureCallCount).to(equal(0))

          sut.primaryButton.tap()

          expect(viewModel.submitPictureCallCount).to(equal(1))
        }

        it("should call imagePickerPresenter.presentImagePickerOptions on tap of pictureButton") {
          expect(imagePickerPresenter.presentImagePickerOptionsCallCount).to(equal(0))

          sut.pictureButton.tap()

          expect(imagePickerPresenter.presentImagePickerOptionsCallCount).to(equal(1))
        }

        it("should call viewModel.skip on tap of skipButton") {
          expect(viewModel.skipCallCount).to(equal(0))

          sut.skipButton.tap()

          expect(viewModel.skipCallCount).to(equal(1))
        }

        it("should set pictureImageView.image when imagePicker.imagePickerController(_:didFinishPickingMediaWithInfo:) is called") {
          let expectedImage = R.image.camera()!
          expect(sut.pictureImageView.image).to(beNil())

          let imagePicker = sut.imagePicker
          expect(sut.pictureImageView.image).to(beNil())
          let delegate = imagePicker!.delegate
          delegate?.imagePickerController?(
            imagePicker!,
            didFinishPickingMediaWithInfo: [.editedImage: expectedImage]
          )

          expect(sut.pictureImageView.image).to(be(expectedImage))
        }
      }

      context("when inputCache has cachedPicture and view is loaded") {
        var inputCachePicture: UIImage!

        beforeEach {
          inputCachePicture = R.image.arrowLeftBlack()!
          inputCache = MockOnboardingInputCache(picture: inputCachePicture)

          sut.inputCache = inputCache
          sut.loadViewIfNeeded()
        }

        afterEach {
          inputCachePicture = nil
          inputCache = nil
        }

        it("should have pictureImageView.image based on inputCache.picture") {
          expect(sut.pictureImageView.image).to(be(inputCachePicture))
        }
      }

      context("when pushed in nav flow and view is loaded") {
        var nc: MockNavigationController!

        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)

          sut.loadViewIfNeeded()
        }

        afterEach {
          nc = nil
        }

        it("should call navigationController.setNavigationBarHidden on viewWillAppear") {
          expect(nc.navigationBar.isHidden).to(beFalse())

          sut.viewWillAppear(false)

          expect(nc.navigationBar.isHidden).to(beFalse())
        }
      }

      context("when initialized with failing viewModel.submit") {
        beforeEach {
          viewModelSubmitError = NSError(domain: #function, code: 1, userInfo: nil)
          viewModel = MockOnboardingPictureViewModel(submitError: viewModelSubmitError)
          sut.viewModel = viewModel

          sut.loadViewIfNeeded()
        }

        afterEach {
          viewModelSubmitError = nil
          viewModel = nil
        }

        context("when initialized with successful viewModel.submit") {
          beforeEach {
            viewModel = MockOnboardingPictureViewModel(submitError: nil)
            sut.viewModel = viewModel

            sut.loadViewIfNeeded()
          }

          afterEach {
            viewModel = nil
          }
        }
      }
    }
  }
}
