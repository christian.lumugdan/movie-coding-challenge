//
//  OnboardingNameControllerTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OnboardingNameControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("OnboardingNameController") {
      var sut: OnboardingNameController!
      var nc: MockNavigationController!

      var viewModelSubmitError: Error!
      var viewModel: MockRegistrationViewModel!

      var logoutTriggerVM: MockLogoutTriggerViewModel!

      var inputCacheName: String!
      var inputCache: MockOnboardingInputCache!

      beforeEach {
        viewModel = MockRegistrationViewModel()
        logoutTriggerVM = MockLogoutTriggerViewModel()

        inputCacheName = "Test"
        inputCache = MockOnboardingInputCache(name: inputCacheName)

        sut = R.storyboard.authSignup.onboardingNameController()!
        sut.viewModel = viewModel
        sut.logoutTriggerVM = logoutTriggerVM
        sut.inputCache = inputCache
      }

      afterEach {
        viewModel = nil
        logoutTriggerVM = nil

        inputCacheName = nil
        inputCache = nil

        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.nameField).toNot(beNil())
          expect(sut.primaryButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.nameInputController).toNot(beNil())
        }

        it("should have nameField.text based on inputCache.name") {
          expect(sut.nameField.text).to(equal(inputCacheName))
        }

        it("should call inputCache.cacheName on viewWillDisappear") {
          expect(inputCache.cacheNameCallCount).to(equal(0))

          sut.viewWillDisappear(false)

          expect(inputCache.cacheNameCallCount).to(equal(1))
        }

        it("should call viewModel.submit on tap of primary button") {
          expect(viewModel.submitCallCount).to(equal(0))

          sut.primaryButton.tap()

          expect(viewModel.submitCallCount).to(equal(1))
        }

        it("should call viewModel.submit on tap of keyboard return key in nameField when nameField.text is not empty") {
          expect(viewModel.submitCallCount).to(equal(0))

          sut.nameField.text = "Test"
          sut.nameField.sendActions(for: .editingDidEndOnExit)

          expect(viewModel.submitCallCount).to(equal(1))
        }

        it("should not call viewModel.submit on tap of keyboard return key when nameField.text is empty") {
          expect(viewModel.submitCallCount).to(equal(0))

          sut.nameField.text = ""
          sut.nameField.sendActions(for: .editingDidEndOnExit)

          expect(viewModel.submitCallCount).to(equal(0))
        }
      }

      context("when initialized in nav flow") {
        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)

          sut.loadViewIfNeeded()
        }

        it("should call navigationController.pushViewController once on tap of primary button") {
          let currentCallCount = nc.pushViewControllerCallCount

          sut.primaryButton.tap()

          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
        }

        afterEach {
          nc = nil
        }
      }

      context("when initialized with failing viewModel.submit") {
        beforeEach {
          viewModelSubmitError = NSError(domain: #function, code: 1, userInfo: nil)
          viewModel = MockRegistrationViewModel(submitError: viewModelSubmitError)
          sut.viewModel = viewModel

          sut.loadViewIfNeeded()
        }

        afterEach {
          viewModelSubmitError = nil
          viewModel = nil
        }

        it("should set nameInputController.errorText to error.localizedDescription on tap of primary button") {
          sut.primaryButton.tap()

          expect(sut.nameInputController.errorText).to(equal(viewModelSubmitError.localizedDescription))
        }
      }

      context("when initialized with successful viewModel.submit") {
        beforeEach {
          viewModel = MockRegistrationViewModel(submitError: nil)
          sut.viewModel = viewModel

          sut.loadViewIfNeeded()
        }

        afterEach {
          viewModel = nil
        }

        it("should clear nameInputController.errorText on tap of primary button") {
          sut.nameInputController.setErrorText("Test", errorAccessibilityValue: nil)

          sut.primaryButton.tap()

          expect(sut.nameInputController.errorText).to(beNil())
        }
      }
    }
  }
}
