//
//  OnboardingPictureViewModelTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OnboardingPictureViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("OnboardingPictureViewModel") {
      var sut: OnboardingPictureViewModel!
      var service: MockProfileService!

      context("when initialized with success validate") {
        beforeEach {
          service = MockProfileService()

          sut = OnboardingPictureViewModel(service: service)
        }

        it("should call service.setPicture method on submitPicture") {
          expect(service.setPictureCallCount).to(equal(0))

          sut.submitPicture(
            with: Data(),
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.setPictureCallCount).to(equal(1))
        }

        it("should call service.markOnboardingAsComplete method on skip") {
          expect(service.markOnboardingAsCompleteCallCount).to(equal(0))

          sut.skip()

          expect(service.markOnboardingAsCompleteCallCount).to(equal(1))
        }
      }
    }
  }
}
