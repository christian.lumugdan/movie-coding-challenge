//
//  MockOnboardingInputCache.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

@testable import MovieCodingChallenge

class MockOnboardingInputCache: OnboardingInputCacheProtocol {
  private(set) var cacheNameCallCount: Int = 0
  private(set) var cachePictureCallCount: Int = 0

  let name: String?
  let picture: UIImage?

  init(
    name: String? = nil,
    picture: UIImage? = nil
  ) {
    self.name = name
    self.picture = picture
  }

  func cacheName(_ name: String?) {
    cacheNameCallCount += 1
  }

  func cachePicture(_ picture: UIImage?) {
    cachePictureCallCount += 1
  }
}
