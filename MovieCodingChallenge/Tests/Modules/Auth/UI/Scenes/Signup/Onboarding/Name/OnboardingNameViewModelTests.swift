//
//  OnboardingNameViewModelTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class OnboardingNameViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("OnboardingNameViewModel") {
      var sut: OnboardingNameViewModel!
      var validator: MockOnboardingNameInputValidator!
      var service: MockProfileService!

      let name = ""

      beforeEach {
        validator = MockOnboardingNameInputValidator(shouldSucceed: true)
        service = MockProfileService()

        sut = OnboardingNameViewModel(
          validate: validator.validate,
          service: service
        )
      }

      context("when initialized with success validate") {
        it("should call validate method on submit") {
          expect(validator.validateCallCount).to(equal(0))

          sut.submit(
            name: name,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(validator.validateCallCount).to(equal(1))
        }

        it("should call setName method on submit") {
          expect(service.setNameCallCount).to(equal(0))

          sut.submit(
            name: name,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.setNameCallCount).to(equal(1))
        }
      }

      context("when initialized with failure validate") {
        beforeEach {
          validator = MockOnboardingNameInputValidator(shouldSucceed: false)
          service = MockProfileService()

          sut = OnboardingNameViewModel(
            validate: validator.validate,
            service: service
          )
        }

        it("should call validate method on submit") {
          expect(validator.validateCallCount).to(equal(0))

          sut.submit(
            name: name,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(validator.validateCallCount).to(equal(1))
        }

        it("should call onError closure callback on submit") {
          var onErrorCalled = false

          sut.submit(
            name: name,
            onSuccess: {},
            onError: { _ in onErrorCalled.toggle() }
          )

          expect(onErrorCalled).toEventually(beTrue())
        }
      }

      context("when initialized with success validate and failure setName") {
        let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          validator = MockOnboardingNameInputValidator(shouldSucceed: true)
          service.errorToReturn = expectedError

          sut = OnboardingNameViewModel(
            validate: validator.validate,
            service: service
          )
        }

        it("should call onError closure callback and receive expected error on submit") {
          var submitError: Error!
          var onErrorCalled = false

          sut.submit(
            name: name,
            onSuccess: {},
            onError: { error in
              onErrorCalled.toggle()
              submitError = error
            }
          )

          expect(onErrorCalled).toEventually(beTrue())
          expect(submitError).toEventually(be(expectedError))
        }
      }

      context("when initialized with success validate and success setName") {
        beforeEach {
          validator = MockOnboardingNameInputValidator(shouldSucceed: true)

          sut = OnboardingNameViewModel(
            validate: validator.validate,
            service: service
          )
        }

        it("should call onSuccess closure callback on submit") {
          var onSuccessCalled = false

          sut.submit(
            name: name,
            onSuccess: { onSuccessCalled.toggle() },
            onError: { _ in }
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }
      }

      afterEach {
        sut = nil
        validator = nil
        service = nil
      }
    }
  }
}
