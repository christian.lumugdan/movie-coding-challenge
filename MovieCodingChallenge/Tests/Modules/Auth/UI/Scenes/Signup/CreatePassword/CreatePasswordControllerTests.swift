//
//  CreatePasswordControllerTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class CreatePasswordControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("CreatePasswordController") {
      var sut: CreatePasswordController!
      var nc: MockNavigationController!

      var infoPresenter: MockInfoPresenter!
      var viewModel: MockCreatePasswordViewModel!

      beforeEach {
        viewModel = MockCreatePasswordViewModel()
        viewModel.usernameText = "Username"
        infoPresenter = MockInfoPresenter()

        sut = R.storyboard.authSignup.createPasswordController()!
        sut.viewModel = viewModel
        sut.infoPresenter = infoPresenter
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.scrollContentView).toNot(beNil())
          expect(sut.accountLabel).toNot(beNil())
          expect(sut.passwordField).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }

        it("should return viewModel as singleFormInputVM") {
          expect(sut.singleFormInputVM).to(be(viewModel))
        }

        it("should return passwordField as field") {
          expect(sut.field).to(be(sut.passwordField))
        }

        it("should set accountLabel.text from viewModel.usernameText") {
          expect(sut.accountLabel.text).to(equal("Username"))
        }

        it("should enable continueButton on set of field.text to non-empty password") {
          sut.field.setText("Password")

          expect(sut.continueButton.isEnabled).to(beTrue())
        }

        it("should not enable continueButton on set of field.text to empty password") {
          sut.field.setText("")

          expect(sut.continueButton.isEnabled).to(beFalse())
        }

        it("should pass field value and call viewModel.register on tap of continueButton") {
          sut.field.text = "Password1"
          expect(viewModel.registerPassword).to(beNil())
          expect(viewModel.registerCallCount).to(equal(0))

          sut.continueButton.tap()

          expect(viewModel.registerPassword).to(equal("Password1"))
          expect(viewModel.registerCallCount).to(equal(1))
        }

        it("should pass field value and call viewModel.register on tap of field keyboard return key") {
          sut.field.text = "Password2"
          expect(viewModel.registerPassword).to(beNil())
          expect(viewModel.registerCallCount).to(equal(0))

          sut.field.tapReturnKey()

          expect(viewModel.registerPassword).to(equal("Password2"))
          expect(viewModel.registerCallCount).to(equal(1))
        }
      }

      context("when initialized in nav flow") {
        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)

          sut.loadViewIfNeeded()
        }

        afterEach {
          nc = nil
        }

        it("should show the navigation bar on viewWillAppear") {
          sut.viewWillAppear(false)

          expect(sut.navigationController?.navigationBar.isHidden).to(beFalse())
        }
      }

      context("when viewModel is set to fail") {
        var submitError: Error!

        beforeEach {
          submitError = NSError(domain: #function, code: 1, userInfo: nil)
          viewModel.errorToReturn = submitError

          sut.loadViewIfNeeded()
        }

        afterEach {
          submitError = nil
        }

        it("should call infoPresenter.presentErrorInfo once on tap of continueButton") {
          expect(infoPresenter.presentErrorInfoCallCount).to(equal(0))
          expect(infoPresenter.presentErrorInfoValue).to(beNil())

          sut.continueButton.tap()

          expect(infoPresenter.presentErrorInfoCallCount).to(equal(1))
          expect(infoPresenter.presentErrorInfoValue).to(be(submitError))
        }
      }

      context("when viewModel is set to succeed") {
        beforeEach {
          viewModel.errorToReturn = nil

          sut.loadViewIfNeeded()
        }

        it("should clear fieldInputController.errorText on tap of continueButton") {
          sut.fieldInputController.setErrorText("Existing error message", errorAccessibilityValue: nil)

          sut.continueButton.tap()

          expect(sut.fieldInputController.errorText).to(beNil())
        }
      }
    }
  }
}
