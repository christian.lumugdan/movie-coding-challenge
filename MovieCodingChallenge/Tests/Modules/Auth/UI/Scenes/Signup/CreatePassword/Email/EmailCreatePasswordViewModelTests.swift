//
//  EmailCreatePasswordViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class EmailCreatePasswordViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("EmailCreatePasswordViewModel") {
      var sut: EmailCreatePasswordViewModel!
      var service: MockRegisterService!

      beforeEach {
        service = MockRegisterService()

        sut = EmailCreatePasswordViewModel(
          email: "Email",
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      it("should use init email as usernameText") {
        expect(sut.usernameText).to(equal("Email"))
      }

      context("when valid password is used") {
        var validPassword: String!

        beforeEach {
          validPassword = "Password"
        }

        afterEach {
          validPassword = nil
        }

        it("should pass correct parameters and call service.registerWithEmail once on register") {
          expect(service.registerWithEmailCallCount).to(equal(0))
          expect(service.registerWithEmailValue).to(beNil())
          expect(service.registerWithEmailPassword).to(beNil())

          sut.register(
            with: "Password",
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.registerWithEmailCallCount).to(equal(1))
          expect(service.registerWithEmailValue).to(equal("Email"))
          expect(service.registerWithEmailPassword).to(equal("Password"))
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure once on register") {
            var onSuccessCallCount = 0

            sut.register(
              with: validPassword,
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          var expectedError: Error!

          beforeEach {
            expectedError = NSError(domain: #function, code: 1, userInfo: nil)
            service.errorToReturn = expectedError
          }

          afterEach {
            expectedError = nil
          }

          it("should receive expected error via onError callback on register") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.register(
              with: validPassword,
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }

      context("when invalid password is used") {
        var invalidPassword: String!

        beforeEach {
          invalidPassword = ""
        }

        afterEach {
          invalidPassword = nil
        }

        it("should pass back NewPasswordInputValidator.ValidationError to onError closure on register") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.register(
            with: invalidPassword,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(beAKindOf(NewPasswordInputValidator.ValidationError.self))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
