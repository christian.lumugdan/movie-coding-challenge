//
//  MockCreatePasswordViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockCreatePasswordViewModel: CreatePasswordViewModelProtocol {
  var usernameText: String = ""

  var errorToReturn: Error?

  private(set) var registerPassword: String?
  private(set) var registerCallCount: Int = 0
}

extension MockCreatePasswordViewModel {
  func register(
    with password: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    registerPassword = password
    registerCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
