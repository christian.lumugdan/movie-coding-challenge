//
//  PhoneVerificationViewModelTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PhoneVerificationViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhoneVerificationViewModel") {
      var sut: PhoneVerificationViewModel!
      var service: MockAccountVerificationService!
      var user: User!

      beforeEach {
        service = MockAccountVerificationService()
        user = User(phoneNumber: "Phone")

        sut = PhoneVerificationViewModel(
          service: service,
          user: user
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      it("should return correct titleText") {
        expect(sut.titleText).to(equal(S.accountVerifViaPhoneLabelsTitle()))
      }

      it("should return user.email as messageText") {
        expect(sut.messageText).to(equal(S.accountVerifViaPhoneLabelsDescription()))
      }

      it("should return nil as subMessageText") {
        expect(sut.subMessageText).to(beNil())
      }

      context("when service is set to succeed") {
        beforeEach {
          service.errorToReturn = nil
        }

        it("should call onSuccess closure once on verify") {
          var onSuccessCallCount = 0

          sut.verify(
            using: "Token",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should pass correct parameters and call service.verify once on verify") {
          expect(service.verifyType).to(beNil())
          expect(service.verifyToken).to(beNil())
          expect(service.verifyCallCount).to(equal(0))

          sut.verify(
            using: "Token",
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.verifyType).to(equal(.phone))
          expect(service.verifyToken).to(equal("Token"))
          expect(service.verifyCallCount).to(equal(1))
        }

        it("should call onSuccess closure once on resendCode") {
          var onSuccessCallCount = 0

          sut.resendCode(
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should pass correct parameters and call service.resendCode once on resendCode") {
          expect(service.resendCodeType).to(beNil())
          expect(service.resendCodeCallCount).to(equal(0))

          sut.resendCode(
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.resendCodeType).to(equal(.phone))
          expect(service.resendCodeCallCount).to(equal(1))
        }
      }

      context("and service is set to fail") {
        var expectedError: Error!

        beforeEach {
          expectedError = NSError(domain: #function, code: 1, userInfo: nil)
          service.errorToReturn = expectedError
        }

        afterEach {
          expectedError = nil
        }

        it("should receive expected error and call onError closure once on verify") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.verify(
            using: "Token",
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should receive expected error and call onError closure once on resendCode") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.resendCode(
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
