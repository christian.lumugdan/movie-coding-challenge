//
//  MockAccountVerificationViewModel.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockAccountVerificationViewModel: AccountVerificationViewModelProtocol {
  var shouldLogOutOnBackTap: Bool = false
  var isSkippable: Bool = false

  var titleText: String = ""
  var messageText: String = ""
  var subMessageText: String?

  var errorToReturn: Error?

  private(set) var verifyToken: String?
  private(set) var verifyCallCount: Int = 0

  private(set) var resendCodeCallCount: Int = 0

  private(set) var skipCallCount: Int = 0
}

extension MockAccountVerificationViewModel {
  func verify(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    verifyToken = token
    verifyCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    resendCodeCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func skip() {
    skipCallCount += 1
  }
}
