//
//  PushNotificationServiceTests.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class PushNotificationServiceTests: QuickSpec {
  override func spec() {
    describe("PushNotificationService") {
      var sut: PushNotificationService!
      var api: MockDevicesAPI!
      var session: MockSessionService!
      var deepLink: MockDeepLinkService!

      beforeEach {
        api = MockDevicesAPI()
        session = MockSessionService()
        deepLink = MockDeepLinkService()
        sut = PushNotificationService(
          api: api,
          session: session,
          deepLink: deepLink
        )
      }

      afterEach {
        sut = nil
        api = nil
      }

      // TODO: Add tests
    }
  }
}
