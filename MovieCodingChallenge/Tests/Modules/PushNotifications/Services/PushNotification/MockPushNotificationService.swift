//
//  MockPushNotificationService.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class MockPushNotificationService: PushNotificationServiceProtocol {
  var errorToReturn: Error?

  private(set) var registerDeviceCallCount: Int = 0

  func reset() {
    errorToReturn = nil
    registerDeviceCallCount = 0
  }
}

// MARK: - Methods

extension MockPushNotificationService {
  func registerDevice() {
    registerDeviceCallCount += 1
  }
}
