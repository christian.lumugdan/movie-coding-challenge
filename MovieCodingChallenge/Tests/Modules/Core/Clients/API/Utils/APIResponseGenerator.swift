//
//  APIReponseGenerator.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import XCTest

import Quick

@testable import MovieCodingChallenge

struct APIReponseGenerator {
  static func modelResponse<T: Decodable>(
    from jsonFileName: String,
    spec: XCTestCase,
    decoder: JSONDecoder,
    file: StaticString = #file,
    line: UInt = #line
  ) -> T? {
    do {
      let dict = try spec.jsonDictionaryFromFile(jsonFileName)
      let data = try JSONSerialization.data(withJSONObject: dict)
      return try decoder.decode(T.self, from: data)
    } catch {
      XCTFail(error.localizedDescription, file: file, line: line)
      return nil
    }
  }
}
