//
//  XCTestCase+Utils.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import XCTest

import MovieCodingChallenge

extension XCTestCase {
  func jsonDictionaryFromFile(_ name: String) throws -> JSONDictionary {
    return try MovieCodingChallenge.jsonDictionaryFromFile(name, bundle: Bundle(for: type(of: self)))!
  }
}
