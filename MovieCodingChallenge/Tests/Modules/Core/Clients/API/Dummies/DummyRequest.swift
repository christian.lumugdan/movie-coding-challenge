//
//  DummyRequest.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import MovieCodingChallenge

class DummyRequest: RequestProtocol {
  func resume() {}
  func suspend() {}
  func cancel() {}
}
