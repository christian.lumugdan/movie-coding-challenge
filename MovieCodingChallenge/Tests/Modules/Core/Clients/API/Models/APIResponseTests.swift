//
//  APIResponseTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import MovieCodingChallenge

class APIResponseTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("APIResponse") {
      var apiResponse: APIResponse!

      context("when decoding non status 422 response") {
        beforeEach {
          apiResponse = self.decodeResponseValue(statusCode: .ok)
        }

        it("should be non-nil") {
          expect(apiResponse).toNot(beNil())
        }

        it("should have nil errors") {
          expect(apiResponse.errors).to(beNil())
        }
      }

      context("when decoding status 422 response") {
        context("and there is 1 error") {
          beforeEach {
            apiResponse = self.decodeResponseValue(statusCode: .unprocessableEntity, exampleNumber: 1)
          }

          it("should be non-nil") {
            expect(apiResponse).toNot(beNil())
          }

          it("should have non-nil errors") {
            expect(apiResponse.errors).toNot(beNil())
          }

          it("should have 1 error") {
            expect(apiResponse.errors?.count).to(equal(1))
          }
        }

        context("and there are 2 errors") {
          beforeEach {
            apiResponse = self.decodeResponseValue(statusCode: .unprocessableEntity, exampleNumber: 2)
          }

          it("should have 2 errors") {
            expect(apiResponse.errors?.count).to(equal(2))
          }
        }
      }
    }
  }
}
