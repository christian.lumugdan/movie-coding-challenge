//
//  UITableView+CellInstance.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import Rswift

extension UITableView {
  func cellInstance<T>(for resource: Rswift.NibResourceType) -> T where T: UITableViewCell {
    register(
      UINib(resource: resource),
      forCellReuseIdentifier: resource.name
    )
    return dequeueReusableCell(withIdentifier: resource.name) as! T
  }
}
