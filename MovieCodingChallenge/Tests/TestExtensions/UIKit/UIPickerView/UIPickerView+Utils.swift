//
//  UIPickerView+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIPickerView {
  func simulateSelect(at row: Int, inComponent component: Int = 0) {
    selectRow(row, inComponent: component, animated: false)
    delegate?.pickerView?(self, didSelectRow: row, inComponent: component)
  }
}
