//
//  UICollectionView+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

extension UICollectionView {
  func tap(row: Int, section: Int = 0) {
    delegate?.collectionView?(
      self,
      didSelectItemAt: IndexPath(row: row, section: section)
    )
  }
}
