//
//  UICollectionView+CellInstance.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import Rswift

extension UICollectionView {
  func cellInstance<T>(for resource: Rswift.NibResourceType) -> T where T: UICollectionViewCell {
    register(
      UINib(resource: resource),
      forCellWithReuseIdentifier: resource.name
    )
    return dequeueReusableCell(
      withReuseIdentifier: resource.name,
      for: IndexPath(row: 0, section: 0)
    ) as! T
  }

  func reusableSupplementaryViewInstance<T>(
    ofKind kind: String,
    for resource: Rswift.NibResourceType
  ) -> T where T: UICollectionReusableView {
    register(
      UINib(resource: resource),
      forSupplementaryViewOfKind: kind,
      withReuseIdentifier: resource.name
    )
    return dequeueReusableSupplementaryView(
      ofKind: kind,
      withReuseIdentifier: resource.name,
      for: IndexPath(row: 0, section: 0)
    ) as! T
  }
}
