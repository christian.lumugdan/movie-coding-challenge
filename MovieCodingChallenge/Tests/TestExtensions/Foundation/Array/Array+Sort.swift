//
//  Array+Sort.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension Array {
  func isSorted(by comparator: (Element, Element) -> Bool) -> Bool {
    for i in 1 ..< count {
      if !comparator(self[i - 1], self[i]) {
        return false
      }
    }
    return true
  }
}
