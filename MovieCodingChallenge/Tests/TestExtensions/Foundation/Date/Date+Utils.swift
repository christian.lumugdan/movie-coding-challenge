//
//  Date+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

extension Date {
  static func dummy(
    _ value: String,
    format: String = "MM-dd-yyyy"
  ) -> Date {
    value.toDate(format, region: .current)!.date
  }
}
