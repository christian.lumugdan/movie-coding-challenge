//
//  Progress+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

extension Progress {
  convenience init(expectedProgress: Float) {
    let adjustedExpectedProgress = min(expectedProgress, 1)
    self.init(totalUnitCount: 100)
    self.completedUnitCount = Int64(Float(100 * adjustedExpectedProgress))
  }
}
