//
//  URL+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension URL {
  static var dummy: URL {
    URL(string: "https://appetiser.com.au")!
  }
  
  static var dummyThumb: URL {
    URL(string: "https://appetiser.com.au/thumb")!
  }
}

extension URL {
  static func ==(lhs: URL, rhs: URL) -> Bool {
    return lhs.absoluteString == rhs.absoluteString
  }
}
