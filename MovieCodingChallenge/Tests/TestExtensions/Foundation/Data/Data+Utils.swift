//
//  Data+Utils.swift
//  MovieCodingChallenge
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2021 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import MovieCodingChallenge

extension Data {
  static var dummyImageData: Data {
    R.image.iconSigninApple()!.jpegData(compressionQuality: 0.1)!
  }
}
